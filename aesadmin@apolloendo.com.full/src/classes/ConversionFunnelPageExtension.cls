global with sharing class ConversionFunnelPageExtension {

    
	private final Conversion_Funnel__c Cfun;
    public string AccName {get; set;}
	
	public ConversionFunnelPageExtension(ApexPages.StandardController controller) { 
	
	       Cfun = (Conversion_Funnel__c)controller.getRecord();
	       AccName=getAccName(Cfun.Practice_Name__c).get(0).name;
	       
	}
	 public PageReference SaveCfun() {
        
       try {
       	
       	Cfun.Name=AccName+' '+date.today().format();
       
       upsert (Cfun);
      
       }
       catch(System.DMLException e) {
           ApexPages.addMessages(e);
           return null;
       }
       
       PageReference cfunPage = new ApexPages.StandardController(Cfun).view();
       cfunPage.setRedirect(true);
        return cfunPage;
     }
  
    
     /**
        * Retrieves name from Account.
        *
        * @param Id
        *
        * @author Nikhil
        */
        private static List<Account> getAccName(Id Aid) {
            return [SELECT Name FROM Account WHERE id = : Aid limit 1  ];
        }
     

}
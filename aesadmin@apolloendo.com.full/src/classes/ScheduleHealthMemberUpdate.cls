/* Created: 05/12/2014
 * Author: Sanjay Sankar
 * Depends on: BatchUpdateHealthMember.cls
 * Test Class(es): ScheduleHealthMemberUpdateTest.cls
 * Purpose: To update Account.Health_Member__c with the user ID of the "Health" Account Team Member. A
 *          boolean is driven off of this field to determine if the current user is a "Health" Team Member
 *          This should be re-worked once Account Team Member object is made customizable by Salesforce
 */

global with sharing class ScheduleHealthMemberUpdate implements Schedulable {
	global void execute (SchedulableContext sc) {
		BatchUpdateHealthMember updMember = new BatchUpdateHealthMember();
		Database.executeBatch(updMember);
	}
}
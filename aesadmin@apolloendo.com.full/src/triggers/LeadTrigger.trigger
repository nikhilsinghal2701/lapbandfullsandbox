trigger LeadTrigger on Lead (after insert, after update) {
	if (Label.LeadTrigger == 'Active') {
		LeadTriggerMethods.logStatusChange(trigger.old, trigger.new, trigger.isInsert);
	}

}
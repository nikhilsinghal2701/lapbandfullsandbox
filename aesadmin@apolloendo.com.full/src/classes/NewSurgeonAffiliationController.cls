/* Created: 01/27/2015
 * Author: Sanjay Sankar
 * Dependency: NewSurgeonAddiliationVF.page 
 * Test Class(es): NewSurgeonAffiliationControllerTest.cls
 * Purpose: Controller for NewSurgeonAffiliationVF.page - custom page for creating a new Surgeon Affiliation 
 *          created to auto-populate the Surgeon Account when creating an affiliation from a Surgeon Record
 */

public with sharing class NewSurgeonAffiliationController {
	public Surgeon_Affiliation__c surgAff { get; set; }
	public String shipToAcct { get; set; }
	private String shipToAcctID;
	ApexPages.StandardController sController;

	public NewSurgeonAffiliationController (ApexPages.StandardController controller) {
		sController = controller;
		surgAff = (Surgeon_Affiliation__c)controller.getRecord();
		shipToAcct = ApexPages.currentPage().getParameters().get('shipTo');
		shipToAcctID = ApexPages.currentPage().getParameters().get('shipToID');
		surgAff.Account__c = shipToAcctID;
	}
	
	public PageReference saveAff() {
		PageReference pRef = new PageReference('/' + shipToAcctID);
		List<Surgeon__c> surgeonList = new List<Surgeon__c>();
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Fatal, 'An error occured while creating this Surgeon Affiliation. Please email support@apolloendo.com for assistance.');
		surgeonList = [select Id, Name, Surgeon_Account__c from Surgeon__c where Id = :surgAff.Surgeon_Name__c];
		if (surgeonList.size() == 0) {
			ApexPages.addMessage(msg);
		} else {
			surgAff.Surgeon_Account__c = surgeonList[0].Surgeon_Account__c;
			pRef = sController.save();
		}
		return pRef;
	}
}
global with sharing class ScheduleMobileFAS implements Schedulable {
    global void execute(SchedulableContext SC) {
        System.debug('Starting Scheduled job @ ' + Date.today());
        ImportGoogleAnalytics.StartDate = System.today()-1;
        ImportGoogleAnalytics.EndDate = System.Today()-1;
        ImportGoogleAnalytics.importMobileFASData();
        System.debug('Finished Scheduled job @ ' + Date.today());
    }
}
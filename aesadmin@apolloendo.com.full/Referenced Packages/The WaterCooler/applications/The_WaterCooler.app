<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>The WaterCooler</label>
    <logo>Intranet/The_WaterCooler_Logo.png</logo>
    <tab>Intranet_Home_v2</tab>
    <tab>Intranet_App__c</tab>
    <tab>Intranet_Event__c</tab>
    <tab>Department__c</tab>
    <tab>Office_Locations__c</tab>
    <tab>Notification__c</tab>
    <tab>standard-Workspace</tab>
    <tab>Yearly_Selling_Days__c</tab>
    <tab>Physician_Segmentation_Survey__c</tab>
    <tab>Currency_Conversion__c</tab>
    <tab>ESS_Price_List_ID__c</tab>
</CustomApplication>

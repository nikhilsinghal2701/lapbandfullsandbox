/* Created: 05/13/2014
 * Author: Sanjay Sankar
 * Dependency: LapBandDocVF.page 
 * Test Class(es): 
 * Purpose: Controller for LapBandDocVF.page - to mimic the standard LapBand Documents page with the added ability of a download link
 *          ****** TO DO: Sort, Index, Hover thumbnail, Document Email ******
 */

public class LapBandDocController {
	public ListCollection Paging {get;set;}
	public Boolean isAdmin {get; set;}
	public Id s3ObjId {get;set;}
	public Map<Id, AWS_S3_Object__c> docsMap = new Map<Id, AWS_S3_Object__c>();

	public LapBandDocController() {
		try {
			Paging = new ListCollection();
			Paging.PageSize = 15;
			Paging.lbDocs = queryLBDocs();
			isAdmin = (UserInfo.getProfileId() == Label.System_Administrator);
		}
		catch (Exception e) {
			ApexPages.addMessages(e);
		}
	}

	private List<AWS_S3_Object__c> queryLBDocs() {
		docsMap = new Map<Id, AWS_S3_Object__c>([select Id, Name, Access__c, Library__c, Description__c, File_Name__c, Bucket_Name__c, Content_Type__c, Preview_Link__c, AmazonLink__c from AWS_S3_Object__c]);
		//return [select Id, Name, Access__c, Library__c, Description__c, File_Name__c, Bucket_Name__c, Content_Type__c, Preview_Link__c, AmazonLink__c from AWS_S3_Object__c];
		return docsMap.values();
	}
	
	public PageReference createsDownloadLink() {
		//AWS_S3_Object__c s3Obj = [SELECT Access__c,AmazonLink__c,Bucket_Name__c,Content_Type__c,CreatedDate,FileLink__c,File_Name__c,Id,Library__c,Name,OwnerId,Preview_Link__c FROM AWS_S3_Object__c WHERE ID = :s3ObjId][0];
		AWS_S3_Object__c s3Obj = docsMap.get(s3ObjId);
		S3FormController s3OrgCredential = new S3FormController();
		s3OrgCredential.credentials = new AWSKeys(s3OrgCredential.AWSCredentialName);

		String signedUrl=null;
		String timeStamp=null;

		timeStamp = calculateTimeStamp();
		SignedURL = calculateSignature(timeStamp,s3Obj,s3OrgCredential);
		s3Obj.FileLink__c=s3Obj.Amazonlink__c+SignedURL;

		PageReference pageRef = new PageReference(String.valueOf(s3Obj.FileLink__c));
		pageRef.setRedirect(false);
    
		return pageRef;
	}

	/**
	* Calculates encrypted URL for private files.
	*
	* @param String - time duration for which the files should be accessible.
	* @param AWS_S3_Object__c - File link in sfdc
	* @return String - SignedURL
	*/
	public Static String calculateSignature(String timeStamp, AWS_S3_Object__c s3Obj,S3FormController s){
		String FileName;
		String getURL;
		String signedURL;

		if (s3Obj.File_Name__c!=null && s3Obj.Bucket_Name__c!=null) {
			FileName=s3Obj.Bucket_Name__c+'/'+s3Obj.File_Name__c;
			if (FileName.containsWhitespace()) Filename.replaceAll(' ','%20');
			getURL='GET\n\n\n'+timeStamp+'\n/'+Filename+'?response-content-disposition=attachment; filename='+s3Obj.File_Name__c;
			String macUrl ;

			String signingKey = EncodingUtil.base64Encode(Blob.valueOf(s.credentials.secret));
			Blob mac = Crypto.generateMac('HMacSHA1',blob.ValueOf(getURL),blob.Valueof(s.credentials.secret));
			macUrl = EncodingUtil.base64Encode(mac);
			macURL=EncodingUtil.urlEncode(macURl, 'UTF-8');
			signedURL='?Signature='+macUrl+'&Expires='+timeStamp+'&AWSAccessKeyId='+s.credentials.key+'&response-content-disposition=attachment; filename='+s3Obj.File_Name__c;

		}

		return signedURL;
	}

	public static String calculateTimeStamp(){
		Long timeStamp = (System.currentTimeMillis())/1000;
		timeStamp = timeStamp + 3600;
		return String.ValueOf(timestamp);
	}

	//Internal Classes
	public class ListCollection extends Pageable {
		public List<AWS_S3_Object__c> lbDocs {get;set;}

		public override integer getRecordCount() {
			return (lbDocs == null? 0 : lbDocs.size());
		}
	}
}
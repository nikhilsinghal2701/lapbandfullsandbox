<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Pardot</label>
    <logo>ProspectInsight/ProspectInsightLogo.png</logo>
    <tab>ProspectInsight</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-AdvForecast</tab>
    <tab>standard-Contract</tab>
    <tab>standard-Case</tab>
    <tab>standard-Solution</tab>
    <tab>standard-Product2</tab>
    <tab>standard-report</tab>
    <tab>standard-Document</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Portal</tab>
    <tab>Pardot_LeadDeck</tab>
    <tab>Currency_Conversion__c</tab>
    <tab>ESS_Price_List_ID__c</tab>
</CustomApplication>

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ScheduleCurrencyLoadTest {

	public static String CRON_EXP = '0 0 0 15 3 ? 2050';

    static testMethod void testSchedule() {
        Test.startTest();
        
        //Schedule the test job
        String jobId = System.schedule ('ScheduleCurrencyLoadTest', CRON_EXP, new ScheduleCurrencyLoad());
        
        Crontrigger ct = [select Id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where Id = :jobId];
        
        //Verify expressions are same
        System.assertEquals (CRON_EXP, ct.CronExpression);
        
        //Verify job has not run
        System.assertEquals (0, ct.TimesTriggered);
        
        //Verify the next time the job will run
        System.assertEquals ('2050-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        
        Test.stopTest();
    }

    static testMethod void testCurrencyLoad() {
        /* Load Currencies__c custom setting */
        List<Currencies__c> currList = new List<Currencies__c>();
        currList.add(new Currencies__c(Name='AUD', Description__c='Australian Dollar'));
        currList.add(new Currencies__c(Name='CAD', Description__c='Canadian Dollar'));

		insert currList;

        Test.setMock(HttpCalloutMock.class, new MockCurrencyServiceResp());
        
        /* START TEST */
        Test.startTest();
        CurrencyLoadUtility.insertCurrencyConversion();
        Test.stoptest();
        
        /* ASSERTS */
        List<Currency_Conversion__c> currConvList = [select Id, Name, External_ID__c, Average_Daily_Ask__c, Average_Daily_Bid__c, Conversion_Rate__c, Data_Source__c, Effective_Date__c, ISO_Currency__c from Currency_Conversion__c where ISO_Currency__c = 'AUD' or ISO_Currency__c = 'CAD'];
        System.assertEquals(2, currConvList.size());
       	if (currConvList[0].ISO_Currency__c == 'AUD') {
       		System.assertEquals (1.5, currConvList[0].Average_Daily_Ask__c);
       		System.assertEquals (2.5, currConvList[0].Average_Daily_Bid__c);
       		System.assertEquals (2.0, currConvList[0].Conversion_Rate__c);
       		System.assertEquals ('2014-11-13', String.valueOf(currConvList[0].Effective_Date__c));

       		System.assertEquals (3.5, currConvList[1].Average_Daily_Ask__c);
       		System.assertEquals (4.5, currConvList[1].Average_Daily_Bid__c);
       		System.assertEquals (4.0, currConvList[1].Conversion_Rate__c);
       		System.assertEquals ('2014-11-13', String.valueOf(currConvList[1].Effective_Date__c));
       	} else {
       		System.assertEquals (3.5, currConvList[0].Average_Daily_Ask__c);
       		System.assertEquals (4.5, currConvList[0].Average_Daily_Bid__c);
       		System.assertEquals (4.0, currConvList[0].Conversion_Rate__c);
       		System.assertEquals ('2014-11-13', String.valueOf(currConvList[0].Effective_Date__c));

       		System.assertEquals (1.5, currConvList[1].Average_Daily_Ask__c);
       		System.assertEquals (2.5, currConvList[1].Average_Daily_Bid__c);
       		System.assertEquals (2.0, currConvList[1].Conversion_Rate__c);
       		System.assertEquals ('2014-11-13', String.valueOf(currConvList[1].Effective_Date__c));
    	}
    }

    public class MockCurrencyServiceResp implements HttpCalloutMock {
    	public HTTPResponse respond ( HTTPRequest request ) {
    		HTTPResponse response = new HTTPResponse();
    		response.setStatusCode(200);
    		response.setHeader('Content-Type', 'application/json');
    		String endPoint = request.getEndpoint();
    		if (endPoint.startsWith('https://www.oanda.com')) {
    			//'&date=' + dateParam + fieldParams + quoteParams
	    		//System.assertEquals('https://www.oanda.com/rates/api/v1/rates/USD.json?&api_key=' + Label.OandaAPIKey + '&decimal_places=' + Label.OandaDecimalPlaces + '&date=', endPoint);
   				response.setBody ('{"base_currency":"USD","meta":{"effective_params":{"data_set":"oanda","date":"2014-11-13","decimal_places":"5","fields":["averages","midpoint"],"quote_currencies":["AUD","CAD"]},"request_time":"2014-11-14T18:21:43+0000","skipped_currencies":[]},"quotes":{"AUD":{"ask":"1.5","bid":"2.5","date":"2014-11-13T21:00:00+0000","midpoint":"2.0"},"CAD":{"ask":"3.5","bid":"4.5","date":"2014-11-13T21:00:00+0000","midpoint":"4"}}}');
    		}
    		
    		return response;
    	}
    }

}
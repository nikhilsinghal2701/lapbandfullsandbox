@isTest
private class LWURLRewriterTest {
	
	@isTest static void testLWURLs() {
		// Implement test code
		List<PageReference> mySalesforceUrls = new List<PageReference>();
		TestUtil.createLWURL();
		LWURLRewriter rewriter = new LWURLRewriter();

		PageReference p = new PageReference('/About-Us');

		PageReference actualResult = rewriter.mapRequestUrl(p);
		PageReference expectedResult = new PageReference('/LWAboutUs');
		mySalesforceUrls.add(expectedResult);
		System.assertEquals(String.valueOf(actualResult),String.valueOf(expectedResult));

		p = new PageReference('/');
		//expectedResult = new PageReference('/sitelogin');
		actualResult = rewriter.mapRequestUrl(p);
		System.assertEquals(String.valueOf(actualResult),null);

		p = new PageReference('/find-a-specialist/Detail|001R000000oZYnFIAW');
		actualResult = rewriter.mapRequestUrl(p);
		expectedResult = new PageReference('/LWSpecialistDetails/Detail|001R000000oZYnFIAW?accountId=001R000000oZYnFIAW');
		System.assertEquals(String.valueOf(actualResult),String.valueOf(expectedResult));

		p = new PageReference('/HCP/');
		actualResult = rewriter.mapRequestUrl(p);
		expectedResult = new PageReference('/LWHCPHome');
		System.assertEquals(String.valueOf(actualResult),String.valueOf(expectedResult));

		List<PageReference> myFriendlyUrls = rewriter.generateUrlFor(mySalesforceUrls);
		system.debug('--mySalesforceUrls--'+mySalesforceUrls);
		System.assertEquals(String.valueOf(myFriendlyUrls[0]),String.valueOf(new PageReference('/About-Us')));
	
	}
	
}
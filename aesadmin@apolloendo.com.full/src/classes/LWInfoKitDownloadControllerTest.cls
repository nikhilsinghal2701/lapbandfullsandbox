/**
Class: LWInfoKitDownloadControllerTest
Purpose: Test class for LWInfoKitDownloadController
Author – Wade Liu
Date – 12/10/2014
*/
@isTest
private class LWInfoKitDownloadControllerTest {
    
    static testMethod void testLWInfoKitDownload() {
        // prepare data
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        Id aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        List<Office__c> officeList = TestUtil.createOffices(20, true, new Map<String, Object>{'Location__Latitude__s' => 37.5548022,
                                                                                             'Location__Longitude__s' => -121.9889206,
                                                                                             'English__c' => true,
                                                                                             'Wheelchair_Access__c' => true,
                                                                                             'Specialist__c' => acc.Id,
                                                                                             'Status__c' => 'Submitted',
                                                                                             'RecordTypeId' => aId}); 
                                                                                             
        String str1 = '37.7925:-122.400556';
        
        PageReference pageRef = Page.LWInfoKitDownload; 
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('latlng', str1);
        ApexPages.currentPage().getParameters().put('zipCode', '94560');
    
        LWInfoKitDownloadController downloadController = new LWInfoKitDownloadController();
        
        system.assertEquals(downloadController.offices.size(),3);
        system.assertEquals(downloadController.zipCode,'94560');
    }
}
/*
Class: LWCOfficeStaffControllerTest
Purpose: Test Class for office staff functionality
Author: Wade
Created DAte: 03/05/2014
*/
@isTest
private class LWCOfficeStaffControllerTest {

    static testMethod void cancelPanelTest() {
        // Unit test for cancel panel render
        LWCOfficeStaffController controller = new LWCOfficeStaffController();
        controller.showCancel();
        system.assert(controller.cancelPopup);
        controller.closeCancel();
        system.assert(!controller.cancelPopup);
    }
    
    static testMethod void deletePanelTest() {
        // Unit test for delete panel render
        LWCOfficeStaffController controller = new LWCOfficeStaffController();
        controller.showDelete();
        system.assert(controller.deletePopup);
        system.assert(controller.displayModal);
        controller.closeDelete();
        system.assert(!controller.deletePopup);
        system.assert(!controller.displayModal);
    }
    
    static testMethod void staffLogicTest(){
        // Unit test for office staff operation
        LWCOfficeStaffController controller = new LWCOfficeStaffController();
        controller.closeStaff();
        system.assert(!controller.staffPopup);
        system.assert(!controller.displayModal);
        
        String str = LWCConstantDeclaration.SURGEONACCOUNTRECORD;
        Account account = TestUtil.createAccounts( 1, true, new Map<String, Object>{ 'RecordTypeId' => LWCWithoutSharing.getRecordTypeId(new List<String>{str})[0].Id })[0];
        //test new staff
        controller.activeEmail = 'newOS';
        controller.showStaff();
        system.assert(controller.staffPopup);
        system.assert(controller.displayModal);
        controller.accountID = account.Id;
        controller.activeOS.First_Name__c = 'OfficeAdd';
        controller.activeOS.Last_Name__c = 'Staff';
        controller.activeOS.Type__c = 'RN';
        controller.activeOS.E_mail__c = 'officeadd.staff@test.com';
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.runAs(usr){
            Test.startTest();
            controller.upsertOfficeStaff();
            Test.stopTest();
        }
        Office_Staff__c osAfterAdd = [select Specialist__c from Office_Staff__c where Id =:controller.activeOS.Id];
        system.assertEquals(osAfterAdd.Specialist__c, controller.accountID);
        List<Office_Staff__c> oss = controller.getOfficeStaffs();
        system.assertEquals(osAfterAdd.Id, oss[0].Id);
        
    }
    
    static testMethod void staffLogicTest2(){
        // Unit test for office staff operation
        LWCOfficeStaffController controller = new LWCOfficeStaffController();
        controller.closeStaff();
        system.assert(!controller.staffPopup);
        system.assert(!controller.displayModal);
        
        String str = LWCConstantDeclaration.SURGEONACCOUNTRECORD;
        Account account = TestUtil.createAccounts( 1, true, new Map<String, Object>{ 'RecordTypeId' => LWCWithoutSharing.getRecordTypeId(new List<String>{str})[0].Id })[0];
        
        //test update staff
        Office_Staff__c os = TestUtil.createOfficeStaffs(1, true, new Map<String, Object>{ 'Specialist__c' => account.Id })[0];
        controller.activeEmail = os.E_mail__c;
        controller.showStaff();
        system.assertEquals(controller.activeOS.Id, os.Id);

        String testLastName = controller.activeOS.Last_Name__c+'TestLastName';
        controller.activeOS.Last_Name__c = testLastName;
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.runAs(usr){
            Test.startTest();
            controller.upsertOfficeStaff();
            Test.stopTest();
        }
        
        system.assertEquals(controller.activeOS.Last_Name__c, testLastName);
        
    }
    
    static testMethod void staffLogicTest3(){
        // Unit test for office staff operation
        LWCOfficeStaffController controller = new LWCOfficeStaffController();
        controller.closeStaff();
        system.assert(!controller.staffPopup);
        system.assert(!controller.displayModal);
        
        String str = LWCConstantDeclaration.SURGEONACCOUNTRECORD;
        Account account = TestUtil.createAccounts( 1, true, new Map<String, Object>{ 'RecordTypeId' => LWCWithoutSharing.getRecordTypeId(new List<String>{str})[0].Id })[0];

        //test delete staff
        Office_Staff__c os = TestUtil.createOfficeStaffs(1, true, new Map<String, Object>{ 'Specialist__c' => account.Id })[0];
        controller.activeEmail = os.E_mail__c;
        controller.showStaff();
        system.assertEquals(controller.activeOS.Id, os.Id);
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.runAs(usr){
            Test.startTest();
            controller.deleteOfficeStaff();
            Test.stopTest();
        }
        Office_Staff__c osAfterDelete = [select Is_Active__c from Office_Staff__c where Id =:os.Id];
        system.assert(!osAfterDelete.Is_Active__c); 
       
    }
    
    static testMethod void checkEmailTest(){
        //test if user exist with same email address
        User user = TestUtil.createUsers(1, true, new Map<String, Object>{ 'User_Email__c' => 'demo.user@perficient.com' } )[0];
        String email = user.User_Email__c;
        boolean canAdd = LWCOfficeStaffController.checkEmail(email);
        system.assert(!canAdd);
        email = 'test'+email;
        canAdd = LWCOfficeStaffController.checkEmail(email);
        system.assert(canAdd);
        email = '';
        canAdd = LWCOfficeStaffController.checkEmail(email);
        system.assert(!canAdd);
    }
    
    static testMethod void otherTest(){
        //no logic test
        LWCOfficeStaffController controller = new LWCOfficeStaffController();
        List<SelectOption> items = controller.getItems();
    }
}
/*
Class: LWSearchSpecialistControllerTest
Purpose: Test Class for search specialist
Author: Chris
Created DAte: 03/10/2014
*/
@isTest
public with sharing class LWOfficeInfoControllerTest {
	
	static testMethod void testShowModal(){
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();
		officeInfoController.showModal();
		system.assertEquals(true, officeInfoController.displayModal);
	}
	
	static testMethod void testShowCancel(){
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();
		officeInfoController.showCancel();
		system.assertEquals(true, officeInfoController.cancelPopup);
	}
	
	static testMethod void testShowConCancel(){
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();
		officeInfoController.showConCancel();
		system.assertEquals(true, officeInfoController.cancelPopup);
		system.assertEquals(40, officeInfoController.top);
	}
	
	static testMethod void testClosePopup(){
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();
		
		Account account = TestUtil.createAccounts(1, true, null)[0];
		LWOfficeWrapper officeWripper = new LWOfficeWrapper(account.Id);
		officeInfoController.activeOffice = officeWripper;
		officeInfoController.closePopup();
		system.assertEquals(false, officeInfoController.cancelPopup);
	}
	
	static testMethod void testShowPreview(){
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();
		Account account = TestUtil.createAccounts(1, true, null)[0];
		LWOfficeWrapper officeWripper = new LWOfficeWrapper(account.Id);
		officeInfoController.activeOffice = officeWripper;
		officeInfoController.showPreview();
		system.assertEquals('preview', officeInfoController.activeOffice.operation);
	}
	
	static testMethod void testContinueEdit(){
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();
		Account account = TestUtil.createAccounts(1, true, null)[0];
		LWOfficeWrapper officeWripper = new LWOfficeWrapper(account.Id);
		officeInfoController.activeOffice = officeWripper;
		officeInfoController.continueEdit();
		system.assertEquals('edit', officeInfoController.activeOffice.operation);
	}
	
	static testMethod void testCloseCancel(){
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();
		officeInfoController.closeCancel();
		system.assertEquals(false, officeInfoController.cancelPopup);
	}
	
	static testMethod void testCloseConCancel(){
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();
		officeInfoController.closeConCancel();
		system.assertEquals(false, officeInfoController.conCancelPopup);
	}
	
	static testMethod void testAddAccr(){
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();		
		//office is  null
		officeInfoController.newAccr = 'newAccr1';
		Account acct1 = TestUtil.createAccounts(1, true, null)[0];
		LWOfficeWrapper officeWripper = new LWOfficeWrapper(acct1.Id);
		List<LWCMembership> memOffList = new List<LWCMembership>();
		
		officeInfoController.activeOffice = officeWripper;
		officeInfoController.newMemOffList = memOffList;
		officeInfoController.addAccr();
		//office is not null
		officeInfoController.newAccr = 'newAccr2';	
				
		Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        List<Account> accts = TestUtil.createAccounts(1, true,
        						 new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon});
		list<Office__c> offices = TestUtil.createOffices(2, true, 
										new Map<String,Object>{'Specialist__c'=>accts[0].Id});
	
		LWOfficeWrapper officeWripper2 = new LWOfficeWrapper(offices.get(0).Id);
		offices.get(0).Approved_Office__c = offices.get(1).Id;
		
		
		officeInfoController.activeOffice = officeWripper2;
		officeInfoController.newMemOffList = memOffList;
		officeInfoController.addAccr();
		
	}
	
	static testMethod void testRemoveOffMem(){
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();	
		List<LWCMembership> menbershipList= new List<LWCMembership>();
		List<Membership_Accreditation__c> lstMembershipAccre;
		Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        List<Account> lstAccounts = TestUtil.createAccounts(1, true, new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon});
        if(lstAccounts != null && lstAccounts.size() > 0){    
        	List<Office__c> lstOffices = TestUtil.createOffices(1, true, new Map<String,Object>{'Specialist__c'=>lstAccounts[0].Id});
    		if(lstOffices != null && lstOffices.size() > 0){
    			lstMembershipAccre = TestUtil.createMembershipAccreditation(1, true, new Map <String,Object>{'Office__c'=>lstOffices[0].Id,'Specialist__c'=>lstAccounts[0].Id}); 						
    		}
    		LWCMembership membership;
    		for( Membership_Accreditation__c membershipAccre : lstMembershipAccre){
    			membership  = new LWCMembership(membershipAccre);
    			menbershipList.add(membership);
    			 		
    		}
        }
        officeInfoController.newMemOffList = menbershipList;
        officeInfoController.removeOffMem();
        
	}
	
	static testMethod void testSaveNewOffice(){
		/*LWOfficeInfoController officeInfoController = new LWOfficeInfoController();	
		List<LWCMembership> newMemOffList = new List<LWCMembership>();
		Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe()
											   .getRecordTypeInfosByName()
											   .get('Surgeon').getRecordTypeId();
	    Id idOfficeRecordType = Office__c.SObjectType.getDescribe()
											   .getRecordTypeInfosByName()
											   .get('Pending Office').getRecordTypeId();
        List<Account> accounts = TestUtil.createAccounts(1, true,
        						 new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon});	
		LWOfficeWrapper officeWripper = new LWOfficeWrapper(accounts[0].Id);
		LWCMembership membership;
		for( Membership_Accreditation__c membershipAccre : lstMembershipAccre){
			membership  = new LWCMembership(membershipAccre);
			newMemOffList.add(membership);
			 		
		}
		officeWripper.office.RecordTypeId = idOfficeRecordType;
		officeInfoController.activeOffice = officeWripper;
		officeInfoController.saveNewOffice();*/
		
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();	
		//prepare membership list
		List<LWCMembership> menbershipList= new List<LWCMembership>();
		List<Membership_Accreditation__c> lstMembershipAccre;
		Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        List<Account> lstAccounts = TestUtil.createAccounts(1, true, new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon});
        if(lstAccounts != null && lstAccounts.size() > 0){    
        	List<Office__c> lstOffices = TestUtil.createOffices(1, true, new Map<String,Object>{'Specialist__c'=>lstAccounts[0].Id});
    		if(lstOffices != null && lstOffices.size() > 0){
    			lstMembershipAccre = TestUtil.createMembershipAccreditation(1, true, new Map <String,Object>{'Office__c'=>lstOffices[0].Id,'Specialist__c'=>lstAccounts[0].Id}); 						
    		}
    		LWCMembership membership;
    		for( Membership_Accreditation__c membershipAccre : lstMembershipAccre){
    			membership  = new LWCMembership(membershipAccre);   
    					
    			menbershipList.add(membership);
    			 		
    		}
        }
        officeInfoController.newMemOffList = menbershipList;
        //test insert new office
        Id idOfficeRecordType = Office__c.SObjectType.getDescribe()
											   .getRecordTypeInfosByName()
											   .get('Pending Office').getRecordTypeId();
        LWOfficeWrapper officeWripper = new LWOfficeWrapper(lstAccounts[0].Id);
		officeWripper.office.RecordTypeId = idOfficeRecordType;
		officeInfoController.activeOffice = officeWripper;
		officeInfoController.saveNewOffice();
		//test update office 
		Membership_Accreditation__c membershipAccreditation = new Membership_Accreditation__c();
		membershipAccreditation.Name = 'for menbership test';
		LWCMembership membership = new LWCMembership(membershipAccreditation);
		menbershipList.add(membership);
		idOfficeRecordType = Office__c.SObjectType.getDescribe()
											   .getRecordTypeInfosByName()
											   .get('Approved Office').getRecordTypeId();
		officeWripper.office.Name = 'for test update office pending';
		officeInfoController.activeOffice = officeWripper;
		officeInfoController.saveNewOffice();
		//test update office 
		menbershipList.get(menbershipList.size()-1).remove = true;
		idOfficeRecordType = Office__c.SObjectType.getDescribe()
											   .getRecordTypeInfosByName()
											   .get('Approved Office').getRecordTypeId();
		officeWripper.office.Name = 'for test update office approved';
		officeWripper.office.RecordTypeId = idOfficeRecordType;
		officeInfoController.activeOffice = officeWripper;
		officeInfoController.saveNewOffice();
	}
	
	static testMethod void testEditOffice(){
		LWOfficeInfoController officeInfoController = new LWOfficeInfoController();	
		
		Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe()
											   .getRecordTypeInfosByName()
											   .get('Surgeon').getRecordTypeId();
        List<Account> accounts = TestUtil.createAccounts(1, true,
        						 new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon});
		/*Office__c office = TestUtil.createOffices(1, true, 
										new Map<String,Object>{'Specialist__c'=>accounts[0].Id})[0];*/
	
		LWOfficeWrapper officeWripper = new LWOfficeWrapper(accounts[0].Id);
		Insert officeWripper.office;
		officeWripper.office = [SELECT Id FROM Office__c];
		officeInfoController.activeOffice = officeWripper;
		officeWripper.office.City__c = 'HANGZHOU';
		officeInfoController.editOffice();
		Office__c office = [SELECT City__c FROM Office__c];
		system.assertEquals('HANGZHOU', office.City__c); 
	}
	
	
	
	
	
}
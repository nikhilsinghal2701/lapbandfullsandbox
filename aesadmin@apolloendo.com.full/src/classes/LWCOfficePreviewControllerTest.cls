/**
Class: LWCOfficePreviewControllerTest
Test Class for LWCOfficePreviewController
Author – Wade Liu
Date – 03/09/2014
 */
@isTest
private class LWCOfficePreviewControllerTest {

    static testMethod void testGetPendingFlag() {
        // prepare data
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        String pOffice = LWCConstantDeclaration.PENDINGOFFICERECORD;
    	ID pId = LWCWithoutSharing.getRecordTypeId(new List<String>{pOffice})[0].Id;
        List<Office__c> officeList = TestUtil.createOffices(10, true, new Map<String, Object>{'Status__c' => 'Submitted',
        																					  'Specialist__c' => acc.Id,
        																					  'RecordTypeId' => pId});
        // create a new controller
        LWCOfficePreviewController controller = new LWCOfficePreviewController();
        
        // set accountId
        controller.accountID = acc.Id;
        
        // get Pending Flag
        Boolean result = controller.getPendingFlag();
        
        System.assertEquals(true, result);
    }
    
    static testMethod void testGetPendingFlag2() {
        // prepare data
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        List<Office__c> officeList = TestUtil.createOffices(10, true, new Map<String, Object>{'Status__c' => 'Rejected',
        																					  'Specialist__c' => acc.Id});
        // create a new controller
        LWCOfficePreviewController controller = new LWCOfficePreviewController();
        
        // set accountId
        controller.accountID = acc.Id;
        
        // get Pending Flag
        Boolean result = controller.getPendingFlag();
        
        System.assertEquals(false, result);
    }
    
     static testMethod void testGetSurgeon() {
     	// prepare data
     	Account acc =  TestUtil.createAccounts(1, true, null)[0];
     	
     	List<Surgeon__c> surgeonList = TestUtil.createSurgeons(10, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id});
     	
     	// create a new controller
        LWCOfficePreviewController controller = new LWCOfficePreviewController();
        
        // set accountId
        controller.accountID = acc.Id;
        
        // get surgeons
        Surgeon__c surgeon = controller.getSurgeon();
        
        System.assertEquals('surgeon_0', surgeon.Display_Name__c);
        System.assertEquals(surgeon.Id, controller.surgeonId);
     }
     
     static testMethod void testGetOffices() {
     	// prepare data
     	Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        List<Office__c> officeList = TestUtil.createOffices(10, true, new Map<String, Object>{'Status__c' => 'Submitted',
        																					  'Specialist__c' => acc.Id,
        																					  'RecordTypeId' => aId,
        																					  'Is_Edit_Flag__c' => false});
     	
     	// create a new controller
        LWCOfficePreviewController controller = new LWCOfficePreviewController();
        
        // set accountId
        controller.accountID = acc.Id;
        
        // get offices
        List<Office__c> offices = controller.getOffices();
        
        System.assertEquals(10, offices.size());
     }
     
     static testMethod void testGetMembers() {
     	// prepare data
     	Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        List<Office__c> officeList = TestUtil.createOffices(10, true, new Map<String, Object>{'Status__c' => 'Submitted',
        																					  'Specialist__c' => acc.Id,
        																					  'RecordTypeId' => aId,
        																					  'Is_Edit_Flag__c' => false});
     	
     	List<Membership_Accreditation__c> membershipList = TestUtil.createMembershipAccreditation(10, true, new Map<String, Object>{'Specialist__c' => acc.Id,
     																																'Office__c' => officeList[0].Id });
     	// create a new controller
        LWCOfficePreviewController controller = new LWCOfficePreviewController();
        
        // set accountId
        controller.accountID = acc.Id;
        
        // get memberships
        List<Membership_Accreditation__c> memberships = controller.getMembers();
        
        System.assertEquals(10, memberships.size());
     }
     
     static testMethod void testGetHospitals() {
     	// prepare data
     	Account acc =  TestUtil.createAccounts(1, true, null)[0];
     	
     	Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id})[0];
     	
     	List<Surgeon_Affiliation__c> surgeonAffiliationList = TestUtil.createSurgeonAffiliations(10, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
     																															   'Surgeon_Name__c' => surgeon.Id});
     	
     	// create a new controller
        LWCOfficePreviewController controller = new LWCOfficePreviewController();
        
        // set accountId
        controller.accountID = acc.Id;
        
        // get surgeon Affiliations
        List<Surgeon_Affiliation__c> surgeonAffiliations = controller.getHospitals();
        
        System.assertEquals(10, surgeonAffiliations.size());
     }
     
     static testMethod void testGetFieldId() {
     	// prepare data
     	Account acc =  TestUtil.createAccounts(1, true, null)[0];
     	
     	List<Surgeon__c> surgeonList = TestUtil.createSurgeons(10, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id});
     	
     	Attachment attachment = TestUtil.createAttachments(1, true, new Map<String, Object>{'parentId' => surgeonList[0].Id})[0];
     	
     	// create a new controller
        LWCOfficePreviewController controller = new LWCOfficePreviewController();
        
        // set accountId
        controller.accountID = acc.Id;
        
        // get surgeon
        Surgeon__c surgeon = controller.getSurgeon();
        
        // get field id
        Id fieldId = controller.getFieldId();
        
        System.assertEquals(attachment.Id, fieldId);
     }
     
     
}
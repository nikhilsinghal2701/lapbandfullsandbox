global with sharing class ScheduleHCPFASAnalytics implements Schedulable {
     global void execute(SchedulableContext sc){
   ScheduleHCPFASProfileTrackingAnalytics cs = new ScheduleHCPFASProfileTrackingAnalytics();
   Database.executeBatch(cs);
  }
}
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LapBandVolumeControllerTest {

    static testMethod void myUnitTest() {
        
    	//get record type ID for Account 
    	Schema.DescribeSObjectResult sObjDescrAcc = Schema.SObjectType.Account;
    	Map<String, Schema.RecordTypeInfo> accRecTypeInfo = sObjDescrAcc.getRecordTypeInfosByName();

    	//get record type ID for Sales Order
    	Schema.DescribeSObjectResult sObjDescrSO = Schema.SObjectType.Sales_Order__c;
    	Map<String, Schema.RecordTypeInfo> soRecTypeInfo = sObjDescrSO.getRecordTypeInfosByName();
    	 
    	//create account
    	Account a1 = TestUtil.createAccounts(1, false, null)[0];
    	a1.RecordTypeId = accRecTypeInfo.get('Ship To').getRecordTypeId();
    	a1.Targeted_Account__c = true;
    	a1.Region_ID__c = '11111';
   	
    	insert a1;

        // create sales order
		Sales_Order__c so1 = new Sales_Order__c();
		so1.Name = 'TEST01';
		so1.PO__c = 'TEST_PO1';
		so1.Type__c ='US';
		so1.RecordTypeId = soRecTypeInfo.get('LAP-BAND SYSTEMS').getRecordTypeId();
		so1.Ship_To__c = a1.id;

		insert so1;
		
		List<Sales_Order_Line_Item__c> items = new List<Sales_Order_Line_Item__c>();
		
        // create sales order line item with Bill Date OLDER THAN LAST YEAR
		items.add(new Sales_Order_Line_Item__c(
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '1,000.00',
			Material__c = 'B-2240',
			Description__c = 'LAP-BAND',
			Integration_Billedqty__c = '5',
			Quantity__c = 5,
			Bill_Date__c = Date.newInstance(2010, 05, 25),
			Due_Date__c = Date.newInstance(2011, 05, 15)
			)
		);

        // create sales order line item with Bill Date LAST YEAR
		items.add(new Sales_Order_Line_Item__c(
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '1,400.00',
			Material__c = 'B-2240',
			Description__c = 'LAP-BAND',
			Integration_Billedqty__c = '7',
			Quantity__c = 7,
			Bill_Date__c = Date.newInstance(Date.Today().addYears(-1).year(), 05, 25),
			Due_Date__c = Date.newInstance(Date.Today().addYears(-1).year(), 06, 25)
			)
		);

        // create sales order line item with Bill Date THIS YEAR, but not THIS MONTH
		Integer monthFactor = (Date.today().month() == 1) ?	1: -1; //to cover the scenario of current month being january
		
		items.add(new Sales_Order_Line_Item__c(
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '1,600.00',
			Material__c = 'B-2240',
			Description__c = 'LAP-BAND',
			Integration_Billedqty__c = '3',
			Quantity__c = 3,
			Bill_Date__c = Date.Today().addMonths(monthFactor),
			Due_Date__c = Date.Today().addMonths(monthFactor+1)
			)
		);

        // create sales order line item with Bill Date THIS MONTH
		items.add(new Sales_Order_Line_Item__c(
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '1,600.00',
			Material__c = 'B-2240',
			Description__c = 'LAP-BAND',
			Integration_Billedqty__c = '8',
			Quantity__c = 8,
			Bill_Date__c = Date.newInstance(Date.Today().year(), Date.today().month(), 1),
			Due_Date__c = Date.Today().addMonths(1)
			)
		);

		insert items;

		ApexPages.Standardcontroller controller = new ApexPages.Standardcontroller(a1);
		
		LapBandVolumeController extn = new LapBandVolumeController(controller);
		
		PageReference ret;

		extn.filterBy = 'This Month';
		ret = extn.calcVolume();
		System.assertEquals(8, extn.totalUnits);
		System.assertEquals(8, extn.volList[0].vol);

		extn.filterBy = 'This Year';
		ret = extn.calcVolume(); 
		System.assertEquals(11, extn.totalUnits);
		if (monthFactor == 1) {
			System.assertEquals(8, extn.volList[0].vol);
			System.assertEquals(3, extn.volList[1].vol);
		} else {
			System.assertEquals(3, extn.volList[0].vol);
			System.assertEquals(8, extn.volList[1].vol);
		}
		
		extn.filterBy = 'This Quarter';
		ret = extn.calcVolume();
		/*** TODO asserts to be written for THIS QUARTER ***/
		
		extn.filterBy = 'Last 3 Months';
		ret = extn.calcVolume();
		if (monthFactor == 1) {
			System.assertEquals(8, extn.totalUnits);
			System.assertEquals(8, extn.volList[0].vol);
		} else {
			System.assertEquals(11, extn.totalUnits);
			System.assertEquals(3, extn.volList[0].vol);
		}
		

		extn.filterBy = 'Last Year';
		ret = extn.calcVolume();
		System.assertEquals(7, extn.totalUnits);
		System.assertEquals(7, extn.volList[0].vol);

		extn.filterBy = 'Custom';
		extn.startDate = '2010-05-24';
		extn.endDate = '2010-05-26';
		ret = extn.calcVolume();
		System.assertEquals(5, extn.totalUnits);
		System.assertEquals(5, extn.volList[0].vol);

    }
}
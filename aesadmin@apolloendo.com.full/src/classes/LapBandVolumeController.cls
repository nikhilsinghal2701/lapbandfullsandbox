/* Created: 08/14/2014
 * Author: Sanjay Sankar
 * Dependency: LapBandVolumeVF.page 
 * Test Class(es): 
 * Purpose: Controller for LapBandVolumeVF.page - to display a chart showing volume sales over a period of time
 */
public with sharing class LapBandVolumeController {

	public final Account acc;
	public String startDate {get; set;}
	public String endDate {get; set;} 
	public String filterBy {get; set; }
	public List<VolumeData> volList {get; set; }
	public Integer totalUnits {get; set; }

	public Map<String,String> monthMap = new Map<String, String>();

	public LapBandVolumeController(ApexPages.StandardController Controller) {
		this.acc = (Account)controller.getRecord();
		filterBy = 'This Year';
		if (startDate==null) startDate= DateTime.newInstance(DateTime.now().year(), 1, 1, 1,0,0).format('YYYY-MM-dd');
		if (endDate==null) endDate= DateTime.now().format('YYYY-MM-dd');
		
		monthMap.put('1','Jan');
		monthMap.put('2','Feb');
		monthMap.put('3','Mar');
		monthMap.put('4','Apr');
		monthMap.put('5','May');
		monthMap.put('6','Jun');
		monthMap.put('7','Jul');
		monthMap.put('8','Aug');
		monthMap.put('9','Sep');
		monthMap.put('10','Oct');
		monthMap.put('11','Nov');
		monthMap.put('12','Dec');
	}
	
	private static Integer getQuarter(Date inputDt) {
		if (inputDt.month() >= 1 && inputDt.month() <= 3) {
			return 1;
		} else if (inputDt.month() >= 4 && inputDt.month() <= 6) {
			return 2;
		} else if (inputDt.month() >= 7 && inputDt.month() <= 9) {
			return 3;
		} else {
			return 4;
		}
		
		return null;
	}
	
    public PageReference calcVolume() {
		Date currentDate = Date.today();
		Date d1;
		Date d2 = currentDate;

		if (filterBy == 'This Month') {
			d1 = currentDate.toStartOfMonth();
		} else if (filterBy == 'This Year') {
			d1 = Date.newInstance(currentDate.year(), 1, 1);
			d2 = Date.newInstance(currentDate.year(), 12, 31);
		} else if (filterBy == 'This Quarter') {
			if (currentDate.month() >= 1 && currentDate.month() <= 3) {
				d1 = Date.newInstance(currentDate.year(), 1, 1);
				d2 = Date.newInstance(currentDate.year(), 3, 31);
			} else if (currentDate.month() >= 4 && currentDate.month() <= 6) {
				d1 = Date.newInstance(currentDate.year(), 4, 1);
				d2 = Date.newInstance(currentDate.year(), 6, 30);
			} else if (currentDate.month() >= 7 && currentDate.month() <= 9) {
				d1 = Date.newInstance(currentDate.year(), 7, 1);
				d2 = Date.newInstance(currentDate.year(), 9, 30);
			} else {
				d1 = Date.newInstance(currentDate.year(), 10, 1);
				d2 = Date.newInstance(currentDate.year(), 12, 31);
			}
		} else if (filterBy == 'Last Year') {
			d1 = Date.newInstance(currentDate.addYears(-1).year(), 1, 1);
			d2 = Date.newInstance(currentDate.addYears(-1).year(), 12, 31);
		} else if (filterBy == 'Last 3 Months') {
			d1 = currentDate.addMonths(-3).toStartOfMonth();
		} else {
			//custom date range or default to current month
			d1 = (startDate == null) ? currentDate.toStartOfMonth() : Date.valueOf(startDate);
			d2 = (endDate == null) ? currentDate : Date.valueOf(endDate);
		}

		totalUnits = 0;
		
		List<VolumeData> retList = new List<VolumeData>();

    	AggregateResult[] aggr;
    	aggr = [select CALENDAR_YEAR(Bill_Date__c) yr, CALENDAR_MONTH(Bill_Date__c) mth, sum(Quantity__c) qty from Sales_Order_Line_Item__c where Sales_Order__r.Ship_To__r.Id = :acc.Id and Sales_Order__r.PO__c != 'move to AGN' and (NOT Sales_Order__r.PO__c like '%TAX Adjustment%') and isLapBandUnits__c = 'TRUE' and Bill_Date__c >= :d1 and Bill_Date__c <= :d2 group by CALENDAR_YEAR(Bill_Date__c), CALENDAR_MONTH(Bill_Date__c) order by CALENDAR_YEAR(Bill_Date__c), CALENDAR_MONTH(Bill_Date__c)];

		if (aggr.size()>0) {
			for (AggregateResult res : aggr) {
				String calMonth = String.valueOf(res.get('mth')) == null ? 'Unknown': monthMap.get(String.valueOf(res.get('mth'))) + ' ' + String.valueOf(res.get('yr'));
				retList.add(new VolumeData(calMonth, Integer.valueOf(String.valueOf(res.get('qty')))));
				totalUnits += Integer.valueOf(String.valueOf(res.get('qty')));
			}
		} else {
			retList.add(new VolumeData('No Sales', 0));
		}
		volList = retList;
		return null;
    }
    
    public class VolumeData {
    	public String name { get; set; }
    	public Integer vol { get; set; }
    	
    	public VolumeData(String name, Integer vol) {
    		this.name = name;
    		this.vol = vol;
    	}
    }
}
/**
 *  Imports analytics data
 */

public with sharing class ImportGoogleAnalytics {
    public static GoogleAnalytics ga;
    public static string singleMetricName = 'Test Single Metric';
    public static  Date StartDate = Date.newInstance(2013,10,27);
    public static  Date EndDate = Date.newInstance(2014,2,27);

    public static void importSingleMetric(string siteMetricSettingsName) {
        if(initialize(siteMetricSettingsName)) {

            Site_Metric_Date__c metricDate = new Site_Metric_Date__c(Name = '2014 Q1', Start_Date__c = StartDate,
                End_Date__c = EndDate);

            GoogleAnalytics.Entry entry = ga.getEntry(metricDate, new list<String>{GoogleAnalytics.VisitorsMetric});

            // only start inserts after all callouts are complete
            insert metricDate;

            Site_Metric__c metric = new Site_Metric__c(Name = singleMetricName, Metric__c = GoogleAnalytics.VisitorsMetric,
                Value__c = decimal.ValueOf(entry.getMetric(GoogleAnalytics.VisitorsMetric)), Site_Metric_Date__c = metricDate.Id);

            insert metric;
        }
    }
//'General Metrics'
    public static void importDailyMetrics(string siteMetricSettingsName,String Status) {
        if(initialize(siteMetricSettingsName)) {
        //    Date startDate = Date.newInstance(2014, 1, 17);
        //    Date endDate = Date.newInstance(2014,1, 19);

            ga.insertDailyMetrics(startDate, endDate,
              new list<String>{GoogleAnalytics.VisitorsMetric,
                  GoogleAnalytics.PageViewsPerVisitMetric,
                  GoogleAnalytics.PercentNewVisitsMetric,
                  GoogleAnalytics.VisitBounceRateMetric,
                  GoogleAnalytics.AverageTimeOnSiteMetric},
              new list<string>{GoogleAnalytics.DateDimension,
                  GoogleAnalytics.TrafficDimension,
                  GoogleAnalytics.CityDimension,
                  GoogleAnalytics.VisitorTypeDimension,
                  GoogleAnalytics.DeviceCategoryDimension,
                  GoogleAnalytics.VisitLengthDimension,
                  GoogleAnalytics.LandingPagePathDimension},
              status);
        }
    }
    //'Page tracking'
    public static void importPageTrackingMetrics(String siteMetricSettingsName, String Status){
      if(initialize(siteMetricSettingsName)) {
        //    Date startDate = Date.newInstance(2014, 1, 17);
        //    Date endDate = Date.newInstance(2014,1, 19);

            ga.insertDailyMetrics(startDate, endDate,
              new list<String>{GoogleAnalytics.EntranceRateMetric,
                  GoogleAnalytics.PageViewsMetric,
                  GoogleAnalytics.PageViewsPerVisitMetric,
                  GoogleAnalytics.AverageTimeOnPageMetric},
              new list<string>{GoogleAnalytics.DateDimension,
                  GoogleAnalytics.VisitLengthDimension,
                  GoogleAnalytics.SecondPagePathDimension,
                  GoogleAnalytics.ExitPagePathDimension,
                  GoogleAnalytics.LandingPagePathDimension},
            status);
        }
    }

    /**
     * Imports video metrics data.
     */
    public static void importVideoMetrics() {
      importPageTrackingMetrics('Video View', 'Video View');
    }

    /**
     * Imports All Website Data
     */
    public static void importAllWebSiteData() {
      importDailyMetrics('All WebSite Data','General Metrics');
    }

    /**
     * Imports FAS Search Data
     */
    public static void FASSearchData() {
      importDailyMetrics('FAS Search','FAS Search');
    }

    /**
     * Imports FAS Profile View Data
     */
    public static void FASProfileViewData() {
      importDailyMetrics('FAS Profile View','FAS Profile View');
    }

    /**
     * Imports HCP FAS Search Data
     */
    public static void HCPFASSearchData() {
      importDailyMetrics('HCP FAS Search','HCP FAS Search');
    }

    /**
     * Imports HCP Profile View Data
     */
    public static void HCPProfileViewData() {
      importDailyMetrics('HCP Profile View','HCP Profile View');
    }

    /**
     * Imports page tracking information
     */
    public static void importWebsitePagesData(){
      importPageTrackingMetrics('All WebSite Data', 'Page Tracking');
    }

    /**
     * Imports Mobile FAS Search information
     */
     public static void importMobileFASData(){
      importPageTrackingMetrics('Mobile FAS Search', 'FAS Search');
    }
    public static void importEvents(string siteMetricSettingsName) {
        if(initialize(siteMetricSettingsName)) {
           // Date startDate = Date.newInstance(2014, 1, 17);
           // Date endDate = Date.newInstance(2014, 5, 19);
            List<String> metrics = new List<String>{GoogleAnalytics.TotalEventsMetric};
            List<String> dimensions = new List<String>{GoogleAnalytics.DateDimension, GoogleAnalytics.EventCategoryDimension,
                GoogleAnalytics.EventActionDimension, GoogleAnalytics.EventLabelDimension};

            list<GoogleAnalytics.Entry> entries = ga.getEntries(startDate, endDate, dimensions, metrics);
            list<Site_Event__c> events = new list<Site_Event__c>();

            for (GoogleAnalytics.Entry entry : entries) {
                Site_Event__c event = new Site_Event__c(
                    Name = entry.getDimension(GoogleAnalytics.EventLabelDimension),
                    Category__c = entry.getDimension(GoogleAnalytics.EventCategoryDimension),
                    Action__c = entry.getDimension(GoogleAnalytics.EventActionDimension),
                    Count__c = decimal.ValueOf(entry.getMetric(GoogleAnalytics.TotalEventsMetric)),
                    Date__c = GoogleAnalytics.parseDate(entry.getDimension(GoogleAnalytics.DateDimension))
                );
                events.add(event);
            }

            insert events;
        }
    }

    static boolean initialize(string siteMetricSettingsName) {
        Site_Metric_Settings__c settings;

        try {
            settings = [Select s.Table_Id__c, s.Password__c, s.Email__c From Site_Metric_Settings__c s where s.Name = :siteMetricSettingsName];
        } catch (QueryException ex) {
            System.debug(Logginglevel.WARN, 'No Site Metric Settings with Name = ' + siteMetricSettingsName + '; stopping import');
            return false;
        }

        ga = new GoogleAnalytics(settings);
        return true;
    }


}
/*****************************************************
Class: LWCHospital
Hospital class for Surgeon_Affiliation__c, include remove, index. Used in profile general
Author – Wade Liu
Date – 02/25/2014
******************************************************/
public with sharing class LWCHospital {
	public Surgeon_Affiliation__c hospital {get; set;}
    public Boolean remove {get; set;}
    public integer index {get;set;}
    
    //iniatialize index, random Id for remove and update
    public LWCHospital(Surgeon_Affiliation__c h){
        this.hospital = h;
        this.remove = false;
        this.index = Math.round(Math.random()*10000);
    }
}
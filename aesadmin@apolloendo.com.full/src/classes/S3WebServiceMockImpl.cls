@isTest
global class S3WebServiceMockImpl implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       S3.DeleteObjectResponse_element respElement = 
           				new S3.DeleteObjectResponse_element();
       S3.Status status = new S3.Status();
       status.Code = 200;
       respElement.DeleteObjectResponse = status;
       response.put('response_x', respElement); 
   }
}
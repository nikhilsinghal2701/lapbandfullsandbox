/* Created: 11/14/2014
 * Author: Sanjay Sankar
 * Depends on: CurrencyLoadUtility.cls
 * Test Class(es): ScheduleCurrencyLoadTest.cls
 * Purpose: To load previous day's currency conversion rates into Currency_Conversion__c sourcing from Oanda
 */

global with sharing class ScheduleCurrencyLoad implements Schedulable {
	global void execute (SchedulableContext sc) {
		CurrencyLoadUtility.InsertCurrencyConversion();
	}
}
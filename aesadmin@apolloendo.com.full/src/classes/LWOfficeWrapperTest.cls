/*
Class: LWOfficeWrapper
Purpose: Test Class for class LWOfficeWrapper
Author: Chris
Created DAte: 03/07/2014
*/
@isTest
public with sharing class LWOfficeWrapperTest {
	static testMethod void testLWOfficeWrapperController(){
		Account account = TestUtil.createAccounts(1, true, null)[0];
		LWOfficeWrapper officeWripper = new LWOfficeWrapper(account.Id);
		System.assertEquals(account.Id, officeWripper.office.Specialist__c);
	}

}
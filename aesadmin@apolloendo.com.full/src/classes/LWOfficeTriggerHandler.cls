/*****************************************************
Class: LWOfficeTriggerHandler
Trigger handler for Offie__c
Author – Vinay L
Date – 01/20/2014
******************************************************/
public with sharing class LWOfficeTriggerHandler {

    public static void handlerOnAfterUpdate(Map<Id,Office__c> oldOfficeMap, Map<Id,Office__c> newOfficeMap){
        List<Id> officeIdList = new List<Id>();
        if(!system.isFuture()){// Prevent second update from future call
            if(newOfficeMap.size() <= 10){
                for(Office__c o :newOfficeMap.values()){
                    if(o.Address_1__c != oldOfficeMap.get(o.Id).Address_1__c || o.Address_2__c != oldOfficeMap.get(o.Id).Address_2__c || o.City__c != oldOfficeMap.get(o.Id).City__c || o.State__c != oldOfficeMap.get(o.Id).State__c || o.Zip_Code__c != oldOfficeMap.get(o.Id).Zip_Code__c){
                        officeIdList.add(o.Id);
                    }
                }
            }
            LWLocationCallouts.getLocation(officeIdList);      
        }
    }
    
    public static void handlerOnAfterInsert(List<Office__c> newOffList){
        List<Id> officeIdList = new List<Id>();
        if(newOffList.size() <= 10){
            for(Office__c o :newOffList){
                if(o.Address_1__c != null || o.Address_2__c != null || o.City__c != null || o.State__c != null || o.Zip_Code__c != null){
                    officeIdList.add(o.Id);
                }
            }
        }
        system.debug('====='+officeIdList);
        LWLocationCallouts.getLocation(officeIdList);
        
    }
    public static void handlerOnBeforeUpdate(Map<Id,Office__c> oldOfficeMap, Map<Id,Office__c> newOfficeMap){
        Id recTypId = [SELECT Id FROM RecordType WHERE DeveloperName = :LWCConstantDeclaration.PENDINGOFFICE AND SobjectType = :LWCConstantDeclaration.OFFICE][0].Id;
        List<Office__c> newApprovalOffice = new List<Office__c>();
        List<Id> resetMasterOfficeId = new List<Id>();
        Map<Id,Office__c> childOffice = new Map<Id,Office__c>(); //Map of Master office and and Child Office record
        //List<Id> officeIdList = new List<Id>();
        if(newOfficeMap.size()<=10){
            
            //Modify record in approval and copy non address data to master record.
            List<Id> listOfMasterOfficeId = new List<Id>();
            Map<Id,Office__c> mapOfMasterOffice = new Map<Id,Office__c>();
            List<Office__c> updateMasterOffice = new List<Office__c>();
            for(Office__c newOff :newOfficeMap.values()){
                if(newOff.RecordTypeId == recTypId && newOff.Approved_Office__c != null){// Office in approval process
                    listOfMasterOfficeId.add(newOff.Approved_Office__c);
                }
            }
            for(Office__c o :[SELECT Id, Bariatric_Surgery_Center__c,Chinese__c,Email__c,English__c,Ext__c,Fax__c,
                                     French__c,German__c,Italian__c,Japanese__c,Korean__c,Live_Seminars_Link__c,
                                     Name,Office_Hours__c,Online_Seminars_Link__c,Phone__c,Russian__c,Spanish__c,
                                     Vietnamese__c,Website__c,Wheelchair_Access__c FROM Office__c WHERE Id in :listOfMasterOfficeId]){
                 mapOfMasterOffice.put(o.Id,o);                     
            }
            //
            for(Office__c newOff :newOfficeMap.values()){
                if(newOff.Is_Edit_Flag__c == false &&
                   newOff.Copy_Address__c == false &&
                     (newOff.Address_1__c != oldOfficeMap.get(newOff.Id).Address_1__c
                   || newOff.Address_2__c != oldOfficeMap.get(newOff.Id).Address_2__c
                   || newOff.City__c != oldOfficeMap.get(newOff.Id).City__c
                   || newOff.State__c != oldOfficeMap.get(newOff.Id).State__c
                   || newOff.Zip_Code__c != oldOfficeMap.get(newOff.Id).Zip_Code__c )
                  ){
                    
                    system.debug('--'+newOff);
                    Office__c cloneOffice = new Office__c();
                    cloneOffice = newOff.clone();
                    //newOff = oldOfficeMap.get(newOff.Id);
                    newOff.Is_Edit_Flag__c = true;
                    newOff.Address_1__c = oldOfficeMap.get(newOff.Id).Address_1__c;
                    newOff.Address_2__c = oldOfficeMap.get(newOff.Id).Address_2__c;
                    newOff.City__c = oldOfficeMap.get(newOff.Id).City__c;
                    newOff.State__c = oldOfficeMap.get(newOff.Id).State__c;
                    newOff.Zip_Code__c = oldOfficeMap.get(newOff.Id).Zip_Code__c;
                    
                    cloneOffice.RecordTypeId = recTypId;
                    cloneOffice.Status__c = LWCConstantDeclaration.SUBMITTED;
                    cloneOffice.Is_Edit_Flag__c = true;
                    cloneOffice.Approved_Office__c = newOff.Id;
                    newApprovalOffice.add(cloneOffice);
                }
                
                if(newOff.Copy_Address__c){
                    newOff.Copy_Address__c = false;
                }
                
                //Modify record in approval and copy non address data to mastr record.
                
                
                if(newOff.RecordTypeId == recTypId && newOff.Approved_Office__c != null){
                    Office__c tmpOffice = new Office__c();
                    tmpOffice = mapOfMasterOffice.get(newOff.Approved_Office__c);
                    tmpOffice.Bariatric_Surgery_Center__c = newOff.Bariatric_Surgery_Center__c;
                    tmpOffice.Chinese__c = newOff.Chinese__c;
                    tmpOffice.Email__c = newOff.Email__c;
                    tmpOffice.English__c = newOff.English__c;
                    tmpOffice.Ext__c = newOff.Ext__c;
                    tmpOffice.Fax__c = newOff.Fax__c;
                    tmpOffice.French__c = newOff.French__c;
                    tmpOffice.German__c = newOff.German__c;
                    tmpOffice.Italian__c = newOff.Italian__c;
                    tmpOffice.Japanese__c = newOff.Japanese__c;
                    tmpOffice.Korean__c = newOff.Korean__c;
                    tmpOffice.Live_Seminars_Link__c = newOff.Live_Seminars_Link__c;
                    tmpOffice.Name = newOff.Name;
                    tmpOffice.Office_Hours__c = newOff.Office_Hours__c;
                    tmpOffice.Online_Seminars_Link__c = newOff.Online_Seminars_Link__c;
                    tmpOffice.Phone__c = newOff.Phone__c;
                    tmpOffice.Russian__c = newOff.Russian__c;
                    tmpOffice.Spanish__c = newOff.Spanish__c;
                    tmpOffice.Vietnamese__c = newOff.Vietnamese__c;
                    tmpOffice.Website__c = newOff.Website__c;
                    tmpOffice.Wheelchair_Access__c = newOff.Wheelchair_Access__c;
                    tmpOffice.Is_Edit_Flag__c = true;
                    updateMasterOffice.add(tmpOffice);                  
                }
                
                // Set master record to is Edit False once child is approved
                if((newOff.RecordTypeId != oldOfficeMap.get(newOff.Id).RecordTypeId && newOff.RecordTypeId  != recTypId && newOff.Approved_Office__c != null)
                ||(newOff.Status__c != oldOfficeMap.get(newOff.Id).Status__c && newOff.Status__c == LWCConstantDeclaration.REJECTED)){
                    resetMasterOfficeId.add(newOff.Approved_Office__c);
                    childOffice.put(newOff.Approved_Office__c,newOff);
                }
                
                if(newOff.RecordTypeId != oldOfficeMap.get(newOff.Id).RecordTypeId && newOff.RecordTypeId  != recTypId && newOff.Approved_Office__c == null){// New office being approved - No master office
                    newOff.Is_Edit_Flag__c = false;
                }
                    
            }
            
            //Modify record in approval and copy non address data to master record.
            LWCWithoutSharing.updateOfficeList(updateMasterOffice);
            
            //Insert new approval office and submit for approval
            LWCWithoutSharing.insertOffice(newApprovalOffice);

            LWCWithoutSharing.submitOfcApproval(newApprovalOffice);
            
            // Set master record to is Edit False once child is approved and copy over the new address
            List<Office__c> updateMasterOfficeList = new List<Office__c>();
            for(Office__c masterOffice : [SELECT ID,Is_Edit_Flag__c FROM Office__c WHERE Id in: resetMasterOfficeId]){
                masterOffice.Is_Edit_Flag__c = false;
                if(childOffice.get(masterOffice.Id).Status__c == LWCConstantDeclaration.REJECTED){
                    
                }else{//copy data to master only if approved
                    masterOffice.Address_1__c = childOffice.get(masterOffice.Id).Address_1__c;
                    masterOffice.Address_2__c = childOffice.get(masterOffice.Id).Address_2__c;
                    masterOffice.City__c = childOffice.get(masterOffice.Id).City__c;
                    masterOffice.State__c = childOffice.get(masterOffice.Id).State__c;
                    masterOffice.Zip_Code__c = childOffice.get(masterOffice.Id).Zip_Code__c;
                    masterOffice.Location__Longitude__s = childOffice.get(masterOffice.Id).Location__Longitude__s;
                    masterOffice.Location__Latitude__s = childOffice.get(masterOffice.Id).Location__Latitude__s;
                }
                masterOffice.Copy_Address__c = true;
                updateMasterOfficeList.add(masterOffice);
            }
            
            LWCWithoutSharing.updateMasterOfficeList(updateMasterOfficeList);

            
        }
    }
}
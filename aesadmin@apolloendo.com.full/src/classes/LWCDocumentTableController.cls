/**
Class: LWCDocumentTableController
Purpose: Controller for LWCDocumentTabel page
Author – Vinay L
Date – 02/16/2014
*/
public with sharing class LWCDocumentTableController {
     public List<DocumentWrapper> docWrapperList        {get;set;}
        
    
     public Id                      s3ObjId         {get;set;}
     public String                  Type            {get;set;}
     public String                  cType           {get;set;}
     public String                  source          {get;set;}
     public Map<String,String>      previewLinkMap  {get;set;}
     
     //constructor to initialize the doc list
     public LWCDocumentTableController(){
        list<AWS_S3_Object__c> docList = [SELECT Access__c,Bucket_Name__c,Content_Type__c,File_Name__c,Id,LastActivityDate,LastModifiedById,
                                    LastModifiedDate,Library__c,Name, Title__c,Description__c 
                                FROM AWS_S3_Object__c 
                                WHERE Access__c = :LWCConstantDeclaration.PRIVATEACCESS 
                                ORDER BY LastModifiedDate DESC];
        docWrapperList = new List<DocumentWrapper>();
        for(AWS_S3_Object__c aso : docList){
            do{
                if(aso.Content_Type__c == 'pdf'){
                    docWrapperList.add(new DocumentWrapper(aso,'0px'));
                    break;
                }
                
                if(aso.Content_Type__c == 'image/gif' || aso.Content_Type__c == 'image/jpeg' ||
                   aso.Content_Type__c == 'image/png' || aso.Content_Type__c == 'gif' ||
                   aso.Content_Type__c == 'jpg' ){
                    docWrapperList.add(new DocumentWrapper(aso,'-480px'));
                    break;
                }
                
                if(aso.Content_Type__c == 'video/x-ms-wmv' || aso.Content_Type__c == 'video/mp4'){
                    docWrapperList.add(new DocumentWrapper(aso,'-720px'));
                    break;
                }
                
                if(aso.Content_Type__c == 'text/plain' || aso.Content_Type__c == 'application/msword' ||
                   aso.Content_Type__c == 'text/html'){
                    docWrapperList.add(new DocumentWrapper(aso,'-120px'));
                    break;
                }
                
                if(aso.Content_Type__c == 'application/zip'){
                    docWrapperList.add(new DocumentWrapper(aso,'-600px'));
                    break;
                }
            }while(false);
        }
        system.debug(docWrapperList);
        
        // Get preview link map to show the preview of the file 2/18/2014 Christine
        if(Apexpages.currentPage().getUrl().contains(LWCConstantDeclaration.LWCDOCUMENTLIBRARY)){
            previewLinkMap = new Map<String,String>();
            List<Id> awsIdList = new List<Id>();
            for(AWS_S3_Object__c aws : docList){
                awsIdList.add(aws.Id);
                previewLinkMap.put(aws.Id,'1');//populate the aws Id
            }
            
            for(Attachment att : [SELECT Id,parentId 
                         FROM Attachment 
                         WHERE parentId IN :awsIdList ORDER BY LastModifiedDate]){
                previewLinkMap.put(att.parentId,att.id);
            }
        }
     }
    
    //created download link from LWCDocumentTableController object
     public PageReference createsDownloadLink() {
        AWS_S3_Object__c s3Obj = [SELECT Access__c,AmazonLink__c,Bucket_Name__c,Content_Type__c,CreatedDate,FileLink__c,File_Name__c,Id,Library__c,Name,OwnerId,Preview_Link__c FROM AWS_S3_Object__c WHERE ID = :s3ObjId][0];
        S3FormController s3OrgCredential = new S3FormController();
        s3OrgCredential.credentials = new AWSKeys(s3OrgCredential.AWSCredentialName);       
        
        String signedUrl=null;
        String timeStamp=null;
        if(s3Obj.Access__c=='private'){
             timeStamp = calculateTimeStamp();
             SignedURL = calculateSignature(timeStamp,s3Obj,s3OrgCredential);
             s3Obj.FileLink__c=s3Obj.Amazonlink__c+SignedURL;
         }
         else if(s3Obj.Access__c=='public-read'){
             s3Obj.FileLink__c=s3Obj.Amazonlink__c;
         }
            
         PageReference pageRef = new PageReference(String.valueOf(s3Obj.FileLink__c));
         //pageRef.getHeaders().put('content-disposition', 'attachemnt; '+'Tool-for-Recommended-Aftercare-TRAC.pdf');
         pageRef.setRedirect(false);
            
         return pageRef;
    
    }
    
     /**
   * Calculates encrypted URL for private files.
   *
   * @param String - time duration for which the files should be accessible.
   * @param AWS_S3_Object__c - File link in sfdc
   * @return String - SignedURL
   */
    public Static String calculateSignature(String timeStamp, AWS_S3_Object__c s3Obj,S3FormController s){
        String FileName;
        String getURL;
        String signedURL;
        if(s3Obj.File_Name__c!=null && s3Obj.Bucket_Name__c!=null){
            FileName=s3Obj.Bucket_Name__c+'/'+s3Obj.File_Name__c;
            if(FileName.containsWhitespace())
            Filename.replaceAll(' ','%20');
            getURL='GET\n\n\n'+timeStamp+'\n/'+Filename;
            getURL='GET\n\n\n'+timeStamp+'\n/'+Filename+'?response-content-disposition=attachment; filename='+s3Obj.File_Name__c;
            String macUrl ;

            String signingKey = EncodingUtil.base64Encode(Blob.valueOf(s.credentials.secret));
            Blob mac = Crypto.generateMac('HMacSHA1',blob.ValueOf(getURL),blob.Valueof(s.credentials.secret));
            macUrl = EncodingUtil.base64Encode(mac);
            System.debug('checking signed URL before..'+macURL);
            macURL=EncodingUtil.urlEncode(macURl, 'UTF-8');
            signedURL='?Signature='+macUrl+'&Expires='+timeStamp+'&AWSAccessKeyId='+s.credentials.key+'&response-content-disposition=attachment; filename='+s3Obj.File_Name__c;
            System.debug('checking signed URL after..'+macURL);

        }

        return signedURL;
    }
     
     //calculate time stamp
     public static String calculateTimeStamp(){
         Long timeStamp = (System.currentTimeMillis())/1000;
         timeStamp=timeStamp+3600;
         return String.ValueOf(timestamp);
    }
    
    //inner class documentwrapper
    public class DocumentWrapper {
        public AWS_S3_Object__c document {get;set;}
        public String   iconPosition    {get;set;}  
        DocumentWrapper(AWS_S3_Object__c doc,String pos){
            this.document = doc;
            this.iconPosition = pos;
        }
    }
}
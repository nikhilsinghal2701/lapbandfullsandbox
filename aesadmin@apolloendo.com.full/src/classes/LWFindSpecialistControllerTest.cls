/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LWFindSpecialistControllerTest {

    static testMethod void testOptOutEmail() {
        // prepare data
        Lead lead = TestUtil.createLeads(1, true, null)[0];
             
        // set parameter
        ApexPages.currentPage().getParameters().put( 'lId', lead.Id );
        
        // create a new controller
        LWFindSpecialistController controller = new LWFindSpecialistController();
        
        controller.optOutEmail();
        
        // assert
        Lead l = [ SELECT Id, HasOptedOutOfEmail FROM Lead WHERE Id = :lead.Id LIMIT 1 ];
        System.assertEquals( true, l.HasOptedOutOfEmail );
    }
    
    static testMethod void testGetOffice() {
        // prepare data
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        Id aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        List<Office__c> officeList = TestUtil.createOffices(5, true, new Map<String, Object>{'Location__Latitude__s' => 37.5548022,
                                                                                             'Location__Longitude__s' => -121.9889206,
                                                                                             'English__c' => new List<Boolean>{true, false, true, true, false},
                                                                                             'Wheelchair_Access__c' => new List<Boolean>{true, true, false, true, false},
                                                                                             'Specialist__c' => acc.Id,
                                                                                             'Status__c' => 'Submitted',
                                                                                             'RecordTypeId' => aId }); 
        
        // set parameters
        String str1 = '37.7925:-122.400556';
        List<Office__c> offices_1 = LWFindSpecialistController.getOffice(str1);
        
        String str2 = '37.7925:-122.400556:50';
        List<Office__c> offices_2 = LWFindSpecialistController.getOffice(str2);
        
        String str3 = '37.7925:-122.400556:50:English';
        List<Office__c> offices_3 = LWFindSpecialistController.getOffice(str3);
        
        String str4 = '37.7925:-122.400556:50:English:checked';
        List<Office__c> offices_4 = LWFindSpecialistController.getOffice(str4);
        
        // assert
        System.assertEquals( 5, offices_1.size() );
        System.assertEquals( 5, offices_2.size() );
        System.assertEquals( 3, offices_3.size() );
        System.assertEquals( 2, offices_4.size() );
    }
    
    static testMethod void testGetOfficeByPage() {
        // prepare data
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        Id aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        List<Office__c> officeList = TestUtil.createOffices(20, true, new Map<String, Object>{'Location__Latitude__s' => 37.5548022,
                                                                                             'Location__Longitude__s' => -121.9889206,
                                                                                             'English__c' => true,
                                                                                             'Wheelchair_Access__c' => true,
                                                                                             'Specialist__c' => acc.Id,
                                                                                             'Status__c' => 'Submitted',
                                                                                             'RecordTypeId' => aId}); 
        
        // set parameters
        String str1 = '37.7925:-122.400556';
        Integer int1 = 2;
        List<OfficeSurgeon> offices_1 = LWFindSpecialistController.getOfficeByPage(str1, int1);
        
        String str2 = '37.7925:-122.400556:50';
        Integer int2 = 1;
        List<OfficeSurgeon> offices_2 = LWFindSpecialistController.getOfficeByPage(str2, int2);
        
        String str3 = '37.7925:-122.400556:50:English';
        Integer int3 = 1;
        List<OfficeSurgeon> offices_3 = LWFindSpecialistController.getOfficeByPage(str3, int3);
        
        String str4 = '37.7925:-122.400556:50:English:checked';
        Integer int4 = 2;
        List<OfficeSurgeon> offices_4 = LWFindSpecialistController.getOfficeByPage(str4, int4);
        
        // assert
        /*System.assertEquals( 5, offices_1.size() );
        System.assertEquals( 15, offices_2.size() );
        System.assertEquals( 15, offices_3.size() );
        System.assertEquals( 5, offices_4.size() );*/
    }
    
    static testMethod void testGetOfficeByDrName() {
        // prepare data
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        Id aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        List<Office__c> officeList = TestUtil.createOffices(5, true, new Map<String, Object>{'Name' => new List<String>{'Name1', 'Name2', 'Name3', 'Name4', 'Name5'},
                                                                                             'Specialist__c' => acc.Id,
                                                                                             'Status__c' => 'Submitted',
                                                                                             'RecordTypeId' => aId}); 
        
        // set parameters
        String str1 = 'Name1';
        List<Office__c> offices_1 = LWFindSpecialistController.getOfficeByDrName(str1);
        
        // assert
        //System.assertEquals( 1, offices_1.size() );
    }
    
     static testMethod void testGetOfficeByName() {
        // prepare data
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        Id aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        List<Office__c> officeList = TestUtil.createOffices(5, true, new Map<String, Object>{'Name' => new List<String>{'Name1', 'Name2', 'Name3', 'Name4', 'Name5'},
                                                                                             'Specialist__c' => acc.Id,
                                                                                             'Status__c' => 'Submitted',
                                                                                             'RecordTypeId' => aId}); 
        
        // set parameters
        String str1 = 'Test Account';
        List<Office__c> offices_1 = LWFindSpecialistController.getOfficeByName(str1);
        
        // assert
        System.assertEquals( 5, offices_1.size() );
    }
    
    static testMethod void testGetOfficeByDrNameByPage() {
        // prepare data
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        Id aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        List<Office__c> officeList = TestUtil.createOffices(20, true, new Map<String, Object>{'Name' => 'nameTest',
                                                                                              'Specialist__c' => acc.Id,
                                                                                              'Status__c' => 'Submitted',
                                                                                              'RecordTypeId' => aId}); 
        
        // set parameters
        String str1 = '***name';
        Integer int1 = 2;
        List<OfficeSurgeon> offices_1 = LWFindSpecialistController.getOfficeByDrNameByPage(str1, 2);
        
        // assert
        //System.assertEquals( 5, offices_1.size() );
    }
    
    static testMethod void testGetSurgeonByAccountId() {
        // prepare data
        Account acc =  TestUtil.createAccounts(1, true, null)[0];
        
        Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
                                                                                      'Name' => 'Test'})[0];
        
        // set parameters
        String accId = acc.Id;
        Surgeon__c s = LWFindSpecialistController.getSurgeonByAccountId( acc.Id );
        
        // assert
        System.assertEquals( 'Test', s.Name );
    }
    
    static testMethod void testGetOfficesByAccountId() {
        // prepare data
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        Id aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        List<Office__c> officeList = TestUtil.createOffices(5, true, new Map<String, Object>{'Name' => 'nameTest',
                                                                                             'Specialist__c' => acc.Id,
                                                                                             'Status__c' => 'Submitted',
                                                                                             'RecordTypeId' => aId}); 
        
        // set parameters
        String accId = acc.Id;
        List<Office__c> offices_1 = LWFindSpecialistController.getOfficesByAccountId(accId);
        
        // assert
        System.assertEquals( 5, offices_1.size() );
    }
    
    static testMethod void testGetProfileID() {
        // prepare data
        List<Account> accs =  TestUtil.createAccounts(2, true, null);
        
        Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => accs[0].Id})[0];
        
        Attachment attachment = TestUtil.createAttachments(1, true, new Map<String, Object>{'parentId' => surgeon.Id})[0];
        
        // set parameters
        String accId = accs[0].Id;
        String result1 = LWFindSpecialistController.getProfileID(accId);
        
        String accId_2 = accs[1].Id;
        String result2 = LWFindSpecialistController.getProfileID(accId_2);
        
        // assert
        System.assertEquals( attachment.Id, result1 );
        System.assertEquals( 'nopicture', result2 );
    }
    
    static testMethod void testGoToDetails() {
        
        // create a new controller
        LWFindSpecialistController controller = new LWFindSpecialistController();
        
        String url = controller.goToDetails().getUrl();
        
        // assert
        System.assertEquals( '/Compare-Lapband', url );
    }
    
    static testMethod void testGetSurgeon() {
        // prepare data
        Account acc =  TestUtil.createAccounts(1, true, null)[0];
        
        Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
                                                                                      'Name' => 'Test'})[0];
        
        // set parameters
        ApexPages.currentPage().getParameters().put( 'accountId', acc.Id );
        
        // create a new controller
        LWFindSpecialistController controller = new LWFindSpecialistController();
        
        Surgeon__c s = controller.getSurgeon();
        
        // assert
        System.assertEquals( 'Test', s.Name );
    }
    
     static testMethod void testGetAccreditations() {
        // prepare data
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        Id aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        Office__c office = TestUtil.createOffices(1, true, new Map<String, Object>{'Name' => 'nameTest',
                                                                                       'Specialist__c' => acc.Id,
                                                                                       'Status__c' => 'Submitted',
                                                                                       'RecordTypeId' => aId})[0]; 
        
        List<Membership_Accreditation__c> membershipList = TestUtil.createMembershipAccreditation(5, true, new Map<String, Object>{'Office__c' => office.Id}); 
        
        // create a new controller
        LWFindSpecialistController controller = new LWFindSpecialistController();
        controller.officeId = office.Id;
        List<Membership_Accreditation__c> memberships = controller.getAccreditations();
        
        // assert
        System.assertEquals( 5, memberships.size() );
    }
    
    static testMethod void testGetMembershipsOrAccreditations() {
        // prepare data
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
       
        List<Membership_Accreditation__c> membershipList = TestUtil.createMembershipAccreditation(5, true, new Map<String, Object>{'Specialist__c' => acc.Id}); 
        
        // set parameters
        ApexPages.currentPage().getParameters().put( 'accountId', acc.Id );
        
        // create a new controller
        LWFindSpecialistController controller = new LWFindSpecialistController();
        
        List<Membership_Accreditation__c> memberships = controller.getMembershipsOrAccreditations();
        
        // assert
        System.assertEquals( 5, memberships.size() );
    }
    
    static testMethod void testGetOffices() {
        // prepare data
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        Id aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        List<Office__c> officeList = TestUtil.createOffices(5, true, new Map<String, Object>{'Name' => 'nameTest',
                                                                                             'Specialist__c' => acc.Id,
                                                                                             'Status__c' => 'Submitted',
                                                                                             'RecordTypeId' => aId}); 
        
        // set parameters
        ApexPages.currentPage().getParameters().put( 'accountId', acc.Id );
        
        // create a new controller
        LWFindSpecialistController controller = new LWFindSpecialistController();
        
        List<Office__c> offices = controller.getOffices();
        
        // assert
        System.assertEquals( 5, offices.size() );
    }
    
    static testMethod void testGetProfileId2() {
        // prepare data
        List<Account> accs =  TestUtil.createAccounts(2, true, null);
        
        Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => accs[0].Id })[0];
        
        Attachment attachment = TestUtil.createAttachments(1, true, new Map<String, Object>{'ParentId' => surgeon.Id})[0];
        
        // set parameters
        ApexPages.currentPage().getParameters().put( 'accountId', accs[0].Id );
        
        // create a new controller
        LWFindSpecialistController controller = new LWFindSpecialistController();
        String result1 = controller.getProfileId();
        
        // assert
        System.assertEquals( attachment.Id, result1 );
        
        // set parameters
        controller = null;
        ApexPages.currentPage().getParameters().put( 'accountId', accs[1].Id );
        controller = new LWFindSpecialistController();
        String result2 = controller.getProfileId();
        System.assertEquals( 'nopicture', result2 );
    }
    
    static testMethod void testGetOfficeByName2() {
        // prepare data
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        Id aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
                                                                                      'Name' => 'Name1' })[0];
        
        List<Office__c> officeList = TestUtil.createOffices(5, true, new Map<String, Object>{'Name' => 'Test',
                                                                                             'Specialist__c' => acc.Id,
                                                                                             'Status__c' => 'Submitted',
                                                                                             'RecordTypeId' => aId}); 
        
        ApexPages.currentPage().getParameters().put( 'practiceName', 'Test Account' );
        LWFindSpecialistController controller = new LWFindSpecialistController();

        controller.getOfficeByName();
        List<Office__c> offices_1 = controller.officeList;
        System.assertEquals( 5, offices_1.size() );
        
        controller = null;
        ApexPages.currentPage().getParameters().put( 'practiceName', '' );
        ApexPages.currentPage().getParameters().put( 'specialistName', 'Name1' );
        controller = new LWFindSpecialistController();

        controller.getOfficeByName();
        List<Office__c> offices_2 = controller.officeList;
        System.assertEquals( 5, offices_2.size() );
    }
    
    static testMethod void testCreateSiteEventDoctor() {
        // prepare data
        Account acc =  TestUtil.createAccounts(1, true, null)[0];
        
        Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
                                                                                      'Name' => 'Test'})[0];
        
        // set parameters
        ApexPages.currentPage().getParameters().put( 'accountId', acc.Id );
        
        // create a new controller
        LWFindSpecialistController controller = new LWFindSpecialistController();
        
        // do createSiteEventDoctor
        controller.createSiteEventDoctor();
        
        // assert
        List<Site_Event__c> events = [ SELECT Surgeon_Name__c, Type__c FROM Site_Event__c
                                       WHERE Type__c = 'Doctor' ];
        System.assertEquals(1, events.size());
    }
    
    static testMethod void testCreateSiteEventWebsite() {
        // prepare data
        Account acc =  TestUtil.createAccounts(1, true, null)[0];
        
        Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
                                                                                      'Name' => 'Test'})[0];
        
        // set parameters
        ApexPages.currentPage().getParameters().put( 'accountId', acc.Id );
        
        // create a new controller
        LWFindSpecialistController controller = new LWFindSpecialistController();
        
        // do createSiteEventDoctor
        controller.createSiteEventWebsite();
        
        // assert
        List<Site_Event__c> events = [ SELECT Surgeon_Name__c, Type__c FROM Site_Event__c
                                       WHERE Type__c = 'Website' ];
        System.assertEquals(1, events.size());
    }
}
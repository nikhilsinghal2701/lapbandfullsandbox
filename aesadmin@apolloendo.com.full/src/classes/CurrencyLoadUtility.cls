/* Created: 11/14/2014
 * Author: Sanjay Sankar
 * Test Class(es): ScheduleCurrencyLoadTest.cls
 * Purpose: To load previous day's currency conversion rates into Currency_Conversion__c sourcing from Oanda
 */


public with sharing class CurrencyLoadUtility {

	@future (callout=true)
	public Static void insertCurrencyConversion() {

		//currency params
		List<Currencies__c> currList = new List<Currencies__c>([select Id, Name, Description__c from Currencies__c]);
		String quoteParams;
		if (currList != null && currList.size() > 0) {
			for (Currencies__c curr : currList) {
				quoteParams = quoteParams != null ? quoteParams + '&quote=' + EncodingUtil.urlEncode(curr.Name, 'UTF-8') : '&quote=' + EncodingUtil.urlEncode(curr.Name, 'UTF-8'); 
			}	
		}
		
		//fields params
		List<String> fieldList = Label.OandaFields.split(',');
		String fieldParams;
		if (fieldList != null && fieldList.size() > 0) {
			for (String fieldName : fieldList) {
				fieldParams = fieldParams != null ? fieldParams + '&fields=' + EncodingUtil.urlencode(fieldName, 'UTF-8') : '&fields=' + EncodingUtil.urlEncode(fieldName, 'UTF-8');
			}
		}
		
		//Date param
		String dateParam = Label.OandaOverrideDefaultDate == 'TRUE' && Label.OandaDateforRate != null ? Label.OandaDateforRate : String.valueOf(Date.Today().addDays(Integer.valueOf(Label.OandaDateOffset))); 
		
		//endpoint URL
		String endPoint = Label.OandaEndPoint + '&api_key=' + Label.OandaAPIKey + '&decimal_places=' + Label.OandaDecimalPlaces + '&date=' + dateParam + fieldParams + quoteParams;
		
		System.debug (endPoint);
		
		//send request
		JSONParser parser = invokeHttpGetRequest(endPoint);
		
		//parse response
		List<Currency_Conversion__c> retList = getCurrConvList(parser, dateParam);
		
		//insert USD
		Currency_Conversion__c baseCurrency = new Currency_Conversion__c();
		baseCurrency.ISO_Currency__c = 'USD';
		baseCurrency.Conversion_Rate__c = 1.0;
		baseCurrency.Effective_Date__c = Date.valueOf(dateParam);
		baseCurrency.External_ID__c = baseCurrency.ISO_Currency__c + dateParam.replace('-','');
		
		retList.add(baseCurrency);
		
		insert retList;
	}

    private static JSONParser invokeHttpGetRequest(String endPoint) {
    	String respStr;
    	HttpRequest req = new HttpRequest();
    	req.setTimeout(60000);
    	req.setMethod('GET');
    	req.setHeader('Accept', 'application/JSON');
    	req.setEndpoint(endPoint);
    	
    	Http httpCall = new Http();
    	HttpResponse resp;
    	
    	resp = new Http().send(req);
    	JSONParser parser;
    	
    	if (resp.getStatusCode() != 200) {
    		System.debug ('### ERROR ###');
    	} else {
	    	parser = JSON.createParser(resp.getBody());
    	}
    	
    	return parser;
    }

 	private static List<Currency_Conversion__c> getCurrConvList(JSONParser parser, String dateParam) {
 		List<Currency_Conversion__c> currConvList = new List<Currency_Conversion__c>();
 		
		//parse response
		while (parser.nextToken() != null) {
			if (String.valueOf(parser.getCurrentToken()) == 'FIELD_NAME' && parser.getText() == 'quotes') {
				parser.nextToken();
				parser.nextToken();
				while (String.valueOf(parser.getCurrentToken()) != 'END_OBJECT') {
 					Currency_Conversion__c currConvRec = new Currency_Conversion__c();
					currConvRec.ISO_Currency__c = parser.getText();
					parser.nextToken();
					while (String.valueOf(parser.getCurrentToken()) != 'END_OBJECT') {
						if (parser.getText() == 'ask') {
							parser.nextToken();
							//System.debug('### ASK: ' + parser.getText().trim());
							currConvRec.Average_Daily_Ask__c = Decimal.valueOf(parser.getText().trim());
						} else if (parser.getText() == 'bid') {
							parser.nextToken();
							//System.debug('### BID: ' + parser.getText().trim());
							currConvRec.Average_Daily_Bid__c = Decimal.valueOf(parser.getText().trim());
						} else if (parser.getText() == 'date') {
							parser.nextToken();
							//System.debug('### DATE: ' + parser.getText().trim().substring(0,10));
							currConvRec.Effective_Date__c = Date.valueOf(parser.getText().trim().substring(0,10));
						} else if (parser.getText() == 'midpoint') {
							parser.nextToken();
							//System.debug('### MIDPOINT: ' + parser.getText().trim());
							currConvRec.Conversion_Rate__c = Decimal.valueOf(parser.getText().trim());
						}
						parser.nextToken();
					}
					currConvRec.External_ID__c = currConvRec.ISO_Currency__c + dateParam.replace('-','');
					currConvRec.Data_Source__c = 'OANDA';
					currConvList.add(currConvRec);
					parser.nextToken();
				}
			}
		}
		return currConvList;
 	}
}
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OUSNegativeAmountTest {

    static testMethod void testOUSCreditOrderTypes() {

        /*** STAGE ***/

    	//get record type ID for Account Ship To
    	Schema.DescribeSObjectResult sObjDescrAcc = Schema.SObjectType.Account;
    	Map<String, Schema.RecordTypeInfo> accRecTypeInfo = sObjDescrAcc.getRecordTypeInfosByName();

    	//get record type ID for Sales Order LAP-BAND SYSTEMS
    	Schema.DescribeSObjectResult sObjDescrSO = Schema.SObjectType.Sales_Order__c;
    	Map<String, Schema.RecordTypeInfo> soRecTypeInfo = sObjDescrSO.getRecordTypeInfosByName();

		List<Sales_Order__c> soList = new List<Sales_Order__c>();
		List<Sales_Order_Line_Item__c> lineList = new List<Sales_Order_Line_Item__c>();

        // create account
    	Account a1 = TestUtil.createAccounts(1, false, null)[0];
    	a1.RecordTypeId = accRecTypeInfo.get('Ship To').getRecordTypeId();
    	a1.Targeted_Account__c = true;
    	a1.Health_Region_ID__c = '11111';
    	a1.Health_Territory_ID__c = '22222';
    	a1.RP_Region__c = '33333';
    	a1.RP_Territory__c = '44444';

    	insert a1;

        //create order1 with a regular sale order type
		Sales_Order__c so1 = new Sales_Order__c();
		so1.Name = 'OUSNegativeAmountTest TEST01';
		so1.PO__c = 'OUSNegativeAmountTest TEST_PO1';
		so1.Type__c ='US';
		so1.Order_Type__c = 'regular';
		so1.RecordTypeId = soRecTypeInfo.get('LAP-BAND SYSTEMS').getRecordTypeId();
		so1.Ship_To__c = a1.id;

		soList.add(so1);

        //create order2 with a credit order type
		Sales_Order__c so2 = new Sales_Order__c();
		so2.Name = 'OUSNegativeAmountTest TEST02';
		so2.PO__c = 'OUSNegativeAmountTest TEST_PO2';
		so2.Type__c ='US';
		so2.Order_Type__c = 'RE';
		so2.RecordTypeId = soRecTypeInfo.get('LAP-BAND SYSTEMS').getRecordTypeId();
		so2.Ship_To__c = a1.id;

		soList.add(so2);

		insert soList;

        /*** TEST ***/
        Test.startTest();
        //create line item1 for order1 with a positive value
		Sales_Order_Line_Item__c line1 = new Sales_Order_Line_Item__c();
		line1.Sales_Order__c = so1.Id;
		line1.Integration_LineAmount__c = '1,500.00';
		line1.Material__c = 'B-2240';
		line1.Description__c = 'LAP-BAND';
		line1.Quantity__c = 2;
		line1.Bill_Date__c = Date.newInstance(2014, 05, 25);
		line1.Due_Date__c = Date.newInstance(2014, 05, 15);
        lineList.add(line1);

        //create line item2 for order1 with a negative value
		Sales_Order_Line_Item__c line2 = new Sales_Order_Line_Item__c();
		line2.Sales_Order__c = so1.Id; 
		line2.Integration_LineAmount__c = '500.00-';
		line2.Material__c = 'B-2245';
		line2.Description__c = 'LAP-BAND';
		line2.Quantity__c = 1;
		line2.Bill_Date__c = Date.newInstance(2014, 05, 25);
		line2.Due_Date__c = Date.newInstance(2014, 05, 15);
        lineList.add(line2);

        //create line item3 for order2 with a positive value
		Sales_Order_Line_Item__c line3 = new Sales_Order_Line_Item__c();
		line3.Sales_Order__c = so2.Id; 
		line3.Integration_LineAmount__c = '750.00';
		line3.Material__c = 'B-2260';
		line3.Description__c = 'LAP-BAND';
		line3.Quantity__c = 1;
		line3.Bill_Date__c = Date.newInstance(2014, 05, 25);
		line3.Due_Date__c = Date.newInstance(2014, 05, 15);
        lineList.add(line3);

        //create line item4 for order2 with a negative value
		Sales_Order_Line_Item__c line4 = new Sales_Order_Line_Item__c();
		line4.Sales_Order__c = so2.Id; 
		line4.Integration_LineAmount__c = '-1200.00';
		line4.Material__c = 'B-22265';
		line4.Description__c = 'LAP-BAND';
		line4.Quantity__c = 2;
		line4.Bill_Date__c = Date.newInstance(2014, 05, 25);
		line4.Due_Date__c = Date.newInstance(2014, 05, 15);
        lineList.add(line4);
        
        insert lineList;
        
        Test.stopTest();

		/*** ASSERTS ***/
		Map<Id, Sales_Order_Line_Item__c> lineMap = new Map<Id, Sales_Order_Line_Item__c>([select Id, Amount__c, Integration_LineAmount__c from Sales_Order_Line_Item__c where Id = :line1.Id or Id = :line2.Id or Id = :line3.Id or Id = :line4.Id]);
		Map<Id, Sales_Order__c> soMap = new Map<Id, Sales_Order__c>([select Id, Is_Amount_Negative__c from Sales_Order__c where Id = :so1.Id or Id = :so2.Id]);

		System.assert(!soMap.get(so1.Id).Is_Amount_Negative__c);
		System.assert(soMap.get(so2.Id).Is_Amount_Negative__c);

		//Amount on item1 should be positive = Integration_LineAmount
		System.assertEquals(1500.00, lineMap.get(line1.Id).Amount__c);

		//Amount on item2 should be negative = Integration_LineAmount
		System.assertEquals(-500.00, lineMap.get(line2.Id).Amount__c);

		//Amount on item3 should be negative = Integration_LineAmount*(-1)
		System.assertEquals(Decimal.valueOf(lineMap.get(line3.Id).Integration_LineAmount__c)*(-1), lineMap.get(line3.Id).Amount__c);
		System.assertEquals(-750.00, lineMap.get(line3.Id).Amount__c);

		//Amount on item4 should be negative = Integration_LineAmount
		System.assertEquals(Decimal.valueOf(lineMap.get(line4.Id).Integration_LineAmount__c), lineMap.get(line4.Id).Amount__c);
		System.assertEquals(-1200.00, lineMap.get(line4.Id).Amount__c);

    }
}
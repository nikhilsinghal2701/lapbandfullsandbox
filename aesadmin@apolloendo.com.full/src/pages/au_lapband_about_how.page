<apex:page showHeader="false" standardStylesheets="false" sidebar="false">
<html>
<head>
<title>How It Works | The LAP-BAND® System</title>
<meta name="pageset" content="aboutNav" />
<meta name="subnavset" content="subHow" />
<meta name="description" content="Let us fill you in on how the LAP-BAND® System will help you feel full faster and stay full longer." />
<c:au_lapband_head />
</head>

<body>

<!-- SET: WRAPPER -->
<div class="wrapper">
    
    <!-- SET: HEADER -->
             <c:au_lapband_nav />
             
                 
            <!-- END: HEADER -->
            
            <div class="baner inner1">
                <ul>
                   <li>
                       <div class="container">
                           <div class="baner_txt inner1">
                               <span class="icon">
                               <apex:image value="{!URLFOR($Resource.r_lapband_images2, 'images/iner11_icon.png')}" alt="img" height="62px" width="62px"/>
                               <!-- <img src="images/iner11_icon.png" alt="img"> -->
                               </span>
                               <h3>How the LAP-BAND<sup style="font-size: 16px; line-height: 0; vertical-align: 2px">®</sup> <br />                               
                                System Works.</h3>                
                           </div>
                       </div>
                   </li>
                </ul>
            </div>
            
  <!-- SET: MAIN CONTENT -->
            <div class="main_content">
                <div class="content">               
                   <div class="container">
                       <div class="para iner11">
                          <p>Okay, it looks like we’ve piqued your interest. That’s great. But now you’ve probably got a lot of questions. </p>
                          <p><em>What will my recovery be like? How long before I can get back to my life? And what exactly will I be doing to my body?</em> (And probably a few others, too.) </p>
                          
                          <p>All of the answers to those questions and more are in this section. To start, you should know that the LAP-BAND<sup>®</sup> System procedure is minimally invasive, typically done as an outpatient procedure in less than an hour, and has a recovery time of about a week.<sup>1</sup> </p>
<br />                         
                         <h5>Feel full faster. Stay full longer. Lose the weight long-term.</h5>
<br />
                          <p>Okay, now the big question: What does the LAP-BAND<sup>®</sup> System actually <i>do</i>? It’s really a pretty simple concept. The LAP-BAND<sup>®</sup> System reduces your stomach's capacity, restricting the amount of food you are able to eat at one time.<sup>1</sup>
</p>     
<p>That simply means you feel full faster and stay full longer. It’s an effective way to gradually lose weight and keep it off long-term. </p>             
<br />
                         <h5>How it's done: Minimally invasive. Minimally disruptive.</h5>
<br />
                          <p>The procedure is performed <i>laparoscopically</i>. That basically means that a tiny camera allows the surgeon to see inside your body as he or she performs the procedure.<sup>1</sup> Only a few very small incisions are made in the abdomen (about 1.5-2.5 cm each). The band is placed around your stomach using long, thin instruments. </p>
                          <p>The LAP-BAND<sup>®</sup> System procedure is usually done on an outpatient basis<sup>2</sup> and the surgery itself typically takes less than an hour.<sup>1</sup> When the procedure is performed in an inpatient setting, the hospital stay is generally less than one day.<sup>2</sup> In most cases, you’ll get back to your life in about a week</p>
                          <p>Adjustments to optimize the system are made through a discrete, comfortable access port that is placed just beneath the skin. </p>

<p>Explore the LAP-BAND<sup>®</sup> System device below or view an animation of the LAP-BAND<sup>®</sup> System procedure by  

<a href="//fast.wistia.net/embed/iframe/6ulexduknb?popover=true" class="wistia-popover[height=360,playerColor=7b796a,width=640]"><strong><i>clicking here</i></strong> </a>
<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/popover-v1.js"></script>.
</p>
                          
                          <span>
                          <div id="imageInfoWrapper" style="margin:0 auto">
                          <apex:image styleClass="set1" value="{!URLFOR($Resource.r_lapband_images2, 'images/circle-selector.png')}" width="32" height="32" />
                          <apex:image styleClass="set2" value="{!URLFOR($Resource.r_lapband_images2, 'images/circle-selector.png')}" width="32" height="32" />
                          <apex:image styleClass="set3" value="{!URLFOR($Resource.r_lapband_images2, 'images/circle-selector.png')}" width="32" height="32" />
                          <apex:image styleClass="set4" value="{!URLFOR($Resource.r_lapband_images2, 'images/circle-selector.png')}" width="32" height="32" />
                            <div align="center" id="infoToShow">
                            <a href="">arrow</a>
                            <div id="shownInfo">
                            
                            </div>
                            <div id="infoHidden">
                            <span info="set1">
                            <apex:image value="{!URLFOR($Resource.r_lapband_images1, 'images/info-image1.jpg')}" height="93" width="106" /><br /><br />
                            <p>The LAP-BAND<sup>&reg;</sup> System silicone tubing and band are stable and well tolerated by your body. It&rsquo;s soft yet durable, and will be placed securely for you.</p>
                            </span>
                            <span info="set2">
                            <apex:image value="{!URLFOR($Resource.r_lapband_images1, 'images/info-image2.jpg')}" height="93" width="106" /><br /><br />
                            <p>The OMNIFORM<sup>&reg;</sup> technology offers a larger inflation range (360&deg;) through individual “pillows” around the inside filled with saline solution.</p>
                            
                            </span>
                            <span info="set3">
                            <apex:image value="{!URLFOR($Resource.r_lapband_images1, 'images/info-image3.jpg')}" height="93" width="106" /><br /><br />
                            <p>Your surgeon will make adjustments by adding or removing saline from these little "pillows" in the LAP-BAND<sup>&reg;</sup> System, through the Access Port and tubing. It&rsquo;s easy, quick, and involves minimal pain.</p>
                            </span>
                            <span info="set4">
                            <apex:image value="{!URLFOR($Resource.r_lapband_images1, 'images/info-image4.jpg')}" height="93" width="106" /><br /><br />
                            <p>The Access Port is where your adjustment are made - easily accessible, comfortable, and discreet. It is securely attached beneath your skin, the best position for adjustments.</p>
                            </span>
                            </div>
                            </div>
                            </div>
                          <!-- <apex:image value="{!URLFOR($Resource.r_lapband_images1, 'images/iner11-surgeon.jpg')}" alt="img" /> -->
                          <!-- <img src="images/iner11-surgeon.jpg" alt="img"> -->
                          </span>
<br /><br />
                          <h5>It's Important to go to a LAP-BAND<sup>®</sup> System Specialist.</h5>
<br />
                          <p>Remember, this is your health—and your body—that we’re talking about. You only get one. And since not all weight loss centers are the same, it's very important that you go to a LAP-BAND<sup>®</sup> System Specialist for your surgery.</p>
                          <p>Use the LAP-BAND<sup>®</sup> System Specialist locator or call 1-800-LAPBAND to find a qualified Specialist near you who can provide honest, expert advice and high-quality aftercare</p>
                          <p>*The Bariatric Outcomes Longitudinal Database™ (BOLD™) was developed to help ensure ongoing compliance with the American Society for Metabolic and Bariatric Surgery (ASMBS) Bariatric Surgery Center of Excellence (BSCOE) program and develop general knowledge about optimal bariatric surgery practices. This analysis of the BOLD™ database includes data from 57,918 patients. </p>
                          
                          <p>The American College of Surgeons Bariatric Surgery Center Network (ACS-BSCN) database was developed as part of the ACS-BSCN accreditation program. This analysis of the ACS database includes data from 28,616 patients.</p>
                        
                        
                       </div>
                       
                       <div class="iner11_rgt_part">
                       
                           <c:au_lapband_module_fit />
                            
                           <c:au_lapband_module_sign_up />
                           
                           <c:au_lapband_module_ahead />
                         
                     </div>
                   </div>
                   
                   <c:au_lapband_call />
                   
              </div>
            </div>
                
          
           <!-- END: MAIN CONTENT -->
            
            <!-- SET: FOOTER -->
            <div class="footer">
               <div class="container">
                  <div class="footer_inn">
                      <c:au_lapband_footer_isi />
                      <br />                 
                      <p style="font-family: 'Nunito', sans-serif; font-size: 12px; color: #656565; font-weight: 300;">
                      <strong>References:</strong>
                      <br />1.  The LAP-BAND<sup>®</sup> System Surgical Aid in the Treatment of Obesity – a decision guide for adults. Austin, TX: Apollo Endosurgery, Inc; 2014 GRF-00305-00R01.
                      <br />2.  Hutter Matthew, et al. First Report from the American College of Surgeons Bariatric Surgery Center Network: Laparoscopic Sleeve Gastrectomy has Morbidity and Effectiveness Positioned Between the Band and the Bypass. September 2001.
                      <br />3.  Directions For Use (DFU). LAP-BAND AP<sup>®</sup> Adjustable Gastric Banding System with OMNIFORM<sup>®</sup> Apollo Endosurgery, Inc, Austin, TX. 10/14. www.lapband.com/HCP/directions-for-use<sup>8</sup>
                      </p>
                  </div>
               </div>
               
               <c:au_lapband_footer_nav />
               
            </div>
            
           <!-- <c:au_lapband_footer /> -->

            

            <!-- END: FOOTER -->
            
        </div>
    <!-- END: CONTAINER -->
    
<!-- </div> -->
<!-- END: WRAPPER -->
<script>
$( document ).ready(function() {
    $('#infoToShow').hide();
    $('#imageInfoWrapper img').click(function() {
        var infoToSee = $(this).attr('class');
        var content = $('#infoHidden > span[info="'+infoToSee+'"]').html();
        $(this).siblings('img').hide();
        $('#infoToShow a').removeClass('arrowDown').addClass('arrowUp');
        $('#infoToShow').animate({ height: '250px'},600, function() {
            $('#infoToShow a').removeClass('arrowUp').addClass('arrowDown');
            });
        $('#infoToShow').show();
        $('#shownInfo').html(content);
    });
    $('#imageInfoWrapper a').click(function() {
        $('#imageInfoWrapper img').show();
        $('#infoToShow').animate({ height: '0px'},600, function(){
            $(this).hide();
        });
    });
});
</script>

<c:au_lapband_footer_scripts />

</body>
</html>

</apex:page>
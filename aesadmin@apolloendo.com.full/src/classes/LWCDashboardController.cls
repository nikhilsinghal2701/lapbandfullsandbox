/**
Class: LWCDashboardController
Purpose: Controller for LWCDashboard page
Author – Vinay L
Date – 02/03/2014
*/
public with sharing class LWCDashboardController {
    public List<AWS_S3_Object__c> libDocList {get;set;}
    public Surgeon__c surgeon {get;set;}
    public Id s3ObjId {get;set;}
    public String fieldId {get;set;}
    public List<Office__c> officeList {get;set;}
    
    //Document Library Page
    public Datetime lastUpDt {get;set;}
    public Map<String,String> previewLinkMap{get;set;}
    
    //constractor to initialize the libdoc list contac, surgeon and office
    public LWCDashboardController(){
        libDocList = [SELECT Access__c,Bucket_Name__c,Content_Type__c,File_Name__c,Id,LastActivityDate,LastModifiedById,
                             LastModifiedDate,Library__c,Name, Title__c,Description__c 
                       FROM AWS_S3_Object__c 
                      WHERE Access__c = :LWCConstantDeclaration.PRIVATEACCESS 
                   ORDER BY LastModifiedDate DESC];
        String pg = Apexpages.currentPage().getParameters().get('pg');          
        if(pg != '' && pg.contains(LWCConstantDeclaration.LWCDASHBOARD)){             
            User u = [SELECT ContactId 
                        FROM User 
                        WHERE Id =:UserInfo.getUserId() limit 1];
                        
            Contact c = [SELECT AccountId 
                           FROM Contact 
                          WHERE Id = :u.ContactId limit 1];
                          
            surgeon = [SELECT Id,Website__c, Phone__c, Email__c, Display_Name__c 
                         FROM Surgeon__c 
                        WHERE Surgeon_Account__c = :c.AccountId limit 1];
            
            List<Attachment> att = [SELECT Id 
                         FROM Attachment 
                         WHERE parentId =:surgeon.Id ORDER BY LastModifiedDate DESC limit 1];
                         
            if(att.size() >0){
                fieldId = att[0].Id;
            }
            else	fieldId='nopicture';
            String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
            ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
            officeList = [Select Website__c, Specialist__c, Id, Name From Office__c o WHERE Status__c NOT IN ('Rejected','Deleted') AND Specialist__c = :c.AccountId AND RecordTypeId =:aId AND Approved_Office__c = NULL];
        }
        else if(pg != '' && pg.contains(LWCConstantDeclaration.LWCDOCUMENTLIBRARY)){
            if(libDocList != null && libDocList.size()>0){
               lastUpDt =   libDocList[0].LastModifiedDate;
            }
            List<Id> awsIdList = new List<Id>();
            previewLinkMap = new Map<String,String>();
            for(AWS_S3_Object__c aws :libDocList){
                awsIdList.add(aws.Id);
                previewLinkMap.put(aws.Id,'');//populate the aws Id
            }
            
            for(Attachment att :[SELECT Id,parentId 
                         FROM Attachment 
                         WHERE parentId IN :awsIdList ORDER BY LastModifiedDate]){
                previewLinkMap.put(att.parentId,att.id);
            }
            
             
        }
        
    }
    
    //created downloadlink from AWS_S3_Object__c object
    public PageReference createsDownloadLink() {
        AWS_S3_Object__c s3Obj = [SELECT Access__c,AmazonLink__c,Bucket_Name__c,Content_Type__c,CreatedDate,FileLink__c,File_Name__c,Id,Library__c,Name,OwnerId,Preview_Link__c FROM AWS_S3_Object__c WHERE ID = :s3ObjId][0];
        S3FormController s3OrgCredential = new S3FormController();
        s3OrgCredential.credentials = new AWSKeys(s3OrgCredential.AWSCredentialName);
        
        String signedUrl=null;
        String timeStamp=null;
        if(s3Obj.Access__c=='private'){
             timeStamp = calculateTimeStamp();
             SignedURL = calculateSignature(timeStamp,s3Obj,s3OrgCredential);
             s3Obj.FileLink__c=s3Obj.Amazonlink__c+SignedURL;
         }
         else if(s3Obj.Access__c=='public-read'){
             s3Obj.FileLink__c=s3Obj.Amazonlink__c;
         }
            
         PageReference pageRef = new PageReference(String.valueOf(s3Obj.FileLink__c));
         pageRef.setRedirect(false);
            
         return pageRef;
    
    }
    
    /**
   * Calculates encrypted URL for private files.
   *
   * @param String - time duration for which the files should be accessible.
   * @param AWS_S3_Object__c - File link in sfdc
   * @return String - SignedURL
   */
    public Static String calculateSignature(String timeStamp, AWS_S3_Object__c s3Obj,S3FormController s){
        String FileName;
        String getURL;
        String signedURL;
        if(s3Obj.File_Name__c!=null && s3Obj.Bucket_Name__c!=null){
            FileName=s3Obj.Bucket_Name__c+'/'+s3Obj.File_Name__c;
            if(FileName.containsWhitespace())
            Filename.replaceAll(' ','%20');
            getURL='GET\n\n\n'+timeStamp+'\n/'+Filename;
            getURL='GET\n\n\n'+timeStamp+'\n/'+Filename+'?response-content-disposition=attachment; filename='+s3Obj.File_Name__c;
            String macUrl ;

            String signingKey = EncodingUtil.base64Encode(Blob.valueOf(s.credentials.secret));
            Blob mac = Crypto.generateMac('HMacSHA1',blob.ValueOf(getURL),blob.Valueof(s.credentials.secret));
            macUrl = EncodingUtil.base64Encode(mac);
            macURL=EncodingUtil.urlEncode(macURl, 'UTF-8');
            signedURL='?Signature='+macUrl+'&Expires='+timeStamp+'&AWSAccessKeyId='+s.credentials.key+'&response-content-disposition=attachment; filename='+s3Obj.File_Name__c;

        }

        return signedURL;
    }
    
    //get time stamp
    public static String calculateTimeStamp(){
        Long timeStamp = (System.currentTimeMillis())/1000;
        timeStamp=timeStamp+3600;
        return String.ValueOf(timestamp);
    }
        
}
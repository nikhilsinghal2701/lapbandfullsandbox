/* Created: 12/26/2014
 * Author: Sanjay Sankar
 * Dependency: ScheduleCurrentQuarterUpdate.cls
 * Test Class(es): ScheduleCurrentQuarterUpdateTest.cls
 * Purpose: To update Forecast_Territory__c.Current_Quarter__c to TRUE for records where current date is within the start and end dates
 *          For all other records that are set to TRUE update the field to FALSE
 */

global class BatchUpdateFCTerritoryCurrentQuarter implements Database.Batchable<sObject>{
	private Date cDate = Date.today();
	private Integer currentQtr;
	
	private Integer getQuarter(Date dateValue) {
		if (dateValue.month() == 1 || dateValue.month() == 2 || dateValue.month() == 3) {
			return 1;
		} else if (dateValue.month() == 4 || dateValue.month() == 5 || dateValue.month() == 6) {
			return 2;
		} else if (dateValue.month() == 7 || dateValue.month() == 8 || dateValue.month() == 9) {
			return 3;
		} 
		
		return 4;
	}

	global Database.Querylocator start(Database.BatchableContext bc) {
		System.debug ('Inside BatchUpdateFCTerritoryCurrentQuarter: Method start()...');
		currentQtr = getQuarter(cDate);
		return Database.getQueryLocator('select Id, Start_Date__c, End_Date__c, Current_Quarter__c from Forecast_Territory__c  where (CALENDAR_QUARTER(Start_Date__c) = :currentQtr and CALENDAR_QUARTER(End_Date__c) = :currentQtr and Current_Quarter__c != true) or ((CALENDAR_QUARTER(Start_Date__c) != :currentQtr or CALENDAR_QUARTER(End_Date__c) != :currentQtr) and Current_Quarter__c = true)');
	}
	
	global void execute (Database.Batchablecontext bc, List<Forecast_Territory__c> queryList) {
		System.debug ('Inside BatchUpdateFCTerritoryCurrentQuarter: Method execute()...' + queryList.size() + ' records');
		List<Forecast_Territory__c> fcUpdList = new List<Forecast_Territory__c>();
		for (Forecast_Territory__c fc : queryList) {
			//fc.Current_Quarter__c = getQuarter(fc.Start_Date__c) == currentQtr && getQuarter(fc.End_Date__c) == currentQtr && fc.Start_Date__c.year() == fc.End_Date__c.year();
			fc.Current_Quarter__c = !fc.Current_Quarter__c;
			fcUpdList.add(fc);
		}
		update fcUpdList;
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug ('Inside BatchUpdateFCTerritoryCurrentQuarter: Method finish()...');
	}
}
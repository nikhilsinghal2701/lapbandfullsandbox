/*****************************************************
Class: LWCURLRewriterTest
Test calss for LWCURLRewriter
Author – Vinay L
Date – 03/04/2014
******************************************************/
@isTest
private class LWCURLRewriterTest {
	
	@isTest static void testLWCURLs() {
		
		TestUtil.createLBCURL();
		LWCURLRewriter rewriter = new LWCURLRewriter();

		PageReference p = new PageReference('/contact-us');

		PageReference actualResult = rewriter.mapRequestUrl(p);
		PageReference expectedResult = new PageReference('/LWCContactUs');
		System.assertEquals(String.valueOf(actualResult),String.valueOf(expectedResult));

		p = new PageReference('/');
		expectedResult = new PageReference('/sitelogin');
		actualResult = rewriter.mapRequestUrl(p);
		System.assertEquals(String.valueOf(actualResult),String.valueOf(expectedResult));
		
		p = new PageReference('/document-library/');
		expectedResult = new PageReference('/LWCdocumentlibrary/');
		actualResult = rewriter.mapRequestUrl(p);	
		System.assertEquals(String.valueOf(actualResult),String.valueOf(expectedResult));	
		
		System.assert(rewriter.generateUrlFor(null) == null);
        
	}
	
}
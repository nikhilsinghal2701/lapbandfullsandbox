/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LeadTriggerTest {

    static testMethod void testLeadStatusLog() {
        //create a lead
        Lead lRec = new Lead();
        lRec.FirstName = 'TestFName';
        lRec.LastName = 'TestLName' + DateTime.now();
        lRec.Data_Source__c = 'MDNet';
        lRec.Status = 'TESTLOG';
        lRec.Last_Updated_Date_Time__c = DateTime.newInstanceGmt(2014,08,15,10,15,25);
        
        insert lRec;
 
        Lead_Status_Log__c log1 = new Lead_Status_Log__c();
        log1 = [select Id, Status__c, Status_Date__c from Lead_Status_Log__c where Lead__c = :lRec.Id limit 1];
        
        System.assertEquals(Date.today(), Date.newInstance(log1.Status_Date__c.year(), log1.Status_Date__c.month(), log1.Status_Date__c.day()));
        System.assertEquals('TESTLOG', log1.Status__c);

		Test.startTest();
		lRec.Status = 'TESTUPDATE';
		lRec.Last_Updated_Date_Time__c = DateTime.newInstanceGmt(2014,08,22,11,30,05);
		update lRec;
		Test.stopTest();

        Lead_Status_Log__c log2 = new Lead_Status_Log__c();
        log2 = [select Id, Status__c, Status_Date__c from Lead_Status_Log__c where Lead__c = :lRec.Id][1];

        System.assertEquals(DateTime.newInstanceGmt(2014,08,22,11,30,05), log2.Status_Date__c);
        System.assertEquals('TESTUPDATE', log2.Status__c);
		
    }
}
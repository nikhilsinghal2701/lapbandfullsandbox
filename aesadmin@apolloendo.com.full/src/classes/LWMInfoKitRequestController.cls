public with sharing class LWMInfoKitRequestController {

    


    public String cityValue { get; set; }

    public String address2Value { get; set; }

    public String address1Value { get; set; }

    public String emailValue { get; set; }

    public String lNameValue { get; set; }

    public String fNameValue { get; set; }

    public String inputValue { get; set; }
	
}
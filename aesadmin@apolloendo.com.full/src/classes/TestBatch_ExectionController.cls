/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestBatch_ExectionController {

    static testMethod void IndividualTest() {
    	Pricing_Contract__c PC =TestUtil.createPricingContract(1,true, new Map<String, Object>{'Contract_Start_Date__c' => date.today(),
                                                                        'Contract_End_Date__c' => date.today().addDays(20),
                                                                         'Price_Type__c' => 'AGGREGATED 98%'})[0];
        Account account = TestUtil.createAccounts(1, true, new Map<String, Object>{'Pricing_Contract__c' => PC.Id})[0];
        Sales_Order__c SO =TestUtil.createSalesOrder(1, true,new Map<String, Object>{'Sold_To__c' => account.Id,
                                                                         'Ship_To__c' => account.Id})[0];
        TestUtil.createSalesOrderLineItem(10, true,new Map<String, Object>{'Sales_Order__c' => SO.Id,
        																	'Quantity__c' => 3,
        																	'Material__c' => 'B-2245',
                                                                         'Bill_Date__c' => date.today()});                                                                                                                                 
        Test.StartTest();
        Batch_ExectionController.executeBatchForPricingContract(PC.id);
        Test.StopTest();
    }
    static testMethod void CombineTest() {
    	Pricing_Contract__c PC =TestUtil.createPricingContract(1,true, new Map<String, Object>{'Contract_Start_Date__c' => date.today(),
                                                                        'Contract_End_Date__c' => date.today().addDays(20),
                                                                         'Price_Type__c' => 'AGGREGATED 98%'})[0];
        Account account = TestUtil.createAccounts(1, true, new Map<String, Object>{'Pricing_Contract__c' => PC.Id})[0];
         Sales_Order__c SO =TestUtil.createSalesOrder(1, true,new Map<String, Object>{'Sold_To__c' => account.Id,
                                                                         'Ship_To__c' => account.Id})[0];
         Sales_Order__c SOD =TestUtil.createSalesOrder(1, true,new Map<String, Object>{'Sold_To__c' => account.Id,
                                                                         'Ship_To__c' => account.Id})[0];                                                                
        TestUtil.createSalesOrderLineItem(10, true,new Map<String, Object>{'Sales_Order__c' => SO.Id,
        																	'Quantity__c' => 3,
        																	'Material__c' => 'B-2245',
                                                                         'Bill_Date__c' => date.today()});                                                                  
        Test.StartTest();
        PricingContractAccountBatch PCAB = new PricingContractAccountBatch();
        ID batchprocessid = Database.executeBatch(PCAB);
        Test.StopTest();
    }
      static testMethod void CombineTestHighest() {
    	Pricing_Contract__c PC =TestUtil.createPricingContract(1,true, new Map<String, Object>{'Contract_Start_Date__c' => date.today(),
                                                                        'Contract_End_Date__c' => date.today().addDays(20),
                                                                         'Price_Type__c' => 'HIGHEST USER 98%'})[0];
        Account account = TestUtil.createAccounts(1, true, new Map<String, Object>{'Pricing_Contract__c' => PC.Id})[0];
         Sales_Order__c SO =TestUtil.createSalesOrder(1, true,new Map<String, Object>{'Sold_To__c' => account.Id,
                                                                         'Ship_To__c' => account.Id})[0];
        TestUtil.createSalesOrderLineItem(10, true,new Map<String, Object>{'Sales_Order__c' => SO.Id,
        																	'Quantity__c' => 3,
        																	'Material__c' => 'B-2245',
                                                                         'Bill_Date__c' => date.today()});                                                                  
        Test.StartTest();
        PricingContractAccountBatch PCAB = new PricingContractAccountBatch();
        ID batchprocessid = Database.executeBatch(PCAB);
        Test.StopTest();
    }
     static testMethod void Sctest() {
    	Pricing_Contract__c PC =TestUtil.createPricingContract(1,true, new Map<String, Object>{'Contract_Start_Date__c' => date.today(),
                                                                        'Contract_End_Date__c' => date.today().addDays(20),
                                                                         'Price_Type__c' => 'HIGHEST USER 98%'})[0];
        Account account = TestUtil.createAccounts(1, true, new Map<String, Object>{'Pricing_Contract__c' => PC.Id})[0];
         Sales_Order__c SO =TestUtil.createSalesOrder(1, true,new Map<String, Object>{'Sold_To__c' => account.Id,
                                                                         'Ship_To__c' => account.Id})[0];
        TestUtil.createSalesOrderLineItem(10, true,new Map<String, Object>{'Sales_Order__c' => SO.Id,
        																	'Quantity__c' => 3,
        																	'Material__c' => 'B-2245',
                                                                         'Bill_Date__c' => date.today()});  
          Test.startTest();                                                                                                                               
         // Schedule the test job

        ScheduledPCABatch m = new ScheduledPCABatch();
        String sch = '0 0 1 * * ?';
        String jobID = system.schedule('Merge Job', sch, m);

      // Get the information from the CronTrigger API object
      CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = : jobID ];

      // Verify the expressions are the same
      System.assertEquals(sch, 
         ct.CronExpression);

      // Verify the job has not run
      System.assertEquals(0, ct.TimesTriggered);

     
        Test.stopTest();

    }
}
/**
Class: LWCHospitalTest
Test Class for Hospital
Author – Wade Liu
Date – 03/06/2014
 */
@isTest
private class LWCHospitalTest {

    static testMethod void hospitalTest() {
        // TO DO: implement unit test
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        List<Account> lstAccounts = TestUtil.createAccounts(1, true, new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon});
        if(lstAccounts != null && lstAccounts.size() > 0){
        	Id idAccount = lstAccounts[0].Id;
        	List<Surgeon__c> lstSurgeons = TestUtil.createSurgeons(1, true, new Map<String,Object>{'Surgeon_Account__c'=>idAccount});
        	if(lstSurgeons != null && lstSurgeons.size() > 0){
        		List<Surgeon_Affiliation__c> lstSurgeonsAffiliations = TestUtil.createSurgeonAffiliations(1, true, new Map<String,Object>{'Surgeon_Name__c'=>lstSurgeons[0].Id,'Surgeon_Account__c'=>idAccount});
        		if(lstSurgeonsAffiliations != null && lstSurgeonsAffiliations.size() > 0){
        			LWCHospital ohospital = new LWCHospital(lstSurgeonsAffiliations[0]);
		        	system.assert(!ohospital.remove);
		        	return;
        		}
        	}  		
        }
        system.assert(false);
    }
}
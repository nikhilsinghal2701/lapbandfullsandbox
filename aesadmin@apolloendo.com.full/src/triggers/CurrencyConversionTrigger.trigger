/*****************************************************
Trigger: CurrencyConversionTrigger 
This trigger helps to Update On Sales Order Line Item
Conversion_Rate
Integration-LineAmount
Author – Wade Liu
Date – 03/17/2014
Revision History
******************************************************/
trigger CurrencyConversionTrigger on Currency_Conversion__c (after update) {
	// Code to fire on the update of Currency_Conversion__c record
    if( trigger.isUpdate ){
        if( trigger.isAfter ){
            // Code to fire on after update
            CurrencyConversionTriggerHandler.handlerOnUpdate(Trigger.new, Trigger.oldMap);
        }
    }
}
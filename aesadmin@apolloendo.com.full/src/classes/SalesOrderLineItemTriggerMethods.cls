public with sharing class SalesOrderLineItemTriggerMethods {

	public static void upsertForecast(List<Sales_Order_Line_Item__c> lineListNew, List<Sales_Order_Line_Item__c> lineListOld) {
		Set<Id> acctIdSet = new Set<Id>();
		Map<String, Set<String>> dateAcctIdMap = new Map<String, Set<String>>();
		List<Date> billDateList = new List<Date>();

		//build list of Bill Date and Account IDs for forecastable line items
		for (Sales_Order_Line_Item__c lineRec : lineListNew) {
			if (lineRec.IsForecastable__c) {
				acctIdSet.add(lineRec.Ship_To_Account_ID__c);
				billDateList.add(lineRec.Bill_Date__c);

				String key = String.valueOf(lineRec.Bill_Date__c.month()) + '-' + String.valueOf(lineRec.Bill_Date__c.year());
				if (dateAcctIdMap.containsKey(key)) {
					dateAcctIdMap.get(key).add(lineRec.Ship_To_Account_ID__c);
				} else {
					Set<String> tempSet = new Set<String>();
					tempSet.add(lineRec.Ship_To_Account_ID__c);
					dateAcctIdMap.put(key, tempSet);
				}
			}
		}

		//query existing forecast records for the line items' accounts for the Bill Dates' date range 
		if (billDateList.size() > 0) {
			billDateList.sort();
			List<Forecast__c> fcList = new List<Forecast__c>();
			fcList = [select Id, Start_Date__c, End_Date__c, Attainment_Amount__c, Account__c 
						from Forecast__c 
						where Account__c in :acctIdSet
							and Start_Date__c >= :billDateList[0].toStartOfMonth()
							and End_Date__c <= :billDateList[billDateList.size() - 1].addMonths(1).toStartOfMonth().addDays(-1)];
	
			
			Map<String, Set<String>> dateFCAcctIdMap = new Map<String, Set<String>>();
			if (fcList.size() > 0) {
				for (Forecast__c fcRec : fcList) {
					String key = String.valueOf(fcRec.Start_Date__c.month()) + '-' + String.valueOf(fcRec.Start_Date__c.year());
					if (dateFCAcctIdMap.containsKey(key)) {
						dateFCAcctIdMap.get(key).add(fcRec.Account__c);
					} else {
						Set<String> tempSet = new Set<String>();
						tempSet.add(fcRec.Account__c);
						dateFCAcctIdMap.put(key, tempSet);
					}
				}
			}
			
			//identify accounts for which Forecast needs to be inserted
			Map<String, Set<String>> insertMap = new Map<String, Set<String>>();
			for (String calMonth : dateAcctIdMap.keySet()) {
				Set<String> lineSet = new Set<String>();
				lineSet = dateAcctIdMap.get(calMonth);
				if (dateFCAcctIdMap.size() > 0 && dateFCAcctIdMap.containsKey(calMonth)) {
					Set<String> fcSet = new Set<String>();
					if (dateFCAcctIdMap.containsKey(calMonth)) {
						fcSet = dateFCAcctIdMap.get(calMonth);
					}
					
					//Remove accounts from the main line item list that already have forecast record
					if (fcSet.size() > 0) {
						Boolean b = lineSet.removeAll(fcSet);
					}
				}
				
				if (lineSet.size() > 0) {
					insertMap.put(calMonth, lineSet);
				}
			}

			//build new forecast records that need to be inserted
			List<Forecast__c> fcInsertList = new List<Forecast__c>();
			if (insertMap.size() > 0) {
				for (String calMonth : insertMap.keySet()) {
					for (String acctId : insertMap.get(calMonth)) {
						Forecast__c fcInsertRec = new Forecast__c();
						fcInsertRec.Account__c = acctId;
						fcInsertRec.Start_Date__c = Date.newInstance(Integer.valueOf(calMonth.substring(calMonth.indexOf('-') + 1)), Integer.valueOf(calMonth.substring(0,calMonth.indexOf('-'))), 1);
						fcInsertRec.End_Date__c = fcInsertRec.Start_Date__c.addMonths(1).toStartofMonth().addDays(-1);
						fcInsertRec.Health_Forecast__c = 0;
						fcInsertRec.RP_Forecast__c = 0;
						fcInsertRec.ES_Forecast__c = 0;
						fcInsertRec.Current_Quarter__c = (Date.today().month() == fcInsertRec.Start_Date__c.month() && Date.today().year() == fcInsertRec.Start_Date__c.year()) ? true:false;
						
						fcInsertList.add(fcInsertRec);
					}
				}
			}
			
			//add the existing forecast records to the forecast list for doing an upsert
			if (fcList.size() > 0) fcInsertList.addAll(fcList);
			if (fcInsertList.size() > 0) upsert fcInsertList;
		}
		
	}

	public static void upsertForecastTerritory(List<Sales_Order_Line_Item__c> lineListNew, List<Sales_Order_Line_Item__c> lineListOld) {
		//Set<Id> acctIdSet = new Set<Id>();
		Set<Id> terrIdSet = new Set<Id>();
		Map<String, Set<String>> dateTerrIdMap = new Map<String, Set<String>>();
		List<Date> billDateList = new List<Date>();

		//build list of Bill Date and Territory IDs for forecastable line items
		for (Sales_Order_Line_Item__c lineRec : lineListNew) {
			if (lineRec.Is_OUS_Forecastable__c && lineRec.Sales_Territory__c != null) {
				terrIdSet.add(lineRec.Sales_Territory__c);
				billDateList.add(lineRec.Bill_Date__c);

				String key = String.valueOf(lineRec.Bill_Date__c.month()) + '-' + String.valueOf(lineRec.Bill_Date__c.year());
				if (dateTerrIdMap.containsKey(key)) {
					dateTerrIdMap.get(key).add(lineRec.Sales_Territory__c);
				} else {
					Set<String> tempSet = new Set<String>();
					tempSet.add(lineRec.Sales_Territory__c);
					dateTerrIdMap.put(key, tempSet);
				}
			}
		}

		//query existing forecast records for the line items' territories for the Bill Dates' date range 
		if (billDateList.size() > 0) {
			billDateList.sort();
			List<Forecast_Territory__c> fcList = new List<Forecast_Territory__c>();
			fcList = [select Id, Start_Date__c, End_Date__c, Attainment_Amount__c, Sales_Territory__c 
						from Forecast_Territory__c 
						where Sales_Territory__c in :terrIdSet
							and Start_Date__c >= :billDateList[0].toStartOfMonth()
							and End_Date__c <= :billDateList[billDateList.size() - 1].addMonths(1).toStartOfMonth().addDays(-1)];
			
			Map<String, Set<String>> dateFCTerrIdMap = new Map<String, Set<String>>();
			if (fcList.size() > 0) {
				for (Forecast_Territory__c fcRec : fcList) {
					String key = String.valueOf(fcRec.Start_Date__c.month()) + '-' + String.valueOf(fcRec.Start_Date__c.year());
					if (dateFCTerrIdMap.containsKey(key)) {
						dateFCTerrIdMap.get(key).add(fcRec.Sales_Territory__c);
					} else {
						Set<String> tempSet = new Set<String>();
						tempSet.add(fcRec.Sales_Territory__c);
						dateFCTerrIdMap.put(key, tempSet);
					}
				}
			}
			
			//identify territories for which Forecast needs to be inserted
			Map<String, Set<String>> insertMap = new Map<String, Set<String>>();
			for (String calMonth : dateTerrIdMap.keySet()) {
				Set<String> lineSet = new Set<String>();
				lineSet = dateTerrIdMap.get(calMonth);
				if (dateFCTerrIdMap.size() > 0 && dateFCTerrIdMap.containsKey(calMonth)) {
					Set<String> fcSet = new Set<String>();
					if (dateFCTerrIdMap.containsKey(calMonth)) {
						fcSet = dateFCTerrIdMap.get(calMonth);
					}
					
					//Remove territories from the main line item list that already have forecast record
					if (fcSet.size() > 0) {
						Boolean b = lineSet.removeAll(fcSet);
					}
				}
				
				if (lineSet.size() > 0) {
					insertMap.put(calMonth, lineSet);
				}
			}

			//build new forecast records that need to be inserted
			List<Forecast_Territory__c> fcInsertList = new List<Forecast_Territory__c>();
			if (insertMap.size() > 0) {
				for (String calMonth : insertMap.keySet()) {
					for (String terrId : insertMap.get(calMonth)) {
						Forecast_Territory__c fcInsertRec = new Forecast_Territory__c();
						fcInsertRec.Sales_Territory__c = terrId;
						fcInsertRec.Start_Date__c = Date.newInstance(Integer.valueOf(calMonth.substring(calMonth.indexOf('-') + 1)), Integer.valueOf(calMonth.substring(0,calMonth.indexOf('-'))), 1);
						fcInsertRec.End_Date__c = fcInsertRec.Start_Date__c.addMonths(1).toStartofMonth().addDays(-1);
						fcInsertRec.Forecast__c = 0;
						fcInsertRec.Forecast_Local_Currency__c = 0;
						/*** TODO fcInsertRec.ISO_Currency__c = ''; **/
						fcInsertRec.Current_Quarter__c = (Date.today().month() == fcInsertRec.Start_Date__c.month() && Date.today().year() == fcInsertRec.Start_Date__c.year()) ? true:false;
						
						fcInsertList.add(fcInsertRec);
					}
				}
			}
			
			//add the existing forecast records to the forecast list for doing an upsert
			if (fcList.size() > 0) fcInsertList.addAll(fcList);
			if (fcInsertList.size() > 0) upsert fcInsertList;
		}
		
	}
	
	public static void setLineValues(List<Sales_Order_Line_Item__c> lineListNew, Boolean isInsert) {
		if (isInsert) {
			
			/*** Line Number ***/
			Set<Id> salesOrderIdSet = new Set<Id>();
			for (Sales_Order_Line_Item__c lineRec : lineListNew) {
				if (lineRec.Data_Source__c != null && lineRec.Data_Source__c.indexOf('UPS') != -1) {
					salesOrderIdSet.add (lineRec.Sales_Order__c);
				
					Decimal convRate = lineRec.Conversion_Rate__c;
				
					if (convRate > 0) {
						if (lineRec.UPS_Unit_Price__c != null) lineRec.UPS_Unit_Price__c = Math.floor((lineRec.UPS_Unit_Price__c/convRate) * 100) / 100;
						if (lineRec.UPS_Freight__c != null) lineRec.UPS_Freight__c = Math.floor((lineRec.UPS_Freight__c/convRate) * 100) / 100;
						if (lineRec.UPS_Misc_Charges__c != null) lineRec.UPS_Misc_Charges__c = Math.floor((lineRec.UPS_Misc_Charges__c/convRate) * 100) / 100;
						if (lineRec.UPS_Invoice_Total__c != null) lineRec.UPS_Invoice_Total__c = Math.floor((lineRec.UPS_Invoice_Total__c/convRate) * 100) / 100;
						if (lineRec.Integration_Tax__c != null) lineRec.Integration_Tax__c = String.valueOf(Math.floor((Decimal.valueOf(lineRec.Integration_Tax__c.trim())/convRate) * 100) / 100);
					}
				}
			}
			
			Map<Id, Sales_Order__c> salesOrderMap = new Map<Id, Sales_Order__c>([select Id, Number_Of_Items__c from Sales_Order__c where Id in :salesOrderIdSet]);
			
			for (Sales_Order_Line_Item__c lineRec : lineListNew) {
				if (lineRec.Data_Source__c != null && lineRec.Data_Source__c.indexOf('UPS') != -1) {
					lineRec.LineItemID__c = lineRec.LineItemID__c + String.valueOf(salesOrderMap.get(lineRec.Sales_Order__c).Number_Of_Items__c);
					lineRec.Name = lineRec.Name + ' - ' + String.valueOf(salesOrderMap.get(lineRec.Sales_Order__c).Number_Of_Items__c);
				}
			}
			
		}
	}

}
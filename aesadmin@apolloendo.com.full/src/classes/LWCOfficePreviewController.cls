/**
Class: LWCOfficePreviewController
Controller used for LWCOfficePreview component
Author – Wade Liu
Date – 02/28/2014
 */
public with sharing class LWCOfficePreviewController {
	public string accountID	{set; get;}
	public ID surgeonId	{set;get;}
	public List<Office__c> offices	{set;}
	
	public Boolean getPendingFlag(){
		String pOffice = LWCConstantDeclaration.PENDINGOFFICERECORD;
    	ID pId = LWCWithoutSharing.getRecordTypeId(new List<String>{pOffice})[0].Id;
		Integer num = [SELECT COUNT() FROM Office__c
						WHERE Status__c NOT IN ('Rejected','Deleted') AND Specialist__c =:accountId
						AND RecordTypeId =:pId];
		if(num>0)	return true;
		else	return false;
	}
	
	//get surgeon list
	public Surgeon__c getSurgeon(){
		Surgeon__c surgeon = [SELECT Id,Website__c, Phone__c, Email__c, Display_Name__c,
                          Surgeon_First_Name__c,Name,Show_Email__c,Gender__c,
                          Live_Seminars__c,Online_Seminars__c,American_Board_of_Medical_Specialties__c,
                          American_College_of_Surgeons_ACS__c,Amr_Society_of_Metabolic_Bariatric__c,
                          Provides_Financing_Options__c, Outpatient_with_LAP_BAND_System_Surgery__c,
                          Accepts_out_of_town_patients_OOTN__c,Bariatrics__c,TOTAL_CARE__c,
                          Frequency_of_LAP_BAND_System_Surgeries__c,Years_of_LAP_BAND_System_Experience__c
                          FROM Surgeon__c 
                          WHERE Surgeon_Account__c = :accountId limit 1];
        surgeonId = surgeon.Id;
		return surgeon;
	}
	
	//get office list, all approve office or master office new submitted
	public List<Office__c> getOffices(){
		String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	String pOffice = LWCConstantDeclaration.PENDINGOFFICERECORD;
    	LIST<RecordType> recordTypeIds = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice,pOffice});
    	Map<String,ID> recordTypeMap = new Map<String,ID>();
    	for(RecordType r : recordTypeIds){
    		recordTypeMap.put(r.Name, r.Id);
    	}
    	ID aId = recordTypeMap.get(aOffice);
    	ID pId = recordTypeMap.get(pOffice);
		List<Office__c> offices = new List<Office__c>();
		offices = [Select Id,Address_2__c, Address_1__c,Website__c, Email__c,Fax__c,Wheelchair_Access__c, 
                          Location__Latitude__s,Location__Longitude__s,State__c,Zip_Code__c,Specialist__r.Name,Name,
                          English__c,Spanish__c,Chinese__c,Japanese__c,French__c,German__c,Vietnamese__c,Italian__c,
                          Korean__c,Russian__c, Phone__c, Specialist__c,Online_Seminars_Link__c,Live_Seminars_Link__c,
                          Office_Hours__c, MapLink__c, RecordTypeId, City__c, Ext__c, Bariatric_Surgery_Center__c,Approved_Office__c,
                          Is_Edit_Flag__c,Status__c, (Select Id, Name From Acceditations__r)
                          From Office__c
                          WHERE Status__c NOT IN ('Rejected','Deleted') AND Specialist__c =:accountId
                          AND ((RecordTypeId =:aId AND Is_Edit_Flag__c= false AND Approved_Office__c = NULL)
                          OR (RecordTypeId =:pId AND Status__c = :LWCConstantDeclaration.SUBMITTED))];
		return offices;
	}
	
	//get membership list under account and office
	public List<Membership_Accreditation__c> getMembers(){
		List<Membership_Accreditation__c> members = new List<Membership_Accreditation__c>();
		members = [SELECT Id,Name,Specialist__c,Office__c,Office__r.Approved_Office__c 
					FROM Membership_Accreditation__c 
					WHERE Membership_Accreditation__c.Specialist__c = :accountId 
					OR Membership_Accreditation__c.Office__c IN:getOffices()];
		return members;
	}
	
	//get hospital list under account
	public List<Surgeon_Affiliation__c> getHospitals(){
		List<Surgeon_Affiliation__c> hospitals = new List<Surgeon_Affiliation__c>();
		hospitals = [SELECT Id,Name, AccountText__c, AccountName_Formula__c 
					FROM Surgeon_Affiliation__c 
					WHERE Surgeon_Account__c = :accountId AND Account__c = null];
		return hospitals;
	}
	
	//get attachment, surgeon photo
	public String getFieldId(){
		String fieldId;
        List<Attachment> att = [SELECT Id 
                         FROM Attachment 
                         WHERE parentId =:surgeonId ORDER BY LastModifiedDate DESC limit 1];
                         
        if(att.size() >0){
            fieldId = att[0].Id;
        }
        else	fieldId = 'nopicture';
        return fieldId;
	}
	
	public static void LWCOfficePreviewController(){
	}
}
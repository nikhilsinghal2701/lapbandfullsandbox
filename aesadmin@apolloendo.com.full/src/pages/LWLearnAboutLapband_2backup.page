<apex:page applyBodyTag="false" showheader="false" standardStylesheets="false" sidebar="false">
 <!-- <apex:composition template="LWTemplate"/> -->
 <head>
        <title>How the LAP-BAND&#174; SYSTEM Device Works</title>
 </head>
 <c:LWTemplate />
 
 <body>
 <div id="container">
    <c:LWHeader />
     <script type="text/javascript">
        $(document).ready(function () {
            $('#how-lapband-works-diagram span').mouseover(function () {
                var diagramLinkId = $(this).attr('id'),
                textId = diagramLinkId.substr(diagramLinkId.length - 2);
                $(this).parent().removeClass();
                $(this).parent().addClass(diagramLinkId);
                $('.deviceText.current').hide().removeClass('current');
                $('.deviceText#how-lapband-works-text' + textId).show().addClass('current');
            });
            $('#bmi-calculator-link').click(function () {
                $('#lapband-is-for-you-bmi-calculator').slideDown();
            });
        });
    </script>
    <div id="content" class="inner-page">
        <div id="inner-page-content">
            <ul id="breadcrumbs">
                <li>
                    <apex:outputLink value="{!URLFOR($Page.LWLearnAboutLapband,null,null)}" styleClass="active">How LAP-BAND<sup>&#174;</sup> SYSTEM Works</apex:outputLink>
                    <!--<a href="lapband_LearnAboutLapband" class="active">How LAP-BAND<sup>&#174;</sup> Works</a>-->
                </li>
                <li>|</li>
                <li>
                    <apex:outputLink value="{!URLFOR($Page.LWLosingWeight,null,null)}">Losing Weight With LAP-BAND<sup>&#174;</sup> SYSTEM</apex:outputLink>
                    <!--<a href="lapband_LosingWeight">Losing Weight With LAP-BAND<sup>&#174;</sup></a>-->
                </li>
                <li>|</li>
                <li>
                    <apex:outputLink value="{!URLFOR($Page.LWIsForYou,null,null)}">Is LAP-BAND<sup>&#174;</sup> SYSTEM Right for You?</apex:outputLink>
                    <!--<a href="lapband_IsForYou">Is LAP-BAND<sup>&#174;</sup> Right for You?</a>-->
                </li>
                <li>|</li>
                <li>
                    <apex:outputLink value="{!URLFOR($Page.LWAffordingLapband,null,null)}">Affording LAP-BAND<sup>&#174;</sup> SYSTEM</apex:outputLink>
                    <!--<a href="lapband_AffordingLapband">Affording LAP-BAND<sup>&#174;</sup></a>-->
                </li>
            </ul>
            <h1 class="learn-about-lapband">How the LAP-BAND&#174; SYSTEM Device Works</h1>
            <div id="content-left">
                <h2 class="learn-about-lapband">Less invasive. Faster recovery.</h2>
                <p>Gastric banding is a minimally invasive weight loss procedure. Unlike sleeve gastrectomy and gastric bypass, the LAP-BAND<sup>&#174;</sup> Adjustable Gastric Banding System involves no use of metal surgical staples, no amputation of any part of the stomach, nor any cutting of the intestines.<sup>1,2</sup></p>
                <p>Data from the Bariatric Outcomes Longitudinal Database&#8482; showed that sleeve gastrectomy had a 3x greater rate of total complications in the first year<sup>4*</sup> when compared to gastric banding. Additionally, data from the American College of Surgeons database within the first 30 days showed<sup>3&#134;</sup>:</p>
                <ul class="learn-about-lapband">
                    <h4>3X</h4>
                    <li id="learn-about-lapband_li_01">3x greater chance of readmission to the hospital</li>
                    <li id="learn-about-lapband_li_02">3x greater chance of reoperation</li>
                    <li id="learn-about-lapband_li_03">3x the length of hospital stay following the procedure</li>
                </ul>
                <p>In addition to a lower rate of complications in recent studies, gastric banding also offers a generally faster recovery time&#8212;it's typically performed as an outpatient procedure and you can usually get back to your normal activity in about a week's time.<sup>2</sup></p>
                <h3>A breakthrough in lasting, healthy weight loss.</h3>
                <p>The LAP-BAND<sup>&#174;</sup> System reduces your stomach's capacity, restricting the amount of food you are able to eat at one time.<sup>2</sup></p>
                <p>You also feel full faster and stay full longer. The revolutionary, yet simple technology is a less invasive bariatric option than either sleeve gastrectomy or gastric bypass<sup>3,4,5</sup> to help you gradually lose weight and keep it off. So you can live a healthier life.</p>
                <h3>How it's done.</h3>
                <p>The LAP-BAND<sup>&#174;</sup> System procedure is performed laparoscopically. This means a few very small incisions are made in the abdomen (about 1.5-2.5 cm each). The surgeon places the band around your stomach using long, thin instruments. A small camera allows the surgeon to see inside your body as he or she performs the procedure.<sup>2</sup></p>
                <p>Unlike sleeve gastrectomy or gastric bypass, which usually requires a stay of up to three days in the hospital, the LAP-BAND<sup>&#174;</sup> System procedure is often done on an outpatient basis<sup>3</sup> and the surgery itself typically takes less than an hour.<sup>2</sup> When the procedure is performed in an inpatient setting, the hospital stay is generally less than one day.<sup>3</sup></p>
                <p>You can explore the LAP-BAND<sup>&#174;</sup> System device below or <a id="view-animation" class="compare-lapband-video lapband-animation" href="javascript:void(0);">view an animation of the LAP-BAND<sup>&#174;</sup> System procedure</a>.</p>
                <div id="learn-about-lapband-featured-content">
                    <div id="how-lapband-works-diagram">
                        <span id="how-lapband-works-diagram-link02"></span>
                        <span id="how-lapband-works-diagram-link03"></span>
                        <span id="how-lapband-works-diagram-link04"></span>
                        <span id="how-lapband-works-diagram-link05"></span>
                    </div>
                    <div class="deviceDesc">
                        <div class="deviceText current ir" id="how-lapband-works-text01">
                            <p>Roll over the highlighted areas to learn more about the <span class="product nowrap">LAP-BAND AP<sup>&#174;</sup></span> System.</p>
                        </div>
                        <div class="deviceText ir" id="how-lapband-works-text02">
                            <div class="learn-about-lapband-featured-content-img"></div>
                            <p>Your surgeon will make adjustments by adding or removing saline from these little 'pillows' in the LAP-BAND<sup>&#174;</sup>, through the Access Port and tubing. It's easy, quick and with minimal pain.</p>
                        </div>
                        <div class="deviceText ir" id="how-lapband-works-text03">
                            <div class="learn-about-lapband-featured-content-img"></div>
                            <p>The OMNI FORM<sup>&#174;</sup> technology offers you a larger inflation range (360º) through individual 'pillows' around the inside. As saline is added or taken out, the pillows 'adjust' your LAP-BAND<sup>&#174;</sup>.</p>
                        </div>
                        <div class="deviceText ir" id="how-lapband-works-text04">
                            <div class="learn-about-lapband-featured-content-img"></div>
                            <p>The <span class="product">LAP-BAND<sup>&#174;</sup></span> System silicone tubing and band are stable and well-tolerated by your body. It's soft yet durable, and will be placed securely for you.</p>
                        </div>
                        <div class="deviceText ir" id="how-lapband-works-text05">
                            <div class="learn-about-lapband-featured-content-img"></div>
                            <p>The Access Port is where your adjustments are made &#8212; easily accessible, comfortable and discreet. It is securely attached beneath your skin, in the best position for adjustments.</p>
                        </div>
                    </div>
                </div>
                <h3>Why it's Important to go to a LAP-BAND<sup>&#174;</sup> Specialist</h3>
                <p>Not all weight loss centers are the same. It's important that you go to a LAP-BAND<sup>&#174;</sup> System specialist for your surgery. <apex:outputLink value="{!URLFOR($Page.LWFindASpecialist,null,null)}">&nbsp;Use the LAP-BAND<sup>&#174;</sup> System specialist locator</apex:outputLink><!--<a href="../find-a-specialist/index.html">Use the LAP-BAND<sup>&#174;</sup> System specialist locator</a>--> to find a qualified specialist near you.</p>
                <p class="reference">*The Bariatric Outcomes Longitudinal Database&#8482; (BOLD&#8482;) was developed to help ensure ongoing compliance with the American Society for Metabolic and Bariatric Surgery (ASMBS) Bariatric Surgery Center of Excellence (BSCOE) program and develop general knowledge about optimal bariatric surgery practices. This analysis of the BOLD&#8482; database includes data from 57,918 patients.</p>
                <p class="reference"><sup>&#134;</sup>The American College of Surgeons Bariatric Surgery Center Network (ACS-BSCN) database was developed as part of the ACS-BSCN accreditation program. This analysis of the ACS database includes data from 28,616 patients.
                </p>
            </div>
            <!-- Start of video play -->
             <div id="lapband-animation-video-lightbox" class="video-lightbox"></div>
            <c:LWVideoIncludeFile />
            <c:LWLearnLapbandRighTrail />
        </div>
    </div>
    <c:LWMapOnFoot />
    <c:LWFooter />
         <div id="footer" style="background:none !important;">
        <div class="footer-references">
                <h5>References:</h5>
                <ol>
                  <li>Needleman Bradley, Happel Lynn. Bariatric Surgery: Choosing the Optimal Procedure. <em>Surgical Clinics of North America.</em> 2008.</li>
                  <li>The LAP-BAND<sup>&#174;</sup> System Surgical Aid in the Treatment of Obesity (PSI). Allergan, Inc. Irvine, CA. 02/11.</li>
                  <li>Hutter Matthew, et al. First Report from the American College of Surgeons Bariatric Surgery Center Network: <em>Laparoscopic Sleeve Gastrectomy has Morbidity and Effectiveness Positioned Between the Band and the Bypass</em>. September 2001.</li>
                  <li>DeMaria Eric, Pate Virginia, Warthen Michael, Winegar Deborah. Baseline data from American Society for Metabolic and Bariatric Surgery-designated Bariatric Surgery Centers of Excellence using the Bariatric Outcomes Longitudinal Database. <em>Surgery for Obesity and Related Diseases 6.</em> 2010.</li>
                  <li>The Longitudinal Assessment of Bariatric Surgery (LABS) Consortium. Perioperative Safety in the Longitudinal Assessment of Bariatric Surgery. <em>The New England Journal of Medicine Vol. 361 No. 5.</em> July 30, 2009.</li>
                  <li>Directions For Use (DFU). LAP-BAND AP<sup>&#174;</sup> Adjustable Gastric Banding System with OMNIFORM<sup>&#174;</sup> Design. Allergan, Inc. Irvine, CA. 02/11.</li>
                </ol>
            </div>
        </div>
         <c:LWFooter2 />
 </div>
</body>

</apex:page>
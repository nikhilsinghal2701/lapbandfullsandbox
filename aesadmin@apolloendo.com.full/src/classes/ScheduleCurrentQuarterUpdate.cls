/* Created: 05/12/2014
 * Author: Sanjay Sankar
 * Depends on: BatchUpdateCurrentQuarter.cls
 * Test Class(es): ScheduleCurrentQuarterUpdateTest.cls
 * Purpose: To update Forecast.Current_Quarter__c to TRUE for records where current date is within the start and end dates
 *          For all other records that are set to TRUE update the field to FALSE
 */

global with sharing class ScheduleCurrentQuarterUpdate implements Schedulable {
	global void execute (SchedulableContext sc) {
		BatchUpdateCurrentQuarter updFC = new BatchUpdateCurrentQuarter();
		Database.executeBatch(updFC);
		BatchUpdateFCTerritoryCurrentQuarter updFCTerr = new BatchUpdateFCTerritoryCurrentQuarter();
		Database.executeBatch(updFCTerr);
	}
}
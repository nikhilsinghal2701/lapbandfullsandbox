global without sharing class SalesReportStageUtility {

	//private static final Date CONST_TODAY = Date.today().addDays(Integer.valueOf(Label.Sales_Date_Offset));
	
	private static Date reportEndDate;
	
	//private static Date beginPY = Date.newInstance(Date.today().addYears(-1).year(), 1, 1);
	//private static Date endCY = Date.newInstance(Date.today().year(), 12, 31);
	private static Date beginPY;
	private static Date endCY;

	/* Health */
	private static Map<Id, Decimal> cmFCMap = new Map<Id, Decimal>();
	private static Map<Id, Decimal> cqFCMap = new Map<Id, Decimal>();
	//private static Map<Id, Decimal> cyFCMap = new Map<Id, Decimal>();
	private static Map<Id, Decimal> pycmFCMap = new Map<Id, Decimal>();
	private static Map<Id, Decimal> pycqFCMap = new Map<Id, Decimal>();
	//private static Map<Id, Decimal> pyFCMap = new Map<Id, Decimal>();

	/* RP */
	private static Map<Id, Decimal> cmFCMapRP = new Map<Id, Decimal>();
	private static Map<Id, Decimal> cqFCMapRP = new Map<Id, Decimal>();
	//private static Map<Id, Decimal> cyFCMapRP = new Map<Id, Decimal>();
	private static Map<Id, Decimal> pycmFCMapRP = new Map<Id, Decimal>();
	private static Map<Id, Decimal> pycqFCMapRP = new Map<Id, Decimal>();
	//private static Map<Id, Decimal> pyFCMapRP = new Map<Id, Decimal>();

	private static Map<Id, Decimal> dailySalesMap = new Map<Id, Decimal>();
	private static Map<Id, Decimal> mtdSalesMap = new Map<Id, Decimal>();
	private static Map<Id, Decimal> qtdSalesMap = new Map<Id, Decimal>();
	private static Map<Id, Decimal> ytdSalesMap = new Map<Id, Decimal>();
	private static Map<Id, Decimal> pyMTDSalesMap = new Map<Id, Decimal>();
	private static Map<Id, Decimal> pyQTDSalesMap = new Map<Id, Decimal>();
	private static Map<Id, Decimal> pyYTDSalesMap = new Map<Id, Decimal>();

	private static String currQuarter;
	private static String prevQuarter;
	
	public static Set<Id> masterAcctSet = new Set<Id>();

	webservice static void buildSalesReportStage() {
		if (Label.Sales_Date_Override == 'true') {
			reportEndDate = Date.parse(Label.Sales_Report_Date);
		} else {
			reportEndDate = Date.today().addDays(Integer.valueOf(Label.Sales_Date_Offset));
		}
		
		beginPY = Date.newInstance(reportEndDate.addYears(-1).year(), 1, 1);
		endCY = Date.newInstance(reportEndDate.year(), 12, 31);
		
		if (truncateStage()) {
			buildFCMaps();
			buildSalesMaps();
			insertStage();
			postProcess();
		}
	}

	@future
	private static void postProcess() {
		Set<Id> acctSet = new Set<Id>();
		Map<Id, Decimal> ytdMap = new Map<Id, Decimal>();

		if (Label.Sales_Date_Override == 'true') {
			reportEndDate = Date.parse(Label.Sales_Report_Date);
		} else {
			reportEndDate = Date.today().addDays(Integer.valueOf(Label.Sales_Date_Offset));
		}

		//update YTD
		AggregateResult[] ytdAggr = [select Sales_Order__r.Ship_To__c acc, sum(Amount__c) amt from Sales_Order_Line_Item__c where IsForecastable__c = true and CALENDAR_YEAR(Bill_Date__c) = :reportEndDate.year() and Bill_Date__c <= :reportEndDate group by Sales_Order__r.Ship_To__c];
		for (AggregateResult res : ytdAggr) {
			acctSet.add(String.valueOf(res.get('acc')));
			ytdMap.put(String.valueOf(res.get('acc')), Decimal.valueOf(String.valueOf(res.get('amt'))));
		}

		List<User> userList = [select Id, Region_ID__c, Territory_ID__c from user where (region_id__c != null or territory_ID__c != null) and lastname != 'Perficient'];
		Map<String, Id> mapRegionUserMap = new Map<String, Id>();
		Map<String, Id> mapTerrUserMap = new Map<String, Id>();
		for (User u : userList) {
			if (u.Territory_ID__c == null) {
				mapRegionUserMap.put(u.Region_ID__c, u.Id);
			} else {
				mapTerrUserMap.put(u.Territory_ID__c, u.Id);	
			}
		}
		
		List<Sales_Report_Stage__c> stageList = [select Id, YTD_Sales__c, Account__c, Health_Region_ID__c, Health_Territory_ID__c, Health_Regional_Director__c, Health_Account_Manager__c, RP_Region_ID__c, RP_Territory_ID__c, RP_Regional_Director__c, RP_Territory_Manager__c from Sales_Report_Stage__c];
		for (Sales_Report_Stage__c stage : stageList) {
			stage.YTD_Sales__c = 0;
			if (ytdMap.containsKey(stage.Account__c)) stage.YTD_Sales__c = ytdMap.get(stage.Account__c);
			
			stage.Health_Regional_Director__c = mapRegionUserMap.containsKey(stage.Health_Region_ID__c) ? mapRegionUserMap.get(stage.Health_Region_ID__c) : null;
			stage.Health_Account_Manager__c = mapTerrUserMap.containsKey(stage.Health_Territory_ID__c) ? mapTerrUserMap.get(stage.Health_Territory_ID__c) : null;
			stage.RP_Regional_Director__c = mapRegionUserMap.containsKey(stage.RP_Region_ID__c) ? mapRegionUserMap.get(stage.RP_Region_ID__c) : null;
			stage.RP_Territory_Manager__c = mapTerrUserMap.containsKey(stage.RP_Territory_ID__c) ? mapTerrUserMap.get(stage.RP_Territory_ID__c) : null;
		}
		
		update stageList;
	}

	private static void insertStage() {
		
		DateTime beginDate, endDate;
		
		Time beginTime = Time.newInstance(0,0,0,0);
		Time endTime = Time.newInstance(24,0,0,0); 
		
		//get Business Hours
		List<BusinessHours> bhList = new List<BusinessHours>([select Id, Name from BusinessHours where Name = 'US']);
		
		//MONTH
		Long MTDBusinessDays = BusinessHours.diff(bhList[0].Id, DateTime.newInstance(reportEndDate.toStartOfMonth(),beginTime), DateTime.newInstance(reportEndDate.addDays(1),endTime))/ (1000 * 60 * 60 * 24);
		System.debug ('### MTDBusinessDays=' + MTDBusinessDays);
		Long CMBusinessDays = BusinessHours.diff(bhList[0].Id, DateTime.newInstance(reportEndDate.toStartOfMonth(),beginTime), DateTime.newInstance(reportEndDate.addMonths(1).toStartOfMonth(),endTime))/ (1000 * 60 * 60 * 24);
		System.debug ('### CMBusinessDays=' + CMBusinessDays);
		
		//QUARTER
		if (reportEndDate.month() >= 1 && reportEndDate.month() <= 3) {
			beginDate = DateTime.newInstance(reportEndDate.year(), 1, 1,0,0,0);
			endDate = DateTime.newInstance(reportEndDate.year(), 3, 31,24,0,0);
		} else if (reportEndDate.month() >= 4 && reportEndDate.month() <= 6) {
			beginDate = DateTime.newInstance(reportEndDate.year(), 4, 1,0,0,0);
			endDate = DateTime.newInstance(reportEndDate.year(), 6, 30,24,0,0);
		} else if (reportEndDate.month() >= 7 && reportEndDate.month() <= 9) {
			beginDate = DateTime.newInstance(reportEndDate.year(), 7, 1,0,0,0);
			endDate = DateTime.newInstance(reportEndDate.year(), 9, 30,24,0,0);
		} else {
			beginDate = DateTime.newInstance(reportEndDate.year(), 10, 1,0,0,0);
			endDate = DateTime.newInstance(reportEndDate.year(), 12, 31,24,0,0);
		}

		Long QTDBusinessDays = BusinessHours.diff(bhList[0].Id, beginDate, DateTime.newInstance(reportEndDate.addDays(1),endTime))/ (1000 * 60 * 60 * 24);
		System.debug ('### QTDBusinessDays=' + QTDBusinessDays);
		Long CQBusinessDays = BusinessHours.diff(bhList[0].Id, beginDate, endDate)/ (1000 * 60 * 60 * 24);
		System.debug ('### CQBusinessDays=' + CQBusinessDays);

		//YEAR
		Long YTDBusinessDays = BusinessHours.diff(bhList[0].Id, DateTime.newInstance(reportEndDate.year(), 1, 1,0,0,0), DateTime.newInstance(reportEndDate.addDays(1),endTime))/ (1000 * 60 * 60 * 24);
		System.debug ('### YTDBusinessDays=' + YTDBusinessDays);
		Long CYBusinessDays = BusinessHours.diff(bhList[0].Id, DateTime.newInstance(reportEndDate.year(), 1, 1,0,0,0), DateTime.newInstance(reportEndDate.year(), 12, 31,24,0,0))/ (1000 * 60 * 60 * 24);
		System.debug ('### CYBusinessDays=' + CYBusinessDays);

		List<Sales_Report_Stage__c> stageInsertList = new List<Sales_Report_Stage__c>();
		for (Id acctId : masterAcctSet) {
			Sales_Report_Stage__c stageRec = new Sales_Report_Stage__c();
			stageRec.Account__c = acctId;

			/* Set Health Forecast */
			stageRec.CM_Forecast__c = cmFCMap.get(acctId) == null ? 0: cmFCMap.get(acctId);
			stageRec.CQ_Forecast__c = cqFCMap.get(acctId) == null ? 0: cqFCMap.get(acctId);
			//stageRec.CY_Forecast__c = cyFCMap.get(acctId) == null ? 0: cyFCMap.get(acctId);
			stageRec.PYCM_Forecast__c = pycmFCMap.get(acctId) == null ? 0: pycmFCMap.get(acctId);
			stageRec.PYCQ_Forecast__c = pycqFCMap.get(acctId) == null ? 0: pycqFCMap.get(acctId);
			//stageRec.PY_Forecast__c = pyFCMap.get(acctId) == null ? 0: pyFCMap.get(acctId);

			stageRec.Daily_Forecast__c = stageRec.CM_Forecast__c / CMBusinessDays;
			stageRec.MTD_Forecast__c = (stageRec.CM_Forecast__c / CMBusinessDays) * MTDBusinessDays;
			stageRec.QTD_Forecast__c = (stageRec.CQ_Forecast__c / CQBusinessDays) * QTDBusinessDays;
			//stageRec.YTD_Forecast__c = (stageRec.CY_Forecast__c / CYBusinessDays) * YTDBusinessDays;

			/* Set RP Forecast */
			stageRec.CM_Forecast_RP__c = cmFCMapRP.get(acctId) == null ? 0: cmFCMapRP.get(acctId);
			stageRec.CQ_Forecast_RP__c = cqFCMapRP.get(acctId) == null ? 0: cqFCMapRP.get(acctId);
			//stageRec.CY_Forecast_RP__c = cyFCMapRP.get(acctId) == null ? 0: cyFCMapRP.get(acctId);
			stageRec.PYCM_Forecast_RP__c = pycmFCMapRP.get(acctId) == null ? 0: pycmFCMapRP.get(acctId);
			stageRec.PYCQ_Forecast_RP__c = pycqFCMapRP.get(acctId) == null ? 0: pycqFCMapRP.get(acctId);
			//stageRec.PY_Forecast_RP__c = pyFCMapRP.get(acctId) == null ? 0: pyFCMapRP.get(acctId);

			stageRec.Daily_Forecast_RP__c = stageRec.CM_Forecast_RP__c / CMBusinessDays;
			stageRec.MTD_Forecast_RP__c = (stageRec.CM_Forecast_RP__c / CMBusinessDays) * MTDBusinessDays;
			stageRec.QTD_Forecast_RP__c = (stageRec.CQ_Forecast_RP__c / CQBusinessDays) * QTDBusinessDays;
			//stageRec.YTD_Forecast_RP__c = (stageRec.CY_Forecast_RP__c / CYBusinessDays) * YTDBusinessDays;

			/* Set Sales */
			stageRec.Daily_Sales__c = dailySalesMap.get(acctId) == null ? 0: dailySalesMap.get(acctId);
			stageRec.MTD_Sales__c = mtdSalesMap.get(acctId) == null ? 0: mtdSalesMap.get(acctId);
			stageRec.QTD_Sales__c = qtdSalesMap.get(acctId) == null ? 0: qtdSalesMap.get(acctId);
			stageRec.YTD_Sales__c = ytdSalesMap.get(acctId) == null ? 0: ytdSalesMap.get(acctId);
			stageRec.PYMTD_Sales__c = pyMTDSalesMap.get(acctId) == null ? 0: pyMTDSalesMap.get(acctId);
			stageRec.PYQTD_Sales__c = pyQTDSalesMap.get(acctId) == null ? 0: pyQTDSalesMap.get(acctId);
			stageRec.PYYTD_Sales__c = pyYTDSalesMap.get(acctId) == null ? 0: pyYTDSalesMap.get(acctId);

			stageInsertList.add(stageRec);
		}
		
		if (stageInsertList.size() > 0) insert stageInsertList;
	}
	
	private static Integer getQuarter(Date inputDt) {
		if (inputDt.month() >= 1 && inputDt.month() <= 3) {
			return 1;
		} else if (inputDt.month() >= 4 && inputDt.month() <= 6) {
			return 2;
		} else if (inputDt.month() >= 7 && inputDt.month() <= 9) {
			return 3;
		} else {
			return 4;
		}
		
		return null;
	}

	private static Boolean truncateStage() {
		try {
			List<Sales_Report_Stage__c> delList = new List<Sales_Report_Stage__c>();
			delList = [select Id from Sales_Report_Stage__c];
			if (delList != null && delList.size() > 0) delete delList;
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	private static void buildSalesMaps() {
		AggregateResult[] dailyAggr;
		//Daily Sales
		if (Label.Sales_Date_Override == 'true') {
			dailyAggr = [select Sales_Order__r.Ship_To__c acc, sum(Amount__c) amt from Sales_Order_Line_Item__c where IsForecastable__c = true and Bill_Date__c = :reportEndDate group by Sales_Order__r.Ship_To__c];	
		} else {
			dailyAggr = [select Sales_Order__r.Ship_To__c acc, sum(Amount__c) amt from Sales_Order_Line_Item__c where IsForecastable__c = true and Is_Bill_Date_Previous_Weekday__c = true group by Sales_Order__r.Ship_To__c];
		}
		
		for (AggregateResult res : dailyAggr) {
			masterAcctSet.add(String.valueOf(res.get('acc')));
			dailySalesMap.put(String.valueOf(res.get('acc')), Decimal.valueOf(String.valueOf(res.get('amt'))));
		}
		
		//MTD Sales
		AggregateResult[] mtdAggr = [select Sales_Order__r.Ship_To__c acc, sum(Amount__c) amt from Sales_Order_Line_Item__c where IsForecastable__c = true and Bill_Date__c >= :reportEndDate.toStartOfMonth() and Bill_Date__c <= :reportEndDate group by Sales_Order__r.Ship_To__c];
		for (AggregateResult res : mtdAggr) {
			masterAcctSet.add(String.valueOf(res.get('acc')));
			mtdSalesMap.put(String.valueOf(res.get('acc')), Decimal.valueOf(String.valueOf(res.get('amt'))));
		}
		
		//QTD Sales
		AggregateResult[] qtdAggr = [select Sales_Order__r.Ship_To__c acc, sum(Amount__c) amt from Sales_Order_Line_Item__c where IsForecastable__c = true and CALENDAR_QUARTER(Bill_Date__c) = :getQuarter(reportEndDate) and CALENDAR_YEAR(Bill_Date__c) = :reportEndDate.year() and Bill_Date__c <= :reportEndDate group by Sales_Order__r.Ship_To__c];
		for (AggregateResult res : qtdAggr) {
			masterAcctSet.add(String.valueOf(res.get('acc')));
			qtdSalesMap.put(String.valueOf(res.get('acc')), Decimal.valueOf(String.valueOf(res.get('amt'))));
		}

		/*
		//YTD Sales
		AggregateResult[] ytdAggr = [select Sales_Order__r.Ship_To__c acc, sum(Amount__c) amt from Sales_Order_Line_Item__c where IsForecastable__c = true and CALENDAR_YEAR(Bill_Date__c) = :reportEndDate.year() and Bill_Date__c <= :reportEndDate group by Sales_Order__r.Ship_To__c];
		for (AggregateResult res : ytdAggr) {
			masterAcctSet.add(String.valueOf(res.get('acc')));
			ytdSalesMap.put(String.valueOf(res.get('acc')), Decimal.valueOf(String.valueOf(res.get('amt'))));
		}
		*/

		//PYMTD Sales
		AggregateResult[] pyMTDAggr = [select Sales_Order__r.Ship_To__c acc, sum(Amount__c) amt from Sales_Order_Line_Item__c where IsForecastable__c = true and Bill_Date__c >= :reportEndDate.toStartOfMonth().addYears(-1) and Bill_Date__c <= :reportEndDate.addYears(-1) group by Sales_Order__r.Ship_To__c];
		for (AggregateResult res : pyMTDAggr) {
			masterAcctSet.add(String.valueOf(res.get('acc')));
			pyMTDSalesMap.put(String.valueOf(res.get('acc')), Decimal.valueOf(String.valueOf(res.get('amt'))));
		}

		//PYQTD Sales
		AggregateResult[] pyQTDAggr = [select Sales_Order__r.Ship_To__c acc, sum(Amount__c) amt from Sales_Order_Line_Item__c where IsForecastable__c = true and CALENDAR_QUARTER(Bill_Date__c) = :getQuarter(reportEndDate) and CALENDAR_YEAR(Bill_Date__c) = :reportEndDate.addYears(-1).year() and Bill_Date__c <= :reportEndDate.addYears(-1) group by Sales_Order__r.Ship_To__c];
		for (AggregateResult res : pyQTDAggr) {
			masterAcctSet.add(String.valueOf(res.get('acc')));
			pyQTDSalesMap.put(String.valueOf(res.get('acc')), Decimal.valueOf(String.valueOf(res.get('amt'))));
		}

		/*
		//PYYTD Sales
		AggregateResult[] pyYTDAggr = [select Sales_Order__r.Ship_To__c acc, sum(Amount__c) amt from Sales_Order_Line_Item__c where IsForecastable__c = true and CALENDAR_YEAR(Bill_Date__c) = :reportEndDate.addYears(-1).year() and Bill_Date__c <= :reportEndDate.addYears(-1) group by Sales_Order__r.Ship_To__c];
		for (AggregateResult res : pyYTDAggr) {
			masterAcctSet.add(String.valueOf(res.get('acc')));
			pyYTDSalesMap.put(String.valueOf(res.get('acc')), Decimal.valueOf(String.valueOf(res.get('amt'))));
		}
		*/
		
	}

	private static void buildFCMaps() {
		//query forecasts
		List<Forecast__c> fcList = new List<Forecast__c>();
		fcList = [select Id, Start_Date__c, End_Date__c, Year__c, Quarter__c, Health_Forecast__c, RP_Forecast__c, ES_Forecast__c, Attainment_Amount__c, Account__c from Forecast__c where start_Date__c >= :beginPY and end_date__c <= :endCY];
		
		if (fcList.size() >0) {
			for (Forecast__c fcRec : fcList) {
				
				masterAcctSet.add(fcRec.Account__c);
				Decimal fcAmt = fcRec.Health_Forecast__c == null?0:fcRec.Health_Forecast__c;
				Decimal fcAmtRP = fcRec.RP_Forecast__c == null?0:fcRec.RP_Forecast__c;
				
				//build CM Forecast total
				if (fcRec.Start_Date__c.month() == reportEndDate.month()) {
					if (fcRec.Year__c == reportEndDate.addYears(-1).year()) {
						if (pycmFCMap.containsKey(fcRec.Account__c)) {
							pycmFCMap.put(fcRec.Account__c, pycmFCMap.get(fcRec.Account__c) + fcAmt);
						} else {
							pycmFCMap.put(fcRec.Account__c, fcAmt);
						}
						if (pycmFCMapRP.containsKey(fcRec.Account__c)) {
							pycmFCMapRP.put(fcRec.Account__c, pycmFCMapRP.get(fcRec.Account__c) + fcAmtRP);
						} else {
							pycmFCMapRP.put(fcRec.Account__c, fcAmtRP);
						}
					} else {
						if (cmFCMap.containsKey(fcRec.Account__c)) {
							cmFCMap.put(fcRec.Account__c, cmFCMap.get(fcRec.Account__c) + fcAmt);
						} else {
							cmFCMap.put(fcRec.Account__c, fcAmt);
						}
						if (cmFCMapRP.containsKey(fcRec.Account__c)) {
							cmFCMapRP.put(fcRec.Account__c, cmFCMapRP.get(fcRec.Account__c) + fcAmtRP);
						} else {
							cmFCMapRP.put(fcRec.Account__c, fcAmtRP);
						}
					}
				}

				//build CQ Forecast total
				
				if (fcRec.Quarter__c.substring(1, 2) == String.valueOf(getQuarter(reportEndDate))) {
					if (fcRec.Year__c == reportEndDate.addYears(-1).year()) {
						if (pycqFCMap.containsKey(fcRec.Account__c)) {
							pycqFCMap.put(fcRec.Account__c, pycqFCMap.get(fcRec.Account__c) + fcAmt);
						} else {
							pycqFCMap.put(fcRec.Account__c, fcAmt);
						}
						if (pycqFCMapRP.containsKey(fcRec.Account__c)) {
							pycqFCMapRP.put(fcRec.Account__c, pycqFCMapRP.get(fcRec.Account__c) + fcAmtRP);
						} else {
							pycqFCMapRP.put(fcRec.Account__c, fcAmtRP);
						}
					} else {
						if (cqFCMap.containsKey(fcRec.Account__c)) {
							cqFCMap.put(fcRec.Account__c, cqFCMap.get(fcRec.Account__c) + fcAmt);
						} else {
							cqFCMap.put(fcRec.Account__c, fcAmt);
						}
						if (cqFCMapRP.containsKey(fcRec.Account__c)) {
							cqFCMapRP.put(fcRec.Account__c, cqFCMapRP.get(fcRec.Account__c) + fcAmtRP);
						} else {
							cqFCMapRP.put(fcRec.Account__c, fcAmtRP);
						}
					}
					
				}
				
				/*
				//build CY Forecast total
				if (fcRec.Year__c == reportEndDate.addYears(-1).year()) {
					if (pyFCMap.containsKey(fcRec.Account__c)) {
						pyFCMap.put(fcRec.Account__c, pyFCMap.get(fcRec.Account__c) + fcAmt);
					} else {
						pyFCMap.put(fcRec.Account__c, fcAmt);
					}
					if (pyFCMapRP.containsKey(fcRec.Account__c)) {
						pyFCMapRP.put(fcRec.Account__c, pyFCMapRP.get(fcRec.Account__c) + fcAmtRP);
					} else {
						pyFCMapRP.put(fcRec.Account__c, fcAmtRP);
					}
				} else { //year is current year
					if (cyFCMap.containsKey(fcRec.Account__c)) {
						cyFCMap.put(fcRec.Account__c, cyFCMap.get(fcRec.Account__c) + fcAmt);
					} else {
						cyFCMap.put(fcRec.Account__c, fcAmt);
					}
					if (cyFCMapRP.containsKey(fcRec.Account__c)) {
						cyFCMapRP.put(fcRec.Account__c, cyFCMapRP.get(fcRec.Account__c) + fcAmtRP);
					} else {
						cyFCMapRP.put(fcRec.Account__c, fcAmtRP);
					}
				}
				*/
			}
		}
	}
}
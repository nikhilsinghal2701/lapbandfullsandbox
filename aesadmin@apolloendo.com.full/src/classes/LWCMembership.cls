/**
Class: LWCMembership
Class for Membership_Accreditation__c, include remove, index. Used in profile general
Author – Vinay L
Date – 03/06/2014
 */
public with sharing class LWCMembership {
        public Membership_Accreditation__c mem {get; set;}
        public Boolean remove {get; set;}
        public integer index {get;set;}
        
        //iniatialize index, random Id for remove and update
        public LWCMembership(Membership_Accreditation__c m){
            this.mem = m;
            this.remove = false;
            this.index = Math.round(Math.random()*10000);
        }
}
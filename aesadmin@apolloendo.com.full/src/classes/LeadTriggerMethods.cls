public with sharing class LeadTriggerMethods {
	public static void logStatusChange(List<Lead> oldLeads, List<Lead> newLeads, Boolean isInsert) {
		List<Lead_Status_Log__c> statusLogs = new List<Lead_Status_Log__c>();
		for (Integer i=0; i<newLeads.size(); i++) {
			if (isInsert || newLeads[i].Status != oldLeads[i].Status) {
				Lead_Status_Log__c statusLog = new Lead_Status_Log__c();
				statusLog.Lead__c = newLeads[i].Id;
				statusLog.Status__c = newLeads[i].Status;
				statusLog.Status_Date__c = DateTime.now();
				if (newLeads[i].Data_Source__c == 'MDNet') {
					if (isInsert && newLeads[i].Created_Date_Time__c != null) {
						statusLog.Status_Date__c = newLeads[i].Created_Date_Time__c;
					} else if (!isInsert && newLeads[i].Last_Updated_Date_Time__c != null && newLeads[i].Last_Updated_Date_Time__c != oldLeads[i].Last_Updated_Date_Time__c) {
						statusLog.Status_Date__c = newLeads[i].Last_Updated_Date_Time__c;
					}
				}
				statusLogs.add(statusLog);
			}
		}
		
		if (statusLogs.size() > 0) insert statusLogs;
	}
}
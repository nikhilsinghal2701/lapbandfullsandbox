/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class SiteLoginController {
    global String username {get; set;}
    global String password {get; set;}
    global boolean remember {get; set;}
    
    global SiteLoginController(){
    	Cookie rememberCookie = ApexPages.currentPage().getCookies().get('remember');
    	Cookie usernameCookie = ApexPages.currentPage().getCookies().get('username');
    	Cookie passwordCookie = ApexPages.currentPage().getCookies().get('password');
    	if(rememberCookie!=null)	remember = Boolean.valueOf(rememberCookie.getValue());
    	if(usernameCookie!=null)	username=usernameCookie.getValue();
    	if(passwordCookie!=null)	password=passwordCookie.getValue();
    }

    global PageReference login() {
    	//set cookie if the remember me check box is checked
    	if(remember)
    	{
    		if((username!=null&&username!='')&&(password!=null&&password!=''))
    		{
    			List<Cookie> cList = new List<Cookie>{new Cookie('remember',String.valueOf(remember),null,-1,false), 
    			new Cookie('username',username,null,-1,false),
    			new Cookie('password',password,null,-1,true)};
    			ApexPages.currentPage().setCookies(cList);
    		}
    	}
    	else{
    		List<Cookie> cList = new List<Cookie>{new Cookie('remember','false',null,-1,false), 
			new Cookie('username','',null,-1,false),
			new Cookie('password','',null,-1,true)};
			ApexPages.currentPage().setCookies(cList);
    	}
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        return Site.login(username, password, startUrl);
        
    }
    
}
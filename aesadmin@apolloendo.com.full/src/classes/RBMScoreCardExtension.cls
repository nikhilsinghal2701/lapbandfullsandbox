public with sharing class RBMScoreCardExtension {
    private final RBM_Score_Card__c RBMSC;
    Map<String, Schema.SObjectField> field_map =new Map<String, Schema.SObjectField>();
    public List<String> picklistfields{get;set;}
    public Map<String,List<selectOption>> PicklistValues{get;set;}
    public Map<String,List<String>> PicklistValuesString{get;set;}
    public Map<String,string> PicklistHelp{get;set;}
    public string AccName {get; set;}
    
     
    public RBMScoreCardExtension(ApexPages.StandardController controller) {
    	
			if (!Test.isRunningTest())  {controller.addFields(new String[]{'Account__c','Average_turnaround_time__c','Checks_and_balances_of_denied_improper__c',
					'Insurance_documentation_and_necessary_di__c','Manage_and_Monitory_aftercare_denails__c','Payer_Employer_Policy_for_Pre_Auth__c',
					'Payer_policy_for_billing_coding_aftercar__c','Portion_of_aftercare_billed_to_3rd_party__c','Process_dedicated_people_for_Prior_Aut__c',
					'Regularly_examine_charge_masters__c','Review_renegotiate_payer_contracts__c','Senior_Leadership_proivde_resources__c',
					'Turnaround_time_for_benefit_verification__c','Update_charge_sheet_or_EMR_for_changes__c','When_verify_patient_benefits__c',
					'Working_with_local_PCP_IM_for_Aftercare__c'});} 
           RBMSC = (RBM_Score_Card__c)controller.getRecord();
           AccName=getAccName(RBMSC.Account__c).get(0).name;
           AllPicklistfileds();
           //AllPicklistfiledsValues();
           AllPicklistfiledsValuesString();
    }
    
        
    public list<string> AllPicklistfileds(){
    	picklistfields = new list<string>();
    	PicklistHelp = new 	 Map<String,string>();            
          field_map=Schema.getGlobalDescribe().get('RBM_Score_Card__c').getDescribe().fields.getMap();
            for(Schema.SObjectField fieldinfo: field_map.values()){
               if(fieldinfo.getDescribe().getType().Name()==Schema.DisplayType.PICKLIST.Name()){                
                  picklistfields.add(fieldinfo.getDescribe().getName());
                  PicklistHelp.put(fieldinfo.getDescribe().getName(),fieldinfo.getDescribe().getInlineHelpText());
               }      
          }
          return picklistfields;               
     }
     
    /**  public Map<String,List<selectOption>> AllPicklistfiledsValues(){
    	PicklistValues =new Map<String,List<selectOption>>();             
            for(String fieldPicklistinfo: picklistfields){
              List<Schema.PicklistEntry> pick_list_values = Schema.getGlobalDescribe().get('RBM_Score_Card__c').getDescribe().fields.getMap().get(fieldPicklistinfo).getDescribe().getPickListValues();
            List<selectOption> options = new List<selectOption>();

           for (Schema.PicklistEntry a : pick_list_values) {
                      options.add(new selectOption(a.getLabel(), a.getValue()));
                      
          } 
          system.debug(options);
           PicklistValues.put(fieldPicklistinfo,options);
               
          }
          return PicklistValues;               
     } */
     
      public Map<String,List<String>> AllPicklistfiledsValuesString(){
    	PicklistValuesString =new Map<String,List<String>>();             
            for(String fieldPicklistinfo: picklistfields){
              List<Schema.PicklistEntry> pick_list_values = Schema.getGlobalDescribe().get('RBM_Score_Card__c').getDescribe().fields.getMap().get(fieldPicklistinfo).getDescribe().getPickListValues();
            List<String> options = new List<String>();

           for (Schema.PicklistEntry a : pick_list_values) {
                      options.add(a.getValue());
                      
          } 
          system.debug(options);
           PicklistValuesString.put(fieldPicklistinfo,options);
               
          }
          return PicklistValuesString;               
     }
     
    public PageReference SaveRBMSC() {
        
       try {
       RBMSC.Name=AccName+' '+date.today().format();	 	
       upsert (RBMSC);
      
       }
       catch(System.DMLException e) {
           ApexPages.addMessages(e);
           return null;
       }
       
       PageReference RBMSCPage = new ApexPages.StandardController(RBMSC).view();
       RBMSCPage.setRedirect(true);
        return RBMSCPage;
     }
     
      /**
        * Retrieves name from Account.
        *
        * @param Id
        *
        * @author Nikhil
        */
        private static List<Account> getAccName(Id Aid) {
            return [SELECT Name FROM Account WHERE id = : Aid limit 1  ];
        }
     
    

}
/*Need to update PricingContract and Account.
**by: nikhil Singhal @Perficient
**date:4/2/2014
*/
global without sharing class PricingContractAccountBatch implements Database.Batchable<sObject> {

    global String Query;
    global PricingContractAccountBatch(){      
        Query = 'SELECT id ,(Select Id,Pricing_Contract__c,Pricing_Contract__r.Contract_Start_Date__c,Pricing_Contract__r.Contract_End_Date__c,Pricing_Contract__r.Group_Units_Logic__c from Accounts__r) FROM Pricing_Contract__c WHERE (IsDeleted = False)';
        if (Test.isRunningTest()) {
            Query += 'LIMIT 199';
        }  
    }

   global Database.QueryLocator start(Database.BatchableContext BC){
		
        return Database.getQueryLocator(Query);
   }
   
   global void execute(Database.BatchableContext BC, List<sObject> scope) {
        Map<id,Account> AccountToUpdate = new Map<id,Account>();
        Map<id,Double> AccountToUnit = new Map<id,Double>();
        Map<id,list<Account>> PcAccLIMap = new Map<id,list<Account>>();
        Map<id,list<Sales_Order_Line_Item__c>> SOLIMap = new Map<id,list<Sales_Order_Line_Item__c>>();
        Map<id,Set<id>> AccSOMap = new Map<id,Set<id>>();
        List<Account> AccountUpdateList = new List<Account>();
        List<Pricing_Contract__c>PCUpdateList = new List<Pricing_Contract__c>();
        double  dTotalForAll = 0;
        double  gTotalForAll = 0;
        // Get all Accounts
        for(sObject so : scope) {
            Pricing_Contract__c Pc = (Pricing_Contract__c)so;
            PcAccLIMap.put(pc.id,Pc.Accounts__r);
            for(Account temp:Pc.Accounts__r){
              AccountToUpdate.put(temp.id,temp);    
            }
        }
        
        // Get all Salesorder and Salesorderlineitem        
      for(Sales_Order__c  SO : [SELECT id,Sold_To__c, (SELECT id,Sales_Order__c,APV_Units__c,Bill_Date__c FROM Sales_Order_Line_Items__r) FROM Sales_Order__c where Sold_To__c IN : AccountToUpdate.keyset() ]){
      		
      		if(AccSOMap.containsKey(SO.Sold_To__c)){
                AccSOMap.get(SO.Sold_To__c).add(SO.id);
        	}else if (SO.Sold_To__c != null ){
                AccSOMap.put(SO.Sold_To__c,new set<id>{SO.id});
            }
      		SOLIMap.put(SO.id,SO.Sales_Order_Line_Items__r);
      
      }
        
        for(Id aid : AccountToUpdate.keyset()){
        	
      			if(AccSOMap.Containskey(aid)){
        			
        			if(!AccSOMap.get(aid).isEmpty()){
        				
        				for(Id so: AccSOMap.get(aid)){
        				
        			     	if(SOLIMap.Containskey(so)){
        				
        						if(!SOLIMap.get(so).isEmpty()){
        					
        							for(Sales_Order_Line_Item__c soli: SOLIMap.get(so)){
        						
        								if(soli.Bill_Date__c != null &&	soli.Bill_Date__c >= AccountToUpdate.get(aid).Pricing_Contract__r.Contract_Start_Date__c &&
        									soli.Bill_Date__c <= AccountToUpdate.get(aid).Pricing_Contract__r.Contract_End_Date__c	){
        										
        										dTotalForAll += soli.APV_Units__c;
        								
        								}
        							
        							}
        						
        						}
        					
        					}
        				        					
        				}
        				
        			}
        			
        		} // update account here
        		
        		AccountUpdateList.add(new Account(id=aid,Individual_Units_Contracts__c=dTotalForAll));
        		dTotalForAll=0;        		
        	 }
        	 
        	 
        	 if(AccountUpdateList.size()>0 && !AccountUpdateList.isEmpty()){
        	 	
        	 	for(Account acc: AccountUpdateList){
        	 		AccountToUnit.put(acc.id,acc.Individual_Units_Contracts__c);
        	 		
        	 	}
          
        	}
        	
        	
        	if(!AccountToUnit.isEmpty()){
        		for(id Pacu:PcAccLIMap.keyset()){
        			if(!PcAccLIMap.get(Pacu).isEmpty()){
        				for(Account acc: PcAccLIMap.get(Pacu)){
        					if(AccountToUpdate.get(acc.id).Pricing_Contract__r.Group_Units_Logic__c == 'Aggregate'){
        						if(AccountToUnit.containskey(acc.id)){
        							gTotalForAll+=AccountToUnit.get(acc.id) == null ? 0 : AccountToUnit.get(acc.id);
        							
        							}
								}else if (AccountToUpdate.get(acc.id).Pricing_Contract__r.Group_Units_Logic__c == 'Highest'){
									if(AccountToUnit.containskey(acc.id)){
										if((AccountToUnit.get(acc.id) == null ? 0 : AccountToUnit.get(acc.id)) > gTotalForAll){
										gTotalForAll=AccountToUnit.get(acc.id) == null ? 0 : AccountToUnit.get(acc.id);
										}	
										        							
        						}
        		
        					}
        					
        				}
        				        				
        			}
        			PCUpdateList.add(new Pricing_Contract__c(id=Pacu,Total_Group_Units__c=gTotalForAll));
        			gTotalForAll=0;        			
        		}
        		        		
        	}
        	
        	 if(AccountUpdateList.size()>0 && !AccountUpdateList.isEmpty()){
        	 	
        	try{
                update AccountUpdateList;
            }catch(System.DMLException dme){
                system.debug('Error:'+dme.getmessage());
            }
          
        	}
        	 if(PCUpdateList.size()>0 && !PCUpdateList.isEmpty()){
        	 	
        	try{
                update PCUpdateList;
            }catch(System.DMLException dme){
                system.debug('Error:'+dme.getmessage());
            }
          
        	}
        
       
   }
   
      
   global void finish(Database.BatchableContext BC){

   }   
}
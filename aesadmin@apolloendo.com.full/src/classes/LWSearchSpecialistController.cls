public with sharing class LWSearchSpecialistController {
	public Integer start				{get;set;}
	public String searchType 			{get;set;}
	public String specialistName 		{get;set;}
	public String practiceName			{get;set;}
	public List<Office__c> officeList	{get;set;}
	public String dataStr				{get;set;}
	public Double firstLat 				{get;set;}
	public Double firstLong 			{get;set;}
		
    public void getOfficeByName (){
    	String q = 'SELECT Address_1__c,Address_2__c,City__c,Location__Latitude__s,Location__Longitude__s,Name,State__c,Zip_Code__c,Email__c,Fax__c,Phone__c,Website__c,Country__c,Specialist__r.Name FROM Office__c WHERE Specialist__r.Name like\'%' + specialistName + '%\'';
        //added for Delete__c
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        q +=' AND Status__c NOT IN (\'Rejected\',\'Deleted\') AND RecordTypeId = \''+aId+'\' AND Is_Edit_Flag__c= false AND Approved_Office__c = NULL';
        officeList = database.query(q);
        string sHref = '';        
        firstLat = 0.0;
        if(officeList != null && officeList.size()>0){
        	dataStr = ''; Integer index = 1;
        	for(Office__c o : officeList){
        		if(firstLat == 0.0){firstLat = o.Location__Latitude__s;firstLong = o.Location__Longitude__s;}
        		dataStr += '[\"<a class=\'sapNameLink\' id=\'mapIcon_' + o.id + '\'>' + o.Specialist__r.Name + '</a><br/><strong>'  
        				 + o.Name + '</strong><br>' + o.Address_1__c + '<br />' + o.City__c + ',' + o.State__c + ','
        				 + o.Zip_Code__c + '\",' + o.Location__Latitude__s + ',' + o.Location__Longitude__s + ',' + index +
        				 ',\"' + o.Name +'\"]';
        		dataStr += ',';
        		index ++;
        	}
        	dataStr = dataStr.substring(0,dataStr.length()-1)  ;
        }
    }
}
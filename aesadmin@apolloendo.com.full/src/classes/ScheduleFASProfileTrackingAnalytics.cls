global class ScheduleFASProfileTrackingAnalytics implements Database.Batchable <Sobject>,Database.AllowsCallouts  {
  global String query;
  Boolean Test;
  global Database.QueryLocator start(Database.BatchableContext BC){
       query = 'SELECT ID FROM Site_Metric__c Limit 10';
        ImportGoogleAnalytics.StartDate = System.today()-1;
        ImportGoogleAnalytics.EndDate = System.Today()-1;
        ImportGoogleAnalytics.FASProfileViewData();
    return Database.getQueryLocator(query);
  }
  global void execute(Database.BatchableContext BC,List<Sobject> queryList){
   System.debug('Starting Scheduled job @ ' + Date.today());
        ImportGoogleAnalytics.StartDate = System.today()-1;
        ImportGoogleAnalytics.EndDate = System.Today()-1;
        ImportGoogleAnalytics.FASSearchData();
        System.debug('Finished Scheduled job @ ' + Date.today());
  }
  global void finish(Database.BatchableContext BC){
    System.debug('Starting Scheduled job @ ' + Date.today());
        ImportGoogleAnalytics.StartDate = System.today()-1;
        ImportGoogleAnalytics.EndDate = System.Today()-1;
        ImportGoogleAnalytics.importMobileFASData();
        System.debug('Finished Scheduled job @ ' + Date.today());
  }
}
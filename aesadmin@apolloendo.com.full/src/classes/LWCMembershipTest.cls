/**
Class: LWCMembershipTest
Test Class for LWCMembership
Author – Wade Liu
Date – 03/06/2014
 */
@isTest
private class LWCMembershipTest {

    static testMethod void membershipTest() {
        // TO DO: implement unit test
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        List<Account> lstAccounts = TestUtil.createAccounts(1, true, new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon});
        if(lstAccounts != null && lstAccounts.size() > 0){    
        	List<Office__c> lstOffices = TestUtil.createOffices(1, true, new Map<String,Object>{'Specialist__c'=>lstAccounts[0].Id});
    		if(lstOffices != null && lstOffices.size() > 0){
    			List<Membership_Accreditation__c> lstMembershipAccre = TestUtil.createMembershipAccreditation(1, true, new Map <String,Object>{'Office__c'=>lstOffices[0].Id,'Specialist__c'=>lstAccounts[0].Id});
    			LWCMembership oMembership = new LWCMembership(lstMembershipAccre[0]);
    			system.assert(!oMembership.remove);
    			return;
    		}
        }
        system.assert(false);
    }
}
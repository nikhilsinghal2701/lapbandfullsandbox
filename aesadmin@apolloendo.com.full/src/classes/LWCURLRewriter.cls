/*****************************************************
Class: LWCURLRewriter
This class is rewrite the vf page url to customized url
Author – Chris Cai,
Date – 1/20/2014
Revision History
******************************************************/
global with sharing class LWCURLRewriter implements Site.UrlRewriter {

    global PageReference mapRequestUrl(PageReference lbcUrl){
        
        //Get the friendly URL for one the page which you access to.
        String url = lbcUrl.getUrl(); 
        if( url.equals('/')){
            return new PageReference('/sitelogin');   
        }
        
        Map<String,String> lbcURL2VFMap = new Map<String,String>();

        for(LBCURLRewriter__c  lapbandURLRewriter : LBCURLRewriter__c.getAll().values()){
            lbcURL2VFMap.put(lapbandURLRewriter.LapbandCenterURL__c.toLowerCase(),lapbandURLRewriter.VisualforcePageURL__c);
        }
        if(lbcURL2VFMap.containsKey(url.toLowerCase())){
             return new PageReference(lbcURL2VFMap.get(url.toLowerCase()));      
        }
        else{
          if(url.endsWith('/')){
                return new PageReference(lbcURL2VFMap.get(url.substring(0,url.length()-1).toLowerCase()));
            }else{
                return null;
            }                  
        } 
                 
    }
    
    
    global List<PageReference> generateUrlFor(List<PageReference> 
            mySalesforceUrls){
         //A list of pages to return after all the links have been evaluated
       /* List<PageReference> lbcUrls = new List<PageReference>();
        //Get the map for visualforce page to Friendly URL
        Map<String,String> vf2LBCURLMap = new Map<String,String>();
        
        //Get the map from visualforce Page URL to Friendly URL from Custom Settings  
        for(LBCURLRewriter__c lapbandURLRewriter : LBCURLRewriter__c.getAll().values()){
            vf2LBCURLMap.put(lapbandURLRewriter.VisualforcePageURL__c, lapbandURLRewriter.LapbandCenterURL__c);
        }
        
        Integer index = -1 ;
        String temp = '',url='';        
        for(PageReference mySalesforceUrl : mySalesforceUrls) {
            //Get the URL of the visualforce page
            url = mySalesforceUrl.getUrl();
            
            index = url.indexOf('?');
            if(index > -1){
                system.debug('----------'+url);
                temp = url.substring(index,url.length());
                url = url.substring(0,index);
                temp = temp.substring((temp.indexOf('=') + 1),temp.length());
                url = vf2LBCURLMap.get(url)  + '|' + temp;
            }
            else{
                url = vf2LBCURLMap.get(url) ;
            }
            lbcUrls.add( new PageReference(url) );
        }
        
        //Return the full list of pages
        return lbcUrls;*/
        return null;
  }
}
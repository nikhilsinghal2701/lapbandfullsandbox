/**
Class: LWCProfileControllerTest
Test Class for LWCProfileController
Author – Wade Liu
Date – 03/10/2014
 */
@isTest
private class LWCProfileControllerTest{
    static testMethod void profileUnitTest1() {
         // prepare data
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        Contact contact = TestUtil.createContacts(1, true, new Map<String, Object>{'AccountId' => acc.Id})[0];
        
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = :LWCConstantDeclaration.CERTIFIEDPROFILE Limit 1];
        User u = TestUtil.createUsers(1, false, new Map<String, Object>{'ContactId' => contact.Id,
                                                                       'ProfileId' => portalProfile.Id })[0];
                                                                             
        Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
                                                                                      'Website__c' => 'www.site.com',
                                                                                      'Live_Seminars__c' => 'www.liveSeminars.com',
                                                                                      'Online_Seminars__c' => 'www.onlineSeminars.com',
                                                                                      'Gender__c' => 'Male'})[0];
        
        Attachment attachment = TestUtil.createAttachments(1, true, new Map<String, Object>{'parentId' => surgeon.Id})[0];
        
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        String pOffice = LWCConstantDeclaration.PENDINGOFFICERECORD;
        LIST<RecordType> recordTypeIds = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice,pOffice});
        Map<String,ID> recordTypeMap = new Map<String,ID>();
        for(RecordType r : recordTypeIds){
            recordTypeMap.put(r.Name, r.Id);
        }
        ID aId = recordTypeMap.get(aOffice);
        ID pId = recordTypeMap.get(pOffice);
        List<Office__c> officeList = TestUtil.createOffices(2, true, new Map<String, Object>{'Status__c' => 'Submitted',
                                                                                             'Specialist__c' => acc.Id,
                                                                                             'RecordTypeId' => new List<String>{String.valueOf(aId), String.valueOf(pId)},
                                                                                             'Is_Edit_Flag__c' => false});
        
        List<Membership_Accreditation__c> membershipList = TestUtil.createMembershipAccreditation(10, true, new Map<String, Object>{'Specialist__c' => acc.Id,
                                                                                                                                    'Office__c' => officeList[0].Id });
                                                                                                                                    
        List<Surgeon_Affiliation__c> surgeonAffiliationList = TestUtil.createSurgeonAffiliations(10, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
                                                                                                                                   'Surgeon_Name__c' => surgeon.Id});
                                                                                                                                   
        System.runAs(u){
            // create a new controller 
            LWCProfileController controller = new LWCProfileController();
            
            // set surgeon
            controller.surgeon = surgeon;
            
            // test getWebURL
            String webSite = controller.getWebURL();
            
            System.assertEquals('http://www.site.com', webSite); 
            
            // test getLiveURL
            String liveURL = controller.getLiveURL();
            
            System.assertEquals('http://www.liveSeminars.com', liveURL); 
            
            // test getOnlineURL
            String onlineURL = controller.getOnlineURL();
            
            System.assertEquals('http://www.onlineSeminars.com', onlineURL); 
            
            //  test getGender
            String gender = controller.getGender();
            
            System.assertEquals('Male', gender); 
            
            // test setGender
            controller.setGender('Female');
            System.assertEquals('Female', controller.gender);
            
            // test getGenders
            controller.getGenders();
        }                                                                                  
    }
    
    static testMethod void profileUnitTest2() {
         // prepare data
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        Contact contact = TestUtil.createContacts(1, true, new Map<String, Object>{'AccountId' => acc.Id})[0];
        
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'LAP-BAND® Central Registered User' Limit 1];
        User u = TestUtil.createUsers(1, false, new Map<String, Object>{'ContactId' => contact.Id,
                                                                       'ProfileId' => portalProfile.Id })[0];
                                                                             
        Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
                                                                                      'Website__c' => 'www.site.com',
                                                                                      'Live_Seminars__c' => 'www.liveSeminars.com',
                                                                                      'Online_Seminars__c' => 'www.onlineSeminars.com',
                                                                                      'Gender__c' => 'Male'})[0];
        
        Attachment attachment = TestUtil.createAttachments(1, true, new Map<String, Object>{'parentId' => surgeon.Id})[0];
        
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        String pOffice = LWCConstantDeclaration.PENDINGOFFICERECORD;
        LIST<RecordType> recordTypeIds = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice,pOffice});
        Map<String,ID> recordTypeMap = new Map<String,ID>();
        for(RecordType r : recordTypeIds){
            recordTypeMap.put(r.Name, r.Id);
        }
        ID aId = recordTypeMap.get(aOffice);
        ID pId = recordTypeMap.get(pOffice);
        List<Office__c> officeList = TestUtil.createOffices(2, true, new Map<String, Object>{'Status__c' => 'Submitted',
                                                                                              'Specialist__c' => acc.Id,
                                                                                              'RecordTypeId' => new List<String>{String.valueOf(aId), String.valueOf(pId)},
                                                                                              'Is_Edit_Flag__c' => false});
        
        List<Membership_Accreditation__c> membershipList = TestUtil.createMembershipAccreditation(10, true, new Map<String, Object>{'Specialist__c' => acc.Id,
                                                                                                                                    'Office__c' => officeList[0].Id });
                                                                                                                                    
        List<Surgeon_Affiliation__c> surgeonAffiliationList = TestUtil.createSurgeonAffiliations(10, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
                                                                                                                                   'Surgeon_Name__c' => surgeon.Id});
        System.runAs(u){
            // create a new controller 
            LWCProfileController controller = new LWCProfileController();
            
            // set surgeon
            controller.surgeon = surgeon;
            
            // test showPopup
            controller.showPopup();
            System.assertEquals(true, controller.displayPopup);
            
            // test showDelete
            controller.deleteIndex = 5;
            controller.showDelete();
            System.assertEquals(true, controller.deletePopup);
            
            // test closeDelete
            controller.closeDelete();
            System.assertEquals(false, controller.deletePopup);
            
            // test deleteOffice
            Id officeId = officeList[0].Id;
            controller.deleteOfficeId = officeId;
            controller.deleteOffice();
            Office__c office = [ SELECT Name, Status__c FROM Office__c WHERE Id = :officeId LIMIT 1];
            System.assertEquals('Deleted', office.Status__c);
        }                                                                                 
    }


    static testMethod void profileUnitTest3() {
         // prepare data
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        
        Account acc =  TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId' => idAccountRecordTypeSurgeon})[0];
        
        Contact contact = TestUtil.createContacts(1, true, new Map<String, Object>{'AccountId' => acc.Id})[0];
        
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = :LWCConstantDeclaration.CERTIFIEDPROFILE Limit 1];
        User u = TestUtil.createUsers(1, false, new Map<String, Object>{'ContactId' => contact.Id,
                                                                       'ProfileId' => portalProfile.Id })[0];
                                                                             
        Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
                                                                                      'Website__c' => 'www.site.com',
                                                                                      'Live_Seminars__c' => 'www.liveSeminars.com',
                                                                                      'Online_Seminars__c' => 'www.onlineSeminars.com',
                                                                                      'Gender__c' => 'Male'})[0];
        
        Attachment attachment = TestUtil.createAttachments(1, true, new Map<String, Object>{'parentId' => surgeon.Id})[0];
        
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        String pOffice = LWCConstantDeclaration.PENDINGOFFICERECORD;
        LIST<RecordType> recordTypeIds = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice,pOffice});
        Map<String,ID> recordTypeMap = new Map<String,ID>();
        for(RecordType r : recordTypeIds){
            recordTypeMap.put(r.Name, r.Id);
        }
        ID aId = recordTypeMap.get(aOffice);
        ID pId = recordTypeMap.get(pOffice);
        List<Office__c> officeList = TestUtil.createOffices(2, true, new Map<String, Object>{'Status__c' => 'Submitted',
                                                                                              'Specialist__c' => acc.Id,
                                                                                              'RecordTypeId' => new List<String>{String.valueOf(aId), String.valueOf(pId)},
                                                                                              'Is_Edit_Flag__c' => false});
        
        List<Membership_Accreditation__c> membershipList = TestUtil.createMembershipAccreditation(10, true, new Map<String, Object>{'Specialist__c' => acc.Id,
                                                                                                                                    'Office__c' => officeList[0].Id });
                                                                                                                                    
        List<Surgeon_Affiliation__c> surgeonAffiliationList = TestUtil.createSurgeonAffiliations(10, true, new Map<String, Object>{'Surgeon_Account__c' => acc.Id,
                                                                                                                                   'Surgeon_Name__c' => surgeon.Id});
                                                                                                                                   
        System.runAs(u){
            // create a new controller 
            LWCProfileController controller = new LWCProfileController();
            System.assertEquals(2, controller.officeNum);

            // set surgeon
            controller.surgeon = surgeon;
            
            // test switchTab
            controller.switchTab();
            
            // test reloadOffice
            controller.reloadOffice();
            System.assertEquals(2, controller.officeNum);
            
            // test insertMasterOffice
            controller.insertMasterOffice();
            
            // test saveProfile
            controller.setGender('Female');
            controller.saveProfile();
            Surgeon__c surgeon_1 = [ SELECT Gender__c FROM Surgeon__c WHERE Id = :surgeon.Id LIMIT 1 ];
            System.assertEquals('Female', surgeon_1.Gender__c);
            
            // test.removeMembership
            controller.removeMembership();
            System.assertEquals(1, controller.formChange);
            
            // test.removeHospital
            controller.removeHospital();
            System.assertEquals(1, controller.formChange);
            
            // test doNothing
            controller.doNothing();
            
            controller.addMembership();
            
            controller.addHospital();
       }                                                                                
    }
}
<apex:page showHeader="false" standardStylesheets="false" sidebar="false">
<html>
<head>
<title>Glossary | The LAP-BAND® System</title>
<meta name="description" content="Review our Privacy Policy and Terms of Use here." />

<c:au_lapband_head />
</head>

<body>

<!-- SET: WRAPPER -->
<div class="wrapper">
    
    <!-- SET: HEADER -->
             <c:au_lapband_nav />
             
                 
            <!-- END: HEADER -->
            


            
  <!-- SET: MAIN CONTENT -->
            <div class="main_content">
                <div class="content">
                   <div class="utility">
                      <div class="container">
                        <div class="para iner11 left">                              

                <h2>Glossary of Terms</h2>
                    <br />
                    <h5>Anesthesia</h5>
                    <p>The loss of sensation and feeling. Also refers to the process or drugs used to produce this effect. Anesthesia is commonly employed prior to surgery so that a patient will not feel any pain or discomfort.</p>
                    <h5>Band Erosion</h5>
                    <p>Erosion of the LAP-BAND<sup>&#174;</sup> System device through the gastric walls and into the lumen of the stomach.</p>
                    <h5>Band Slippage</h5>
                    <p>When part of the stomach below the Lap-Band System, migrates up through the band, creating a larger pouch above the band.</p>
                    <h5>Bariatric</h5>
                    <p>Related to the branch of medicine that deals with the prevention and treatment of obesity.</p>
                    <h5>Bariatric Surgeon</h5>
                    <p>A surgeon who specializes in the surgical treatment of obesity.</p>
                    <h5>Body Mass Index (BMI)</h5>
                    <p>The most widely used measurement for obesity. The BMI approximates body mass using a mathematical ratio of weight and height [(weight in kg ÷ height in meters<sup>2</sup>) or (weight in pounds ÷ height in inches<sup>2</sup> x 703)]. A BMI of 30 or more is regarded by most health agencies as the threshold for obesity. A BMI of 40 or more generally qualifies as morbid obesity. However, note that BMI measurements in body-builders and athletes may not be accurate determinants of obesity because the BMI does not distinguish between muscle and fat.</p>
                    <h5>Comorbidity</h5>
                    <p>A medical condition that exists in addition to and is caused or worsened by obesity or any other primary disease being studied or treated. With sufficient weight loss, obesity-related comorbidities such as type 2 diabetes, hypertension and sleep apnea generally improve or completely resolve.</p>
                    <h5>Dumping Syndrome</h5>
                    <p>A physiological reaction frequently seen following gastric bypass surgery. This operation is designed to alter the function of the stomach and intestines and interrupt normal digestion. Therefore, whenever patients eat certain foods, such as sugar and sweets, they may experience "dumping," characterized by symptoms of nausea, flushing and sweating, light-headedness and watery diarrhea.</p>
                    <h5>Gastric Bypass</h5>
                    <p>A surgical procedure for the treatment of obesity where a thumb-sized stomach pouch is created using stapling techniques to divide the stomach and then connect the outlet of the pouch directly to the intestine (also known as the bowel), essentially "bypassing" the lower stomach. The flow of digestive juices is preserved, however. This procedure achieves its effect by restricting the volume of food consumed and also the type of food consumed. Gastric bypass surgery can be performed via open surgery (one large incision) or less invasively with laparoscopic techniques (several tiny incisions). </p>
                    <h5>Gastroesophageal Reflux</h5>
                    <p>The backward flow of stomach contents into the esophagus due to a malfunction in the sphincter at the end of the esophagus. This can cause heartburn and discomfort. When it occurs repeatedly, it may become gastroesophageal reflux disease (GERD), where stomach acid can eventually cause scarring of the esophagus and other chronic problems.</p>
                    <h5>Hypertension</h5>
                    <p>The medical term for high blood pressure. Usually, this means that a patient has a blood pressure of 140/90 or higher. In older adults, this number is adjusted upwards slightly. The top number is systolic pressure (pressure in blood vessels when heart is pumping out blood), while the bottom number represents diastolic pressure (when heart is at rest). This condition is also associated with obesity due to the excess weight that the heart has to sustain.</p>
                    <h5>Ideal Weight</h5>
                    <p>Generally, this term refers to what a person of a given height and body frame should weigh. In other words, the desired weight for optimal health and fitness. There are several problems, however with current calculations  of ideal weight: a) body fat percentage of distribution is not accounted for; b) only some tables account for different body frames or ages; and c) most importantly, there is no consensus about which formula or table to use.</p>
                    <p>While the topic remains subjective, there are two primary sources for determination of ideal weights: the Metropolitan Life Height and Weight Table, and The World Health Organization Classification of Adults According to BMI. The WHO classifies "the normal range" of BMI as 18.50-24.99, and "overweight" as >=25.00<sup>2</sup></p>
                    <p>In a clinical study of LAP-BAND in people with a BMI of 35 or above, the Metropolitan Life Table was used to determine ideal weight. In a separate clinical study of LAP-BAND in people with a BMI between 30 and 40, the WHO definition of 25 BMI was used as ideal weight.</p>
                    <h5>Laparoscopic Surgery</h5>
                    <p>A minimally invasive surgical approach where the surgeon makes several small incisions to access the interior of the body. A long, slender camera attached to a light source and chopstick-like instruments are used to perform the operation. Compared to the large incision of conventional open surgery, there is typically less pain and scarring following this operation. Usually, hospital stay and overall recovery time are also reduced.</p>
                    <h5>Laparoscopic Gastric Bypass</h5>
                    <p>A minimally invasive method of performing gastric bypass surgery. (See full definition of "Gastric Bypass.") </p>
                    <h5>Lumen</h5>
                    <p>The cavity or channel within a tube or tubular organ.</p>
                    <h5>Malabsorption</h5>
                    <p>A condition where the small intestine cannot absorb nutrients from foods.</p>
                    <h5>Morbid Obesity</h5>
                    <p>A disease in which excess weight begins to interfere with basic physiological functions such as breathing and walking. Generally, it can be defined as weighing 100 pounds more than your ideal weight. A more precise indicator, however, is a Body Mass Index (BMI) of 40 or greater.</p>
                    <h5>Obesity</h5>
                    <p>A condition where there is excess body weight due to an abnormal accumulation of fat. Defined objectively as a Body Mass Index (BMI) of 30 or more, obesity is associated with markedly increased health risks.</p>
                    <h5>Overweight</h5>
                    <p>A condition of increased body weight compared to established standards. The weight may result from bone, fat, muscle, and/or water. Defined objectively as a Body Mass Index (BMI) of 25-29.9.</p>
                    <h5>Pancreatic enzymes</h5>
                    <p>Proteins released by the pancreas that help break down food during the digestive process. This process creates energy that can be carried through the body by the blood.</p>
                    <h5>Port complication</h5>
                    <p>Any complication associated with the Access Port of the LAP-BAND<sup>&#174;</sup> System such as infection or dislodgement.</p>
                    <h5>Saline</h5>
                    <p>A salt solution (sodium chloride) similar to tears, the body's natural liquid. Used to fill the inner surface of the LAP-BAND<sup>&#174;</sup> System to adjust the degree of restriction and the rate of weight loss.</p>
                    <h5>Sleep Apnea</h5>
                    <p>The temporary cessation of breathing during sleep. Typically, the sufferer will awake gasping for breath. Sleep apnea may occur repeatedly, resulting in a poor night's sleep and daytime drowsiness. One of the comorbidities associated with morbid obesity.</p>
                    <h5>Sleeve Gastrectomy</h5> 
                    <p>Sleeve gastrectomy is a weight loss surgery in which the surgeon amputates a large portion of the stomach. The new, smaller stomach is about the size of a banana. It limits the amount of food a person can eat by making the body feel fuller after eating small amounts of food.</p>
                    <h5>Stoma</h5>
                    <p>The outlet to the stomach created by stapling or placing an adjustable band around its upper part, which divides the stomach into two parts &#8211; the small upper stomach pouch and the lower stomach &#8211; resulting in restriction of the amount of food the stomach can hold and increasing the time it takes to empty. The LAP-BAND<sup>&#174;</sup> System allows the stoma to be adjusted by inflating or deflating the inner surface of the band in order to modify the degree of restriction.</p>
                              
                              
                              
                          </div>
                      </div>
                       
                   </div>
                   
                </div>
            </div>
                
          
           <!-- END: MAIN CONTENT -->
            <!-- SET: FOOTER -->
            <c:au_lapband_footer />
            <!-- END: FOOTER -->
            
        </div>
    <!-- END: CONTAINER -->
    
<!-- </div> -->
<!-- END: WRAPPER -->

<c:au_lapband_footer_scripts />

</body>
</html>

</apex:page>
/**
Class: LWCOfficeStaffController
Controller for LWCOfficeStaff componenet
Author – Wade Liu
Date – 02/18/2014
 */
public with sharing class LWCOfficeStaffController {
	public string accountID	{set; get;}
	public boolean staffPopup	{set; get;}
	public Office_Staff__c activeOS	{set; get;}
	public boolean cancelPopup {set; get;}
	public boolean deletePopup {set; get;}
	public string errorMessg {set; get;}
	public string activeEmail	{set; get;}
	public boolean displayModal	{get;set;}
	public boolean userExist	{get;set;}
	
	//popup window for add and edit office staff
	public void showStaff(){
		//add record, activeEmail == 'newOS'
		if(activeEmail == 'newOS')	activeOS = new Office_Staff__c();
		//edit record
		else	activeOS = [select Id, First_Name__c, Last_Name__c, Type__c, E_mail__c, Specialist_Profile__c, Office_Profiles__c, Office_Staff_Profiles__c from Office_Staff__c where E_mail__c = :activeEmail limit 1];
		staffPopup = true;
		displayModal = true;
	}
	
	//close office staff popup window
	public void closeStaff(){
		closeCancel();
		staffPopup = false;
		closeDelete();
		displayModal = false;
	}
	
	//popup cancel panel
	public void showCancel(){
		cancelPopup = true;
	}
	
	//close cancel panenl
	public void closeCancel(){
		cancelPopup = false;
	}
	
	//popup delete panel
	public void showDelete(){
		deletePopup = true;
		displayModal = true;
	}
	
	//close delete panel
	public void closeDelete(){
		deletePopup = false;
		displayModal = false;
	}
	
	//upsert office staff recode, save button
	public PageReference upsertOfficeStaff(){
		if(activeEmail == 'newOS'){
			activeOS.Specialist__c = accountID;
			//insert activeOS;
			LWCWithoutSharing.insertOfficeStaff(activeOS);
		}
		else{
			//update activeOS;
			LWCWithoutSharing.updateOfficeStaff(activeOS);
		}
		closeStaff();
		return null;
	}
	
	//delete office staff record, inactive
	public PageReference deleteOfficeStaff(){
		activeOS = [select Id, E_mail__c, Is_Active__c from Office_Staff__c where E_mail__c = :activeEmail limit 1];
		LWCWithoutSharing.deleteOfficeStaff(activeOS);
		closeStaff();
		return null;
	}
	
	public LWCOfficeStaffController(){
	}
	
	//get active office staff list under account
	public List<Office_Staff__c> getOfficeStaffs(){
		List<Office_Staff__c> oss =[select First_Name__c, Last_Name__c, E_mail__c from Office_Staff__c where Specialist__c =:accountID AND Is_Active__c = true];
		return oss;
	}
	
	//get options of picklist Office_Staff__c.Type__c
	public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Select'));
        Schema.sObjectType objType = Office_Staff__c.getSObjectType(); 
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        list<Schema.PicklistEntry> values = fieldMap.get('Type__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : values){
        	options.add(new SelectOption(a.getLabel(), a.getValue()));
        }
        return options;
    }
        
   //remote method, check email address is unique for new user
   @RemoteAction
   public static boolean checkEmail(string email){
   		if(email!=null&&email!=''){
   			List<String> emails = new List<String>();
   			emails.add(email);
   			List<User> users = LWCWithoutSharing.getUsers(emails);
   			if(users.size()>0)	return false;
   			else return true;
   		}
   		else	return	false;
   }
}
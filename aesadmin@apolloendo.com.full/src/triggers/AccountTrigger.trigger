trigger AccountTrigger on Account (before insert, before update) {
	if (Label.AccountTrigger == 'Active') {
		SalesAlignmentUtility.alignAccounts(trigger.new);
	}
}
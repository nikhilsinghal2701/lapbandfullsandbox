/*****************************************************
Class: CurrencyConversionTriggerHandlerTest 
Test class for CurrencyConversionTrigger trigger and the CurrencyConversionTriggerHandler class.
Author – Wade Liu
Date – 03/17/2013
Revision History

******************************************************/
@isTest
private class CurrencyConversionTriggerHandlerTest {

    static testMethod void updateConversionRateTest() {
        //insert Currency_Conversion__c
        List<Currency_Conversion__c> currencyConversionList = TestUtil.createCurrencyConversions(100, true, null);
        //create Account
        Account accunt = TestUtil.createAccounts(1, true, null)[0];
        //create Sales Order
        Sales_Order__c salesOrder = new Sales_Order__c();
        salesOrder.Ship_To__c = accunt.Id;
        salesOrder.Type__c = 'AUD';
        insert salesOrder;
        //insert salesOrderLineItems
        List<Sales_Order_Line_Item__c> salesOrderLineItems = new List<Sales_Order_Line_Item__c>();
        for(Currency_Conversion__c cc : currencyConversionList){
        	Sales_Order_Line_Item__c soli = new Sales_Order_Line_Item__c();
        	soli.Currency_Conversion__c = cc.Id;
        	soli.Unconverted_Amount__c = 1000.00;
        	soli.Sales_Order__c = salesOrder.Id;
        	salesOrderLineItems.add(soli);
        }
        if(salesOrderLineItems.size()>0)	insert salesOrderLineItems;
        //test start
        Test.startTest();
        //update Conversion_Rate__c
        for(Currency_Conversion__c cc : currencyConversionList){
        	cc.Conversion_Rate__c = cc.Conversion_Rate__c + 0.2;
        }
        update currencyConversionList;
        //assert result
        List<Sales_Order_Line_Item__c> soli = [select Id, Unconverted_Amount__c, Conversion_Rate__c, Integration_LineAmount__c, Currency_Conversion__c
        								from Sales_Order_Line_Item__c where Currency_Conversion__c =:currencyConversionList[50].Id];
        system.assertEquals(soli.size(),1);
        system.assertEquals(soli[0].Conversion_Rate__c,currencyConversionList[50].Conversion_Rate__c);
        Decimal toRoundLineAmount = soli[0].Unconverted_Amount__c/soli[0].Conversion_Rate__c;
        Decimal roundedLineAmount = toRoundLineAmount.setScale(2, RoundingMode.HALF_UP);
        system.assertEquals(Decimal.valueOf(soli[0].Integration_LineAmount__c), roundedLineAmount);
        //test stop
        Test.stopTest();
    }
}
/*****************************************************
Class: LWFindSpecialistController
This class is the java remote controller for google maps.
Author – Vinay L
Date – 01/20/2014
Revision History
Date - 01/28/2014 add functions to get surgeon, offices information for LWSpecialistDetails page
******************************************************/
global without sharing class LWFindSpecialistController {

    public String officeId {get; set;}        
   
    //private Id accountId = '001R000000oZYjw';
    private Id accountId;
    
    
    public Integer start                                            {get;set;}    
    public String searchType                                        {get;set;}
    public String specialistName                                    {get;set;}
    public String practiceName                                      {get;set;}
    public List<Office__c> officeList                               {get;set;}
    public List<Membership_Accreditation__c> membersList            {get;set;}
    public String dataStr                                           {get;set;}
    public Double firstLat                                          {get;set;}
    public Double firstLong                                         {get;set;}
    public Integer page                                             {get;set;}
    
    public String zipCode                                           {get;set;}
    public String searchRadius                                      {get;set;}
    public String searchLanguage                                    {get;set;}
    public String searchWheel                                       {get;set;}
    public String mile                                              {get;set;}
    public String latlng                                            {get;set;}
    public Boolean showInPage                                       {get;set;} 
    public Boolean hcp                                              {get;set;}   
    public String leadId;
    public String zipCodeInMobile                                   {get;set;}
    public Surgeon__c curSurgeon;
     
    public LWFindSpecialistController(){
        system.debug( 'LWFindSpecialistController STARTED' );
        Map<String,String> postArguments = ApexPages.currentPage().getParameters();
        system.debug( 'postArguments CREATED:' + postArguments);
        specialistName = ( postArguments.get( 'specialistName' ) == null ) ? postArguments.get( 'search-name' ) : postArguments.get( 'specialistName' );
        practiceName = ( postArguments.get( 'practiceName' ) == null ) ? practiceName : postArguments.get( 'practiceName' );
        zipCode = ( postArguments.get( 'searchZip' ) == null ) ? postArguments.get( 'search-zip' ) : postArguments.get( 'searchZip' );
        accountId = ( postArguments.get( 'accountId' ) == null ) ? accountId : postArguments.get( 'accountId' );
        searchRadius =  ( postArguments.get( 'search-radius' ) == null ) ? searchRadius : postArguments.get( 'search-radius' );
        searchLanguage =  ( postArguments.get( 'search-language' ) == null ) ? searchLanguage : postArguments.get( 'search-language' );
        searchWheel =  ( postArguments.get( 'search-wheelchair' ) == null ) ? searchWheel : postArguments.get( 'search-wheelchair' );
        searchType = postArguments.get( 'searchType' );
        zipCodeInMobile = ( postArguments.get( 'zipcode' ) == null ) ? zipCodeInMobile : postArguments.get( 'zipcode' );
        page = ( postArguments.get( 'page' ) == null ) ? 1 : Integer.valueOf( postArguments.get( 'page' ) );
        system.debug( '------------------------' );
        System.debug( 'zipCodeInMobile = ' + zipCodeInMobile );
        system.debug( 'postArguments=' + postArguments );
        if(specialistName != null){
            getOfficeByName();
        }
        
        leadId = postArguments.get( 'lId' ) ;
        system.debug( 'leadId=' + leadId );
             
    }
    
    
    public PageReference optOutEmail(){
        if(leadId != null){
            try{
               system.debug( 'leadId=' + leadId );  
               Lead l =  [SELECT Id,HasOptedOutOfEmail FROM Lead WHERE Id = :leadId][0];
               l.HasOptedOutOfEmail = true;
               update l;
            }catch (Exception e){
                
            }
        }
        return null;
    }    
    
    @RemoteAction
    global static List<Office__c> getOffice(String loc) {
        //loc=lantitude:longtitude:miles:language:wheelchair
        String selectStr = 'SELECT Address_1__c,Address_2__c,City__c,Location__Latitude__s,MapLink__c,Location__Longitude__s,Name,State__c,Zip_Code__c,Country__c,Specialist__r.Name,Phone__c,Fax__c,Email__c,Wheelchair_Access__c,English__c,Spanish__c,Chinese__c,Japanese__c,French__c,German__c,Vietnamese__c,Italian__c,Korean__c,Russian__c,Website__c,Live_Seminars_Link__c,Online_Seminars_Link__c FROM Office__c';
        String countStr = 'SELECT COUNT() FROM Office__c';
        String whereStr = ' where ';                
        List<String> arguments = loc.split(':');
        Integer argumentSize = arguments.size();
        
        if(argumentSize < 2){
            return null;
        }        
        
        String orderbyStr = ' ORDER BY DISTANCE(location__c, GEOLOCATION(' +arguments[0] + ',' +arguments[1]+'), \'mi\' )';
        
        do{
            if( argumentSize > 4){
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < '+arguments[2]);
                if( arguments[4] == 'checked' ){
                    whereStr += ' and Wheelchair_Access__c = true ';
                } 
                
                if(arguments[3] != 'Any'){
                    whereStr += ' and '+ arguments[3] +'__c = true';
                }                           
                break;
            }
            
            if( argumentSize > 3 ){
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < '+arguments[2]);
                if(arguments[3] != 'Any'){
                    whereStr += ' and '+ arguments[3] +'__c = true';
                }                           
                break;
            }
            
            if( argumentSize > 2 ){
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < ' + arguments[2]);
                break;
            }
            
            if(argumentSize > 1) {
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < 50 ');
            }
        }while(false);
        
        //added for Delete__c
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        whereStr +=' AND Status__c NOT IN (\'Rejected\',\'Deleted\') AND RecordTypeId = \''+aId+'\' AND Approved_Office__c = NULL';
        
        String sql = selectStr + whereStr + orderbyStr;   
        System.debug('sql = ' + sql);
        List<Office__c> officeList = database.query(sql);        
        return officeList;
    }
    
    
    @RemoteAction
    global static List<OfficeSurgeon> getOfficeByPage(String loc,Integer page) {
        //loc=lantitude:longtitude:miles:language:wheelchair
        String selectStr = 'SELECT Address_1__c,Address_2__c,City__c,Location__Latitude__s,MapLink__c,Location__Longitude__s,' + 
        ' Name,State__c,Zip_Code__c,Country__c,Specialist__c, Specialist__r.Name,Phone__c,Fax__c,Email__c,Wheelchair_Access__c,' +
        ' English__c,Spanish__c,Chinese__c,Japanese__c,French__c,German__c,Vietnamese__c,Italian__c,Korean__c,Russian__c,' +
        ' Website__c,Live_Seminars_Link__c,Online_Seminars_Link__c,Office_Hours__c ' +
        ' FROM Office__c';
        String countStr = 'SELECT COUNT() FROM Office__c';
        String whereStr = ' where ';                
        String pageStr = '';
        Integer pageSize = Integer.valueOf(System.Label.pagesize);
        List<String> arguments = loc.split(':');
        Integer argumentSize = arguments.size();
        
        if(argumentSize < 2){
            return null;
        }        
        
        String orderbyStr = ' ORDER BY DISTANCE(location__c, GEOLOCATION(' +arguments[0] + ',' +arguments[1]+'), \'mi\' )';
        
        do{
            if( argumentSize > 4){
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < '+arguments[2]);
                if( arguments[4] == 'checked' ){
                    whereStr += ' and Wheelchair_Access__c = true ';
                } 
                
                if(arguments[3] != 'Any'){
                    whereStr += ' and '+ arguments[3] +'__c = true';
                }                           
                break;
            }
            
            if( argumentSize > 3 ){
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < '+arguments[2]);
                if(arguments[3] != 'Any'){
                    whereStr += ' and '+ arguments[3] +'__c = true';
                }                           
                break;
            }
            
            if( argumentSize > 2 ){
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < ' + arguments[2]);
                break;
            }
            
            if(argumentSize > 1) {
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < 50 ');
            }
        }while(false);
        
        //added for Delete__c
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        whereStr +=' AND Status__c NOT IN (\'Rejected\',\'Deleted\') AND RecordTypeId = \''+aId+'\' AND Approved_Office__c = NULL';
        
        
        //pageStr += ' LIMIT ' + pageSize +' OFFSET '+ (page-1)*pageSize;
        
        String sql = selectStr + whereStr + orderbyStr + pageStr;   
        System.debug('sql = ' + sql);
        List<Office__c> officeList = database.query(sql);
        //DEBUG
        for (Office__c o : officeList) {
            System.debug('$$o = ' + o.Name);
        }

        String countSql = countStr + whereStr;
        System.debug('countSql='+countSql);
        Integer recordCount = database.countQuery(countSql);
        if(officeList != null && officeList.size()>0){
            officeList[0].Address_2__c += '***' +String.valueOf(recordCount);
        }

        System.debug('recordCount = ' + recordCount);

        /*Map<Id, Office__c> accountIdOfficeMap = new Map<Id, Office__c>();
        for (Office__c office : officeList) {
            accountIdOfficeMap.put(office.Specialist__c, office);
        }*/

        Map<Id, List<Office__c>> accountIdOfficeMap = new Map<Id, List<Office__c>>();
        for (Office__c office : officeList) {
            List<Office__c> sOfficeList = new List<Office__c>();
            if(accountIdOfficeMap.containsKey(office.Specialist__c)){
                sOfficeList = accountIdOfficeMap.get(office.Specialist__c);
            }
            sOfficeList.add(office);
            accountIdOfficeMap.put(office.Specialist__c, sOfficeList);
        }

        Map<Id, List<Membership_Accreditation__c>> accountIdAccrediationMap = getAccreditations(accountIdOfficeMap.keySet());

        List<OfficeSurgeon> officeSurgeons = new List<OfficeSurgeon>();
        /*for (Surgeon__c surgeon : getSurgeonsByAccounts(accountIdOfficeMap.keySet())) {
            //Office__c office = accountIdOfficeMap.get(surgeon.Surgeon_Account__c);
            List<Office__c> sOfficeList = accountIdOfficeMap.get(surgeon.Surgeon_Account__c);
            List<Membership_Accreditation__c> memberships = accountIdAccrediationMap.get(surgeon.Surgeon_Account__c);
            for(Office__c office : sOfficeList){
                officeSurgeons.add(new OfficeSurgeon(office, surgeon, memberships));
            }
        }*/

        Map<Id, Surgeon__c> surgeonMap = new Map<Id, Surgeon__c>();
        for (Surgeon__c s : getSurgeonsByAccounts(accountIdOfficeMap.keySet())) {
            surgeonMap.put(s.Surgeon_Account__c, s);
        }

        for (Office__c office : officeList) {
            Id accountId = office.Specialist__c;
            Surgeon__c surgeon = surgeonMap.get(office.Specialist__c);
            List<Membership_Accreditation__c> memberships = 
                (surgeon == null || surgeon.Surgeon_Account__c == null) 
                    ? null : accountIdAccrediationMap.get(surgeon.Surgeon_Account__c);
            officeSurgeons.add(new OfficeSurgeon(office, surgeon, memberships));
        }


        return officeSurgeons;
    } 
    
    global static List<Surgeon__c> getSurgeonsByAccounts(Set<Id> accountIds) {
        List<Surgeon__c> surgeons 
                 = [SELECT Surgeon_Account__c, Phone__c, Name, Gender__c, Email__c, 
                           Amr_Society_of_Metabolic_Bariatric__c, American_College_of_Surgeons_ACS__c, 
                           American_Board_of_Medical_Specialties__c,Outpatient_with_LAP_BAND_System_Surgery__c,
                           Accepts_out_of_town_patients_OOTN__c,Provides_Financing_Options__c,Years_of_LAP_BAND_System_Experience__c,
                           Frequency_of_LAP_BAND_System_Surgeries__c,Bariatrics__c,Live_Seminars__c,Online_Seminars__c,TOTAL_CARE__c,
                           (Select Account__r.Name,Account__c,AccountName_Formula__c From Surgeon_Affiliations__r WHERE Account__c = null) 
                    FROM Surgeon__c 
                    WHERE Surgeon_Account__c IN :accountIds];

        return surgeons;
    }
    
    
    //get accredirations by office Id
    global static Map<Id, List<Membership_Accreditation__c>> getAccreditations(Set<Id> accountIds){
        Map<Id, List<Membership_Accreditation__c>> m = new Map<Id, List<Membership_Accreditation__c>>();
        List<Membership_Accreditation__c> accountMembershipsOrAccreditions = [SELECT Name,Specialist__c 
                                            FROM Membership_Accreditation__c
                                            WHERE Specialist__c IN: accountIds];
        for (Membership_Accreditation__c a : accountMembershipsOrAccreditions) {
            List<Membership_Accreditation__c> l = m.get(a.Specialist__c);
            if (l == null) {
                l = new List<Membership_Accreditation__c>();
                m.put(a.Specialist__c, l);
            }
            l.add(a);
        }                                            
        System.debug('m = ' + m);
        return m;
 
 
    }
    
    /*
    @RemoteAction
    global static List<Office__c> getOfficeByPage(String loc,Integer page) {
        //loc=lantitude:longtitude:miles:language:wheelchair
        String selectStr = 'SELECT Address_1__c,Address_2__c,City__c,Location__Latitude__s,MapLink__c,Location__Longitude__s,' + 
        ' Name,State__c,Zip_Code__c,Country__c,Specialist__c, Specialist__r.Name,Phone__c,Fax__c,Email__c,Wheelchair_Access__c,' +
        ' English__c,Spanish__c,Chinese__c,Japanese__c,French__c,German__c,Vietnamese__c,Italian__c,Korean__c,Russian__c,' +
        ' Website__c,Live_Seminars_Link__c,Online_Seminars_Link__c,Office_Hours__c '
        ' FROM Office__c';
        String countStr = 'SELECT COUNT() FROM Office__c';
        String whereStr = ' where ';                
        String pageStr = '';
        Integer pageSize = Integer.valueOf(System.Label.pagesize);
        List<String> arguments = loc.split(':');
        Integer argumentSize = arguments.size();
        
        if(argumentSize < 2){
            return null;
        }        
        
        String orderbyStr = ' ORDER BY DISTANCE(location__c, GEOLOCATION(' +arguments[0] + ',' +arguments[1]+'), \'mi\' )';
        
        do{
            if( argumentSize > 4){
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < '+arguments[2]);
                if( arguments[4] == 'checked' ){
                    whereStr += ' and Wheelchair_Access__c = true ';
                } 
                
                if(arguments[3] != 'Any'){
                    whereStr += ' and '+ arguments[3] +'__c = true';
                }                           
                break;
            }
            
            if( argumentSize > 3 ){
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < '+arguments[2]);
                if(arguments[3] != 'Any'){
                    whereStr += ' and '+ arguments[3] +'__c = true';
                }                           
                break;
            }
            
            if( argumentSize > 2 ){
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < ' + arguments[2]);
                break;
            }
            
            if(argumentSize > 1) {
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < 50 ');
            }
        }while(false);
        
        //added for Delete__c
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
        ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        whereStr +=' AND Status__c NOT IN (\'Rejected\',\'Deleted\') AND RecordTypeId = \''+aId+'\' AND Approved_Office__c = NULL';
        
        pageStr += ' LIMIT ' + pageSize +' OFFSET '+ (page-1)*pageSize;
        
        String sql = selectStr + whereStr + orderbyStr + pageStr;   
        System.debug('sql = ' + sql);
        List<Office__c> officeList = database.query(sql);
        String countSql = countStr + whereStr;
        System.debug('countSql='+countSql);
        Integer recordCount = database.countQuery(countSql);
        if(officeList != null && officeList.size()>0){
            officeList[0].Address_2__c += '***' +String.valueOf(recordCount);
        }
        System.debug('recordCount = ' + recordCount);
        return officeList;
    } */
    
    @RemoteAction
    global static List<OfficeSurgeon> getOfficeByDrNameByPage(String Name,Integer page) {
        String selectStr = 'SELECT Address_1__c,Address_2__c,City__c,MapLink__c,Location__Latitude__s,Location__Longitude__s,' +
            ' Name,State__c,Zip_Code__c,Website__c,Country__c,Specialist__r.Name,Phone__c,Fax__c,Email__c,Wheelchair_Access__c,' +
            ' English__c,Spanish__c,Chinese__c,Japanese__c,French__c,German__c,Vietnamese__c,Italian__c,Korean__c,Russian__c,' +
            ' Live_Seminars_Link__c,Online_Seminars_Link__c,Office_Hours__c FROM Office__c';
        String whereStr = ' ';
        String pageStr = '';
        Integer pageSize = Integer.valueOf(System.Label.pagesize);
        
        if(Name.indexOf('***') == 0 ){
            Name = Name.substring(3,Name.length());
            whereStr  += 'WHERE Name like\'%'+Name+ '%\'';
        }else{
           // whereStr  += ' WHERE Specialist__C IN (SELECT Surgeon_Account__c FROM Surgeon__c WHERE Name like \'%' + Name + '%\' )';
            whereStr  += 'WHERE Specialist__r.Name like\'%'+Name+ '%\'';
        }
        //added for Delete__c
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        whereStr +=' AND Status__c NOT IN (\'Rejected\',\'Deleted\') AND RecordTypeId = \''+aId+'\' AND Approved_Office__c = NULL';
        
        //pageStr += ' LIMIT ' + pageSize +' OFFSET '+ (page-1)*pageSize;
        
        List<Office__c> officeList = database.query(selectStr+whereStr+pageStr);
        
        String countSql = 'SELECT COUNT() FROM Office__c';
        Integer recordCount = database.countQuery(countSql+whereStr);
        if(officeList != null && officeList.size()>0){
            officeList[0].Address_2__c += '***' +String.valueOf(recordCount);
        }
        //return officeList;

        /*Map<Id, Office__c> accountIdOfficeMap = new Map<Id, Office__c>();
        for (Office__c office : officeList) {
            accountIdOfficeMap.put(office.Specialist__c, office);
        }*/

        Map<Id, List<Office__c>> accountIdOfficeMap = new Map<Id, List<Office__c>>();
        for (Office__c office : officeList) {
            List<Office__c> sOfficeList = new List<Office__c>();
            if(accountIdOfficeMap.containsKey(office.Specialist__c)){
                sOfficeList = accountIdOfficeMap.get(office.Specialist__c);
            }
            sOfficeList.add(office);
            accountIdOfficeMap.put(office.Specialist__c, sOfficeList);
        }

        Map<Id, List<Membership_Accreditation__c>> accountIdAccrediationMap = getAccreditations(accountIdOfficeMap.keySet());

        List<OfficeSurgeon> officeSurgeons = new List<OfficeSurgeon>();
        for (Surgeon__c surgeon : getSurgeonsByAccounts(accountIdOfficeMap.keySet())) {
            //Office__c office = accountIdOfficeMap.get(surgeon.Surgeon_Account__c);
            List<Office__c> sOfficeList = accountIdOfficeMap.get(surgeon.Surgeon_Account__c);
            List<Membership_Accreditation__c> memberships = accountIdAccrediationMap.get(surgeon.Surgeon_Account__c);
            for(Office__c office : sOfficeList){
                officeSurgeons.add(new OfficeSurgeon(office, surgeon, memberships));
            }
        }

        return officeSurgeons;
        
    }
    
    
    @RemoteAction
    global static List<Office__c> getOfficeByDrName(String Name) {
        String q = '';
        if(Name.indexOf('***') == 0 ){
            Name = Name.substring(3,Name.length());
            q = 'SELECT Address_1__c,Address_2__c,City__c,MapLink__c,Location__Latitude__s,Location__Longitude__s,Name,State__c,Zip_Code__c,Website__c,Country__c,Specialist__r.Name,Phone__c,Fax__c,Email__c,Wheelchair_Access__c,English__c,Spanish__c,Chinese__c,Japanese__c,French__c,German__c,Vietnamese__c,Italian__c,Korean__c,Russian__c,Live_Seminars_Link__c,Online_Seminars_Link__c FROM Office__c WHERE Specialist__r.Name like\'%'+Name+ '%\'';
        }else{
            q = 'SELECT Address_1__c,Address_2__c,City__c,MapLink__c,Location__Latitude__s,Location__Longitude__s,Name,State__c,Zip_Code__c,Email__c,Fax__c,Phone__c,Website__c,Country__c,Specialist__r.Name, Wheelchair_Access__c,English__c,Spanish__c,Chinese__c,Japanese__c,French__c,German__c,Vietnamese__c,Italian__c,Korean__c,Russian__c,Live_Seminars_Link__c,Online_Seminars_Link__c FROM Office__c WHERE Name like\'%'+Name+ '%\'';
        }

        //added for Delete__c
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        q +=' AND Status__c NOT IN (\'Rejected\',\'Deleted\') AND RecordTypeId = \''+aId+'\' AND Approved_Office__c = NULL';
        
        List<Office__c> officeList = database.query(q);                
        
        return officeList;
    }
    
    @RemoteAction
    global static List<Office__c> getOfficeByName(String Name) {
        String q = 'SELECT Address_1__c,Address_2__c,City__c,Location__Latitude__s,MapLink__c,Location__Longitude__s,Name,State__c,Zip_Code__c,Country__c,Specialist__r.Name,Phone__c,Fax__c,Email__c,Wheelchair_Access__c,English__c,Spanish__c,Chinese__c,Japanese__c,French__c,German__c,Vietnamese__c,Italian__c,Korean__c,Russian__c,Website__c,Live_Seminars_Link__c,Online_Seminars_Link__c FROM Office__c WHERE Specialist__r.Name like\'%'+Name+ '%\'';
        //added for Delete__c
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        q +=' AND Status__c NOT IN (\'Rejected\',\'Deleted\') AND RecordTypeId = \''+aId+'\' AND Approved_Office__c = NULL';
        List<Office__c> officeList = database.query(q);
        return officeList;
    }
    
    @RemoteAction
    global static Surgeon__c getSurgeonByAccountId(String accountId) {
        Surgeon__c surgeon 
                 = [SELECT Surgeon_Account__c, Phone__c, Name, Gender__c, Email__c, 
                           Amr_Society_of_Metabolic_Bariatric__c, American_College_of_Surgeons_ACS__c, 
                           American_Board_of_Medical_Specialties__c,Outpatient_with_LAP_BAND_System_Surgery__c,
                           Accepts_out_of_town_patients_OOTN__c,Provides_Financing_Options__c,Years_of_LAP_BAND_System_Experience__c,
                           Frequency_of_LAP_BAND_System_Surgeries__c,Bariatrics__c,Live_Seminars__c,Online_Seminars__c,TOTAL_CARE__c,
                           (Select Account__r.Name,Account__c,AccountName_Formula__c From Surgeon_Affiliations__r WHERE Account__c = null) 
                    FROM Surgeon__c 
                    WHERE Surgeon_Account__c=:accountId];
        return surgeon;
    }
    
    @RemoteAction
    global static List<Office__c> getOfficesByAccountId(String accountId) {
    	String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        List<Office__c> accountOffices = [Select Address_2__c, Address_1__c,Website__c, Email__c,Fax__c,Wheelchair_Access__c, 
                                                 Location__Latitude__s,Location__Longitude__s,State__c,Zip_Code__c,Specialist__r.Name,Name,
                                                 English__c,Spanish__c,Chinese__c,Japanese__c,French__c,German__c,Vietnamese__c,Italian__c,
                                                 Korean__c,Russian__c, Phone__c, Specialist__c,Online_Seminars_Link__c,Live_Seminars_Link__c,
                                                 Office_Hours__c,MapLink__c
                                          From Office__c 
                                          WHERE Status__c NOT IN ('Rejected','Deleted') AND Specialist__c =:accountId AND RecordTypeId =:aId AND Approved_Office__c = NULL];//AND Is_Edit_Flag__c= false
        return accountOffices;
    }
    
    @RemoteAction
    global static String getProfileID(String accountId) {
        List<Attachment> profile = [SELECT Id,Name FROM Attachment WHERE ParentId IN (SELECT Id FROM Surgeon__c WHERE Surgeon_Account__c=:accountId) ORDER BY LastModifiedDate DESC];
        if(profile != null && profile.size() > 0){
            return profile[0].Id;
        }
        return 'nopicture';
    }
    
    public PageReference goToDetails(){
        System.debug('officId = ' + officeId);
        PageReference page = new PageReference('/Compare-Lapband');
        page.setRedirect(false);
        return page;
    }
    

    //get surgeon by account id
    public Surgeon__c getSurgeon(){
        Surgeon__c accountSurgeon 
                 = [SELECT Surgeon_Account__c, Phone__c, Display_Name_Formula__c, Surgeon_First_Name__c, Name, Gender__c, Email__c, 
                                 Amr_Society_of_Metabolic_Bariatric__c, American_College_of_Surgeons_ACS__c, 
                                 American_Board_of_Medical_Specialties__c,Outpatient_with_LAP_BAND_System_Surgery__c,
                                 Accepts_out_of_town_patients_OOTN__c,Provides_Financing_Options__c,Years_of_LAP_BAND_System_Experience__c,
                                 Frequency_of_LAP_BAND_System_Surgeries__c,Bariatrics__c,Website__c,Live_Seminars__c,Online_Seminars__c,TOTAL_CARE__c,
                                 (Select Account__r.Name,Account__c,AccountName_Formula__c From Surgeon_Affiliations__r WHERE Account__c = null) 
                          FROM Surgeon__c 
                          WHERE Surgeon_Account__c=:accountId];
        if(accountSurgeon != null){
            specialistName = accountSurgeon.Name;
        }
        curSurgeon = accountSurgeon;
        return accountSurgeon;
    }
    
    
    //get accredirations by office Id
    public List<Membership_Accreditation__c> getAccreditations(){
        List<Membership_Accreditation__c> accountMembershipsOrAccreditions = new List<Membership_Accreditation__c>();
        accountMembershipsOrAccreditions = [SELECT Name,Office__c 
                                            FROM Membership_Accreditation__c
                                            WHERE Office__c =:officeId];
        System.debug(accountMembershipsOrAccreditions);
        return accountMembershipsOrAccreditions;
 
 
    }
    
    
    //get memberships or accredirations by account id
    public List<Membership_Accreditation__c> getMembershipsOrAccreditations(){
        List<Membership_Accreditation__c> accountMembershipsOrAccreditions = new List<Membership_Accreditation__c>();
        accountMembershipsOrAccreditions = [SELECT Name,Specialist__c 
                                            FROM Membership_Accreditation__c
                                            WHERE Specialist__c =:accountId];
        System.debug(accountMembershipsOrAccreditions);
        return accountMembershipsOrAccreditions;
    }
    
    //get offices by account id
    public List<Office__c> getOffices(){
    	String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        List<Office__c> accountOffices = new List<Office__c>();
        accountOffices = [Select Address_2__c, Address_1__c,Website__c, Email__c,Fax__c,Wheelchair_Access__c, 
                          Location__Latitude__s,Location__Longitude__s,City__c,State__c,Zip_Code__c,Specialist__r.Name,Name,
                          English__c,Spanish__c,Chinese__c,Japanese__c,French__c,German__c,Vietnamese__c,Italian__c,
                          Korean__c,Russian__c, Phone__c, Specialist__c,Online_Seminars_Link__c,Live_Seminars_Link__c,
                          Office_Hours__c, MapLink__c, (Select Id, Name From Acceditations__r)
                          From Office__c 
                          WHERE Status__c NOT IN ('Rejected','Deleted') AND Specialist__c =:accountId AND RecordTypeId =:aId  AND Approved_Office__c = NULL];//AND Is_Edit_Flag__c= false
        return accountOffices;
    }
    
    //get profile by surgeon
    public  String getProfileId(){  
        List<Attachment> profile = [SELECT Id,Name FROM Attachment WHERE ParentId IN (SELECT Id FROM Surgeon__c WHERE Surgeon_Account__c=:accountId) ORDER BY LastModifiedDate DESC LIMIT 1];
        system.debug('profile=='+profile);
        if(profile != null && profile.size() > 0){
            return profile[0].Id;
        }
        return 'nopicture';
    }       
    
    public PageReference getOfficeByName (){
        String q = '';
        if(practiceName != null && practiceName != ''){
            q = 'SELECT Address_1__c,Address_2__c,City__c,Location__Latitude__s,MapLink__c,Location__Longitude__s,Name,State__c,Zip_Code__c,Email__c,Fax__c,Phone__c,Website__c,Country__c,Specialist__r.Name FROM Office__c WHERE Specialist__r.Name like\'%' + practiceName + '%\'';
        }
        else{
            q = 'SELECT Address_1__c,Address_2__c,City__c,Location__Latitude__s,MapLink__c,Location__Longitude__s,Name,State__c,Zip_Code__c,Email__c,Fax__c,Phone__c,Website__c,Country__c,Specialist__r.Name FROM Office__c WHERE Specialist__C IN (SELECT Surgeon_Account__c FROM Surgeon__c WHERE Name like \'%' + specialistName + '%\')';
        }
        //added for Delete__c
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        q +=' AND Status__c NOT IN (\'Rejected\',\'Deleted\') AND RecordTypeId = \''+aId+'\' AND Approved_Office__c = NULL';
        officeList = database.query(q);
        string sHref = '';        
        firstLat = 0.0;
        if(officeList != null && officeList.size()>0){
            dataStr = ''; Integer index = 1;
            for(Office__c o : officeList){
                if(firstLat == 0.0){firstLat = o.Location__Latitude__s;firstLong = o.Location__Longitude__s;}
                dataStr += '[\"<a class=\'mapNameLink\' id=\'mapIcon_' + o.id + '\'>' + o.Specialist__r.Name + '</a><br/><strong>'  
                         + o.Name + '</strong><br>' + o.Address_1__c + '<br />' + o.City__c + ',' + o.State__c + ','
                         + o.Zip_Code__c + '\",' + o.Location__Latitude__s + ',' + o.Location__Longitude__s + ',' + index +
                         ',\"' + o.Name +'\"]';
                dataStr += ',';
                index ++;
            }
            dataStr = dataStr.substring(0,dataStr.length()-1)  ;
        }
        system.debug('*******************');
        return null;
    }

    public void createSiteEventDoctor(){
        insertSiteEvent('Doctor');
    }

    public void createSiteEventWebsite(){
        insertSiteEvent('Website');
    }

    private void insertSiteEvent(String Type){
        Site_Event__c sevent = new Site_Event__c();
        Surgeon__c s = getSurgeon();
        sevent.Surgeon_Name__c = s.Name;
        sevent.Surgeon_Account__c = s.Surgeon_Account__c;
        sevent.Count__c = 1;
        sevent.Type__c = Type;
        insert sevent;
    }    

}
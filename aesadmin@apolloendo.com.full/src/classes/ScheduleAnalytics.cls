global class ScheduleAnalytics implements Schedulable{
  global void execute(SchedulableContext sc){
   ScheduleWebSiteLoadAnalytics cs = new ScheduleWebSiteLoadAnalytics();
   Database.executeBatch(cs);
  }

}
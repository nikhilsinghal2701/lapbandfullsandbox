public with sharing class DataChangeRequestController {
	public Data_Change_Request__c dcrRec;
	public Boolean showAcct {get; set; }
	public Boolean showSurg {get; set; }
 	public Boolean isRemoveDup {get; set; }
 	private final String CONST_REM_DUP = 'Remove Duplicate';
 	
	public DataChangeRequestController (ApexPages.StandardController sController) {
		dcrRec = (Data_Change_Request__c)sController.getRecord();
		
		if (dcrRec.Account__c == null) dcrRec.Account__c = ApexPages.currentPage().getParameters().get('acctID');
		if (dcrRec.Surgeon__c == null) dcrRec.Surgeon__c = ApexPages.currentPage().getParameters().get('surgID');
		if (dcrRec.Requestor__c == null) dcrRec.Requestor__c = UserInfo.getUserID();
	
		showAcct = (dcrRec.Account__c != null);
		showSurg = (dcrRec.Surgeon__c != null);
		
		if (dcrRec.Requested_Action__c == CONST_REM_DUP) isRemoveDup = true;
		if (dcrRec.Status__c == null) dcrRec.Status__c = 'Open';
	}

	public PageReference evalRemoveDup() {
		isRemoveDup = (dcrRec.Requested_Action__c == CONST_REM_DUP);
		return null;
	}
}
/* Created: 06/23/2014
 * Author: Sanjay Sankar
 * Depends on: SalesReportStageUtility.cls
 * Test Class(es): SalesReportStageUtilityTest.cls
 * Purpose: To truncate and reload the Sales Report Stage object
 */

global with sharing class ScheduleSalesReportStageUpdate implements Schedulable {
	global void execute (SchedulableContext sc) {
		SalesReportStageUtility.buildSalesReportStage();
	}
}
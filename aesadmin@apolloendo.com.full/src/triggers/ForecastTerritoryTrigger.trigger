trigger ForecastTerritoryTrigger on Forecast_Territory__c (before insert, before update) {
	if (Label.ForecastTerritoryTrigger == 'Active') {
		ForecastTerritoryTriggerMethods.setCalculatedFields(trigger.new, trigger.old, trigger.isInsert, trigger.isUpdate);
	}
}
/**
Class: LWCDocumentTableControllerTest
Purpose: Test Class for LWCDocumentTableController
Author – Wade Liu
Date – 03/06/2014
*/
@isTest
private class LWCDocumentTableControllerTest {

    static testMethod void testConstruct() {
        // TO DO: implement unit test
        List<AWS_S3_Object__c> lstAWSDocs = TestUtil.createDocs(5, true, new Map<String,Object>{'Content_Type__c'=>'pdf','File_Name__c'=>'testfile'});
        if(lstAWSDocs != null && lstAWSDocs.size() > 4 ){
        	lstAWSDocs[1].Content_Type__c = 'image/gif';
        	lstAWSDocs[2].Content_Type__c = 'video/x-ms-wmv';
        	lstAWSDocs[3].Content_Type__c = 'text/plain';
        	lstAWSDocs[4].Content_Type__c = 'application/zip';
        	update lstAWSDocs;
        
		    PageReference currentPageRef = new PageReference('/apex/' + LWCConstantDeclaration.LWCDOCUMENTLIBRARY);
		    Test.setCurrentPage(currentPageRef);
		    
		    test.startTest();
		    LWCDocumentTableController oDocumentTableController = new LWCDocumentTableController();
		    if(oDocumentTableController.docWrapperList != null){
		    	List<LWCDocumentTableController.DocumentWrapper> lstDocWrapper = oDocumentTableController.docWrapperList;
		    	system.assert(lstDocWrapper[0].iconPosition == '0px');
		    	system.assert(lstDocWrapper[1].iconPosition == '-480px');
		    	system.assert(lstDocWrapper[2].iconPosition == '-720px');
		    	system.assert(lstDocWrapper[3].iconPosition == '-120px');
		    	system.assert(lstDocWrapper[4].iconPosition == '-600px');
		    }
		    test.stopTest();   
		    return;
        }
        system.assert(false);
    }
    
    static testMethod void testCreatesDownloadLink() {
    	List<AWS_S3_Object__c> lstAWSDocs = TestUtil.createDocs(2, true, new Map<String,Object>{'Content_Type__c'=>'pdf','File_Name__c'=>'testfile','Bucket_Name__c'=>'testbucket'});
    	List<AWSKey__c> lstAWSKey = TestUtil.createKeys(1, true, null);
    	PageReference currentPageRef = new PageReference('/apex/' + LWCConstantDeclaration.LWCDOCUMENTLIBRARY);
		Test.setCurrentPage(currentPageRef);
		LWCDocumentTableController oDocumentTableController = new LWCDocumentTableController();
    	if(lstAWSDocs != null && lstAWSDocs.size() > 1){
    		lstAWSDocs[1].Access__c = 'public-read';    
    		lstAWSDocs[1].File_Name__c = 'test file';	
	    	update lstAWSDocs;	    	
	    	
	    	if(lstAWSKey != null && lstAWSKey.size()>0){
	    		test.startTest();	    			    	
	    		
		    	oDocumentTableController.s3ObjId = lstAWSDocs[0].Id;
		    	PageReference testPageRef = oDocumentTableController.createsDownloadLink();
		    	system.assert(testPageRef.getUrl() != '');
		    	
		    	oDocumentTableController.s3ObjId = lstAWSDocs[1].Id;
		    	testPageRef = oDocumentTableController.createsDownloadLink();
		    	system.assert(testPageRef.getUrl() != '');
		    	
		    	test.stopTest();
		    	return;
	    	}	    	
    	}
    	system.assert(false);
    }
    
    static testMethod void testCalculateSignature(){
    	
    }
    
}
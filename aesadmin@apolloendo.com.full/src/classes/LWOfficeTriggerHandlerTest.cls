/*
Class: LWOfficeWrapper
Purpose: Test Class for trigger LWOfficeTriggerHandler
Author: Chris
Created DAte: 03/07/2014
*/
@isTest
public with sharing class LWOfficeTriggerHandlerTest {
	
	@isTest static void testHandlerOnAfterInsert(){
                                                                
		Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
		Id idOfficeRecordType = Office__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Pending Office').getRecordTypeId();
		Account acc = TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon})[0];
		
		
		Office__c office = new Office__c();
		office.Specialist__c = acc.Id;
		office.RecordTypeId = idOfficeRecordType;
		office.Status__c = 'Submitted';
		office.Zip_Code__c = '94536';
		office.Name = 'Dr Office 5';
		office.Address_1__c = '38881 Hastings Street';
		office.City__c = 'Fremont';
		office.State__c = 'CA';
		
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new LWLocationCalloutsMockTest());
		
		Insert office;
		Test.stopTest();
		/*Test.startTest();
		List<Office__c> offices = TestUtil.createOffices( 1, true, new Map<String, Object>{'Specialist__c'=> acc.Id,
																						   'RecordTypeId'=> idOfficeRecordType,
																						   'Status__c'=>'Submitted',
																						   'Zip_Code__c'=>'94536',
																						   'Name'=>'Dr Office 4',
																						   'Address_1__c'=>'38881 Hastings Street',
																						   'City__c'=>'Fremont',
																						   'State__c'=>'CA'}
																						   );
		Test.stopTest();	*/																			   
	 office = [SELECT Location__Latitude__s,Location__Longitude__s,Zip_Code__c FROM Office__c][0];	
																	
		system.debug(office.Location__Latitude__s);
		system.debug(office.Zip_Code__c);
		system.assertNotEquals(null,office.Location__Latitude__s );	
		system.assertNotEquals(null, office.Location__Longitude__s);
			
		
		
	}
	
	static testMethod void testHandlerOnAfterUpdate(){
		Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
		Id idOfficeRecordTypePending = Office__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Pending Office').getRecordTypeId();
		Id idOfficeRecordTypeApproved = Office__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Approved Office').getRecordTypeId();
		List<Account> lstAccounts = TestUtil.createAccounts(1, true, new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon} );
		if( lstAccounts != null && lstAccounts.size() == 1){
			List<Office__c> lstPendingOffices = TestUtil.createOffices(1, true, new Map<String,Object>{'Specialist__c'=>lstAccounts[0].Id,'RecordTypeId'=>idOfficeRecordTypePending});
			List<Office__c> lstApprovedOffices = TestUtil.createOffices(2, true, new Map<String,Object>{'Specialist__c'=>lstAccounts[0].Id,'RecordTypeId'=>idOfficeRecordTypeApproved});
			lstPendingOffices[0].Approved_Office__c = lstApprovedOffices[1].Id;
			update lstPendingOffices[0];
			
			test.startTest();
			lstApprovedOffices[0].Address_2__c = 'test address2 update';
			lstApprovedOffices[0].Is_Edit_Flag__c = false;
			lstApprovedOffices[0].Copy_Address__c = false;
			update lstApprovedOffices[0];
			
			lstPendingOffices[0].Website__c = 'www.test.com';
			update lstPendingOffices[0];
			test.stopTest();
		}
	}
	
	static testMethod void testHandlerOnBeforeUpdate(){
	
	}
	
}
/* Created: 01/27/2015
 * Author: Sanjay Sankar
 * Dependency: SurgeonViewVF.page 
 * Test Class(es): SurgeonViewControllerTest.cls
 * Purpose: Controller for SurgeonViewVF.page - this overrides the standard View page for Surgeon__c. 
 *          Created to provide visibility to Offices that are linked to the related Surgeon Account
 */

public with sharing class SurgeonViewController {
	public List<Office__c> officeList {get; set; }
	public Boolean hasOffices {get; set; }
	public Surgeon__c surgeon {get; set; }
	public String specialistID {get; set; }
	public String specialistName {get; set; }
	
	public SurgeonViewController (ApexPages.StandardController controller) {
		if (!Test.isRunningTest()) { // Adding fields to the controller is not supported in test classes
			controller.addFields(new List<String> {'Surgeon_Account__c'});
			controller.addFields(new List<String> {'Surgeon_Account__r.Name'});
		}
		this.surgeon = (Surgeon__c)controller.getRecord();
		specialistID = surgeon.Surgeon_Account__c;
		specialistName = surgeon.Surgeon_Account__r.Name; 
		officeList = [select Id, Name, Specialist__r.Name, Status__c, Address_1__c, Address_2__c, Phone__c from Office__c where Specialist__c = :surgeon.Surgeon_Account__c];
		this.hasOffices = officeList.size() > 0;
	}
}
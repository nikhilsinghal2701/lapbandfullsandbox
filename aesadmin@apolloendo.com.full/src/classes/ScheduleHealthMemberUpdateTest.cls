/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ScheduleHealthMemberUpdateTest {

	public static String CRON_EXP = '0 0 0 15 3 ? 2050';

    static testMethod void testSchedule() {
        Test.startTest();
        
        //Schedule the test job
        String jobId = System.schedule ('ScheduleHealthMemberUpdateTest', CRON_EXP, new ScheduleHealthMemberUpdate());
        
        Crontrigger ct = [select Id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where Id = :jobId];
        
        //Verify expressions are same
        System.assertEquals (CRON_EXP, ct.CronExpression);
        
        //Verify job has not run
        System.assertEquals (0, ct.TimesTriggered);
        
        //Verify the next time the job will run
        System.assertEquals ('2050-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        
        Test.stopTest();
    }
    
    static testMethod void testBatch() {
    	//create users
		List<user> userlist = new List<User>();
		userList = TestUtil.createUsers(3, false, null);
		userList[0].Username = 'admin.perficient@apolloendo.com.' + Math.random();
		userList[1].Username = 'HealthMemberUpdateTest@apolloendo.com.' + Math.random();
		userList[2].Username = 'integration@apolloendo.com.' + Math.random();
		
		insert userList;
    	
    	//get record type ID for Account Ship To
    	Schema.DescribeSObjectResult sObjDescr = Schema.SObjectType.Account;
    	Map<String, Schema.RecordTypeInfo> accRecTypeInfo = sObjDescr.getRecordTypeInfosByName();

    	//create account
    	Account a1 = TestUtil.createAccounts(1, false, null)[0];
    	a1.RecordTypeId = accRecTypeInfo.get('Ship To').getRecordTypeId();
    	insert a1;

    	//create account team members
    	List<AccountTeamMember> teamList = new List<AccountTeamMember>();
    	Map<String, Object> teamNameValueMap = new Map<String, Object>();

    	teamNameValueMap.put ('AccountId', a1.Id); 
    	teamNameValueMap.put ('TeamMemberRole', 'Health');
    	teamNameValueMap.put ('UserId', userList[0].Id);

    	teamList = TestUtil.createAccountTeamMembers(3, false, teamNameValueMap);
    	teamList[1].UserId = userList[1].Id;
    	teamList[2].UserId = userList[2].Id;

    	insert teamList;
    	
		Test.startTest();

		BatchUpdateHealthMember hmUpdate = new BatchUpdateHealthMember();
		Database.executeBatch(hmUpdate);

		Test.stopTest();

        Account a2 = (Account) [select Id, Health_Member__c, RecordType.Name from Account where Id = :a1.Id];
        System.assertEquals (userList[1].Id, a2.Health_Member__c);
    }
}
<apex:page applyBodyTag="false" showheader="false" standardStylesheets="false" sidebar="false" >
<!--  <apex:composition template="LWTemplate"/> -->
    <head>
        <title>Is LAP-BAND&#174; SYSTEM Right for You?</title>
    </head>
    
    <c:LWTemplate />
 
    <body>
        <div id="container">
            <c:LWHeader />
             <div id="content" class="inner-page">
                <div id="inner-page-content">
                    <ul id="breadcrumbs">
                        <li>
                        <apex:outputLink value="{!URLFOR($Page.LWLearnAboutLapband,null,null)}" >How the LAP-BAND<sup>&#174;</sup> System Works</apex:outputLink>
                        <!--<a href="lapband_LearnAboutLapband" class="active">How LAP-BAND<sup>&#174;</sup> Works</a>-->
                        </li>
                        <li>
                            <apex:outputLink value="{!URLFOR($Page.LWLosingWeight,null,null)}">Losing weight with the LAP-BAND<sup>&#174;</sup> System</apex:outputLink>
                            <!--<a href="lapband_LosingWeight">Losing Weight With LAP-BAND<sup>&#174;</sup></a>-->
                        </li>
                        <li>
                            <apex:outputLink value="{!URLFOR($Page.LWIsForYou,null,null)}" styleClass="active">Is the LAP-BAND<sup>&#174;</sup> System Right for You?</apex:outputLink>
                            <!--<a href="lapband_IsForYou">Is LAP-BAND<sup>&#174;</sup> Right for You?</a>-->
                        </li>
                        <li>
                            <apex:outputLink value="{!URLFOR($Page.LWAffordingLapband,null,null)}">Affording the LAP-BAND<sup>&#174;</sup> System</apex:outputLink>
                            <!--<a href="lapband_AffordingLapband">Affording LAP-BAND<sup>&#174;</sup></a>-->
                        </li>
                    </ul>         
                    <h1 class="lapband-is-for-you">Is LAP-BAND<sup>&#174;</sup> SYSTEM Right for You?</h1>
                    <div id="content-left">
                        <p class="blueText">We hope you can be the next successful LAP-BAND<span style="vertical-align: 35%; font-size: 50%">®</span> System patient. But before you take the next step, you have to know if you’re medically qualified. You need to be healthy enough for the minimally invasive procedure. While only your physician or surgeon can give you a complete medical evaluation, the information listed here should get you off to a good start.</p> 
                        <p>The LAP-BAND<sup>&#174;</sup> System is a minimally invasive procedure that has helped hundreds of thousands of people lose weight.<sup>2</sup> But be aware that there are reasons why you may not be a candidate.</p>

                        <div class="content-left-2column">
                            <h3>The LAP-BAND<sup>&#174;</sup> System may be right for <br />you if:<sup>1</sup></h3>
                            <ul>
                                <li>Your Body Mass Index (BMI) is at least 40, or</li>
                                <ul>
                                    <li>you are at least 30 pounds overweight with a BMI of at least 30 kg/m&#178; with one or more obesity related comorbid condition. You can determine your BMI using the <a id="bmi-calculator-link" href="javascript:void(0);">BMI Calculator</a>.</li>
                                </ul>
                            </ul>
                            <div id="lapband-is-for-you-bmi-calculator">
                                <h3>BMI Calculator</h3>
                                <div id="lapband-is-for-you-bmi-calculator-content">
                                    <form name="bmi_calculator_rightForYou" id="bmi_calculator_rightForYou" method="" action="">
                                        <fieldset>
                                            <span>
                                                <label class="primary" for="">Height:</label>
                                                <input type="text" class="text" name="bmi_calculator_height_feet" id="bmi_calculator_height_feet" value="" onchange="calculateBMI();" />
                                                <label for="bmi-calculator-height-feet">Feet</label>
                                                <input type="text" class="text" name="bmi_calculator_height_inches" id="bmi_calculator_height_inches" value="" onchange="calculateBMI();" />
                                                <label for="bmi_calculator_height_inches">Inches</label>
                                            </span>
                                            <span>
                                                <label class="primary">Weight:</label>
                                                <input type="text" class="text" name="bmi_calculator_weight" id="bmi_calculator_weight" value="" onchange="calculateBMI();" />
                                                <label for="bmi_calculator_weight">Pounds</label>
                                            </span>
                                            <span>
                                                <input type="checkbox" class="checkbox" name="18OrOlder" id="18OrOlder" value="" />
                                                <label for="18OrOlder">I confirm that I am 18 or older.</label>
                                            </span>
                                            <span>
                                                <input type="submit" class="submit" />
                                            </span>
                                        </fieldset>
                                    </form>
                                    <div id="lapband-is-for-you-bmi-calculator-results">
                                        <p><strong>Your BMI is <span id="BMI" class="bmi-results"></span></strong></p>
                                        <p id="bmi-results-advice"></p>
                                        <p id="request-info-kit"><a href="../info-kit.html">Request free information kit</a></p>
                                        <p class="reference">Keep in mind that only a qualified physician or surgeon will properly evaluate all the factors in your case. This result is a guideline for you and can help you make the decision to seek more information about weight loss surgery using the LAP-BAND<sup>&#174;</sup> System.</p>
                                    </div>
                                </div>
                            </div>
                            <ul>
                                <li>You are at least 18 years old.</li>
                                <li>Your serious weight loss attempts have had only short-term success.</li>
                                <li>You are not currently suffering from any other disease that may have caused your excess weight.</li>
                                <li>You are prepared to make major changes in your eating habits and lifestyle.</li>
                                <li>You do not drink alcohol in excess.</li>
                                <li>You are not currently pregnant. (Patients who become pregnant after band placement may require deflation of their bands.)</li>
                            </ul>
                        </div> <!-- content-left-2column -->
                        <div class="content-left-2column">
                            <h3>The LAP-BAND<sup>&#174;</sup> System is not right for <br />you if:<sup>1</sup></h3>
                            <ul>
                                <li>You have an inflammatory disease or condition of the gastrointestinal tract, such as ulcers, severe esophagitis, or Crohn&rsquo;s disease.</li>
                                <li>You have severe heart or lung disease, or any other disease that makes you a poor candidate for any surgery.</li>
                                <li>You have a problem that could cause bleeding in the esophagus or stomach. That might include esophageal or gastric varices (dilated veins). It might also be something such as congenital or acquired intestinal telangiectasia (dilation of a small blood vessel).</li>
                                <li>You have portal hypertension.</li>
                                <li>Your esophagus, stomach, or intestine is not normal (congenital or acquired). For instance, you might have a narrowed opening.</li>
                                <li>You have/have experienced an intraoperative gastric injury, such as a gastric perforation at or near the location of the intended band placement.</li>
                                <li>You have cirrhosis.</li>
                                <li>You have chronic pancreatitis.</li>
                                <li>You are addicted to alcohol or drugs.</li>
                                <li>You have an infection anywhere in your body, or one that could contaminate the surgical area.</li>
                                <li>You are on chronic, long-term steroid treatment.</li>
                                <li>You might be allergic to materials in the device.</li>
                                <li>You cannot tolerate pain from an implanted device.</li>
                                <li>You or someone in your family has an autoimmune connective tissue disease, such as systemic lupus erythematosus or scleroderma. The same is true if you have symptoms of one of these diseases.</li>
                            </ul>
                        </div>
                    </div>
                    <c:LWLearnLapbandRighTrail />
                 </div>
             </div> <!-- content -->
            <!-- <c:LWMapOnFoot /> -->
             <c:LWFooter />
         <div id="footer" style="background:none !important;">
        <div class="footer-references">
                <h5>References:</h5>
                <ol>
                    <li>Directions For Use (DFU). LAP-BAND AP<sup>&#174;</sup> Adjustable Gastric Banding System with OMNIFORM<sup>&#174;</sup> Design. Allergan, Inc. Irvine, CA. 11/12.</li>
                    <li>Data on File. Allergan, Inc. Irvine, CA. Historical Sales Summary. December, 2012.</li>
                </ol>
            </div>
        </div>
         <c:LWFooter2 />
        </div>
 
        <script type="text/javascript">
           $(document).ready(function () {
                $('#how-lapband-works-diagram span').mouseover(function () {
                   var diagramLinkId = $(this).attr('id'),
                   textId = diagramLinkId.substr(diagramLinkId.length - 2);
                   $(this).parent().removeClass();
                   $(this).parent().addClass(diagramLinkId);
                   $('.deviceText.current').hide().removeClass('current');
                   $('.deviceText#how-lapband-works-text' + textId).show().addClass('current');
               });
               
            $('#bmi-calculator-link').click(function () {
                $('#lapband-is-for-you-bmi-calculator').slideDown();
            });
           
               $('#bmi_calculator_rightForYou input').val('');
               $("#bmi_calculator_rightForYou").validate({
                   messages: {
                       bmi_calculator_height_feet: {
                           number: ''
                       },
                       bmi_calculator_height_inches: {
                           number: ''
                       },
                       bmi_calculator_weight: {
                           number: ''
                       }
                   },
                   rules: {
                       bmi_calculator_height_feet: {
                           required: true,
                           number: true
                       },
                       bmi_calculator_height_inches: {
                           required: true,
                           number: true
                       },
                       bmi_calculator_weight: {
                           required: true,
                           number: true
                       },
                       birthday_month: {
                           required: true
                       },
                       '18OrOlder': {
                           required: true
                       }
                   },
                   submitHandler: function (form) {
                       $('#lapband-is-for-you-bmi-calculator-results').slideDown();
                       $('#bmi-results-advice').empty();
                       var currentBMI = $('#BMI').html();
                       var BMIh = $('#bmi_calculator_height_feet').val() + "ft " + $('#bmi_calculator_height_inches').val() + "in";
                       var BMIw = $('#bmi_calculator_weight').val();
                       siteCatalystRightForYouBMI(BMIh, BMIw, currentBMI);
                       if (currentBMI > 30) {
                           $('#bmi-results-advice').html('Based on the information you entered you may qualify as LAP-BAND<sup>&#174;</sup> System candidate.')
                           $('#request-info-kit').show();
                       }
                       else {
                           $('#bmi-results-advice').html('Based on the information you entered you do not qualify as a LAP-BAND<sup>&#174;</sup> System candidate.')
                           $('#request-info-kit').hide();
                       }
                   }
               });
           });
        </script>
    </body>

</apex:page>
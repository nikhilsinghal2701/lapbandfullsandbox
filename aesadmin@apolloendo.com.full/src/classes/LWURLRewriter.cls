/*****************************************************
Class: LWURLRewriter
This class is rewrite the vf page url to customized url
Author – Chris Cai
Date – 1/20/2014
Revision History
******************************************************/
global with sharing class LWURLRewriter implements Site.UrlRewriter {

    global PageReference mapRequestUrl(PageReference myFriendlyUrl){
        
        //Get the friendly URL for one the page which you access to.
        String url = myFriendlyUrl.getUrl(); 
        if( url.equals('/')){
            return null;
        
        }
        
        system.debug(url);
        Map<String,String> lapbandFriendlyURLToVisualforcePageURLMap = new Map<String,String>();
        //Get the map from Friendly URL tovisualforce Page URL from Custom Settings 
        for(LWURLRewriter__c  lapbandURLRewriter : LWURLRewriter__c.getAll().values()){
            lapbandFriendlyURLToVisualforcePageURLMap.put(lapbandURLRewriter.LapbandFriendlyURL__c,lapbandURLRewriter.VisualforcePageURL__c);
        }
        system.debug('--URL--'+url);
        Integer index = url.indexOf('|');
        if(index > -1){
            String temp = lapbandFriendlyURLToVisualforcePageURLMap.get(url.substring(0,index));
            system.debug('--Lead'+url.substring(index+1,index+4));
            Integer endIndex = (url.indexOf('tab') > -1) ? (url.indexOf('tab') -1 ) : url.length();
            if(url.substring(index+1,index+4) == '00Q'){
               url = temp + '?lid=' + url.substring(index+1,endIndex);
            }else{
               url = temp + '?accountId=' + url.substring(index+1,endIndex);
            }
            system.debug('-------url'+url);
            return new PageReference(url);
        }else{
            for(LWURLRewriter__c  lapbandURLRewriter : LWURLRewriter__c.getAll().values()){
                lapbandFriendlyURLToVisualforcePageURLMap.put(lapbandURLRewriter.LapbandFriendlyURL__c.toLowerCase(),lapbandURLRewriter.VisualforcePageURL__c);
            }
            if(lapbandFriendlyURLToVisualforcePageURLMap.containsKey(url.toLowerCase())){
                return new PageReference(lapbandFriendlyURLToVisualforcePageURLMap.get(url.toLowerCase()));      
            }else{
                if(url.endsWith('/')){
                    system.debug(url.substring(1,2)+'==url');
                    return new PageReference(lapbandFriendlyURLToVisualforcePageURLMap.get(url.substring(0,url.length()-1).toLowerCase()));
                }else if(url.contains('?')){
                    System.debug('url contains?');
                    //fix for google ads and parameters
                    Integer reI = url.indexOf('?');
                    String fURL = url.substring(0,reI);
                    String param = url.substring(reI+1,url.length());
                    String redirect = '';
                    System.debug('furl = ' + fURL);
                    if(fURL.equals('/')||fURL.equals('')) {
                        redirect = '';
                    }   
                    else if(fURL.endsWith('/')){
                        fURL = fURL.substring(0,fURL.length()-1);
                        redirect = lapbandFriendlyURLToVisualforcePageURLMap.get(fURL).toLowerCase();
                    }
                    else {
                        if (lapbandFriendlyURLToVisualforcePageURLMap.get(fURL) != null) {
                            redirect = lapbandFriendlyURLToVisualforcePageURLMap.get(fURL).toLowerCase();
                            } else {
                                redirect = fURL;
                            }
                        
                    }    
                    return new PageReference(redirect+'?'+param);
                }
                else{
                    return null;
                }                  
            } 
                 
        }
    
     }
    
    global List<PageReference> generateUrlFor(List<PageReference> 
            mySalesforceUrls){
            system.debug('***'+mySalesforceUrls);
         //A list of pages to return after all the links have been evaluated
        List<PageReference> myFriendlyUrls = new List<PageReference>();
        //Get the map for visualforce page to Friendly URL
        Map<String,String> visualforcePageURLToLapbandFriendlyURLMap = new Map<String,String>();
        
        //Get the map from visualforce Page URL to Friendly URL from Custom Settings  
        for(LWURLRewriter__c lapbandURLRewriter : LWURLRewriter__c.getAll().values()){
            visualforcePageURLToLapbandFriendlyURLMap.put(lapbandURLRewriter.VisualforcePageURL__c,
                                                            lapbandURLRewriter.LapbandFriendlyURL__c);
            system.debug('=='+lapbandURLRewriter.VisualforcePageURL__c+'=='+lapbandURLRewriter.LapbandFriendlyURL__c);
        }
        
        
        Integer index = -1 ;
        String temp = '',url='';        
        for(PageReference mySalesforceUrl : mySalesforceUrls) {
            //Get the URL of the visualforce page
            url = mySalesforceUrl.getUrl();
            
            index = url.indexOf('?');
            if(index > -1){
                system.debug('----------'+url);
                temp = url.substring(index,url.length());
                url = url.substring(0,index);
                temp = temp.substring((temp.indexOf('=') + 1),temp.length());
                url = visualforcePageURLToLapbandFriendlyURLMap.get(url)  + '|' + temp;
                system.debug('--------temp'+temp);
            }
            else{
                url = visualforcePageURLToLapbandFriendlyURLMap.get(url) ;
            }
            myFriendlyUrls.add( new PageReference(url) );
            
         /*   for(String visualforcePageURL : visualforcePageURLToLapbandFriendlyURLMap.keySet()){
                if(url.equals( visualforcePageURL )){
                    if(index > -1){
                        url = visualforcePageURLToLapbandFriendlyURLMap.get( visualforcePageURL ) ;
                        url += temp;
                        myFriendlyUrls.add(new PageReference( url ) );
                    }else{
                        myFriendlyUrls.add(new PageReference( visualforcePageURLToLapbandFriendlyURLMap.get( visualforcePageURL ) ) );
                    }
                } 
            }*/
       
        }
        
        //Return the full list of pages
        system.debug('-----'+myFriendlyUrls);
        return myFriendlyUrls;
  }
}
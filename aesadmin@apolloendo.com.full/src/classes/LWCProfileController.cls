/**
Class: LWCProfileController
Controller for LWCProfile page 
Author – Vinay L
Date – 02/06/2014
*/
public with sharing class LWCProfileController {
    public LWCProfileController selfRef {get;set;}
    public Surgeon__c surgeon {get;set;}
    public String gender = null;
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}  
    public String fieldId {get;set;}
    public String deleteOfficeId    {get; set;}
    public Integer deleteIndex {get;set;}
    //css  top
    public Integer deleteTop {get;set;}
    
    public Integer formChange {get;set;}
    public List<LWCMembership> listOfMemAcc {get;set;}
    public List<LWCHospital> listOfHosAcc {get;set;}
    public Integer remMemId {get;set;}
    public Integer remHosId {get;set;}
    public LWCMembership newMem {get;set;}
    public LWCHospital newHos   {get;set;}
    public Id accId {get;set;}
    
    public List<Office__c> offices {get;set;}
    public Map<Id,List<Membership_Accreditation__c>> mapOfAccreditation {get;set;}
    
    public LWOfficeWrapper activeOffice {get;set;}
    public boolean displayPopup {get; set;}
    public boolean cancelPopup  {get; set;}
    public boolean previewPopup {get; set;}
    public boolean deletePopup {set; get;}
    
    public Integer officeNum {get;set;}
    public Contact c         {get;set;}
    
    public String curOffice {get;set;}
    public List<LWCMembership> newMemOffList               {get;set;}
    
    /*  Boolean parameters to set permissions of office staff users  */
    public boolean isProfileEdit {get;set;}
    public boolean isOfficesEdit {get;set;}
    public boolean isOfficeStaffEdit {get;set;}
    
    public Boolean pendingFlag {get;set;}
    public ID pendingId	{set;}
    
    public ID getPendingId(){
    	String recodeTypeName = LWCConstantDeclaration.PENDINGOFFICERECORD;
    	ID pId = LWCWithoutSharing.getRecordTypeId(new List<String>{recodeTypeName})[0].Id;
    	return pId;
    }
    

    //Websites need to support www.[website].com and not require http://
    public String getWebURL(){
        String webURL = surgeon.Website__c;
        if(webURL!=null){
            webURL.toLowerCase();
            if(!webURL.contains('http'))    webURL = 'http://'+webURL;
        }
        return webURL;
    }
    public String getLiveURL(){
        String liveURL = surgeon.Live_Seminars__c;
        if(liveURL!=null){
            liveURL.toLowerCase();
            if(!liveURL.contains('http'))   liveURL = 'http://'+liveURL;
        }
        return liveURL;
    } 
    public String getOnlineURL(){
        String onlineURL = surgeon.Online_Seminars__c;
        if(onlineURL!=null){
            onlineURL.toLowerCase();
            if(!onlineURL.contains('http')) onlineURL = 'http://'+onlineURL;
        }
        return onlineURL;
    }  
    
    //popup window for add and edit office record
    public void showPopup() {
        newMemOffList = new List<LWCMembership>();
        activeOffice = new LWOfficeWrapper(accId);
        if(curOffice != null){
            for(Office__c o :offices){
                if(o.Id == curOffice){
                    activeOffice.office = o;
                }
            }
        }
        curOffice = null;
        displayPopup = true;
        loadMembershipAccr();    
    }
    
    //popup window for delete panel
    public void showDelete(){
        deletePopup = true;
        deleteTop = 100+ (deleteIndex-1)*250;
    }
    
    //close delete panel
    public void closeDelete(){
        deletePopup = false;
    }
    
    //delete office record, set status to Deleted
    public Pagereference deleteOffice() {
        LWCWithoutSharing.deleteOffice(deleteOfficeId);
        closeDelete();
        return null;
    } 
       
    public void loadMembershipAccr(){
        //Load Membership
        Id masterOffId ;// Office Id to fetch membrship data
        if(activeOffice.office.Approved_Office__c == null){
            masterOffId = activeOffice.office.Id;
        }else{
            masterOffId = activeOffice.office.Approved_Office__c;
        }
        for(Membership_Accreditation__c m: [SELECT Id,Name,Office__c 
                                              FROM Membership_Accreditation__c 
                                             WHERE Office__c = :masterOffId AND Office__c != NULL]){
            
            LWCMembership mem = new LWCMembership(m);
            newMemOffList.add(mem);
        }
    }
    
    public List<SelectOption> getGenders() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Male','Male')); 
        options.add(new SelectOption('Female','Female')); 
        return options; 
    }
                   
    public String getGender() {
        return gender;
    }
                    
    public void setGender(String gender) { this.gender = gender; }
    
     public Attachment attachment {
        get {
          if (attachment == null)
            attachment = new Attachment();
          return attachment;
        }
        set;
    }    
  
    public LWCProfileController(){
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=10');
        selfRef = this;
        formChange = 0;
        //selectedTab = 'GENERAL';
        pendingFlag = false;
        //changesSaved = false;
        loadProfile();
    }
    
    //load profile
    public void loadProfile(){
    	User u = [SELECT ContactId 
                        FROM User 
                        WHERE Id = :UserInfo.getUserId() limit 1];
        c = [SELECT AccountId 
                       FROM Contact 
                      WHERE Id = :u.ContactId limit 1];
        accId = c.AccountId;
        
        newMem = new LWCMembership(new Membership_Accreditation__c(Specialist__c = accId));
        newMem.remove = false;
        
                      
        surgeon = [SELECT Id,Website__c, Phone__c, Email__c, Display_Name__c,
                          Surgeon_First_Name__c,Name,Show_Email__c,Gender__c,
                          Live_Seminars__c,Online_Seminars__c,American_Board_of_Medical_Specialties__c,
                          American_College_of_Surgeons_ACS__c,Amr_Society_of_Metabolic_Bariatric__c,
                          Provides_Financing_Options__c, Outpatient_with_LAP_BAND_System_Surgery__c,
                          Accepts_out_of_town_patients_OOTN__c,Bariatrics__c,
                          Frequency_of_LAP_BAND_System_Surgeries__c,Years_of_LAP_BAND_System_Experience__c
                          
                     FROM Surgeon__c 
                    WHERE Surgeon_Account__c = :c.AccountId limit 1];
        gender = surgeon.Gender__c;
        
        //Hospital
        newHos = new LWCHospital(new Surgeon_Affiliation__c(Surgeon_Account__c = accId,Surgeon_Name__c = surgeon.Id));
        newHos.remove = false;
        
        List<Attachment> att = [SELECT Id 
                         FROM Attachment 
                         WHERE parentId =:surgeon.Id ORDER BY LastModifiedDate DESC limit 1];
                         
        if(att.size() >0){
            fieldId = att[0].Id;
        }
        else	fieldId='nopicture';
        
        loadOffice();
        Id profileId = Userinfo.getProfileId();
        String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId LIMIT 1].Name;
        List<PermissionSetAssignment> permissionSetList = new List<PermissionSetAssignment>();
        if(profileName.equals(LWCConstantDeclaration.REGISTEREDPROFILE)){
            
            permissionSetList   = [SELECT p.PermissionSet.Name, p.PermissionSetId, p.Assignee.Id, p.AssigneeId 
                                     FROM PermissionSetAssignment p 
                                    WHERE p.Assignee.Id = :Userinfo.getUserId()];
                                    
            isProfileEdit = false;
            isOfficesEdit = false;
            isOfficeStaffEdit = false;
            
            for(PermissionSetAssignment p : permissionSetList){
                if(p.PermissionSet.Name == LWCConstantDeclaration.OFFICESTAFFPERMISSIONSET){
                    isOfficeStaffEdit = true;
                }else if(p.PermissionSet.Name == LWCConstantDeclaration.OFFICEUSERPERMISSIONSET){
                    isOfficesEdit = true;
                }else if(p.PermissionSet.Name == LWCConstantDeclaration.SPECIALISTUSERPERMISSIONSET){
                    isProfileEdit = true;
                }
            }                                                    
            
        }else{
            isProfileEdit = true;
            isOfficesEdit = true;
            isOfficeStaffEdit = true;
        }
    }
    
    //load office, used to control google map loading
    public void loadOffice(){
        
        offices = new List<Office__c>();              
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	String pOffice = LWCConstantDeclaration.PENDINGOFFICERECORD;
    	LIST<RecordType> recordTypeIds = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice,pOffice});
    	Map<String,ID> recordTypeMap = new Map<String,ID>();
    	for(RecordType r : recordTypeIds){
    		recordTypeMap.put(r.Name, r.Id);
    	}
    	ID aId = recordTypeMap.get(aOffice);
    	ID pId = recordTypeMap.get(pOffice);
        List<Office__c> appOffice = new List<Office__c>();
        List<Office__c> pendOffice = new List<Office__c>();
        Map<Id,Id> mapOfOfficeToApprovalOff = new Map<Id,Id>();//OfficeId to Approved_Office__c
        for(Office__c o:[Select Id,Address_2__c, Address_1__c,Website__c, Email__c,Fax__c,Wheelchair_Access__c, 
                          Location__Latitude__s,Location__Longitude__s,State__c,Zip_Code__c,Specialist__r.Name,Name,
                          English__c,Spanish__c,Chinese__c,Japanese__c,French__c,German__c,Vietnamese__c,Italian__c,
                          Korean__c,Russian__c, Phone__c, Specialist__c,Online_Seminars_Link__c,Live_Seminars_Link__c,
                          Office_Hours__c, MapLink__c, RecordTypeId, City__c, Ext__c, Bariatric_Surgery_Center__c,Approved_Office__c,
                          Is_Edit_Flag__c,Status__c
                          From Office__c
                          WHERE Status__c NOT IN ('Rejected','Deleted') AND Specialist__c =:accId]){
                            
            if(o.RecordTypeId == aId && o.Is_Edit_Flag__c== false && o.Approved_Office__c == NULL){
                appOffice.add(o);
            }
            if(o.RecordTypeId == pId  && o.Status__c == LWCConstantDeclaration.SUBMITTED){
                pendOffice.add(o);
            }
            if(o.Approved_Office__c != null){
                mapOfOfficeToApprovalOff.put(o.Id,o.Approved_Office__c);
            }
        }
        
        if(appOffice.size()>0)  offices.addAll(appOffice);
        if(pendOffice.size()>0){
        	 offices.addAll(pendOffice);
        	 pendingFlag = true;
        }
        
        officeNum = offices.size();
        listOfMemAcc = new List<LWCMembership>();
        mapOfAccreditation = new Map<Id,List<Membership_Accreditation__c>>(); 
        List<Membership_Accreditation__c> temp ;
        for(Membership_Accreditation__c m:[SELECT Id,Name,Specialist__c,Office__c,Office__r.Approved_Office__c  
                                             FROM Membership_Accreditation__c 
                                            WHERE Membership_Accreditation__c.Specialist__c = :c.AccountId 
                                            OR Membership_Accreditation__c.Office__c IN:offices
                                            OR Membership_Accreditation__c.Office__c IN :mapOfOfficeToApprovalOff.values()]){
            LWCMembership mem = new LWCMembership(m);
            if(m.Specialist__c != null){
                listOfMemAcc.add(mem);
            }
            if(m.Office__c != null){
                temp = new List<Membership_Accreditation__c>();
                if(mapOfAccreditation.get(m.Office__c) != null){
                    temp = mapOfAccreditation.get(m.Office__c);
                }
                temp.add(m);
                mapOfAccreditation.put(m.Office__c,temp);
            }                                    
        }
        loadHospitals();
        
        for(Office__c o : offices){
            if(mapOfAccreditation.get(o.Id) == null){
                temp = new List<Membership_Accreditation__c>();
                mapOfAccreditation.put(o.Id,temp);
            }
        }
        
         //Get Accreditation from parent
        for(Id oId : mapOfAccreditation.keySet()){
            if(mapOfOfficeToApprovalOff.get(oId) != null){
                Id approvalOfcId ;
                approvalOfcId = mapOfOfficeToApprovalOff.get(oId);
                if(mapOfAccreditation.get(approvalOfcId) != null){
                  mapOfAccreditation.put(oId,mapOfAccreditation.get(approvalOfcId));
                }
            }
        }
    }
    
    public void loadHospitals(){
    	listOfHosAcc = new List<LWCHospital>();
        for(Surgeon_Affiliation__c h:[SELECT Id,Name, AccountText__c, AccountName_Formula__c FROM Surgeon_Affiliation__c 
                                            WHERE Surgeon_Account__c = :c.AccountId AND Account__c = null]){
            LWCHospital hospital = new LWCHospital(h);
            listOfHosAcc.add(hospital);
        }
    }
    
    
    public Pagereference switchTab(){
    	
    	return null;
    }
    public Pagereference reloadOffice(){
        loadOffice();
        return null;
    }
    public void insertMasterOffice(){
    //  insert activeOffice;
    }
    
    //save profile record including memebership and hospital
    public PageReference saveProfile() {
    	
    	surgeon.Gender__c  = gender;
    	LWCWithoutSharing.updateSurgeon(surgeon);
        

        if(attachment.Body != null){
            attachment.Name = surgeon.Display_Name__c+'_'+system.now();
            attachment.ParentId = surgeon.Id;
            LWCWithoutSharing.insertAttachment(attachment);
            fieldId = attachment.Id;
            attachment = new Attachment();
        }
       
        
        List<Membership_Accreditation__c> remMemList = new List<Membership_Accreditation__c>();
        List<Membership_Accreditation__c> newMemList = new List<Membership_Accreditation__c>();
        
        for(LWCMembership m: listOfMemAcc){
            if(m.remove == true && m.mem.Id != null){
                remMemList.add(m.mem);
            }
            else if(m.remove == false && m.mem.Id == null){
                newMemList.add(m.mem);
            }
        }
        
        if(remMemList.size()>0){
        	LWCWithoutSharing.deleteMembership(remMemList);
        }
        
        if(newMemList.size()>0){
        	LWCWithoutSharing.insertMembership(newMemList);
        }
        
        //Hospital
        List<Surgeon_Affiliation__c> remHosList = new List<Surgeon_Affiliation__c>();
        List<Surgeon_Affiliation__c> newHosList = new List<Surgeon_Affiliation__c>();
        
        for(LWCHospital h: listOfHosAcc){
            if(h.remove == true && h.hospital.Id != null){
                remHosList.add(h.hospital);
            }
            else if(h.remove == false && h.hospital.Id == null){
                newHosList.add(h.hospital);
            }
        }
        
        if(remHosList.size()>0){
        	LWCWithoutSharing.deleteHospital(remHosList);
        }
        
        if(newHosList.size()>0){
            LWCWithoutSharing.insertHospital(newHosList);
        }
        
        loadOffice();// Reload profile
        
        if(oldPassword != null && oldPassword != '' && newPassword != null && newPassword!= '' && verifyNewPassword != null && verifyNewPassword != ''){
            try{
                site.changePassword(newPassword,verifyNewPassword,oldPassword);
            }catch(exception e){
                return null; 	
            }
            
        }
        
        if(ApexPages.getMessages().size() == 0){// No other error message on page
	        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.info,'Your profile updates have been submitted');
	        ApexPages.addMessage(myMsg);
        }
            
        return null;
    }
    
    //remove membership on page, mark remove to true
    public PageReference removeMembership(){
        for(LWCMembership m : listOfMemAcc){
            if(m.index == remMemId){
                m.remove = true;
            }
        }
        formChange = 1;
        
        return null;
    }
    
    //remove hospital on page. mark remove to true
    public PageReference removeHospital(){
        for(LWCHospital h : listOfHosAcc){
            if(h.index == remHosId){
                h.remove = true;
            }
        }
        formChange = 1;
        
        return null;
    }
    
    
    public PageReference doNothing(){
        return null;
    }
    
    //add membership, remove = false
    public PageReference addMembership(){
    	if(newMem.mem.Name!=null&&newMem.mem.Name!=''){
	        listOfMemAcc.add(newMem);
	        Membership_Accreditation__c newMemRec = new Membership_Accreditation__c();
	        newMemRec.Specialist__c = accId;
	        newMem = new LWCMembership(newMemRec);
	        newMem.remove = false;
	        formChange = 1;
    	}else{
    		formChange = 0;
    	}
        return null;
    }
    
    //add hospital, remove = false
    public PageReference addHospital(){
    	if(newHos.hospital.AccountText__c!=null&&newHos.hospital.AccountText__c!=''){
	        listOfHosAcc.add(newHos);
	        Surgeon_Affiliation__c newHosRec = new Surgeon_Affiliation__c();
	        newHosRec.Surgeon_Account__c = accId;
	        newHosRec.Surgeon_Name__c = surgeon.Id;
	        newHos = new LWCHospital(newHosRec);
	        newHos.remove = false;
	        formChange = 1;
        }else{
        	formChange = 0;
        }
        return null;
    }
        
}
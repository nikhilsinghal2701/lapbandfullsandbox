trigger SalesOrderLineItemTriggerTemp on Sales_Order_Line_Item__c (after insert, after update, before insert) {
	if (Label.SalesOrderLineItemTriggerTemp == 'Active') {
		if (trigger.isAfter) {
			SalesOrderLineItemTriggerMethods.upsertForecastTerritory(trigger.new, trigger.old);
		}
	}		
}
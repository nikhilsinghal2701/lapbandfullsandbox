@isTest
public class S3Test {
	
	static testMethod void testDeleteObject(){
		Test.startTest();
		Test.setMock(WebServiceMock.class, new S3WebServiceMockImpl());
        S3.AmazonS3 amazonS3 = new S3.AmazonS3();
        // Call the method that invokes a callout
        Integer output = amazonS3.DeleteObject('for test',null,null,null,null,null).Code;
        
        // Verify that a fake result is returned
        System.assertEquals(200, output); 
        Test.stopTest();
        
	}
	/*
	static testMethod void testListBucket(){
		Test.startTest();
		Test.setMock(WebServiceMock.class, new S3WebServiceMockImpl());
        S3.AmazonS3 amazonS3 = new S3.AmazonS3();
        // Call the method that invokes a callout
        Integer output = amazonS3.ListBucket('for test',null,null,null,null,null,null,null,null,null).Code;
        
        // Verify that a fake result is returned
        System.assertEquals(200, output); 
        Test.stopTest();
	}*/
	static testmethod void test6(){
       S3.AmazonS3 S3 = new S3.AmazonS3();
       S3.PutObjectInline_element putObjectElem = new S3.PutObjectInline_element();
       putObjectElem.Bucket='test';
       putObjectElem.Key = 'testKey';
       putObjectElem.Metadata = null;
       putObjectElem.Data= 'a';
       putObjectElem.ContentLength = 1;
       putObjectElem.AccessControlList = null;
       putObjectElem.StorageClass='classname';
       putObjectElem.AWSAccessKeyId='asdf';
       putObjectElem.Timestamp = Datetime.now();
       putObjectElem.Signature='test';
       putObjectElem.Credential='test';	
    }
  
     
}
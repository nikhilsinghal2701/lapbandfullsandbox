/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AccountTriggerTest {

    static testMethod void accountAlignmentTestOUS() {
     	//get record type ID for Account Ship To
    	Schema.DescribeSObjectResult sObjDescrAcc = Schema.SObjectType.Account;
    	Map<String, Schema.RecordTypeInfo> accRecTypeInfo = sObjDescrAcc.getRecordTypeInfosByName();
 
     	//create region
    	Sales_Region__c reg = new Sales_Region__c();
    	reg.Region_ID__c = 'TESTREGID1';
    	reg.Name = reg.Region_ID__c;
    	reg.Role__c = 'should not matter';
    	reg.Assigned_To__c = UserInfo.getUserId();
    	insert reg;
    	
    	List<Sales_Territory__c> terrList = new List<Sales_Territory__c>();
    	
    	//create territory 1
		Sales_Territory__c terr1 = new Sales_Territory__c (
    		Territory_ID__c = 'TESTTERRID1',
    		Name = 'TESTTERRID1',
    		Role__c = 'does not matter',
    		Sales_Region__c = reg.Id
    		);
    	terrList.add(terr1);

    	//create territory 2
		Sales_Territory__c terr2 = new Sales_Territory__c (
    		Territory_ID__c = 'TESTTERRID2',
    		Name = 'TESTTERRID2',
    		Role__c = 'does not matter',
    		Sales_Region__c = reg.Id
    		);
    	terrList.add(terr2);

    	//create territory 3
		Sales_Territory__c terr3 = new Sales_Territory__c (
    		Territory_ID__c = 'TESTTERRID3',
    		Name = 'TESTTERRID3',
    		Role__c = 'does not matter',
    		Sales_Region__c = reg.Id
    		);
    	terrList.add(terr3);

    	//create territory 4
		Sales_Territory__c terr4 = new Sales_Territory__c (
    		Territory_ID__c = 'TESTTERRID4',
    		Name = 'TESTTERRID4',
    		Role__c = 'does not matter',
    		Sales_Region__c = reg.Id
    		);
    	terrList.add(terr4);

    	//create territory 5
		Sales_Territory__c terr5 = new Sales_Territory__c (
    		Territory_ID__c = 'TESTTERRID5',
    		Name = 'TESTTERRID5',
    		Role__c = 'does not matter',
    		Sales_Region__c = reg.Id
    		);
    	terrList.add(terr5);

    	//create territory 6
		Sales_Territory__c terr6 = new Sales_Territory__c (
    		Territory_ID__c = 'TESTTERRID6',
    		Name = 'TESTTERRID6',
    		Role__c = 'does not matter',
    		Sales_Region__c = reg.Id
    		);
    	terrList.add(terr6);
    	
    	insert terrList;
    	
		List<Sales_Alignment__c> alignList = new List<Sales_Alignment__c>();
		
		//create Alignment rule for ZIPCODE
		alignList.add (new Sales_Alignment__c (
			Type__c = 'Zipcode',
			Sales_Territory__c = terr1.Id,
			Country_Region__c = 'United Kingdom',
			ISO_Country_Code__c = 'UK',
			Value__c = 'TF1',
			Active__c = true
			)
		);
		
		//create Alignment rule for EXTERNAL ID
		alignList.add (new Sales_Alignment__c (
			Type__c = 'AccountExternalID',
			Sales_Territory__c = terr2.Id,
			Country_Region__c = 'Germany',
			ISO_Country_Code__c = 'DE',
			Value__c = 'ALIGNTESTEXTID2',
			Active__c = true
			)
		);

		//create Alignment rule for COUNTRY
		alignList.add (new Sales_Alignment__c (
			Type__c = 'Country',
			Sales_Territory__c = terr3.Id,
			Country_Region__c = 'France',
			ISO_Country_Code__c = 'FR',
			Value__c = 'does not matter', //goes by the ISO Country Code
			Active__c = true			
			)
		);

		//create Alignment rule for CITY
		alignList.add (new Sales_Alignment__c (
			Type__c = 'City',
			Sales_Territory__c = terr4.Id,
			Country_Region__c = 'Italy',
			ISO_Country_Code__c = 'IT',
			Value__c = 'Torino',
			Active__c = true
			)
		);

		//create Alignment rule for STATE
		alignList.add (new Sales_Alignment__c (
			Type__c = 'State',
			Sales_Territory__c = terr5.Id,
			Country_Region__c = 'Canada',
			ISO_Country_Code__c = 'CA',
			Value__c = 'NL',
			Active__c = true
			)
		);

		//create DEFAULT Alignment rule for a country
		alignList.add (new Sales_Alignment__c (
			Type__c = 'Default',
			Sales_Territory__c = terr6.Id,
			Country_Region__c = 'India',
			ISO_Country_Code__c = 'IN',
			Value__c = 'does not matter', //goes by the ISO Country Code
			Active__c = true			
			)
		);
		
		insert alignList;
		
    	List<Account> acctList = TestUtil.createAccounts(6, false, null);
    	Id recTypeID = accRecTypeInfo.get('Ship To').getRecordTypeId();

		//create Account 1 for alignment by ZIPCODE
    	acctList[0].External_ID__c = 'ALIGNTESTEXTID1';
    	acctList[0].RecordTypeId = recTypeID;
    	acctList[0].ShippingCountry = 'UK';
    	acctList[0].ShippingPostalCode = 'TF1 6TF';
    	acctList[0].Trigger_Alignment__c = true;

		//create Account 2 for alignment by EXTERNAL ID
    	acctList[1].External_ID__c = 'ALIGNTESTEXTID2';
    	acctList[1].RecordTypeId = recTypeID;
    	acctList[1].ShippingCountry = 'DE';
    	acctList[1].Trigger_Alignment__c = true;

		//create Account 3 for alignment by COUNTRY
    	acctList[2].External_ID__c = 'ALIGNTESTEXTID3';
    	acctList[2].RecordTypeId = recTypeID;
    	acctList[2].ShippingCountry = 'FR';
    	acctList[2].Trigger_Alignment__c = true;

		//create Account 4 for alignment by CITY
    	acctList[3].External_ID__c = 'ALIGNTESTEXTID4';
    	acctList[3].RecordTypeId = recTypeID;
    	acctList[3].ShippingCountry = 'IT';
    	acctList[3].ShippingCity = 'Torino';
    	acctList[3].Trigger_Alignment__c = true;

		//create Account 5 for alignment by STATE
    	acctList[4].External_ID__c = 'ALIGNTESTEXTID5';
    	acctList[4].RecordTypeId = recTypeID;
    	acctList[4].ShippingCountry = 'CA';
    	acctList[4].ShippingState = 'NL';
    	acctList[4].Trigger_Alignment__c = true;

		//create Account 3 for alignment by COUNTRY
    	acctList[5].External_ID__c = 'ALIGNTESTEXTID6';
    	acctList[5].RecordTypeId = recTypeID;
    	acctList[5].ShippingCountry = 'IN';
    	acctList[5].Trigger_Alignment__c = true;
		
		Test.startTest();
		
		//insert accounts
		insert acctlist;
		
		Test.stopTest();
		
		List<Account> acctAfterList = [select Id, External_ID__c, Trigger_Alignment__c, Sales_Territory__c, Sales_Territory__r.Territory_ID__c from Account];
		for (Account acct : acctAfterList) {
			System.assertEquals(6, acctAfterList.size());
			System.assertEquals(false, acct.Trigger_Alignment__c);
			if (acct.External_ID__c == 'ALIGNTESTEXTID1') {
				System.assertEquals('TESTTERRID1', acct.Sales_Territory__r.Territory_ID__c);
 			} else if (acct.External_ID__c == 'ALIGNTESTEXTID2') {
				System.assertEquals('TESTTERRID2', acct.Sales_Territory__r.Territory_ID__c);
 			} else if (acct.External_ID__c == 'ALIGNTESTEXTID3') {
				System.assertEquals('TESTTERRID3', acct.Sales_Territory__r.Territory_ID__c);
 			} else if (acct.External_ID__c == 'ALIGNTESTEXTID4') {
				System.assertEquals('TESTTERRID4', acct.Sales_Territory__r.Territory_ID__c);
 			} else if (acct.External_ID__c == 'ALIGNTESTEXTID5') {
 				System.assertEquals('TESTTERRID5', acct.Sales_Territory__r.Territory_ID__c);
 			} else {// should be ALIGNTESTEXTID6
 				System.assertEquals('TESTTERRID6', acct.Sales_Territory__r.Territory_ID__c);
			}
		}
		
		//override alignment
		acctAfterList[0].Sales_Territory__c = null;
		update acctAfterList[0];
		
		Account acct1 = [select Id, Trigger_Alignment__c, Sales_Territory__c from Account where Id = :acctAfterList[0].Id];
		System.assertEquals(null, acct1.Sales_Territory__c);
		System.assertEquals(false, acct1.Trigger_Alignment__c);
		
		//auto-align on update
		acct1.Trigger_Alignment__c = true;
		
		update acct1;
		
		Account acct2 = [select Id, Trigger_Alignment__c, Sales_Territory__c, Sales_Territory__r.Territory_ID__c from Account where Id = :acct1.Id];
		System.assertEquals('TESTTERRID1', acct2.Sales_Territory__r.Territory_ID__c);
		System.assertEquals(false, acct2.Trigger_Alignment__c);
		
    }
}
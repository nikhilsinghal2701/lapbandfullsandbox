public with sharing class ForecastTriggerMethods {

	private static Integer getQuarter(Date dateValue) {
		if (dateValue.month() == 1 || dateValue.month() == 2 || dateValue.month() == 3) {
			return 1;
		} else if (dateValue.month() == 4 || dateValue.month() == 5 || dateValue.month() == 6) {
			return 2;
		} else if (dateValue.month() == 7 || dateValue.month() == 8 || dateValue.month() == 9) {
			return 3;
		} 
		
		return 4;
	}

	public static void setCalculatedFields (List<Forecast__c> fcListNew, List<Forecast__c> fcListOld, Boolean isInsert, Boolean isUpdate) {
		//BUILD Account ID list
		Set<Id> acctIdSet = new Set<Id>();
		for (Forecast__c fcRec : fcListNew) {
			acctIdSet.add(fcRec.Account__c);
		}

		//QUERY Attainment Amount and Account Details
		Map<Id, Account> acctMap = new Map<Id, Account>();	
		Map<Id, Map<Date, Decimal>> acctAttainMap = new Map<Id, Map<Date, Decimal>>();
		if (acctIdSet.Size() > 0) {
			acctAttainMap =  getAttainAmt(acctIdSet);
			
			acctMap = new Map<Id, Account> ([
				select Id, Name, 
					Health_Region_ID__c, Health_Territory_ID__c, 
					RP_Region__c, RP_Territory__c,
					ES_Region_ID__c, ES_Territory_ID__c
				from Account 
				where Id in :acctIdSet
			]);
		}
		
		//SET CALCULATED FIELDS
		for (Integer i=0; i<fcListNew.size(); i++) {
			Forecast__c fcRecNew = fcListNew[i];
			Forecast__c fcRecOld = new Forecast__c();
			if (fcListOld != null) fcRecOld = fcListOld[i];

			// DATE_RANGE__c
			if (isInsert || (isUpdate && (fcRecNew.Start_Date__c != fcRecOld.Start_Date__c || fcRecNew.End_Date__c != fcRecOld.End_Date__c))) {
				fcRecNew.Date_Range__c = fcRecNew.Start_Date__c.format() + '-' + fcRecNew.End_Date__c.format();
			}

			// ATTAINMENT_AMOUNT__c
			Decimal attainAmt = 0.0;
			if (acctAttainMap != null && acctAttainMap.Size() > 0 && acctAttainMap.containsKey(fcRecNew.Account__c)) {
				for (Date dt : acctAttainMap.get(fcRecNew.Account__c).keySet()) {
					attainAmt = (dt >= fcRecNew.Start_Date__c && dt <= fcRecNew.End_Date__c) ? (attainAmt + acctAttainMap.get(fcRecNew.Account__c).get(dt)) : attainAmt;
				}
			}
			fcRecNew.Attainment_Amount__c = attainAmt;
			
			// CURRENT_QUARTER__c
			fcRecNew.Current_Quarter__c = (getQuarter(Date.today()) == getQuarter(fcRecNew.Start_Date__c) && Date.today().year() == fcRecNew.Start_Date__c.year()) ? true:false;
			
			/*
			// REGION and TERRITORY IDs (only on the initial creation of the Forecast record)
			if (isInsert) {
				fcRecNew.Health_Region_ID__c = acctMap.get(fcRecNew.Account__c).Health_Region_ID__c;
				fcRecNew.Health_Territory_ID__c = acctMap.get(fcRecNew.Account__c).Health_Territory_ID__c;
				fcRecNew.RP_Region_ID__c = acctMap.get(fcRecNew.Account__c).RP_Region__c;
				fcRecNew.RP_Territory_ID__c = acctMap.get(fcRecNew.Account__c).RP_Territory__c;
				fcRecNew.ES_Region_ID__c = acctMap.get(fcRecNew.Account__c).ES_Region_ID__c;
				fcRecNew.ES_Territory_ID__c = acctMap.get(fcRecNew.Account__c).ES_Territory_ID__c;
			}
			*/
		}
		
	}

	
	private static Map<Id, Map<Date, Decimal>> getAttainAmt (Set<Id> acctIdSet) {		
		//QUERY
		AggregateResult[] aggrResult = [
			select Sales_Order__r.Ship_To__c acc, Bill_Date__c bill, sum(Amount__c) amt 
			from Sales_Order_Line_Item__c
			where IsForecastable__c = true and Sales_Order__r.Ship_To__r.Id in :acctIdSet
			group by Sales_Order__r.Ship_To__c, Bill_Date__c
		];
/*
		AggregateResult[] aggrResult = [
			select Sales_Order__r.Ship_To__c acc, Bill_Date__c bill, sum(Amount__c) amt 
			from Sales_Order_Line_Item__c
			where Sales_Order__r.Ship_To__r.RecordType.Name = 'Ship To' 
				and Sales_Order__r.Ship_To__r.Targeted_Account__c = true 
				and Material__c != 'B-20390'
				and Sales_Order__r.RecordType.Name = 'LAP-BAND SYSTEMS'
				and (not Material__c LIKE '%Freight%')
				and Sales_Order__r.Ship_To_Health_Region_ID__c != null
				and Sales_Order__r.PO__c != 'move to AGN'
				and (not Sales_Order__r.PO__c LIKE '%TAX Adjustment%')
				and Sales_Order__r.Type__c = 'US' 
				and Sales_Order__r.Ship_To__r.Id in :acctIdSet
			group by Sales_Order__r.Ship_To__c, Bill_Date__c
		];
*/		
		//PARSE QUERY RESULTS AND BUILD THE MAP
		Map<Id, Map<Date, Decimal>> acctAttainMap = new Map<Id, Map<Date, Decimal>>();
		if (aggrResult != null && aggrResult.size() > 0) {
			for (AggregateResult obj : aggrResult) {
				String accId = String.valueOf(obj.get('acc'));
				String billDate = String.valueOf(obj.get('bill'));
				String amt = String.valueOf(obj.get('amt'));
				if (accId != null && billDate != null && amt != null) {
					if (acctAttainMap != null && acctAttainMap.containsKey(accId)) {
						acctAttainMap.get(accId).put(Date.valueOf(billDate), Decimal.valueOf(amt));
					} else {
						Map<Date, Decimal> tempMap = new Map<Date, Decimal>();
						if (billDate != null && amt != null) {
							tempMap.put(Date.valueOf(billDate), Decimal.valueOf(amt));
							acctAttainMap.put(accId, tempMap);
						}
					}
				}
			}
		}
		return acctAttainMap;
	}
}
public with sharing class LWLocationCallouts {
     @future (callout=true)  // future method needed to run callouts from Triggers
      static public void getLocation(List<id> officeIdList){
        // gather account info
        List<Office__c> upOfcList = new List<Office__c>();
        List<Office__c> oList = [SELECT Id,Address_1__c,Address_2__c,City__c,State__c,Zip_Code__c,Country__c FROM Office__c WHERE id IN: officeIdList];
        for(Office__c o : oList){
            String address = '';
            if (o.Address_1__c != null)
                address += o.Address_1__c +', ';
            if (o.Address_2__c != null)
            address += o.Address_2__c +', ';
            if (o.City__c != null)
                address += o.City__c +', ';
            if (o.State__c != null)
                address += o.State__c +' ';
            if (o.Zip_Code__c != null)
                address += o.Zip_Code__c +', ';
            if(o.Country__c != null)
                address += o.Country__c;
            
            address = EncodingUtil.urlEncode(address, 'UTF-8'); 
            System.debug('=='+address);
            // build callout
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint('http://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false');
            req.setMethod('GET');
            req.setTimeout(60000);
            
            try{
            // callout
            HttpResponse res = h.send(req);
 
            // parse coordinates from response
            JSONParser parser = JSON.createParser(res.getBody());
            double lat = null;
            double lon = null;
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                    (parser.getText() == 'location')){
                       parser.nextToken(); // object start
                       while (parser.nextToken() != JSONToken.END_OBJECT){
                           String txt = parser.getText();
                           parser.nextToken();
                           if (txt == 'lat')
                               lat = parser.getDoubleValue();
                           else if (txt == 'lng')
                               lon = parser.getDoubleValue();
                       }
 
                }
            }
 
            // update coordinates if we get back
            if (lat != null){
                o.Location__Latitude__s = lat;
                o.Location__Longitude__s = lon;
                system.debug('++'+o.Location__Latitude__s);
                upOfcList.add(o);
            }
 
            } catch (Exception e) {
            }
        
        }
        LWCWithoutSharing.updateOfficeList(upOfcList);

        /*
        // create an address string
        String address = '';
        if (o.Address_1__c != null)
            address += o.Address_1__c +', ';
        if (o.City__c != null)
            address += o.City__c +', ';
        if (o.State__c != null)
            address += o.State__c +' ';
        if (o.Zip_Code__c != null)
            address += o.Zip_Code__c +', ';
            
            address += 'USA';
        /*if (o. != null)
            address += a.BillingCountry;*/
        /*
        address = EncodingUtil.urlEncode(address, 'UTF-8');
 
        // build callout
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false');
        req.setMethod('GET');
        req.setTimeout(60000);
 
        try{
            // callout
            HttpResponse res = h.send(req);
 
            // parse coordinates from response
            JSONParser parser = JSON.createParser(res.getBody());
            double lat = null;
            double lon = null;
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                    (parser.getText() == 'location')){
                       parser.nextToken(); // object start
                       while (parser.nextToken() != JSONToken.END_OBJECT){
                           String txt = parser.getText();
                           parser.nextToken();
                           if (txt == 'lat')
                               lat = parser.getDoubleValue();
                           else if (txt == 'lng')
                               lon = parser.getDoubleValue();
                       }
 
                }
            }
 
            // update coordinates if we get back
            if (lat != null){
                o.Location__Latitude__s = lat;
                o.Location__Longitude__s = lon;
                update o;
            }
 
        } catch (Exception e) {
        }*/
    }
}
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCFPRBMPClasses {

    static testMethod void myUnitTestCFPE() {
        Account acc = TestUtil.createAccounts(1, true, new Map<String, Object>{'Name' => 'Test'})[0];
        Conversion_Funnel__c cfun = new Conversion_Funnel__c();
        cfun.Practice_Name__c=acc.id;
        insert cfun;
        ApexPages.StandardController sc = new ApexPages.StandardController(cfun);
        Test.startTest();
        ConversionFunnelPageExtension testcfp = new ConversionFunnelPageExtension(sc);
        testcfp.SaveCfun();
        Test.stopTest();
    }
     static testMethod void myUnitTestRBMScore() {
        Account acc = TestUtil.createAccounts(1, true, new Map<String, Object>{'Name' => 'Test'})[0];
        RBM_Score_Card__c rbms = new RBM_Score_Card__c();
        rbms.Account__c=acc.id;
        insert rbms;
        ApexPages.StandardController sc = new ApexPages.StandardController(rbms);
        Test.startTest();
        RBMScoreCardExtension testrbms = new RBMScoreCardExtension(sc);
        testrbms.SaveRBMSC();
        Test.stopTest();
    }
}
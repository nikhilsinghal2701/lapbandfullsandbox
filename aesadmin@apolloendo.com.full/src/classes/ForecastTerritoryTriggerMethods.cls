public with sharing class ForecastTerritoryTriggerMethods {

	private static Integer getQuarter(Date dateValue) {
		if (dateValue.month() == 1 || dateValue.month() == 2 || dateValue.month() == 3) {
			return 1;
		} else if (dateValue.month() == 4 || dateValue.month() == 5 || dateValue.month() == 6) {
			return 2;
		} else if (dateValue.month() == 7 || dateValue.month() == 8 || dateValue.month() == 9) {
			return 3;
		} 
		
		return 4;
	}

	public static void setCalculatedFields (List<Forecast_Territory__c> fcListNew, List<Forecast_Territory__c> fcListOld, Boolean isInsert, Boolean isUpdate) {
		//BUILD Territory ID list
		Set<Id> terrIdSet = new Set<Id>();
		for (Forecast_Territory__c fcRec : fcListNew) {
			terrIdSet.add(fcRec.Sales_Territory__c);
		}

		//QUERY Attainment Amount and Territory Details
		Map<Id, Map<Date, String>> terrAttainMap = new Map<Id, Map<Date, String>>();
		if (terrIdSet.Size() > 0) {
			terrAttainMap =  getAttainAmt(terrIdSet);
		}
		
		//SET CALCULATED FIELDS
		for (Integer i=0; i<fcListNew.size(); i++) {
			Forecast_Territory__c fcRecNew = fcListNew[i];

			// ATTAINMENT_AMOUNT__c
			Decimal attainAmtUSD = 0.0;
			Decimal attainAmtLocal = 0.0;
			/* NOTE: The attainment Amount in USD and Local currency are combined as a string separated by a pipe (|): USD_VALUE|LOCAL_VALUE */
			if (terrAttainMap != null && terrAttainMap.Size() > 0 && terrAttainMap.containsKey(fcRecNew.Sales_Territory__c)) {
				for (Date dt : terrAttainMap.get(fcRecNew.Sales_Territory__c).keySet()) {
					if (dt >= fcRecNew.Start_Date__c && dt <= fcRecNew.End_Date__c) {
						List<String> amtList = terrAttainMap.get(fcRecNew.Sales_Territory__c).get(dt).split('::');
						System.debug('### amtList: ' + amtList);
						if (amtList[0] != null && amtList[0].trim().length() > 0) {
							attainAmtUSD += Decimal.valueOf(amtList[0].trim());
						}
						if (amtList[1] != null && amtList[1].trim().length() > 0) {
							attainAmtLocal += Decimal.valueOf(amtList[1].trim());
						}
					} 
				}
			}
			fcRecNew.Attainment_Amount__c = attainAmtUSD;
			fcRecNew.Attainment_Amount_Local_Currency__c = attainAmtLocal;
			
			// CURRENT_QUARTER__c
			fcRecNew.Current_Quarter__c = (getQuarter(Date.today()) == getQuarter(fcRecNew.Start_Date__c) && Date.today().year() == fcRecNew.Start_Date__c.year()) ? true:false;
		}
	}

	private static Map<Id, Map<Date, String>> getAttainAmt (Set<Id> terrIdSet) {		
		//QUERY
		AggregateResult[] aggrResult = [
			select Sales_Order__r.Ship_To__r.Sales_Territory__c terr, Bill_Date__c bill, sum(Amount__c) amt, sum(Amount_Local_Currency__c) localAmt  
			from Sales_Order_Line_Item__c
			where Is_OUS_Forecastable__c = true and Sales_Order__r.Ship_To__r.Sales_Territory__c in :terrIdSet
			group by Sales_Order__r.Ship_To__r.Sales_territory__c, Bill_Date__c
		];
		
		//PARSE QUERY RESULTS AND BUILD THE MAP
		Map<Id, Map<Date, String>> terrAttainMap = new Map<Id, Map<Date, String>>();
		if (aggrResult != null && aggrResult.size() > 0) {
			for (AggregateResult obj : aggrResult) {
				String terrId = String.valueOf(obj.get('terr'));
				String billDate = String.valueOf(obj.get('bill'));
				String usdAmt = String.valueOf(obj.get('amt')) != null ? String.valueOf(obj.get('amt')) : '0.0';
				String localAmt = String.valueOf(obj.get('localAmt')) != null ? String.valueOf(obj.get('localAmt')) : '0.0';
				if (terrId != null && billDate != null) {
					if (terrAttainMap != null && terrAttainMap.containsKey(terrId)) {
						terrAttainMap.get(terrId).put(Date.valueOf(billDate), usdAmt + '::' + localAmt);
					} else {
						Map<Date, String> tempMap = new Map<Date, String>();
						if (billDate != null) {
							tempMap.put(Date.valueOf(billDate), usdAmt + '::' + localAmt);
							terrAttainMap.put(terrId, tempMap);
						}
					}
				}
			}
		}
		return terrAttainMap;
	}
}
public with sharing class LWOfficeInfoController {
	//public Office__c activeOffice		{get;set;}
	public LWOfficeWrapper activeOffice                    {get;set;}
	public Boolean   cancelPopup		                   {get; set;}  //true = show the cancel pop up 
	public Boolean conCancelPopup {get;set;}
    public Boolean   previewPopup		                   {get; set;}  //true = show the preview. 
    public Boolean 	 editpopup			                   {get;set;}   //true = close all popups
    public String    newAccr                               {get;set;}
    public List<LWCMembership> newMemOffList               {get;set;}
    public Integer remMemberId                             {get;set;}
    public Boolean displayModal                            {get;set;}
    public Integer top {get;set;}
    
    public LWOfficeInfoController(){
    	//activeOffice = new Office__c();
    	editpopup = true;
    	cancelPopup = false;
    	previewPopup = false;
    	displayModal = true;
    	//newMemOffList = new List<LWCMembership>();
    	System.debug('LWOfficeInfoController construct start');
    }
    
    public void showModal(){
    	displayModal = true;
    }
	
	public void showCancel(){    	
		cancelPopup = true;
    }
    
    public void showConCancel(){    	
		cancelPopup = true;
		top =40;
    }
    public void closePopup() {                
        activeOffice.operation='cancel';
        cancelPopup = false;      
        activeOffice = null;
        displayModal = false;
    }
    public void showPreview(){    	
		activeOffice.operation='preview';
    }
    public void continueEdit() {
    	activeOffice.operation='edit';
    }
    public void closeCancel(){
    	cancelPopup = false;
    }
    
    public void closeConCancel(){
    	conCancelPopup = false;
    }
    
    public Pagereference addAccr(){
    	Membership_Accreditation__c m = new Membership_Accreditation__c();
    	m.Name = newAccr;
    	if( activeOffice.office.Approved_Office__c != null){
    	   m.Office__c = activeOffice.office.Approved_Office__c;
    	}else{
    		m.Office__c = activeOffice.office.Id;
    	}
    	LWCMembership mem = new LWCMembership(m);
    	newMemOffList.add(mem);
    	newAccr= '';
    	return null;
    }
    
    public Pagereference removeOffMem(){
    	for(LWCMembership mem :newMemOffList){
    		if(mem.index == remMemberId){
    			mem.remove = true;
    			if(activeOffice.office.Approved_Office__c != null){
    			    mem.mem.Office__c = activeOffice.office.Approved_Office__c;
    			}
    		}
    	}
    	return null;
    }
    
    public Pagereference saveNewOffice(){
    	//cancelPopup = true;
    	Id recTyId = LWCWithoutSharing.gtRecordTypeId(LWCConstantDeclaration.PENDINGOFFICE);
    	
    	if(activeOffice.office.RecordTypeId == recTyId && activeOffice.office.Id != null){
    		LWCWithoutSharing.rejectOfcApproval(activeOffice.office.Id);
    		LWCWithoutSharing.updateOffice(activeOffice.office);
    		LWCWithoutSharing.submitOfcApproval(activeOffice.office.Id);
    		
    	}
    	else if(activeOffice.office.Id == null) {// New office - insert and submit for approval
    		
	    	activeOffice.office.Is_Edit_Flag__c = true;
	    	//insert activeOffice.office;
	    	Office__c off = LWCWithoutSharing.insertOffice(activeOffice.office);
	    	
	    	/*Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
	    	app.setObjectId(off.Id);
	    	Approval.ProcessResult result = Approval.process(app);*/
	        LWCWithoutSharing.submitOfcApproval(off.Id);
    	}else{// edit an unapproved office
    		LWCWithoutSharing.updateOffice(activeOffice.office);
    	}
    	List<Membership_Accreditation__c> newMemList = new List<Membership_Accreditation__c>();
    	List<Membership_Accreditation__c> remMemList = new List<Membership_Accreditation__c>();
    	for(LWCMembership m :newMemOffList){
    		
    		if(m.remove == false && m.mem.Id == null){
    			newMemList.add(m.mem);
    		}else if(m.remove == true && m.mem.Id != null){
    			remMemList.add(m.mem);
    		}
    	}
        
        insert newMemList;
        delete remMemList;
        
        activeOffice.operation='cancel';
        cancelPopup = false;      
        activeOffice = null;
        displayModal = false;
        
        return null;	
    }
    
     public Pagereference editOffice(){
     	cancelPopup = true;
     	displayModal = true;
        LWCWithoutSharing.updateOffice(activeOffice.office);
        return null;	
     }
}
<apex:page applyBodyTag="false" showheader="false" standardStylesheets="false" sidebar="false" >
  <!-- <apex:composition template="LWTemplate"/>  -->
    <head>
        <title>Identifying-A-Patient The Original Gastric Banding System | LAP-BAND&#174;</title>
    </head>
    
    <c:LWHCPTemplate />
    
    <body>
        <div id="container">
            <c:LWHCPHeader />
            <div id="content" class="inner-page identify">
                <div id="inner-page-content" class="identify">
                <h1 class="h1_identify long">Identifying a LAP-BAND&#174; Patient</h1>
                <div id="content-full">
            
                    <p>Talking to patients who are dealing with obesity about weight loss and bariatric surgery isn't easy. Many of them have been struggling to manage their weight for years, if not most of their lives. For these patients, working together to be more aggressive in managing their weight could help them achieve the weight-loss they need and may help improve their overall health and obesity-related conditions.<sup>1-4</sup> </p>
                    
                    <p>Let them know that weight loss surgery can be a proven option for obese patients whose weight is affecting their health and who have tried other methods of weight loss without success. Help your patients&nbsp; <apex:outputLink value="{!URLFOR($Page.LWHCPFindASpecialist,null,null)}">find a qualified LAP-BAND<sup>&#174;</sup> specialist.</apex:outputLink><!--<a href="../find-a-specialist/index.html">find a qualified LAP-BAND<sup>&#174;</sup> specialist.</a>--></p>
        
                    <h3>Treat obesity to improve overall health</h3>

                    <ul>
                        <li>For many obese patients, a combination of lifestyle changes and medication at best achieve a 5% to 10% loss in body weight&#8212;often insufficient to improve such obesity-related conditions as type 2 diabetes&#178;</li>
                        <li>Bariatric surgery has been shown to provide substantial and sustainable weight loss in appropriate obese patients<sup>5-7</sup></li>
                    </ul>

                    <h3>The lap-band AP<sup>&#174;</sup> system is the only fda-approved lagb* for patients with bmi &#8805;30 and &#8805;1 obesity-related comorbidity or bmi &#8805;40</h3>

                    <div class="identify-chart ir">
        
                        <h4>BMI 25+</h4>
                        <ul>
                            <li>Diet</li>
                            <li>Exercise</li>
                            <li>Behavior Modification</li>
                        </ul>
                        <p>5-10% TWL (total weight loss) on average</p>
        
                        <h4>BMI 27+</h4>
                        <ul>
                            <li>Diet</li>
                            <li>Exercise</li>
                            <li>Behavior Modification</li>
                        </ul>
                        <p>5-10% TWL on average</p>
        
                        <h4>BMI 35+</h4>
                        <p>46% EWL at 1 year (N=371) and 52% EWL at 2 years (N=159) in severely obese patients.*</p>
        
                        <h4>BMI 30-40</h4>
                        <p>Data from the Allergan LBMI Pivotal Study shows 65% EWL and 18% TWL at 1 year (N=143) and 70% EWL and 20% TWL at 2 years (N=128) in obese patients.<sup>&#8224;</sup></p>
        
                        <h4>BMI 40+</h4>
                        <p>34.5% EWL at 1 year (N=233) and 37.8% EWL at 2 years (N=189) in morbidly obese patients.‡ 55% to 58% EWL at 5-year follow-up for LAGB (N=640) and RYGB (N=176), respectively, in one review.</p>
        
                        <h4>Excess Weight Loss (EWL) Defined</h4>
                        <p>The % of a patient's excess weight (the difference between starting weight and ideal weight) lost</p>
                        <p>Example:<br />
                        280 lb (starting weight) - 180 lb (ideal weight) = 100 lb excess weight</p>
                        <p>50 lb (actual weight loss) &#247; 100 lb (excess weight) &#215; 100% = 50% excess weight loss</p>
                    </div>

                    <div class="identify-chart-notes">
                        <p class="reference first">Total Weight Loss is the measure typically used in pharmaceutical and behavior modification studies.</p>
                        <p class="reference">* Data based on interim analysis of ongoing LAP-BAND AP<sup>&#174;</sup> trial.</p>
                        <p class="reference"><sup>&#8224;</sup> Year-1 data from Allergan’s LBMI pivotal study is based on a complete database of 143 obese adults with a Month-12 visit. The currently available Year-2 results are based on a database snapshot of an incomplete data set that has not been source verified. These data were reviewed by FDA in support of the recently approved LAP-BAND® label expansion in the United  States.</p>
                        <p class="reference"><sup>&#8225;</sup> The LAP-BAND<sup>&#174;</sup> System was approved in the United States on the basis of a nonrandomized, single-arm study (N=299). Significant improvement in percent of excess weight loss vs baseline was achieved at 12 months (34.5%), 24 months (37.8%), and 36 months (36.2%).</p>
                        <p class="reference"><sup>&#167;</sup>  From a systematic review of medium-term weight loss after bariatric operations. Mean % EWL for LAGB includes patients after LAP-BAND<sup>&#174;</sup> (n=272) and other commercially available systems (n=368).<sup>7</sup></p>
                        <p class="reference"><sup>&#124;</sup> LAGB=Laparoscopic Adjustable Gastric Banding; RYGB=Roux-en-Y Gastric Bypass.</p>
                    </div>


                    <h3>Consider the LAP-BAND AP<sup>&#174;</sup> System for obese patients meeting the following criteria<sup>5</sup>:</h3>

                    <div class="identify-table ir">
                        <table cellpadding="0" cellspacing="0" border="1" width="995">
                            <tr>
                                <td width="155">Age</td>
                                <td width="837">&#10004; At least 18 years old</td>
                            </tr>
                            <tr>
                                <td>BMI</td>
                                <td>&#10004; &#8805;30 lb above their estimated ideal weight, with BMI of &#8805;30 kg/m&#178; and &#8805;1 obesity-related comorbid condition<br />
                                or<br />
                            &#10004; BMI ≥40 kg/m&#178;</td>
                             </tr>
                            <tr>
                                <td>Prior Failures</td>
                                <td>&#10004; Failed nonsurgical weight loss (diet, exercise, behavior modification, drug therapy), as determined by tracking patient over time</td>
                            </tr>
                            <tr>
                                <td>Considerations</td>
                                <td>&#10004; Are willing to adhere to postoperative care and follow-up
                                    <ul>
                                        <li>Use LAP-BAND&#174; System patient education materials to assess willingness</li>
                                    </ul>
                                &#10004; Are aware of and accept the operative risks</td>
                            </tr>
                        </table>
                    </div>                  

                    <h3>The LAP-BAND AP<sup>&#174;</sup> system is contraindicated for patients who<sup>5</sup>:</h3>
                    <ul>
                        <li>Are younger than 18 years.</li>
                        <li>Have conditions that may make the risk of surgery greater than the benefits.</li>
                        <li>Are unwilling or unable to comply with the required lifelong dietary, exercise, and medical requirements</li>
                        <li>Are currently or may become pregnant.</li>
                        <li>Have an inflammatory disease or condition of the gastrointestinal tract, such as ulcers, severe esophagitis, or Crohn's disease.</li>
                        <li>Have severe heart or lung disease, or any other disease that makes you a poor candidate for any surgery.</li>
                        <li>Have a problem that could cause bleeding in the esophagus or stomach. That might include esophageal or gastric varices (dilated veins). It might also be something such as congenital or acquired intestinal telangiectasia (dilation of a small blood vessel).</li>
                        <li>Have portal hypertension.</li>
                        <li>Have an abnormal esophagus, stomach, or intestine (congenital or acquired). For instance, your patient might have a narrowed opening.</li>
                        <li>Have/experienced an intraoperative gastric injury, such as a gastric perforation at or near the location of the intended band placement.</li>
                        <li>Have cirrhosis.</li>
                        <li>Have chronic pancreatitis.</li>
                        <li>Are addicted to alcohol or drugs.</li>
                        <li>Have an infection anywhere in their body, or one that could contaminate the surgical area.</li>
                        <li>Are on chronic, long-term steroid treatment.</li>
                        <li>Might be allergic to materials in the device.</li>
                        <li>Cannot tolerate pain from an implanted device.</li>
                        <li>Has an autoimmune connective tissue disease, such as systemic lupus erythematosus or scleroderma, or someone in your patient's family has one of these diseases. The same is true if your patient has symptoms of one of these diseases.</li>
                        <li>More detailed <a href="{!URLFOR($Resource.Lapband,'Lapband/pdf/lapband_AP_dfu.pdf')}" target="_blank" class="localhreflink">risk information</a> is available in the Directions for Use.</li>
                    </ul>
                </div> <!-- id="content-full" -->
                </div>
            </div> <!-- id="content" -->
            <c:LWHCPMapOnFoot />
                      
        
        <c:LWHCPFooter />
        <div id="footer" style="background:none !important;">
            <div class="footer-references">
                <h5>References:</h5>
                <ol>
                    <li>Dixon John, Chapman Leon, O&#8217;Brien Paul. Marked Improvement in Asthma after LAP-BAND<sup>&#174;</sup> Surgery for Morbid Obesity. <em>Obesity Surgery</em>. 1999.</li>
                    <li>Dixon John, O&#8217;Brien Paul. Health Outcomes of Severely Obese Type 2 Diabetic Subjects 1 Year After Laparoscopic Adjustable Gastric Banding. <em>Diabetes Care, Volume 25, Number 2.</em> February 2002.</li>
                    <li>Dixon John, Schachter Linda, O&#8217;Brien Paul. Sleep Disturbance and Obesity: Changes Following Surgically Induced Weight Loss. <em>Arch Intern Med/Vol 161.</em> Jan 8, 2001.</li>
                    <li>Dixon John, O&#8217;Brien Paul. Gastroesophageal Reflux in Obesity: The Effect of LAP-BAND<sup>&#174;</sup> Placement. <em>Obesity Surgery.</em> 1999.</li>
                    <li>LAP-BAND<sup>&#174;</sup> [directions for use (DFU)]. Santa Barbara, CA: Allergan, Inc; 2011.</li>
                    <li>LAP-BAND<sup>&#174;</sup> Adjustable Gastric Banding System P000008/S017. Gastroenterology and Urology Devices Panel, Food and Drug Administration. http://www.fda.gov/downloads/AdvisoryCommittees/CommitteesMeetingMaterials/MedicalDevice/MedicalDevicesAdvisoryCommittee/Gastroenterology-UrologyDevicesPanel/UCM237422.ppt. Accessed March 16, 2011.</li>
                    <li>O&#8217;Brien PE, McPhail T, Chaston TB, Dixon JB. Systematic review of medium-term weight loss after bariatric operations. <em>Obes Surg.</em> 2006.</li>
                </ol>
            </div>
            </div>
  
        <c:LWHCPFooter2 />
        </div> <!-- id="container" -->   
    </body>      
</apex:page>
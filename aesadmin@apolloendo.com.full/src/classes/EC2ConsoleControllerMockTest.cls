@isTest
global class EC2ConsoleControllerMockTest implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req) {
    	
    	system.debug(req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"location":"its address","lat":1.11111111,"lng":2.22222222}');
        res.setStatusCode(200);
        return res;
    }
}
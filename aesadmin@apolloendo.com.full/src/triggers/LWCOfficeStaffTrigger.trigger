trigger LWCOfficeStaffTrigger on Office_Staff__c (after insert, after update, after delete) {
	if(Trigger.isInsert){
        if(Trigger.isAfter){
        	LWCOfficeStaffTriggerHandler.handlerOnInsert(Trigger.new);
        }
    }
    
    if(Trigger.isUpdate){
    	if(Trigger.isAfter){
        	if(Trigger.old[0].Is_Active__c!=Trigger.new[0].Is_Active__c && Trigger.new[0].Is_Active__c== false)	LWCOfficeStaffTriggerHandler.handlerOnDelete(Trigger.new);
        	else if(Trigger.old[0].Is_Active__c!=Trigger.new[0].Is_Active__c && Trigger.new[0].Is_Active__c== true)	LWCOfficeStaffTriggerHandler.handlerOnActivate(Trigger.new);
        	else	LWCOfficeStaffTriggerHandler.handlerOnUpdate(Trigger.old, Trigger.new);
        }
    }
}
<apex:page showHeader="false" standardStylesheets="false" sidebar="false">
<html>
<head>
<title>What Will It Cost? | The LAP-BAND® System</title>
<meta name="pageset" content="costNav" />
<meta name="subnavset" content="subCost" />
<meta name="description" content="Learn more about the cost of the LAP-BAND® System and the benefits of insurance verification." />
<c:au_lapband_head />
</head>

<body>

<!-- SET: WRAPPER -->
<div class="wrapper">
    
    <!-- SET: HEADER -->
             <c:au_lapband_nav />
             
                 
            <!-- END: HEADER -->
            
            <div class="baner inner8">
                <ul>
                   <li>
                       <div class="container">
                           <div class="baner_txt inner1">
                               <span class="icon">
                               <apex:image value="{!URLFOR($Resource.r_lapband_images2, 'images/3.2_WhatObesityCostsYou.png')}" alt="img" />
                               </span>
                               <h3>What Will It Cost?</h3>             
                           </div>
                       </div>
                   </li>
                </ul>
            </div>
            
  <!-- SET: MAIN CONTENT -->
            <div class="main_content">
                <div class="content">               
                   <div class="container">
                       <div class="para iner11">
                       
<p>Okay, let’s get down to brass tacks. We’re going to help you figure out how the LAP-BAND<sup>®</sup> System can fit your budget.</p>

<p>In this section you can find information and support that will give you a good idea of what the LAP-BAND<sup>®</sup> System might cost, as well as your financing options. Of course, the actual costs will depend on your individual situation and several factors, such as your health insurance plan, and the surgeon and hospital you choose. </p>

<p>Researching your insurance coverage first could ease your mind when it comes to financing the procedure. Over 90% of the LAP-BAND<sup>®</sup> System cases performed in 2012 were reimbursed in full or in part by insurance, according to market research.<sup>1</sup>
</p>
<br />
<h5>Average Costs of the LAP-BAND<sup>®</sup> System. </h5>
<br />
<p>The LAP-BAND<sup>®</sup> System typically costs anywhere between $9,000 and $18,000. The cost of your LAP-BAND<sup>®</sup> System may or may not fall within this range. Geography, medical profile, and insurance coverage are all factors that determine your final cost. </p>

<p>Let us answer your questions. <a href="tel:1-800-LAPBAND"><strong>Call 1-800-LAPBAND</strong></a>.</p>
<br />             
<h5>Having the LAP-BAND<sup>®</sup> System procedure outside of the U.S.</h5>
<br />
<p>Long-term success with the LAP-BAND<sup>®</sup> System requires follow-up and support from a bariatric practice—something that a doctor in another country may not be able to provide. Regular care and follow-up with a qualified surgeon close to home is important for losing the most weight with your LAP-BAND<sup>®</sup> System. Contact a local LAP-BAND<sup>®</sup> System Specialist to learn more about your insurance coverage and financing options.</p>

<p>Please note that the latest version of the LAP-BAND<sup>®</sup> System is not available in Mexico.</p>
                        
                       </div>
                       <div class="iner11_rgt_part">
                       
                           <c:au_lapband_module_call />
                           
                           <c:au_lapband_module_financing />
                           
                           <c:au_lapband_module_ahead />
                       
                       </div>
                   </div>
                   
                   <c:au_lapband_call />
                   
              </div>
            </div>
                
          
           <!-- END: MAIN CONTENT -->
            <!-- SET: FOOTER -->
            <div class="footer">
               <div class="container">
                  <div class="footer_inn">
                      <c:au_lapband_footer_isi />
                      <br />                 
                          <p style="font-family: 'Nunito', sans-serif; font-size: 12px; color: #656565; font-weight: 300;">
                          <strong>Reference:</strong>
                          <br />1. Apollo Endosurgery, Inc., Bariatric Surgeon Market Share Tracker. November 2012, Slide 14.
                          </p>
                  </div>
               </div>
               
               <c:au_lapband_footer_nav />
               
            </div>
            <!-- END: FOOTER -->
            
        </div>
    <!-- END: CONTAINER -->
    
<!-- </div> -->
<!-- END: WRAPPER -->

<c:au_lapband_footer_scripts />

</body>
</html>

</apex:page>
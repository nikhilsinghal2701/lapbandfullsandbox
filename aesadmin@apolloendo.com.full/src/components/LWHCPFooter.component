<apex:component layout="none">      
    <div id="footer">
        <div id="footer-header">
            <h4>Have a question? Need more information? 1-800-LAP-BAND The LAP-BAND&reg; System Information Hotline is available 7 days a week.</h4>
            <ul id="social-nav">
                <li>
                    <a class="twitterIcon" target="_blank" title="Follow our Twitter Account: @LapBandAP" href="http://www.twitter.com/lapbandap">twitter</a>
                </li>
                <li>
                    <a class="youtubeIcon" target="_blank" title="Visit our YouTube Page: www.youtube.com/lapband" href="http://www.youtube.com/lapband">youtube</a>
                </li>
                <li>
                    <a class="facebookIcon" target="_blank" title="Visit our Facebook Page: www.facebook.com/pages/LAP-BAND-APR-System/106128463816" href="http://www.facebook.com/pages/LAP-BAND-APR-System/106128463816">facebook</a>
                </li>
                <li>
                    <a class="lbtalkIcon" target="_blank" title="Visit the largest LAP-BAND® AP Forum - LapBandTalk: www.lapbandtalk.com/resources/" href="http://www.lapbandtalk.com/">lapbandtalk</a>
                </li>
            </ul>
        </div>
        <div id="footer-isi">
            <h5>Important LAP-BAND<sup>&reg;</sup> System Safety Information</h5>
            <p><strong>Indications:</strong> The LAP-BAND<sup>&reg;</sup> System is indicated for weight reduction for patients with obesity, with a Body Mass Index (BMI) of at least 40 kg/m<sup>2</sup> or a BMI of at least 30 kg/m<sup>2</sup> with one or more obesity-related comorbid conditions.</p>
            <p>It is indicated for use only in adult patients who have failed more conservative weight reduction alternatives, such as supervised diet, exercise and behavior modification programs. Patients who elect to have this surgery must make the commitment to accept significant changes in their eating habits for the rest of their lives.</p>
            <p><strong>Contraindications:</strong> The LAP-BAND<sup>&reg;</sup> System is not recommended for non-adult patients, patients with conditions that may make them poor surgical candidates or increase the risk of poor results (e.g., inflammatory or cardiopulmonary diseases, GI conditions, symptoms or family history of autoimmune disease, cirrhosis), who are unwilling or unable to comply with the required dietary restrictions, who have alcohol or drug addictions, or who currently are or may be pregnant.</p>
            <p><strong>Warnings:</strong> The LAP-BAND<sup>&reg;</sup> System is a long-term implant. Explant and replacement surgery may be required. Patients who become pregnant or severely ill, or who require more extensive nutrition may require deflation of their bands. Anti-inflammatory agents, such as aspirin, should be used with caution and may contribute to an increased risk of band erosion.</p>
            <p><strong>Adverse Events:</strong> Placement of the LAP-BAND<sup>&reg;</sup> System is major surgery and, as with any surgery, death can occur. Possible complications include the risks associated with the medications and methods used during surgery, the risks associated with any surgical procedure, and the patient's ability to tolerate a foreign object implanted in the body.</p>
            <p>Band slippage, erosion and deflation, reflux, obstruction of the stomach, dilation of the esophagus, infection, or nausea and vomiting may occur. Reoperation may be required.</p>
            <p>Rapid weight loss may result in complications that may require additional surgery. Deflation of the band may alleviate excessively rapid weight loss or esophageal dilation.</p>
            <p><strong>Important:</strong> For full safety information please <apex:outputLink value="{!URLFOR($Page.LWHCPSafetyInformation,null,null)}"> click here</apex:outputLink><!--<a href="/Safety-Information/">click here</a>-->, talk with your doctor, or call Apollo Customer Support at 1-855-551-3123.</p>
            <p><strong>CAUTION: Rx only.</strong></p>
        </div>
    
        
        </div>
    <script type="text/javascript">
          var _gaq = _gaq || [];
           _gaq.push(['_setAccount', 'UA-47491562-1']);
           _gaq.push(['_trackPageview']);
          (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
 
         ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
 
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
          
         window.mobilecheck = function() {
            var check = false;
            (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|android|ipad|playbook|silk|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
            return check; 
         }
         
         jQuery(document).ready(function(){
            if(window.mobilecheck ()){
                window.location.href = 'http://m.lapband.com/';
            }
        });
        
   </script>

    <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
         
          ga('create', 'UA-44927259-2', 'lapband.com');
          ga('send', 'pageview');
    </script>
    
    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-44927259-3']);
      _gaq.push(['_setDomainName', 'lapband.com']);
      _gaq.push(['_setAllowLinker', true]);
      _gaq.push(['_trackPageview']);
    
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
    
    <script type="text/javascript">
        piAId = '37432';
        piCId = '1494';
        (function() {
        function async_load(){
        var s = document.createElement('script'); s.type = 'text/javascript';
        s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
        var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
        }
        if(window.attachEvent) { window.attachEvent('onload', async_load); }
        else { window.addEventListener('load', async_load, false); }
        })();
    </script>
    
<!--- GetSmart code --->
<script type="text/javascript">
    var _gsc = _gsc || [];
    _gsc.push(['_setWebsite', '1DDJVWMN']);
    (function() {
        var gsc = document.createElement('script'); gsc.type = 'text/javascript'; gsc.async = true;
        gsc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'c.getsmartcontent.com/gsc.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gsc, s);
    })();
</script>    
<!--- End GetSmart code --->    
  
</apex:component>
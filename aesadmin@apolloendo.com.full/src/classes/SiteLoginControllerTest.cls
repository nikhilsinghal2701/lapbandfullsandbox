/**
 * An apex page controller that exposes the site login functionality
 */
@IsTest global with sharing class SiteLoginControllerTest {
    @IsTest(SeeAllData=true) global static void testSiteLoginController () {
        PageReference PageRef = Page.SiteLogin;
        Test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('startURL','');
        
        // Instantiate a new controller with all parameters in the page
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
        controller.remember = true;

        System.assertEquals(controller.login(),null);
        
        controller.remember = false;
        
        System.assert(controller.login() == null);                           
    }    
}
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SurgeonViewControllerTest {

    static testMethod void surgeonViewTest() {

    	//get record type ID for Account 
    	Schema.DescribeSObjectResult sObjDescrAcc = Schema.SObjectType.Account;
    	Map<String, Schema.RecordTypeInfo> accRecTypeInfo = sObjDescrAcc.getRecordTypeInfosByName();
		
    	Account a1 = TestUtil.createAccounts(1, false, null)[0];
    	a1.RecordTypeId = accRecTypeInfo.get('Surgeon').getRecordTypeId();
    	a1.Name = 'Test SurgeonViewController';
   	
    	insert a1;
		
        //create Surgeon
        Surgeon__c s1 = TestUtil.createSurgeons(1, false, null)[0];
        s1.Surgeon_Account__c = a1.Id;
        
        insert s1;
         
        //create office
        Office__c o1 = TestUtil.createOffices(1, false, null)[0];
        o1.Specialist__c = a1.Id;
        
        insert o1;
        
        Test.startTest();
        
        ApexPages.StandardController ctrl = new ApexPages.StandardController(s1);
        SurgeonViewController extn = new SurgeonViewController(ctrl);

        Test.stopTest();

        System.assertEquals (1, extn.officeList.size());
        System.assertEquals (a1.Id, extn.specialistID);
        System.assert (extn.hasOffices);
    }
}
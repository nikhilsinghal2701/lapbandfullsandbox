@isTest
public with sharing class EC2ConsoleControllerTest {	
	
	public static testmethod void testConstructor() { 
		EC2ConsoleController ecc = new EC2ConsoleController();
		String credName = ecc.createTestCredentials();
	 	ecc.AWSCredentialName = credName;
	 	//test start	
		Test.startTest();	
		Test.setMock(HttpCalloutMock.class, new EC2ConsoleControllerMockTest());
		ecc.constructor();			
		Test.stopTest();
		//test end
	}
	public static testmethod void testRunInstances() { 
		try{
			EC2ConsoleController ecc = new EC2ConsoleController();
			String credName = ecc.createTestCredentials();
	 		ecc.AWSCredentialName = credName;
			PageReference pageRef = new PageReference('for RunInstances test');
			pageRef.getParameters().put('imageId','im01');
			//test start	
			Test.startTest();	
			Test.setMock(HttpCalloutMock.class, new EC2ConsoleControllerMockTest());
			ecc.constructor();	
			ecc.RunInstances();			
			Test.stopTest();
			//test end
			
		}catch(Exception ex){
			
		}	
	}
	public static testmethod void testTerminateInstances() { 
		try{
			EC2ConsoleController ecc = new EC2ConsoleController();
			String credName = ecc.createTestCredentials();
	 		ecc.AWSCredentialName = credName;
			PageReference pageRef = new PageReference('for TerminateInstances test');
			pageRef.getParameters().put('instanceId','in01');
			//test start	
			Test.startTest();	
			Test.setMock(HttpCalloutMock.class, new EC2ConsoleControllerMockTest());
			ecc.constructor();	
			ecc.TerminateInstances();			
			Test.stopTest();
			//test end
			
		}catch(Exception ex){
			
		}	
	}
	public static testmethod void testRebootInstances() {
		try{
			EC2ConsoleController ecc = new EC2ConsoleController();
			String credName = ecc.createTestCredentials();
	 		ecc.AWSCredentialName = credName;
			PageReference pageRef = new PageReference('for RebootInstances test');
			pageRef.getParameters().put('instanceId','ri01');
			//test start	
			Test.startTest();	
			Test.setMock(HttpCalloutMock.class, new EC2ConsoleControllerMockTest());
			ecc.constructor();	
			ecc.RebootInstances();			
			Test.stopTest();
			//test end
			
		}catch(Exception ex){
			
		}
	}
	public static testmethod void testGetOwner() { 
		try{
			EC2ConsoleController ecc = new EC2ConsoleController();
			
			//test start	
			Test.startTest();	
			ecc.getOwners();
			Test.stopTest();
			//test end
			
		}catch(Exception ex){
			
		}
			
	}
	
	public static testmethod void testRefreshImages() { 
		try{
			EC2ConsoleController ecc = new EC2ConsoleController();
			String credName = ecc.createTestCredentials();
	 		ecc.AWSCredentialName = credName;
			//test start	
			Test.startTest();	
			Test.setMock(HttpCalloutMock.class, new EC2ConsoleControllerMockTest());
			ecc.refreshImages();
			Test.stopTest();
			//test end
			
		}catch(Exception ex){
			
		}	
	}
}
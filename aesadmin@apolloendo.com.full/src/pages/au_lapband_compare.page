<apex:page showHeader="false" standardStylesheets="false" sidebar="false">
<html>
<head>
<title>Compare to Other Surgical | The LAP-BAND® System</title>
<meta name="description" content="Compare the LAP-BAND® System to other surgical options like Gastric Bypass and Sleeve Gastrectomy." />
<meta content="aboutNav" name="pageset" />
<meta name="subnavset" content="subRight" />
<c:au_lapband_head />
</head>

<body>

<!-- SET: WRAPPER -->
<div class="wrapper">
    
    <!-- SET: HEADER -->
             <c:au_lapband_nav />
             
                 
            <!-- END: HEADER -->
            
            <div class="baner inner-compare">
                <ul>
                   <li>
                       <div class="container">
                           <div class="baner_txt inner1">
                               <span class="icon">
                               <apex:image value="{!URLFOR($Resource.r_lapband_images2, 'images/1.3.1_Compare_KO.png')}" alt="img" />
                               <!-- <img src="images/iner11_icon.png" alt="img"> -->
                               </span>
                               <h3>Compare to other<br />surgical options.</h3>                
                           </div>
                       </div>
                   </li>
                </ul>
            </div>

            
  <!-- SET: MAIN CONTENT -->
            <div class="main_content">
                <div class="content">
                   <div class="utility">
                      <div class="container">
                      
                        <div class="utility_in right-border">
                          
                          <p>The LAP-BAND<sup>&reg;</sup> System is truly unique. 
                          Not only because it&rsquo;s a proven weight loss solution—and not only because it works for many people who’ve 
                          had little success with diet and exercise—but also because it’s far less invasive than irreversible 
                          procedures like Gastric Bypass and Sleeve Gastrectomy.<br /><br />
                          Watch an animation of the LAP-BAND<sup>®</sup> System Procedure.
                          <br />
                          <br />
                          
                          <a href="//fast.wistia.net/embed/iframe/6ulexduknb?popover=true" class="wistia-popover[height=360,playerColor=7b796a,width=640]">
                          <img src="https://embed-ssl.wistia.com/deliveries/1b2ed3c8fa6e4d475b0db5dc47b107948093c43d.jpg?image_play_button=true&image_play_button_color=7b796ae0&image_crop_resized=280x158" alt="" />
                          </a>
                          </p>
                          <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/popover-v1.js"></script>
                          <br /><br />
                                
<h5 align="center">Sleeve Gastrectomy</h5>

<p style="padding-top:0px;">
    <span>Performed laparoscopically, more than 80% of the stomach is amputated to create a sleeve.<sup>1,2</sup></span><br /><br />
    <span>A more invasive procedure than a gastric band that is permanent and irreversible.<sup>2</sup></span><br /><br />
    <span>Cannot be adjusted.<sup>2</sup></span><br /><br />
    <span>Watch an animation of a Sleeve Gastrectomy Procedure</span>
    <br /><br />
    
<a href="//fast.wistia.net/embed/iframe/da8eif2899?popover=true" class="wistia-popover[height=360,playerColor=7b796a,width=640]">
<img src="https://embed-ssl.wistia.com/deliveries/b8fc5a335b2670e8cfd89a03c75698776bda0af4.jpg?image_play_button=false&image_play_button_color=7b796ae0&image_crop_resized=280x158" alt="" />
<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/popover-v1.js"></script>        
               
    <!-- <div class="video">[Video player]</div> -->
    
</a>
    
    
</p>
<br />
                                
                                <h5 align="center">Gastric Bypass</h5>


    <p style="padding-top:0px;">
        <span>Performed either open or laparoscopically, the stomach is cut and the intestines rerouted to bypass significant digestion.<sup>3</sup></span> <br /><br />
        <span>Extremely difficult to reverse.<sup>3</sup></span> <br /><br />
        <span>Cannot be adjusted.<sup>3</sup></span> <br /><br />
        <span>Watch an animation of a Gastric Bypass Procedure</span>
       <br /><br />
        
<a href="//fast.wistia.net/embed/iframe/bf3ktyi2k3?popover=true" class="wistia-popover[height=360,playerColor=7b796a,width=640]">
<img src="https://embed-ssl.wistia.com/deliveries/3dea68b89c1cd0207bc1258cd77068e5d1151028.jpg?image_play_button=false&image_play_button_color=7b796ae0&image_crop_resized=280x158" alt="" />
<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/popover-v1.js"></script>
        
        <!-- <div class="video">[Video player]</div> -->

</a><br />        
      <br />  
    </p>

                                <div>
<h5 style="text-align: left;">A Safety Overview.</h5>
<p style="padding-top:0px;">A recent review of data from the American Society for Metabolic and Bariatric Surgery and the American College of Surgeons databases<sup>4,5</sup> showed the following:</p><br />
                                
                               
                                <table id="compare-graph" border="1" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <th valign="middle">&nbsp;</th>
                                    <th style="border-left: 1px #00bce8 solid;" align="left" valign="middle">LAPAROSCOPIC<br />
                                    ADJUSTABLE<br />
                                    GASTRIC BANDING</th>
                                    <th valign="middle">SLEEVE GASTRECTOMY</th>
                                    <th valign="middle">GASTRIC BYPASS</th>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="middle">Total Complications<sup>5</sup></td>
                                    <td align="center" valign="middle">6%</td>
                                    <td align="center" valign="middle">8%</td>
                                    <td align="center" valign="middle">24%</td>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="middle">Reoperation Rate<sup>4</sup></td>
                                    <td align="center" valign="middle">1%</td>
                                    <td align="center" valign="middle">3%</td>
                                    <td align="center" valign="middle">5%</td>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="middle">Hospital Stay<br />After Procedure<sup>4</sup></td>
                                    <td align="center" valign="middle">Less than 1</td>
                                    <td align="center" valign="middle">Approximately 3 Days</td>
                                    <td align="center" valign="middle">Approximately 3 Days</td>
                                  </tr>
                                  <tr>
                                    <td align="left" valign="middle">Illness After 30 Days<sup>4</sup></td>
                                    <td align="center" valign="middle">1%</td>
                                    <td align="center" valign="middle">6%</td>
                                    <td align="center" valign="middle">6%</td>
                                  </tr>
                          </table>

                        </div>
                          </div>
                          
                           <div class="iner11_rgt_part">
                           
                           <c:au_lapband_module_call />
                           
                           <c:au_lapband_module_fit />
                           
                           <c:au_lapband_module_sign_up />
                           
                           </div>
                        
                      </div>
                       
                   </div>
                   
                     <c:au_lapband_call />
                   
                </div>
            </div>
                 
                    
          
           <!-- END: MAIN CONTENT -->
            <!-- SET: FOOTER -->
            <div class="footer">
               <div class="container">
                  <div class="footer_inn">
                      <c:au_lapband_footer_isi />
                      <br />                 
                      <p style="font-family: 'Nunito', sans-serif; font-size: 12px; color: #656565; font-weight: 300;">
                      <strong>References:</strong>
       
<br />1. Weiner Rudolf, et al. Laparoscopic Sleeve Gastrectomy- Influence of Sleeve Size and Resected Gastric Volume. Obesity Surgery. 2007.

<br />2. Needleman Bradley, Happel Lynn. Bariatric Surgery: Choosing the Optimal Procedure. Surgical Clinics of North America. 2008.

<br />3. The LAP-BAND<sup>®</sup> System Surgical Aid in the Treatment of Obesity – a decision guide for adults. Austin, TX: Apollo Endosurgery, Inc; 2014 GRF-00305-00R01.

<br />4. Hutter Matthew, et al. First Report from the American College of Surgeons Bariatric Surgery Center Network: Laparoscopic Sleeve Gastrectomy has Morbidity and Effectiveness Positioned Between the Band and the Bypass. September 2001.

<br />5. DeMaria Eric, Pate Virginia, Warthen Michael, Winegar Deborah. Baseline data from American Society for Metabolic and Bariatric Surgery-designated Bariatric Surgery Centers of Excellence using the Bariatric Outcomes Longitudinal Database. Surgery for Obesity and Related Diseases 6. 2010.
               
                      
                      
                      </p>
                  </div>
               </div>
               
               <c:au_lapband_footer_nav />
               
            </div>
            <!-- END: FOOTER -->
            
        </div>
    <!-- END: CONTAINER -->
    
<!-- </div> -->
<!-- END: WRAPPER -->

<c:au_lapband_footer_scripts />

</body>
</html>

</apex:page>
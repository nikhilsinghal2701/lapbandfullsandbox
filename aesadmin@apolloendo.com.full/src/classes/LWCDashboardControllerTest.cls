/**
Class: LWCDashboardControllerTest
Purpose: Test Class for LWCDashboardController
Author – Wade Liu
Date – 03/10/2014
*/
@isTest
private class LWCDashboardControllerTest{

    static testMethod void testCreatesDownloadLink() {
        // prepare data
        Account account = TestUtil.createAccounts(1, true, null)[0];
        
        Contact contact = TestUtil.createContacts(1, true, new Map<String, Object>{'AccountId' => account.Id})[0];
        
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = :LWCConstantDeclaration.REGISTEREDPROFILE Limit 1];
        User u = TestUtil.createUsers(1, false, new Map<String, Object>{'ContactId' => contact.Id,
                                                                        'ProfileId' => portalProfile.Id })[0];
        

        Surgeon__c surgeon = TestUtil.createSurgeons(1, true, new Map<String, Object>{'Surgeon_Account__c' => account.Id})[0];
        
        List<Attachment> attachments = TestUtil.createAttachments(10, true, new Map<String, Object>{'ParentId' => surgeon.Id});
        
     //   List<AWS_S3_Object__c> docList = TestUtil.createDocs(10, true, null);
        
        System.runAs(u){
            List<AWS_S3_Object__c> docList = TestUtil.createDocs(10, true, null);
            
            PageReference pageRef = Page.LWCDashboard;
            Test.setCurrentPage(pageRef);
            
            // add parameter pg = LWCDashboard
            ApexPages.currentPage().getParameters().put('pg', LWCConstantDeclaration.LWCDASHBOARD);
            
            LWCDashboardController controller = new LWCDashboardController();
            
            System.assertEquals( 10, controller.libDocList.size() );
            System.assertEquals( 'surgeon_0', controller.surgeon.Display_Name__c );
            
            
            // add parameter pg = LWCdocumentlibrary
            ApexPages.currentPage().getParameters().put('pg', LWCConstantDeclaration.LWCDOCUMENTLIBRARY);
            
            LWCDashboardController controller2 = new LWCDashboardController();
            System.assertEquals( 10, controller2.previewLinkMap.keySet().size() );
            
            List<String> access = new List<String>{'private', 'public-read'};
            List<AWS_S3_Object__c> docs = TestUtil.createDocs(2, true, new Map<String, Object>{'FileLink__c' => 'test.pdf',
                                                                                    'File_Name__c' => 'file',
                                                                                    'Bucket_Name__c' => 'bucket',
                                                                                    'Access__c' => access});
            
            
            AWSKey__c t1 = TestUtil.createKeys(1, true, null)[0];
            
            
            controller2.s3ObjId = docs[0].Id;
            String downloadUrl = controller2.createsDownloadLink().getUrl();
            
            controller2.s3ObjId = docs[1].Id;
            downloadUrl = controller2.createsDownloadLink().getUrl();
        }
    }
  
}
/*
Class: LWLocationCalloutsTest
Purpose: Test Class for Callout Class LWLocationCallouts
Author: Chris
Created DAte: 03/07/2014
*/
@isTest
public with sharing class LWLocationCalloutsTest {
	
	
	
	static testMethod void testGetLocation(){
		Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
		Id idOfficeRecordType = Office__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Pending Office').getRecordTypeId();
		Account acc = TestUtil.createAccounts(1, true, new Map<String, Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon})[0];
		
		List<Office__c> offices = new List<Office__c>();
		Office__c office = new Office__c();
		
		office.Specialist__c = acc.Id;
		office.RecordTypeId = idOfficeRecordType;
		office.Status__c = 'Submitted';
		office.Zip_Code__c = '94536';
		office.Name = 'Dr Office 5';
		office.Address_1__c = '38881 Hastings Street';
		office.City__c = 'Fremont';
		office.State__c = 'CA';
		offices.add(office);
		Insert offices;
		Map<Id,Office__c> officeIdToOfficeMap = new Map<Id,Office__c>([SELECT Id FROM Office__c]);
		List<Id> officeIds = new List<Id>(officeIdToOfficeMap.keySet());
		system.debug('+++'+officeIds);
		Test.startTest();
		
		Test.setMock(HttpCalloutMock.class, new LWLocationCalloutsMockTest());
		
		LWLocationCallouts.getLocation(officeIds);
		Test.stopTest();
		offices = [SELECT Location__Latitude__s,Location__Longitude__s,Zip_Code__c FROM Office__c];
		for( Office__c o : offices ){
			system.assertNotEquals(null, o.Location__Latitude__s);
			system.assertNotEquals(null, o.Location__Longitude__s);
		
		}
	}
}
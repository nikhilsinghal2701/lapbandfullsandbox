/**
Class: LWCWithoutSharing
without sharing class for standard object access
Author – Vinay L
Date – 02/19/2014
 */
public without sharing class LWCWithoutSharing {
    
    public class insertException extends Exception {
    }
    
    //update surgeon
    public static void updateSurgeon(Surgeon__c surgeon ){
        update surgeon;
    }
    
    //update surgeon
    public static void insertAttachment(Attachment att ){
        insert att;
    }
    
    //delete membership
    public static void deleteMembership(List<Membership_Accreditation__c> remMemList ){
    	delete remMemList;
    }
    
    //insert membership
    public static void insertMembership(List<Membership_Accreditation__c> newMemList ){
        insert newMemList;
    }
    
    //get record type id
    public static Id gtRecordTypeId(String devName){
        Id recTypId = [SELECT Id FROM RecordType WHERE DeveloperName = :devName AND SobjectType = :LWCConstantDeclaration.OFFICE][0].Id;
        return recTypId;
    }
    
    //insert office
    public static Office__c insertOffice(Office__c ofc){
       insert ofc;
       return ofc;  
    }
   
   //update office 
    public static Office__c updateOffice(Office__c ofc){
       update ofc;
       return ofc;  
    }
    
    //delete office, set status to delete, change master edit flag to false if child record is deleted
    public static void deleteOffice(String deleteOfficeId){
        List<Office__c> deleteOffices = [select Id, Approved_Office__c from Office__c
                                        where Id = :deleteOfficeId];
        List<Id> parentIdList = new List<Id>();
        for(Office__c o : deleteOffices){
            o.Status__c = 'Deleted';
            if(o.Approved_Office__c !=null)	parentIdList.add(o.Approved_Office__c);
        }
        if(parentIdList.size()>0){
            List<Office__c> parentList = [select Id, Is_Edit_Flag__c from Office__c where Id IN :parentIdList];
            for(Office__c o : parentList){
                o.Is_Edit_Flag__c = false;
            }
            update parentList;
        }
        
        update deleteOffices;
    }
    
    //reject office
    public static void rejectOfcApproval(Id ofcId){
        List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId 
                                                FROM ProcessInstanceWorkitem 
                                               WHERE ProcessInstance.TargetObjectId =: ofcId];
        Approval.Processworkitemrequest appR = new Approval.Processworkitemrequest();
         
        appR.setComments('Rejected as requested');
        appR.setAction('Reject');
        appR.setWorkitemId(piwi.get(0).Id);
        Approval.ProcessResult result = Approval.process(appR);
    }
    
    //submit office for approval
    public static void submitOfcApproval(Id ofcId){
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
        app.setObjectId(ofcId);
        Approval.ProcessResult result = Approval.process(app);
        return;
    }
    
    //insert office list record
    public static void insertOffice(List<Office__c> ofcList){
       insert ofcList;
       return ;  
    }
    
    //update office list reocrd
    public static void updateOfficeList(List<Office__c> ofcList){
       update ofcList;
       return ;  
    }
    
    //submit office list for approval
    public static void submitOfcApproval(List<Office__c> offAppList){
        for(Office__c o : offAppList){
            Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
            app.setObjectId(o.Id);
            Approval.ProcessResult result = Approval.process(app);
        }
        return;
    }
    
    //insert office staff
    public static Office_Staff__c insertOfficeStaff(Office_Staff__c os){
        
        if(getUsers(new List<String>{os.E_mail__c}).size()>0)   throw new insertException('User already exists with same email address. Add fail!');
        else    insert os;
        return os;
    }
    
    //update office staff
    public static Office_Staff__c updateOfficeStaff(Office_Staff__c os){
        update os;
        return os;
    }
    
    //delet office staff, inactive
    public static void deleteOfficeStaff(Office_Staff__c os){
        os.Is_Active__c = false;
        update os;
    }
    
    //get user list, used for check unique email address
    public static List<User> getUsers(List<String> emails){
        List<User> users = [select User_Email__c, FirstName, LastName, Email, LastLoginDate from User where User_Email__c IN :emails];
        return users;
    }
    
    //insert hospital list
    public static List<Surgeon_Affiliation__c> insertHospital(List<Surgeon_Affiliation__c> newHosList){
        insert newHosList;
        return newHosList;
    }
    
    //delete hospital list
    public static void deleteHospital(List<Surgeon_Affiliation__c> delHosList){
        delete delHosList;
        return ;
    }
    
    //update master office list
    public static void updateMasterOfficeList(List<Office__c> offices){
        update offices;
    }
    
    //search record type Id
    public static LIST<RecordType> getRecordTypeId(LIST<String> recordTypeName){
    	LIST<RecordType> recordTypeId = [select Id,Name from RecordType where Name IN:recordTypeName];
    	return recordTypeId;
    }  
}
public with sharing class TestUtil {

    //Create custom setting LWURLRewriter__c
    public static List<LWURLRewriter__c> createLWURL(){
        List<LWURLRewriter__c> lwURLs = new List<LWURLRewriter__c>();

        LWURLRewriter__c l = new LWURLRewriter__c();
        l.Name = 'LWAboutUs';
        l.LapbandFriendlyURL__c = '/About-Us';
        l.VisualforcePageURL__c = '/LWAboutUs';
        lwURLs.add(l);

        l = new LWURLRewriter__c();
        l.Name = 'HCPHome';
        l.LapbandFriendlyURL__c = '/HCP';
        l.VisualforcePageURL__c = '/LWHCPHome';
        lwURLs.add(l);

        l = new LWURLRewriter__c();
        l.Name = 'find-a-specialist';
        l.LapbandFriendlyURL__c = '/find-a-specialist/Detail';
        l.VisualforcePageURL__c = '/LWSpecialistDetails/Detail|001R000000oZYnFIAW';
        lwURLs.add(l);


        insert lwURLs;

        return lwURLs;
    }    


    //Create custom setting LBCURLRewriter__c
    public static List<LBCURLRewriter__c> createLBCURL(){
        List<LBCURLRewriter__c> lbcURLs = new List<LBCURLRewriter__c>();
        
        LBCURLRewriter__c l = new LBCURLRewriter__c();
        l.Name = 'aboutUs';
        l.LapbandCenterURL__c = '/about-us';
        l.VisualforcePageURL__c = '/LWCAboutUs';
        lbcURLs.add(l);

        l = new LBCURLRewriter__c();
        l.Name = 'contactUs';
        l.LapbandCenterURL__c = '/contact-us';
        l.VisualforcePageURL__c = '/LWCContactUs';
        lbcURLs.add(l);

        l = new LBCURLRewriter__c();
        l.Name = 'documentlib';
        l.LapbandCenterURL__c = '/document-library';
        l.VisualforcePageURL__c = '/LWCdocumentlibrary/';
        lbcURLs.add(l);

        insert lbcURLs;

        return lbcURLs;
        
    }
    
    //Create AWS_S3_Object__c
    public static List<AWS_S3_Object__c> createDocs( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<AWS_S3_Object__c> docsToInsert = new List<AWS_S3_Object__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            AWS_S3_Object__c doc = new AWS_S3_Object__c();
            doc.Title__c = 'Title_' + i;
            doc.Access__c = 'private';
            docsToInsert.add( doc );
        }
        return createRecords( docsToInsert, doInsert, nameValueMap );
    }
    
    //Create Contacts
    public static List<Contact> createContacts( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Contact> contactsToInsert = new List<Contact>();
        for( Integer i=0; i< numToInsert; i++ ){
            Contact contact = new Contact();
            contact.LastName = 'test_' + i;
            contactsToInsert.add( contact );
        }
        return createRecords( contactsToInsert, doInsert, nameValueMap );
    }
    
    //Create Surgeons
    public static List<Surgeon__c> createSurgeons( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Surgeon__c> surgeonsToInsert = new List<Surgeon__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Surgeon__c surgeon = new Surgeon__c();
            surgeon.Display_Name__c = 'surgeon_' + i;
            surgeonsToInsert.add( surgeon );
        }
        return createRecords( surgeonsToInsert, doInsert, nameValueMap );
    }
    
    public static List<Pricing_Contract__c> createPricingContract( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Pricing_Contract__c> PCToInsert = new List<Pricing_Contract__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Pricing_Contract__c PC = new Pricing_Contract__c();
            PC.Name = 'TestPC' + i;
            PCToInsert.add( PC );
        }
        return createRecords( PCToInsert, doInsert, nameValueMap );
    }
     public static List<Sales_Order__c> createSalesOrder( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Sales_Order__c> SOToInsert = new List<Sales_Order__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Sales_Order__c SO = new Sales_Order__c();
            SO.Name = 'TestSO' + i;
            SOToInsert.add( SO );
        }
        return createRecords( SOToInsert, doInsert, nameValueMap );
    }
    public static List<Sales_Order_Line_Item__c> createSalesOrderLineItem( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Sales_Order_Line_Item__c> SOLIToInsert = new List<Sales_Order_Line_Item__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Sales_Order_Line_Item__c SOLI = new Sales_Order_Line_Item__c();
            SOLI.Name = 'TestSOLI' + i;
            SOLIToInsert.add( SOLI );
        }
        return createRecords( SOLIToInsert, doInsert, nameValueMap );
    }
    
    //Create Attachments
    public static List<Attachment> createAttachments( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Attachment> attachmentsToInsert = new List<Attachment>();
        for( Integer i=0; i< numToInsert; i++ ){
            Attachment attachment = new Attachment();
            attachment.Name = 'attachement_' + i;
            attachment.Body = Blob.valueOf('Test_' + i);
            attachmentsToInsert.add( attachment );
        }
        return createRecords( attachmentsToInsert, doInsert, nameValueMap );
    }
    
    //Create Offices
    public static List<Office__c> createOffices( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Office__c> officesToInsert = new List<Office__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Office__c office = new Office__c ();
            office.Name = 'Office_' + i;
            officesToInsert.add( office );
        }
        return createRecords( officesToInsert, doInsert, nameValueMap );
    }
    
    //Create AWSKey__c
    public static List<AWSKey__c> createKeys( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<AWSKey__c> keysToInsert = new List<AWSKey__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            AWSKey__c key = new AWSKey__c ();
            key.Name = 'AmazonKeys';
            key.key__c = 's' + i;
            key.secret__c = 's' + i;
            keysToInsert.add( key );
        }
        return createRecords( keysToInsert, doInsert, nameValueMap );
    }
    
    //Create PermissionSetAssignment
    public static List<PermissionSetAssignment> createPermissionSetAssignments( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<PermissionSetAssignment> permissionsToInsert = new List<PermissionSetAssignment>();
        for( Integer i=0; i< numToInsert; i++ ){
            PermissionSetAssignment permission = new PermissionSetAssignment ();
            permissionsToInsert.add( permission );
        }
        return createRecords( permissionsToInsert, doInsert, nameValueMap );
    }
    
    //Create PermissionSet
    public static List<PermissionSet> createPermissionSets( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<PermissionSet> permissionSetsToInsert = new List<PermissionSet>();
        for( Integer i=0; i< numToInsert; i++ ){
            PermissionSet permissionSet = new PermissionSet ();
            permissionSetsToInsert.add( permissionSet );
        }
        return createRecords( permissionSetsToInsert, doInsert, nameValueMap );
    }
    
    //Create Leads
    public static List<Lead> createLeads( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Lead> leadsToInsert = new List<Lead>();
        for( Integer i=0; i< numToInsert; i++ ){
            Lead lead = new Lead ();
            lead.LastName = 'test_' + i;
            leadsToInsert.add( lead );
        }
        return createRecords( leadsToInsert, doInsert, nameValueMap );
    }

    //Create Accounts
    public static List<Account> createAccounts( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Account> accountsToInsert = new List<Account>();
        for( Integer i=0; i< numToInsert; i++ ){
            Account acc = new Account();
            acc.Name = 'Test Account' + i;
            accountsToInsert.add( acc );
        }
        return createRecords( accountsToInsert, doInsert, nameValueMap );
    }
    
    //Create AccountTeamMembers
    public static List<AccountTeamMember> createAccountTeamMembers( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
    	List<AccountTeamMember> accountTeamsToInsert = new list<AccountTeamMember>();
    	for( Integer i=0; i< numToInsert; i++ ){
    		AccountTeamMember accTeam = new AccountTeamMember();
    		if (nameValueMap == null || !nameValueMap.containsKey('AccountId')) accTeam.AccountId = createAccounts(1, true, null)[0].Id;
    		if (nameValueMap == null || !nameValueMap.containsKey('UserId')) accTeam.UserId = createUsers(1, true, null)[0].Id;
    		if (nameValueMap == null || !nameValueMap.containsKey('TeamMemberRole')) accTeam.TeamMemberRole = 'Health';
    		accountTeamsToInsert.add(accTeam);
    	}
    	return createRecords(accountTeamsToInsert, doInsert, nameValueMap);
    }
    
    public static List<Territory_Alignment__c> createTAs( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Territory_Alignment__c> tasToInsert = new List<Territory_Alignment__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Territory_Alignment__c ta = new Territory_Alignment__c();
            ta.Zip_Code__c = '000' + i;
            tasToInsert.add( ta );
        }
        return createRecords( tasToInsert, doInsert, nameValueMap );
    }
    
    public static List<Membership_Accreditation__c> createMembershipAccreditation( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Membership_Accreditation__c> membershipAccreditationsToInsert = new List<Membership_Accreditation__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Membership_Accreditation__c membershipAccreditation = new Membership_Accreditation__c ();
            membershipAccreditation.Name = 'membershipAccreditation_' + i;
            membershipAccreditationsToInsert.add( membershipAccreditation );
        }
        return createRecords( membershipAccreditationsToInsert, doInsert, nameValueMap );
    }
    
    
    public static List<Surgeon_Affiliation__c> createSurgeonAffiliations( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Surgeon_Affiliation__c> surgeonAffiliationsToInsert = new List<Surgeon_Affiliation__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Surgeon_Affiliation__c surgeonAffiliation = new Surgeon_Affiliation__c ();
            surgeonAffiliationsToInsert.add( surgeonAffiliation );
        }
        return createRecords( surgeonAffiliationsToInsert, doInsert, nameValueMap );
    }
    
    
    public static List<User> createUsers( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<User> usersToInsert = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        for( Integer i=0; i< numToInsert; i++ ){
            User user = new User();
            user.Alias = 'tUser' + i;
            user.Email='demo.user@perficient.com';
            user.LastName = 'DemoUser' + i;
            user.userName = 'demoUser' + i +'@perficient.com';
            user.ProfileId = p.Id;
            user.EmailEncodingKey = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            user.LocaleSidKey = 'en_US';
            user.TimeZoneSidKey = 'America/Los_Angeles';
            
            usersToInsert.add( user );
        }
        return createRecords( usersToInsert, doInsert, nameValueMap );
    }
    
    public static List<Office_Staff__c> createOfficeStaffs( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Office_Staff__c> officeStaffsToInsert = new List<Office_Staff__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Office_Staff__c os = new Office_Staff__c();
            os.First_Name__c = 'Office' + i;
            os.Last_Name__c = 'Staff' + i;
            os.Type__c = 'RN';
            os.E_mail__c = 'office'+i+'.staff@test.com';
            officeStaffsToInsert.add( os );
        }
        return createRecords( officeStaffsToInsert, doInsert, nameValueMap );
    }
    
    public static List<Currency_Conversion__c> createCurrencyConversions( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
    	List<Currency_Conversion__c> currencyConversionsToInsert = new List<Currency_Conversion__c>();
    	for( Integer i=0; i< numToInsert; i++ ){
            Currency_Conversion__c cc = new Currency_Conversion__c();
            cc.Conversion_Rate__c = 1.13+i;
            cc.ISO_Currency__c = 'AUD';
            currencyConversionsToInsert.add( cc );
        }
        return createRecords( currencyConversionsToInsert, doInsert, nameValueMap );
    }
    
    public static List<Sales_Region__c> createSalesRegions( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
    	List<Sales_Region__c> salesRegionsToInsert = new List<Sales_Region__c>();
    	for( Integer i=0; i<numToInsert; i++ ){
    		Sales_Region__c sr = new Sales_Region__c();
 			salesRegionsToInsert.add(sr);
    	}
    	return createRecords( salesRegionsToInsert, doInsert, nameValueMap );
    }

    public static List<Sales_Territory__c> createSalesTerritories( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
    	List<Sales_Territory__c> salesTerritoriesToInsert = new List<Sales_Territory__c>();
    	for( Integer i=0; i<numToInsert; i++ ){
    		Sales_Territory__c st = new Sales_Territory__c();
 			salesTerritoriesToInsert.add(st);
    	}
    	return createRecords( salesTerritoriesToInsert, doInsert, nameValueMap );
    }
    
    public static List<SObject> createRecords( List<SObject> records, Boolean doInsert, Map<String, Object> attributes ){
  
        Integer i = 0;
        if( attributes != null ){
            for ( Integer j =0; j < records.size(); j ++ ) {
            Sobject record = records[j];
            for (String key : attributes.keySet()) {
                Object value = attributes.get(key);
                if (value instanceof List<Object>) {
                    object obj =  ((List<Object>) value).get(i);
                if( obj instanceof Sobject ){
                  Id sObjectId = ((SObject) obj).Id;
                  record.put( key,sObjectId );
                }
                else {
                  record.put(key,obj);
                }
              } else {
                record.put(key, value);
              }
            }
            i++;
            }
         }
      
        if (doInsert) {
            insert records;
        }
        return records;
    }
}
/* Created: 05/12/2014
 * Author: Sanjay Sankar
 * Dependency: ScheduleHealthMemberUpdate.cls
 * Test Class(es): ScheduleHealthMemberUpdateTest.cls
 * Purpose: To update Account.Health_Member__c with the user ID of the "Health" Account Team Member. A
 *          boolean is driven off of this field to determine if the current user is a "Health" Team Member
 *          This should be re-worked once Account Team Member object is made customizable by Salesforce
 */

global class BatchUpdateHealthMember implements Database.Batchable<sObject>{
	
	global Database.Querylocator start(Database.BatchableContext bc) {
		System.debug ('Inside BatchUpdateHealthMember: Method start()...');
		return Database.getQueryLocator('select Id, Health_Member__c, (select AccountId, UserId from AccountTeamMembers where TeamMemberRole = \'Health\' and (NOT User.UserName like \'admin.perficient@apolloendo.com%\') and (NOT User.UserName like \'integration@apolloendo.com%\')) from account where RecordType.Name = \'Ship To\'');
	}
	
	global void execute (Database.Batchablecontext bc, List<Account> queryList) {
		
		System.debug ('Inside BatchUpdateHealthMember: Method execute()...' + queryList.size() + ' records');
		Map<Id, Id> acctMemberMap = new Map<Id, Id>();
		List<Account> accUpdList = new List<Account>();
		for (Account acc : queryList) {
			if (null != acc.AccountTeamMembers && 0 < acc.AccountTeamMembers.size() && acc.Health_Member__c != acc.AccountTeamMembers[0].UserId) {
				acc.Health_Member__c = acc.AccountTeamMembers[0].UserId;
				accUpdList.add(acc);
			}
		}
		if (accUpdList.size() > 0) {
			update accUpdList;
		}
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug ('Inside BatchUpdateHealthMember: Method finish()...');
	}
}
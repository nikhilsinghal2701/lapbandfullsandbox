global with sharing class ScheduleFASSearchAnalytics implements Schedulable {
  global void execute(SchedulableContext sc){
   ScheduleFASProfileTrackingAnalytics cs = new ScheduleFASProfileTrackingAnalytics();
   Database.executeBatch(cs);
  }
}
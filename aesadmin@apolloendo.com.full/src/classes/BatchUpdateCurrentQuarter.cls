/* Created: 05/12/2014
 * Author: Sanjay Sankar
 * Dependency: ScheduleCurrentQuarterUpdate.cls
 * Test Class(es): ScheduleCurrentQuarterUpdateTest.cls
 * Purpose: To update Forecast.Current_Quarter__c to TRUE for records where current date is within the start and end dates
 *          For all other records that are set to TRUE update the field to FALSE
 */

global class BatchUpdateCurrentQuarter implements Database.Batchable<sObject>{
	private Date cDate = Date.today();
	private Integer currentQtr;
	
	private Integer getQuarter(Date dateValue) {
		if (dateValue.month() == 1 || dateValue.month() == 2 || dateValue.month() == 3) {
			return 1;
		} else if (dateValue.month() == 4 || dateValue.month() == 5 || dateValue.month() == 6) {
			return 2;
		} else if (dateValue.month() == 7 || dateValue.month() == 8 || dateValue.month() == 9) {
			return 3;
		} 
		
		return 4;
	}

	global Database.Querylocator start(Database.BatchableContext bc) {
		System.debug ('Inside BatchUpdateCurrentQuarter: Method start()...');
		currentQtr = getQuarter(cDate);
		return Database.getQueryLocator('select Id, Start_Date__c, End_Date__c, Current_Quarter__c from Forecast__c  where (CALENDAR_QUARTER(Start_Date__c) = :currentQtr and CALENDAR_QUARTER(End_Date__c) = :currentQtr and Current_Quarter__c != true) or ((CALENDAR_QUARTER(Start_Date__c) != :currentQtr or CALENDAR_QUARTER(End_Date__c) != :currentQtr) and Current_Quarter__c = true)');
	}
	
	global void execute (Database.Batchablecontext bc, List<Forecast__c> queryList) {
		System.debug ('Inside BatchUpdateCurrentQuarter: Method execute()...' + queryList.size() + ' records');
		List<Forecast__c> fcUpdList = new List<Forecast__c>();
		for (Forecast__c fc : queryList) {
			fc.Current_Quarter__c = getQuarter(fc.Start_Date__c) == currentQtr && getQuarter(fc.End_Date__c) == currentQtr && fc.Start_Date__c.year() == fc.End_Date__c.year();
			fcUpdList.add(fc);
		}
		update fcUpdList;
	}
	
	global void finish(Database.BatchableContext BC) {
		System.debug ('Inside BatchUpdateCurrentQuarter: Method finish()...');
	}
}
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ForecastTerritoryAttainmentTest {

    static testMethod void attainmentCalculationTest() {
    	//get record type ID for Account Ship To
    	Schema.DescribeSObjectResult sObjDescrAcc = Schema.SObjectType.Account;
    	Map<String, Schema.RecordTypeInfo> accRecTypeInfo = sObjDescrAcc.getRecordTypeInfosByName();

    	//get record type ID for Account Ship To
    	Schema.DescribeSObjectResult sObjDescrSO = Schema.SObjectType.Sales_Order__c;
    	Map<String, Schema.RecordTypeInfo> soRecTypeInfo = sObjDescrSO.getRecordTypeInfosByName();
    	
    	//create region
    	Sales_Region__c reg = new Sales_Region__c();
    	reg.Region_ID__c = 'TESTREGID1';
    	reg.Name = reg.Region_ID__c;
    	reg.Role__c = 'should not matter';
    	reg.Assigned_To__c = UserInfo.getUserId();
    	insert reg;
    	
    	//create territory
    	Sales_Territory__c terr = new Sales_Territory__c();
    	terr.Territory_ID__c = 'TESTTERRID1';
    	terr.Name = terr.Territory_ID__c;
    	terr.Role__c = 'does not matter';
    	terr.Sales_Region__c = reg.Id;
    	terr.Assigned_To__c = UserInfo.getUserId();
    	insert terr;
    	 
    	//create account
    	Account a1 = TestUtil.createAccounts(1, false, null)[0];
    	a1.RecordTypeId = accRecTypeInfo.get('Ship To').getRecordTypeId();
    	a1.Sales_Territory__c = terr.Id;
   	
    	insert a1;

		Sales_Order__c so1 = new Sales_Order__c();
		so1.Name = 'TEST01';
		so1.PO__c = 'TEST_PO1';
		so1.Type__c ='OUS';
		so1.order_type__c = 'OR';
		so1.RecordTypeId = soRecTypeInfo.get('LAP-BAND SYSTEMS').getRecordTypeId();
		so1.Ship_To__c = a1.id;
		so1.Sold_To__c = a1.id;

		insert so1;
		
		Forecast_Territory__c fc1 = new Forecast_Territory__c();
		fc1.Sales_Territory__c = terr.Id;
		fc1.Attainment_Amount__c = 900.0;
		fc1.Start_Date__c = Date.newInstance(2014, 05, 01);
		fc1.End_Date__c = Date.newInstance(2014, 05, 31);
		fc1.Forecast__c = 500.0;

		Forecast_Territory__c fc2 = new Forecast_Territory__c();
		fc2.Sales_Territory__c = terr.Id;
		fc2.Attainment_Amount__c = 1900.0;
		fc2.Start_Date__c = Date.newInstance(2014, 07, 01);
		fc2.End_Date__c = Date.newInstance(2014, 07, 31);
		fc2.Forecast__c = 1500.0;

		insert fc1;
		insert fc2;
		
		for (Forecast_Territory__c fcRec : [select Id, Attainment_Amount__c from Forecast_Territory__c where Sales_Territory__c = :terr.Id]) {
			System.assertEquals(0, fcRec.Attainment_Amount__c);
		}
				
		Test.startTest();
		
		List<Sales_Order_Line_Item__c> items = new List<Sales_Order_Line_Item__c>();
		
		items.add(new Sales_Order_Line_Item__c(
			LineItemId__c = '1',
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '1,500.00',
			Material__c = 'B-23423',
			Description__c = 'LAP-BAND',
			Quantity__c = 2,
			Bill_Date__c = Date.newInstance(2014, 05, 25),
			Due_Date__c = Date.newInstance(2014, 05, 15)
			)
		);

		items.add(new Sales_Order_Line_Item__c(
			LineItemId__c = '2',
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '600.00',
			Material__c = 'B-23423',
			Description__c = 'LAP-BAND',
			Quantity__c = 1,
			Bill_Date__c = Date.newInstance(2014, 06, 03),
			Due_Date__c = Date.newInstance(2014, 06, 23)
			)
		);

		items.add(new Sales_Order_Line_Item__c(
			LineItemId__c = '3',
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '400.00',
			Material__c = 'B-23423',
			Description__c = 'LAP-BAND',
			Quantity__c = 1,
			Bill_Date__c = Date.newInstance(2014, 06, 03),
			Due_Date__c = Date.newInstance(2014, 06, 23)
			)
		);

		items.add(new Sales_Order_Line_Item__c(
			LineItemId__c = '4',
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '800.00',
			Material__c = 'B-23423',
			Description__c = 'LAP-BAND',
			Quantity__c = 1,
			Bill_Date__c = Date.newInstance(2014, 07, 20),
			Due_Date__c = Date.newInstance(2014, 07, 10)
			)
		);
		
		insert items;

		Test.stopTest();
		
		Integer fcCount = [select count() from Forecast_Territory__c where Sales_Territory__c = :terr.Id];
		System.assertEquals(3, fcCount);

		for (Forecast_Territory__c fcRec : [select Id, Quarter__c, Attainment_Amount__c, Forecast__c from Forecast_Territory__c where Sales_Territory__c = :terr.Id]) {
			if (fcRec.Id == fc1.Id) {
				System.assertEquals('Q2-2014', fcRec.Quarter__c);
				System.assertEquals(1500.0, fcRec.Attainment_Amount__c);
				System.assertEquals(500.0, fcRec.Forecast__c);
			} else if (fcRec.Id == fc2.Id) {
				System.assertEquals('Q3-2014', fcRec.Quarter__c);
				System.assertEquals(800.0, fcRec.Attainment_Amount__c);
				System.assertEquals(1500.0, fcRec.Forecast__c);
			} else {
				System.assertEquals('Q2-2014', fcRec.Quarter__c);
				System.assertEquals(1000.0, fcRec.Attainment_Amount__c);
				System.assertEquals(0, fcRec.Forecast__c);
			}
		}
		
		for (Sales_Order_Line_Item__c lineRec : [select Id, Is_OUS_Forecastable__c from Sales_Order_Line_Item__c where Sales_Order__c = :so1.Id]) {
			System.assert(lineRec.Is_OUS_Forecastable__c);
		}
		
    }
}
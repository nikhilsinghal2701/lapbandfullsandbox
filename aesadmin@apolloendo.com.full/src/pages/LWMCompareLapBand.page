<apex:page applyBodyTag="false" showheader="false" standardStylesheets="false" sidebar="false" >
 <head>
    <title>Compare the LAP-BAND&#174; System to Other Options</title>
 </head>
 <apex:composition template="LWMTemplate"/>
 
 <body>
 <div id="content" class="wrapper">
    <c:LWMobileHeader />
     <div id="lapbandCompare" class="mainContent">
        <h1 class="title">Compare the LAP-BAND System to Other Options</h1>

        <p><strong>Gastric banding was shown to be safer than sleeve gastrectomy</strong></p>
        <p>According to data from the Bariatric Outcomes Longitudinal Database<sup><small>5</small></sup>*, sleeve gastrectomy had a 3x greater rate of total complications in the first year when compared to gastric banding.</p>

        <p class="title" id="videosTitle">Watch Procedure Videos</p>
        <p><strong>LAP-BAND<sup><small>&reg;</small></sup> System Procedure</strong></p>
        <section class="video">
            <a class="videoLink" id="lapBandAnimation" data-trackingname="LAP-BAND Procedure Animation" href="https://s3.amazonaws.com/lapband-dev/LapBand_Animation_576x324.mp4"><img src="{!URLFOR($Resource.lapband_mobile_assets,'/Content/images/video/video-thumb-lapband-animation.jpg')}" alt="" width="147" height="84"/></a>
            <p>Animation</p>
        </section>
        <section class="video">
            <a class="videoLink" id="lapBandFootage" data-trackingname="LAP-BAND Procedure Surgical Footage" href="https://s3.amazonaws.com/lapband-dev/LapBand_SurgicalFootage_v06_434x324.mp4"><img src="{!URLFOR($Resource.lapband_mobile_assets,'/Content/images/video/video-thumb-lapband-footage.jpg')}" alt="" width="147" height="84"/></a>
            <p>Surgical Footage</p>
        </section>

        <p><strong>Sleeve Gastrectomy Procedure</strong></p>
        <section class="video">
            <a class="videoLink" id="sleeveAnimation" data-trackingname="Sleeve Gastrectomy Procedure Animation" href="https://s3.amazonaws.com/lapband-dev/Sleeve_Animation_576x324.mp4"><img src="{!URLFOR($Resource.lapband_mobile_assets,'/Content/images/video/video-thumb-gastrectomy-animation.jpg')}" alt="" width="147" height="84"/></a>
            <p>Animation</p>
        </section>
        <section class="video">
            <a class="videoLink" id="sleeveFootage" data-trackingname="Sleeve Gastrectomy Procedure Surgical Footage" href="https://s3.amazonaws.com/lapband-dev/Sleeve_SurgicalFootage_v05_576x322.mp4"><img src="{!URLFOR($Resource.lapband_mobile_assets,'/Content/images/video/video-thumb-gastrectomy-footage.jpg')}" alt="" width="147" height="84"/></a>
            <p>Surgical Footage</p>
        </section>

        <p><strong>Gastric Bypass Procedure</strong></p>
        <section class="video">
            <a class="videoLink" id="bypassAnimation" data-trackingname="Gastric Bypass Procedure Animation" href="https://lapband-dev.s3.amazonaws.com/Bypass_Animation_576x324.mp4"><img src="{!URLFOR($Resource.lapband_mobile_assets,'/Content/images/video/video-thumb-bypass-animation.jpg')}" alt="" width="147" height="84"/></a>
            <p>Animation</p>
        </section>
        <section class="video">
            <a class="videoLink" id="bypassFootage" data-trackingname="Gastric Bypass Procedure Surgical Footage" href="https://lapband-dev.s3.amazonaws.com/Bypass_SurgicalFootage_v06_576x322.mp4"><img src="{!URLFOR($Resource.lapband_mobile_assets,'/Content/images/video/video-thumb-bypass-footage.jpg')}" alt="" width="147" height="84"/></a>
            <p>Surgical Footage</p>
        </section>

        <p class="title" id="whyTitle">Why you should ask for the LAP-BAND Adjustable Gastric Banding System by name:</p>

        <div class="dropdownButton">
            <p class="title" id="apButton">LAP-BAND<sup><small>&reg;</small></sup> AP System</p>
            <button class="downArrow"></button>
        </div>
        <div id="dropdown1" class="dropdown" data-hiding="true">
            <ul>
                <li class="outerList"><p>Performed laparoscopically, a saline filled band is placed around the top of the stomach. There is no stomach amputation or cutting of your intestines during this procedure.<sup><small>2</small></sup></p></li>
                <li class="outerList"><p>Designed for long-term use. Can be removed and reversed if desired.<sup><small>2</small></sup></p></li>
                <li class="outerList"><p>Can be adjusted over time to fit the needs of the patient.<sup><small>2</small></sup></p></li>
            </ul>
        </div>

        <div class="dropdownButton">
            <p class="title" id="sleeveButton">LAP-BAND<sup><small>&reg;</small></sup> AP System</p>
            <button class="downArrow"></button>
        </div>
        <div id="dropdown2" class="dropdown" data-hiding="true">
            <ul>
                <li class="outerList"><p>Performed laparoscopically, more than 80% of the stomach is amputated to create a sleeve.<sup><small>3,4</small></sup></p></li>
                <li class="outerList"><p>A more radical procedure than a gastric band that is permanent and irreversible.<sup><small>4</small></sup></p></li>
                <li class="outerList"><p>Cannot be adjusted.<sup><small>4</small></sup></p></li>
            </ul>
        </div>

        <div class="dropdownButton">
            <p class="title" id="gastricButton">LAP-BAND<sup><small>&reg;</small></sup> AP System</p>
            <button class="downArrow"></button>
        </div>
        <div id="dropdown3" class="dropdown" data-hiding="true">
            <ul>
                <li class="outerList"><p>Performed either open or laparoscopically, the stomach is cut and the intestines rerouted to bypass significant digestion.<sup><small>3,4</small></sup></p></li>
                <li class="outerList"><p>Extremely difficult to reverse.<sup><small>4</small></sup></p></li>
                <li class="outerList"><p>Cannot be adjusted.<sup><small>4</small></sup></p></li>

            </ul>
        </div>

        

        <h3>Safety Overview</h3>
        <p>There are a number of databases that healthcare organizations and professionals use to monitor bariatric procedures after surgery. A recent review of data from the American Society for Metabolic and Bariatric Surgery and the American College of Surgeons databases showed the following:</p>
</div>

        <table id="safetyChart">
            <tr>
                <th valign="middle" class="lap">LAPAROSCOPIC ADJUSTABLE GASTRIC BANDING (INCLUDING THE LAP-BAND<sup><small>&reg;</small></sup> SYSTEM)</th>
                <th valign="middle" class="sleeve">SLEEVE GASTRECTOMY</th>
                <th valign="middle" class="bypass">GASTRIC BYPASS</th>
            </tr>
            <tr>
                <td colspan="3" class="factors">Total Complications<sup><small>5</small></sup></td>
            </tr>
            <tr>
                <td>6%</td>
                <td class="middle">18%</td>
                <td>24%</td>
            </tr>
            <tr>
                <td colspan="3" class="factors">Reoperation Rate<sup><small>1</small></sup></td>
            </tr>
            <tr>
                <td>1%</td>
                <td class="middle">3%</td>
                <td>5%</td>
            </tr>
            <tr>
                <td colspan="3" class="factors">Hospital Stay After Procedure<sup><small>1</small></sup></td>
            </tr>
            <tr>
                <td>Less than 1 day</td>
                <td class="middle">Approximately <span class="nobreak">3 days</span></td>
                <td>Approximately <span class="nobreak">3 days</span></td>
            </tr>
            <tr>
                <td colspan="3" class="factors">Illness After 30 Days<sup><small>1</small></sup></td>
            </tr>
            <tr>
                <td>1%</td>
                <td class="middle">6%</td>
                <td>6%</td>
            </tr>
        </table>



    <div id="lapbandCompare" class="mainContent lower">
        <p class="smaller">* The Bariatric Outcomes Longitudinal Database<sup><small>SM</small></sup> (BOLD<sup><small>SM</small></sup>) was developed to help ensure ongoing compliance with the American Society for Metabolic and Bariatric Surgery (ASMBS) Bariatric Surgery Center of Excellence (BSCOE) program and develop general knowledge about optimal bariatric surgery practices. This analysis of the BOLD database includes data from 57,918 patients.</p>
        <p id="refs"><strong>References:</strong></p>
        <p class="smaller">1. Hutter Matthew, et al. First Report from the American College of Surgeons Bariatric Surgery Center Network: <em>Laparoscopic Sleeve Gastrectomy has Morbidity and Effectiveness Positioned Between the Band and the Bypass</em>. September 2001.</p>
        <p class="smaller">2. The LAP-BAND<sup><small>&reg;</small></sup> System Surgical Aid in the Treatment of Obesity (PSI). Apollo Endosurgery, Inc. Irvine, CA. 02/11.</p>
        <p class="smaller">3. Weiner Rudolf, et al. Laparoscopic Sleeve Gastrectomy- Influence of Sleeve Size and Resected Gastric Volume. <em>Obesity Surgery</em>. 2007.</p>
        <p class="smaller">4. Needleman Bradley, Happel Lynn. Bariatric Surgery: Choosing the Optimal Procedure. <em>Surgical Clinics of North America</em>. 2008.</p>
        <p class="smaller">5. DeMaria Eric, Pate Virginia, Warthen Michael, Winegar Deborah. Baseline data from American Society for Metabolic and Bariatric Surgery-designated Bariatric Surgery Centers of Excellence using the Bariatric Outcomes Longitudinal Database. <em>Surgery for Obesity and Related Diseases 6</em>. 2010.</p>

    </div>
    <c:LWMSpecialistSearch />

   <apex:outputLink value="{!URLFOR($Page.LWMConsideringSleeve,null,null)}" html-class="nextMod">
        <h3>Next:</h3>
        <p>Considering Sleeve Gastrectomy?</p>
        <button class="rightArrow"></button>
    </apex:outputLink>

    <div class="backToTopButton">
        <p>Back To Top</p>
        <button class="upArrow"></button>
    </div>
   
    
    <c:LWMobileFooter />
 </div>
 <c:LWMobileFooter2 />
 </body>

</apex:page>
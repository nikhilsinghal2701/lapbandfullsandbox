trigger ForecastTrigger on Forecast__c (before insert, before update) {
	if (Label.ForecastTrigger == 'Active') {
		ForecastTriggerMethods.setCalculatedFields(trigger.new, trigger.old, trigger.isInsert, trigger.isUpdate);
	}
}
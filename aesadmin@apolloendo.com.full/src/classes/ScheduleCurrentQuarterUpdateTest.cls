/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ScheduleCurrentQuarterUpdateTest {

	public static String CRON_EXP = '0 0 0 15 3 ? 2050';

    static testMethod void testSchedule() {
        Test.startTest();
        
        //Schedule the test job
        String jobId = System.schedule ('ScheduleCurrentQuarterUpdateTest', CRON_EXP, new ScheduleCurrentQuarterUpdate());
        
        Crontrigger ct = [select Id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where Id = :jobId];
        
        //Verify expressions are same
        System.assertEquals (CRON_EXP, ct.CronExpression);
        
        //Verify job has not run
        System.assertEquals (0, ct.TimesTriggered);
        
        //Verify the next time the job will run
        System.assertEquals ('2050-03-15 00:00:00', String.valueOf(ct.NextFireTime));
        
        Test.stopTest();
    }
    
    static testMethod void testBatch() {
    	//get record type ID for Account Ship To
    	Schema.DescribeSObjectResult sObjDescrAcc = Schema.SObjectType.Account;
    	Map<String, Schema.RecordTypeInfo> accRecTypeInfo = sObjDescrAcc.getRecordTypeInfosByName();

    	//create account
    	Account a1 = TestUtil.createAccounts(1, false, null)[0];
    	a1.RecordTypeId = accRecTypeInfo.get('Ship To').getRecordTypeId();
    	a1.Targeted_Account__c = true;
    	a1.Health_Region_ID__c = '11111';
    	a1.Health_Territory_ID__c = '22222';
    	a1.RP_Region__c = '33333';
    	a1.RP_Territory__c = '44444';
   	
    	insert a1;

		List<Forecast__c> fcList = new List<Forecast__c>();
		fcList.add(new Forecast__c(
			Account__c = a1.Id,
			Start_Date__c = Date.newInstance(2014, 01, 01),
			End_Date__c = Date.newInstance(2014, 03, 31),
			Health_Forecast__c = 500.00,
			RP_Forecast__c = 750.00));

		fcList.add(new Forecast__c(
			Account__c = a1.Id,
			Start_Date__c = Date.newInstance(2014, 04, 01),
			End_Date__c = Date.newInstance(2014, 06, 30),
			Health_Forecast__c = 1500.00,
			RP_Forecast__c = 1750.00));

		fcList.add(new Forecast__c(
			Account__c = a1.Id,
			Start_Date__c = Date.newInstance(2014, 07, 01),
			End_Date__c = Date.newInstance(2014, 09, 30),
			Health_Forecast__c = 2500.00,
			RP_Forecast__c = 2750.00));

		fcList.add(new Forecast__c(
			Account__c = a1.Id,
			Start_Date__c = Date.newInstance(2014, 10, 01),
			End_Date__c = Date.newInstance(2014, 12, 31),
			Health_Forecast__c = 3500.00,
			RP_Forecast__c = 3750.00));

		insert fcList;
		
		/*
		Account a2 = (Account)[select Id, CQ_Attainment__c, CQ_Health_Forecast__c, CQ_RP_Forecast__c from Account where Id = :a1.Id];
		System.assertEquals (0.0, a2.CQ_Attainment__c);
		System.assertEquals (500.0, a2.CQ_Health_Forecast__c);
		System.assertEquals (750.0, a2.CQ_RP_Forecast__c);
		*/

		Test.startTest();

		BatchUpdateCurrentQuarter fcUpdate = new BatchUpdateCurrentQuarter();
		Database.executeBatch(fcUpdate);

		Test.stopTest();

		//ASSERTS
		/* The batch update is uses Date.today(). So depending on when the test class is run, the results will vary and so we cannot do the asserts.
		 * This needs to be modified to pass a mock current date to the batchable class if Test.isRunningTest
		 */
    }
 
     static testMethod void testBatchFCTerritory() {
    	//get record type ID for Account Ship To
    	Schema.DescribeSObjectResult sObjDescrAcc = Schema.SObjectType.Account;
    	Map<String, Schema.RecordTypeInfo> accRecTypeInfo = sObjDescrAcc.getRecordTypeInfosByName();

    	//create region
    	Sales_Region__c reg1 = TestUtil.createSalesRegions(1,false,null)[0];
    	reg1.Region_ID__c = 'REGIDUNITTEST1';
    	reg1.Role__c = 'Regional Director';
    	insert reg1;
    	
    	//create territory
    	Sales_territory__c terr1 = TestUtil.createSalesTerritories(1,false,null)[0];
    	terr1.Sales_Region__c = reg1.Id;
    	terr1.Territory_ID__c = 'TERIDUNITTEST1';
    	terr1.Role__c = 'Account Manager';
    	insert terr1;
    	 
    	//create account
    	Account a1 = TestUtil.createAccounts(1, false, null)[0];
    	a1.RecordTypeId = accRecTypeInfo.get('Ship To').getRecordTypeId();
    	a1.Sales_Territory__c = terr1.Id;
   	
    	insert a1;

		List<Forecast_Territory__c> fcList = new List<Forecast_Territory__c>();
		fcList.add(new Forecast_Territory__c(
			Sales_Territory__c = terr1.Id,
			Start_Date__c = Date.newInstance(2014, 01, 01),
			End_Date__c = Date.newInstance(2014, 03, 31),
			Forecast__c = 500.00,
			Forecast_Local_Currency__c = 750.00));

		fcList.add(new Forecast_Territory__c(
			Sales_Territory__c = terr1.Id,
			Start_Date__c = Date.newInstance(2014, 04, 01),
			End_Date__c = Date.newInstance(2014, 06, 30),
			Forecast__c = 1500.00,
			Forecast_Local_Currency__c = 1750.00));

		fcList.add(new Forecast_Territory__c(
			Sales_Territory__c = terr1.Id,
			Start_Date__c = Date.newInstance(2014, 07, 01),
			End_Date__c = Date.newInstance(2014, 09, 30),
			Forecast__c = 2500.00,
			Forecast_Local_Currency__c = 2750.00));

		fcList.add(new Forecast_Territory__c(
			Sales_Territory__c = terr1.Id,
			Start_Date__c = Date.newInstance(2014, 10, 01),
			End_Date__c = Date.newInstance(2014, 12, 31),
			Forecast__c = 3500.00,
			Forecast_Local_Currency__c = 3750.00));

		insert fcList;
		
		Test.startTest();

		BatchUpdateFCTerritoryCurrentQuarter fcUpdate = new BatchUpdateFCTerritoryCurrentQuarter();
		Database.executeBatch(fcUpdate);

		Test.stopTest();

		//ASSERTS
		/* The batch update is uses Date.today(). So depending on when the test class is run, the results will vary and so we cannot do the asserts.
		 * This needs to be modified to pass a mock current date to the batchable class if Test.isRunningTest
		 */
    }
}
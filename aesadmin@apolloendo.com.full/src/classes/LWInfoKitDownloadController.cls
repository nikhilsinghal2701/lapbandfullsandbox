/**
Class: LWInfoKitDownloadController
Purpose: Controller for LWInfoKitDownload page
Author – Wade Liu
Date – 12/10/2014
*/
global without sharing class LWInfoKitDownloadController {
    
    private final string SEARCH_RANGE = '150';
    
    public List<Office__c> offices {get;set;}
    public String zipCode {get;set;}
    
    public LWInfoKitDownloadController(){
        String latlng = apexpages.currentpage().getparameters().get('latlng');
        zipCode = apexpages.currentpage().getparameters().get('zipCode');
        offices = new List<Office__c>();
        
        if(latlng!=null&&latlng!=''){
            List<String> arguments = latlng.split(':');
            Integer argumentSize = arguments.size();
            if(argumentSize==2){
                String selectStr = 'SELECT Address_1__c,City__c,Specialist__r.Name,State__c,Zip_Code__c,Phone__c FROM Office__c';
                String whereStr = ' where ';
                String orderbyStr = ' ORDER BY DISTANCE(location__c, GEOLOCATION(' +arguments[0] + ',' +arguments[1]+'), \'mi\' )';
                whereStr += ('DISTANCE(location__c, GEOLOCATION('+arguments[0]+','+arguments[1]+'), \'mi\') < ' + SEARCH_RANGE);
                String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
                ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
                whereStr +=' AND Status__c NOT IN (\'Rejected\',\'Deleted\') AND RecordTypeId = \''+aId+'\' AND Approved_Office__c = NULL';
                String pageStr = ' LIMIT 3';
                String sql = selectStr + whereStr + orderbyStr + pageStr;  
                offices = database.query(sql);
            }
        }
    }
}
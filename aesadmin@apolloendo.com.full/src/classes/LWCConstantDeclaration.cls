/*
    Class  : LWCConstantDeclaration
    Usage  : This calss is used to declare constants to be used in any LWC classes or trggers
    Author : Vinay Lakshmeesh - Perficient Inc.
             Date : 02/03/2014 
*/
public with sharing class LWCConstantDeclaration {
     public static final String PRIVATEACCESS = 'private';
     public static final String LWCDASHBOARD = 'LWCDashboard';
     public static final String LWCDOCUMENTLIBRARY = 'LWCdocumentlibrary';
     public static final String SUBMITTED = 'Submitted';
     public static final String REJECTED = 'Rejected';
     public static final String PENDINGOFFICE= 'Pending_Office';
     public static final String OFFICE= 'Office__c';
     public static final String EDIT= 'EDIT';
     public static final String NEWO= 'NEWO';
     public static final String REGISTEREDPROFILE = 'LAP-BAND® Central Registered User';
     public static final String CERTIFIEDPROFILE ='LAP-BAND® Central Certified User';
     public static final String OFFICESTAFFPERMISSIONSET = 'Office_Staff_User_Permission_Set';
     public static final String OFFICEUSERPERMISSIONSET = 'Office_User_Permission_Set';
     public static final String SPECIALISTUSERPERMISSIONSET = 'Specialist_User_Permission_Set';
     public static final String APPROVEDOFFICERECORD = 'Approved Office';
     public static final String PENDINGOFFICERECORD = 'Pending Office';
     public static final String SURGEONACCOUNTRECORD = 'Surgeon';
}
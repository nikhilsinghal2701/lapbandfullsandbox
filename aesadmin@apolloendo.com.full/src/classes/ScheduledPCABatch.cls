global class ScheduledPCABatch implements Schedulable {
	global void execute(SchedulableContext sc) {
      PricingContractAccountBatch b = new PricingContractAccountBatch(); 
      database.executebatch(b);
   }
}
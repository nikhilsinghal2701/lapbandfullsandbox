public with sharing class SalesAlignmentUtility {

	public static final String CONST_ACCT_EXT_ID = 'AccountExternalID';
	public static final String CONST_ZIPCODE = 'Zipcode';
	public static final String CONST_CITY = 'City';
	public static final String CONST_COUNTRY = 'Country';
	public static final String CONST_STATE = 'State';
	public static final String CONST_DEFAULT = 'Default';

	//Map<ISO Code, Map<ExternalId, Sales Alignment Record>>
	public static Map<String, Map<String, Sales_Alignment__c>> alignExtIDMap = new Map<String, Map<String, Sales_Alignment__c>>();
		 
	//Map<ISO Code, Map<Zipcode, Sales Alignment Record>>
	public static Map<String, Map<String, Sales_Alignment__c>> alignZipMap = new Map<String, Map<String, Sales_Alignment__c>>();
		 
	//Map<ISO Code, Map<City, Sales Alignment Record>>
	public static Map<String, Map<String, Sales_Alignment__c>> alignCityMap = new Map<String, Map<String, Sales_Alignment__c>>();

	//Map<ISO Code, Map<State, Sales Alignment Record>>
	public static Map<String, Map<String, Sales_Alignment__c>> alignStateMap = new Map<String, Map<String, Sales_Alignment__c>>();
		 
	//Map<ISO Code, Sales Alignment Record>
	public static Map<String, Sales_Alignment__c> alignCountryMap = new Map<String, Sales_Alignment__c>();

	//Map<ISO Code, Sales Alignment Record>
	public static Map<String, Sales_Alignment__c> alignDefaultMap = new Map<String, Sales_Alignment__c>();

	public static void alignAccounts (List<Account> newAccounts) {
		Set<String> shipCountrySet = new Set<String>();
		Set<String> citySet = new Set<String>();
		Set<String> stateSet = new Set<String>();
		//Set<String> zipSet = new Set<String>();
		Set<String> extIDSet = new Set<String>();
		for (Account newAcc : newAccounts) {
			if (newAcc.Trigger_Alignment__c) {
				if (newAcc.ShippingCountry != null) shipCountrySet.add (newAcc.ShippingCountry);
				if (newAcc.ShippingCity != null) citySet.add(newAcc.ShippingCity);
				if (newAcc.ShippingState != null) stateSet.add(newAcc.ShippingState);
				//if (newAcc.ShippingPostalCode != null) zipSet.add(newAcc.ShippingPostalCode);
				if (newAcc.external_id__c != null) extIDSet.add(newAcc.External_ID__c); 
			}
		}
		
		System.debug (shipCountrySet);
		
		if (shipCountrySet.size() > 0) {
			//buildAlignmentMaps(shipCountrySet, citySet, zipSet, extIDSet);
			buildAlignmentMaps(shipCountrySet, citySet, stateSet, extIDSet);
			setAlignments(newAccounts);
			System.debug ('EXT ID Map: ' + alignExtIDMap);
			System.debug ('ZIP Map: ' + alignZipMap);
			System.debug ('CITY Map: ' + alignCityMap);
			System.debug ('STATE Map: ' + alignStateMap);
			System.debug ('COUNTRY Map: ' + alignCountryMap);
		}
	}

	//private static void buildAlignmentMaps (Set<String> shipCountrySet, Set<String> citySet, Set<String> zipSet, Set<String> extIDSet) {
	private static void buildAlignmentMaps (Set<String> shipCountrySet, Set<String> citySet, Set<String> stateSet, Set<String> extIDSet) {
		
		List<Sales_Alignment__c> alignList = new List<Sales_Alignment__c>();
		alignList = [select Id, ISO_Country_Code__c, Product__c, Sales_Territory__c, Sales_Territory__r.Assigned_To__c, Territory_ID__c, Type__c, Value__c 
		             from Sales_Alignment__c 
		             where ISO_Country_Code__c in :shipCountrySet and
		                   //((Type__c = :CONST_ZIPCODE and Value__c in :zipSet) or
		                   ((Type__c = :CONST_ZIPCODE) or
		                    (Type__c = :CONST_CITY and Value__c in :citySet) or
		                    (Type__c = :CONST_STATE and Value__c in :stateSet) or
		                    (Type__c = :CONST_DEFAULT) or
		                    (Type__c = :CONST_ACCT_EXT_ID and Value__c in :extIDSet) or
		                    (Type__c = :CONST_COUNTRY)) and
		                   Active__c = true and
		                   //Sales_Territory__r.Assigned_To__c != null and
		                   Sales_Territory__c != null];
		
		System.debug (alignList.size());
		/* Alignment Check Order
		 1. AccountExternalID
		 2. Zipcode
		 3. ShippingCity
		 4. State
		 5. Country
		 */
		
		for (Sales_Alignment__c alignRec : alignList) {
			if (alignRec.Type__c == CONST_ACCT_EXT_ID) {
				if (alignExtIdMap.containsKey(alignRec.ISO_Country_Code__c)) {
					if (!alignExtIDMap.get(alignRec.ISO_Country_Code__c).containsKey(alignRec.Value__c)) {
						alignExtIDMap.get(alignRec.ISO_Country_Code__c).put(alignRec.Value__c.toUpperCase(), alignRec);
					}
				} else {
					Map<String, Sales_Alignment__c> tempMap = new Map<String, Sales_Alignment__c>();
					tempMap.put(alignRec.Value__c.toUpperCase(), alignRec);
					alignExtIdMap.put(alignRec.ISO_Country_Code__c, tempMap);
				}
			} else if (alignRec.Type__c == CONST_ZIPCODE) {
				if (alignZipMap.containsKey(alignRec.ISO_Country_Code__c)) {
					if (!alignZipMap.get(alignRec.ISO_Country_Code__c).containsKey(alignRec.Value__c)) {
						alignZipMap.get(alignRec.ISO_Country_Code__c).put(alignRec.Value__c.toUpperCase(), alignRec);
					}
				} else {
					Map<String, Sales_Alignment__c> tempMap = new Map<String, Sales_Alignment__c>();
					tempMap.put(alignRec.Value__c.toUpperCase(), alignRec);
					alignZipMap.put(alignRec.ISO_Country_Code__c, tempMap);
				}
			} else if (alignRec.Type__c == CONST_CITY) {
				if (alignCityMap.containsKey(alignRec.ISO_Country_Code__c)) {
					if (!alignCityMap.get(alignRec.ISO_Country_Code__c).containsKey(alignRec.Value__c)) {
						alignCityMap.get(alignRec.ISO_Country_Code__c).put(alignRec.Value__c.toUpperCase(), alignRec);
					}
				} else {
					Map<String, Sales_Alignment__c> tempMap = new Map<String, Sales_Alignment__c>();
					tempMap.put(alignRec.Value__c.toUpperCase(), alignRec);
					alignCityMap.put(alignRec.ISO_Country_Code__c, tempMap);
				}
			} else if (alignRec.Type__c == CONST_STATE) {
				if (alignStateMap.containsKey(alignRec.ISO_Country_Code__c)) {
					if (!alignStateMap.get(alignRec.ISO_Country_Code__c).containsKey(alignRec.Value__c)) {
						alignStateMap.get(alignRec.ISO_Country_Code__c).put(alignRec.Value__c.toUpperCase(), alignRec);
					}
				} else {
					Map<String, Sales_Alignment__c> tempMap = new Map<String, Sales_Alignment__c>();
					tempMap.put(alignRec.Value__c.toUpperCase(), alignRec);
					alignStateMap.put(alignRec.ISO_Country_Code__c, tempMap);
				}
			} else if (alignRec.Type__c == CONST_COUNTRY) {
				if (!alignCountryMap.containsKey(alignRec.ISO_Country_Code__c)) {
					alignCountryMap.put(alignRec.ISO_Country_Code__c, alignRec);
				}
			} else if (alignRec.Type__c == CONST_DEFAULT) {
				if (!alignDefaultMap.containsKey(alignRec.ISO_Country_Code__c)) {
					alignDefaultMap.put(alignRec.ISO_Country_Code__c, alignRec);
				}
			}
		} 
	}
	
	private static void setAlignments(List<Account> newAccounts) {
		List<Account> retAcctList = new List<Account>(); 
		String ownerId;

		for (Account acct : newAccounts) {
			if (acct.Trigger_Alignment__c) {
				// reset Trigger_Alignment boolean regardless of if there is a successful alignment or not
				acct.Trigger_Alignment__c = false;
				// all alignments based on Country. If there is no Country, skip it
				if (acct.ShippingCountry != null) {
					ownerId = null;
					
					// Align based on EXTERNAL ID
					if (acct.External_ID__c != null && alignExtIDMap.containsKey(acct.ShippingCountry) && alignExtIDMap.get(acct.ShippingCountry).containsKey(acct.External_ID__c)) {
						acct.Sales_Territory__c = alignExtIDMap.get(acct.ShippingCountry).get(acct.External_ID__c).Sales_Territory__c;
						ownerId = alignExtIDMap.get(acct.ShippingCountry).get(acct.External_ID__c).Sales_Territory__r.Assigned_To__c;
						if (ownerId != null) acct.OwnerId = ownerId;
						continue;
					} 
					
					// Align based on ZIPCODE
					if (acct.ShippingPostalCode != null && alignZipMap.containsKey(acct.ShippingCountry)) {
						for (String s : alignZipMap.get(acct.ShippingCountry).keySet()) {
							if (acct.ShippingPostalCode.startsWith(s)) {
								acct.Sales_Territory__c = alignZipMap.get(acct.ShippingCountry).get(s).Sales_Territory__c;
								ownerId = alignZipMap.get(acct.ShippingCountry).get(s).Sales_Territory__r.Assigned_To__c;
								break;
							}
						}
						if (ownerId != null) acct.OwnerId = ownerId;
						continue;
					} 
					
					// Align based on CITY
					if (acct.ShippingCity != null && alignCityMap.containsKey(acct.ShippingCountry) && alignCityMap.get(acct.ShippingCountry).containsKey(acct.ShippingCity.trim().toUpperCase())) {
						acct.Sales_Territory__c = alignCityMap.get(acct.ShippingCountry).get(acct.ShippingCity.trim().toUpperCase()).Sales_Territory__c;
						ownerId = alignCityMap.get(acct.ShippingCountry).get(acct.ShippingCity.trim().toUpperCase()).Sales_Territory__r.Assigned_To__c;
						if (ownerId != null) acct.OwnerId = ownerId;
						continue;
					} 

					// Align based on STATE
					if (acct.ShippingState != null && alignStateMap.containsKey(acct.ShippingCountry) && alignStateMap.get(acct.ShippingCountry).containsKey(acct.ShippingState.trim().toUpperCase())) {
						acct.Sales_Territory__c = alignStateMap.get(acct.ShippingCountry).get(acct.ShippingState.trim().toUpperCase()).Sales_Territory__c;
						ownerId = alignStateMap.get(acct.ShippingCountry).get(acct.ShippingState.trim().toUpperCase()).Sales_Territory__r.Assigned_To__c;
						if (ownerId != null) acct.OwnerId = ownerId;
						continue;
					} 
					
					// Align based on COUNTRY
					if (alignCountryMap.get(acct.ShippingCountry) != null) {
						acct.Sales_Territory__c = alignCountryMap.get(acct.ShippingCountry).Sales_Territory__c;
						ownerId = alignCountryMap.get(acct.ShippingCountry).Sales_Territory__r.Assigned_To__c;
						if (ownerId != null) acct.OwnerId = ownerId;
					}

					// Align to DEFAULT terr/owner if available for the country
					if (alignDefaultMap.get(acct.ShippingCountry) != null) {
						acct.Sales_Territory__c = alignDefaultMap.get(acct.ShippingCountry).Sales_Territory__c;
						ownerId = alignDefaultMap.get(acct.ShippingCountry).Sales_Territory__r.Assigned_To__c;
						if (ownerId != null) acct.OwnerId = ownerId;
					}
					
				} //END if (acct.ShippingCountry != null)
			} //END if (acct.Trigger_Alignment__c)
		} //END for loop
	}
}
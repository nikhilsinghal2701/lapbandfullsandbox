/**
Class: LWCWithoutSharingTest
Purpose: Test Class for LWCWithoutSharing
Author – Wade Liu
Date – 03/10/2014
*/
@isTest
private class LWCWithoutSharingTest {

    static testMethod void testGtRecordTypeId() {
    	//test get record type id
        Id idRecordTypeApprovalOffice = LWCWithoutSharing.gtRecordTypeId('Approved_Office');
        system.assert(idRecordTypeApprovalOffice != null) ;
    }
    
    static testMethod void testInsertOffice() {
    	//test insert office
        Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
        List<Account> lstAccounts = TestUtil.createAccounts(1, true, new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon});
        if(lstAccounts != null && lstAccounts.size() > 0){
        	Office__c oOffice = new Office__c (Name='test',Specialist__c=lstAccounts[0].Id);
        	
        	test.startTest();        
	        LWCWithoutSharing.insertOffice(oOffice);
	        List<Office__c> lstRetOffice = [SELECT Name FROM Office__c where NAME='test'];       
	        system.assert(lstRetOffice.size() == 1) ;	        
	        test.stopTest();
        }   
    }
    
    static testMethod void testUpdateOffice(){
    	//test update office
    	List<Office__c> lstOffices = createOffice( 1 , 'Approved Office' , true );
    	if(lstOffices != null){    		
    		test.startTest();
    		lstOffices[0].Website__c = 'www.test.com';
    		LWCWithoutSharing.updateOffice(lstOffices[0]);
    		Office__c testOffice = [SELECT Website__c FROM Office__c WHERE Id = : lstOffices[0].Id LIMIT 1];
    		system.assert(testOffice.Website__c == 'www.test.com');
    		test.stopTest();
    	}
   	}
    
    static testMethod void testDeleteOffice() {
        //test delete office
       List<Office__c> lstOffices = createOffice( 2 , 'Approved Office' , true );
       if(lstOffices != null) {      		
       		lstOffices[0].Approved_Office__c = lstOffices[1].Id;
       		update lstOffices[0];
        	test.startTest();  
        	LWCWithoutSharing.deleteOffice(lstOffices[0].Id);
        	Office__c office = [SELECT Status__c FROM Office__c WHERE Id = :lstOffices[0].Id][0];  
        	system.assert(office.Status__c == 'Deleted');           
	        test.stopTest();
	        return;
        }   
        system.assert(false);
    }
    
    static testMethod void testSubmitOfcApproval(){
    	//test submit office list to approval
    	List<Office__c> lstOffices = createOffice( 1 , 'Pending Office' , true);
    	if ( lstOffices != null){
    		test.startTest();
    		LWCWithoutSharing.submitOfcApproval(lstOffices[0].Id);
    		List<ProcessInstanceWorkitem> piwi = [SELECT Id, ProcessInstanceId, ProcessInstance.TargetObjectId 
                                                FROM ProcessInstanceWorkitem 
                                               WHERE ProcessInstance.TargetObjectId =: lstOffices[0].Id];
            //system.assert(piwi.size() == 1);
    		test.stopTest();
    		return;
    	}
    	system.assert(false);
    	
    }
    
    static testMethod void testRejectOfcApproval() {
    	//test reject submitted office list
    	List<Office__c> lstOffices = createOffice( 1 , 'Pending Office' , true );
    	if(lstOffices != null){
    		test.startTest();
    		Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
    		req1.setComments('Submitting request for approval.');
    		req1.setObjectId(lstOffices[0].Id);
    		Approval.ProcessResult result = Approval.process(req1);
    		System.assert(result.isSuccess());
    		LWCWithoutSharing.rejectOfcApproval(lstOffices[0].Id);
			Office__c testOffice = [SELECT Status__c FROM Office__c WHERE Id = : lstOffices[0].Id LIMIT 1];    		system.assert(testOffice.Status__c == 'Rejected');
    		test.stopTest();
    		return;
    	}
    }
    
     static testMethod void testUpdateOfficeList() { 
     	//test update office list
     	 List<Office__c> lstOffice = createOffice( 2 , 'Pending Office' , true);
     	 if( lstOffice != null ){
     	 	test.startTest();
     	 	lstOffice[0].Website__c = 'www.test.com';
     	 	lstOffice[1].Website__c = 'www.test.com';
     	 	LWCWithoutSharing.updateOfficeList(lstOffice);
     	 	List<Office__c> lstRetOffice = [SELECT Website__c FROM Office__c WHERE Id in (:lstOffice[0].Id , :lstOffice[1].Id)];
     	 	system.assert(lstRetOffice[0].Website__c == 'www.test.com');
     	 	system.assert(lstRetOffice[1].Website__c == 'www.test.com');
     	 	test.stopTest();
     	 	return;
     	 }
     	 system.assert(false);
     }
     
     static testMethod void testInsertOfficeStaff(){
     	//test insert office staff
     	List<Office__c> lstOffice = createOffice( 1 , 'Approved Office' , true);
     	if( lstOffice != null ){
     		test.startTest();
     		Office_Staff__c oOfficeStaff = new Office_Staff__c(E_mail__c='test@email.com',First_Name__c='firstname',Last_Name__c='lastname',Specialist__c=lstOffice[0].Specialist__c);
     		LWCWithoutSharing.insertOfficeStaff(oOfficeStaff);
     		test.stopTest();
     		return;
     	}
     	system.assert(false);
     }
     
     static testMethod void testInsertHospital(){
     	//test insert hospital
     	Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
     	Id idAccountRecordTypeSoldTo = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Sold To').getRecordTypeId();
     	List<Account> lstAccounts = TestUtil.createAccounts(1, true, new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon});
     	List<Account> lstSoldToAccounts = TestUtil.createAccounts(1, true, new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSoldTo});
     	List<Surgeon__c> lstSurgeons = TestUtil.createSurgeons(1, true, new Map<String,Object>{'Surgeon_Account__c'=>lstAccounts[0].Id});
     	test.startTest();
     	List<Surgeon_Affiliation__c> lstSurgeonAffiliations = TestUtil.createSurgeonAffiliations(1, false, new Map<String,Object>{'Account__c'=>lstSoldToAccounts[0].Id,'Surgeon_Account__c'=>lstAccounts[0].Id,'Surgeon_Name__c'=>lstSurgeons[0].Id});
     	LWCWithoutSharing.insertHospital(lstSurgeonAffiliations);
     	test.stopTest();
     }
     
     
     static List<Office__c> createOffice ( Integer num , String recordType , Boolean insertFlag ){
     	//test insert office list
    	Id idAccountRecordTypeSurgeon = Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('Surgeon').getRecordTypeId();
    	Id idOfficeRecordTypeApproved = Office__c.SObjectType.getDescribe().getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        List<Account> lstAccounts = TestUtil.createAccounts(1, true, new Map<String,Object>{'RecordTypeId'=>idAccountRecordTypeSurgeon} );
        if(lstAccounts != null && lstAccounts.size() > 0){    
        	List<Office__c> lstOffices = TestUtil.createOffices(num, insertFlag, new Map<String,Object>{'Specialist__c'=>lstAccounts[0].Id,'RecordTypeId'=>idOfficeRecordTypeApproved});
        	if(lstOffices != null && lstOffices.size() == num)
        		return lstOffices;
        }
        return null;
    }
    
    static testMethod void testGetRecordType(){
    	String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	String pOffice = LWCConstantDeclaration.PENDINGOFFICERECORD;
    	LIST<String> strs = new LIST<String>();
    	strs.add(aOffice);
    	strs.add(pOffice);
    	LIST<RecordType> recordTypeIds = LWCWithoutSharing.getRecordTypeId(strs);
    	
    }
     
}
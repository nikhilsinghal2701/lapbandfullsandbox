/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LapBandDocControllerTest {

    static testMethod void lapBandDocPageTest() {
        PageReference pageRef = Page.LapBandDocVF;
        Test.setCurrentPage(pageRef);
        
        AWSKey__c awsKey = new AWSKey__c();
        awsKey.Name = 'AmazonKeys';
        awsKey.Key__c = 'TESTKEY';
        awsKey.Secret__c = 'TESTSECRET';
        
        insert awsKey;
        
        AWS_S3_Object__c awsObj = new AWS_S3_Object__c();
        awsObj.Access__c = 'private';
        awsObj.FileLink__c = 'https://s3.amazonaws.com/lapband-prod/test.jpg';
        awsObj.Bucket_Name__c = '';
        awsObj.Title__c = 'test file';
        awsObj.Library__c = 'Lapband Specialists';
        awsObj.File_Name__c = 'test.jpg';
        awsObj.Description__c = 'dummy file name used for test class. File does not exist';
        awsObj.Content_Type__c = 'jpg';
        awsObj.Bucket_Name__c = 'lapband-prod';
        
        insert awsObj;

        LapBandDocController ctrl = new LapBandDocController();
        
        ctrl.s3ObjId = awsObj.Id;
        PageReference downloadPage = ctrl.createsDownloadLink();
        
        AWS_S3_Object__c awsObj1 = [select Id, AmazonLink__c, FileLink__c from AWS_S3_Object__c][0];
        
        System.assertEquals('https://s3.amazonaws.com/lapband-prod/test.jpg', awsObj1.AmazonLink__c);
        
    }
}
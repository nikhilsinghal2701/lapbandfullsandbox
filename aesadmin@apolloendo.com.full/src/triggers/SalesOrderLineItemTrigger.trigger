trigger SalesOrderLineItemTrigger on Sales_Order_Line_Item__c (before insert, after insert, after update) {
	
	if (Label.SalesOrderLineItemTrigger == 'Active') {
		if (trigger.isAfter) {
			SalesOrderLineItemTriggerMethods.upsertForecast(trigger.new, trigger.old);
			//SalesOrderLineItemTriggerMethods.upsertForecastTerritory(trigger.new, trigger.old);
		}
		
		if (trigger.isBefore) {
			SalesOrderLineItemTriggerMethods.setLineValues(trigger.new, trigger.isInsert);
		}
	}		
}
<apex:page applyBodyTag="false" showheader="false" standardStylesheets="false" sidebar="false" >
    <c:LWHCPTemplate />
    <body>
        <div id="container">
        <c:LWHCPHeader />
        
        <!-- Content -->
        
        <div id="content" class="inner-page">
            <div id="inner-page-content">
                <h1 class="safety-information full-width">Important LAP-BAND&#174; System Safety Information</h1>
                <div id="content-full">
                    <p><strong>Indications:</strong> The LAP-BAND<sup>&#174;</sup> System is indicated for weight reduction for patients with obesity, with a Body Mass Index (BMI) of at least 40 kg/m2 or a BMI of at least 30 kg/m2 with one or more obesity related comorbid conditions.</p> 
                    <p>It is indicated for use only in adult patients who have failed more conservative weight reduction alternatives, such as supervised diet, exercise and behavior modification programs.  Patients who elect to have this surgery must make the commitment to accept significant changes in their eating habits for the rest of their lives.</p>
                    <p><strong>Contraindications:</strong> The LAP-BAND<sup>&#174;</sup> System is not recommended for non-adult patients, patients with conditions that may make them poor surgical candidates or increase the risk of poor results (e.g., inflammatory or cardiopulmonary diseases, GI conditions, symptoms or family history of autoimmune disease, cirrhosis), who are unwilling or unable to comply with the required dietary restrictions, who have alcohol or drug addictions, or who currently are or may be pregnant.</p>
                    <p><strong>Warnings:</strong> The LAP-BAND<sup>&#174;</sup> System is a long-term implant. Explant and replacement surgery may be required. Patients who become pregnant or severely ill, or who require more extensive nutrition may require deflation of their bands.</p>
                    <p>Anti-inflammatory agents, such as aspirin, should be used with caution and may contribute to an increased risk of band erosion.</p>
                    <p><strong>Adverse Events:</strong>  Placement of the LAP-BAND<sup>&#174;</sup> System is major surgery and, as with any surgery, death can occur. Possible complications include the risks associated with the medications and methods used during surgery, the risks associated with any surgical procedure, and the patient's ability to tolerate a foreign object implanted in the body.</p>
                    <p>Band slippage, erosion and deflation, reflux, obstruction of the stomach, dilation of the esophagus, infection, or nausea and vomiting may occur. Reoperation may be required.</p>
                    <p>Rapid weight loss may result in complications that may require additional surgery. Deflation of the band may alleviate excessively rapid weight loss or esophageal dilation.</p>
                    <p><strong>Important:</strong> For full safety information please download the <a href="{!URLFOR($Resource.Lapband,'Lapband/pdf/lapband_AP_dfu.pdf')}">Lap-Band<sup>&#174;</sup> System Directions for Use</a>, talk with your doctor, or call Apollo Endosurgery Product Support at 1-800-527-2263.</p>
                    <p><strong>CAUTION: Rx only.</strong></p>
                </div>
            </div>
        </div>
        <!-- End of Content -->
         
        <c:LWHCPMapOnFoot />
        <c:LWHCPFooter />
            
        
        <c:LWHCPFooter2 />
        </div> <!-- container -->
    </body>

</apex:page>
/*****************************************************
Class: LWCOfficeStaffTriggerHandler
Trigger handler for Office_Staff__c
Author – Wade Liu
Date – 02/20/2014
******************************************************/
public without sharing class LWCOfficeStaffTriggerHandler {
     
     //handler insert operation, add contact, user and assign permission set at same time
     public static void handlerOnInsert( List<Office_Staff__c> newOSs ){
        List<Contact> contacts = new List<Contact>();
        List<User> users = new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='LAP-BAND® Central Registered User'];
        
        List<string> specialistEmails = new List<string>();
        List<string> officeEmails = new List<string>();
        List<string> staffEmails = new List<string>();
        
        String nameappend = Label.LWC_User_Name_Append;
        if(nameappend=='production')	nameappend='';
        
        for(Office_Staff__c os : newOSs){
            Contact c = new Contact(FirstName = os.First_Name__c, LastName = os.Last_Name__c, Email = os.E_mail__c, OwnerId = UserInfo.getUserId(), AccountId = os.Specialist__c, OfficeStaff__c = os.Id);
            contacts.Add(c);
            
            User user = new User();
            user.Email = os.E_mail__c;
            user.FirstName = os.First_Name__c;
            user.LastName = os.Last_Name__c;
            string aname = os.First_Name__c+'.'+string.valueOf(os.Last_Name__c).substring(0, 1);
            if(aname.length() > 8)  aname = aname.substring(0, 8);
            user.Alias = aname;
            //user.userName = os.E_mail__c+'.full';
            user.Username = os.E_mail__c + nameappend;
            user.ProfileId = p.Id;
            user.EmailEncodingKey = 'UTF-8';
            user.LanguageLocaleKey = 'en_US';
            user.LocaleSidKey = 'en_US';
            user.TimeZoneSidKey = 'America/Los_Angeles';
            user.IsActive = true;
            user.User_Email__c = os.E_mail__c;
            users.Add(user);
            
            if(os.Specialist_Profile__c == true)    specialistEmails.add(os.E_mail__c);
            if(os.Office_Profiles__c == true)   officeEmails.add(os.E_mail__c);
            if(os.Office_Staff_Profiles__c == true) staffEmails.add(os.E_mail__c);
        }
        
            insert contacts;
            
            Map<String, ID> contactMap = new Map<String, ID>();
            for(Contact c : contacts){
                contactMap.put(c.Email, c.Id);
            }
            for(User u : users){
                u.ContactId = contactMap.get(u.Email);
            }
            insert users;
            
            //future call assign permission
            assignPermissions(specialistEmails, officeEmails, staffEmails);
     }
     
     @future
     //future permission assign, used for office staff insert
     public static void assignPermissions(List<string> specialistEmails, List<string> officeEmails, List<string> staffEmails){
        PermissionSet specialistUserSet = [select Id from PermissionSet where name = 'Specialist_User_Permission_Set' limit 1];
        PermissionSet officeUserSet = [select Id from PermissionSet where name = 'Office_User_Permission_Set' limit 1];
        PermissionSet staffUserSet = [select Id from PermissionSet where name = 'Office_Staff_User_Permission_Set' limit 1];
        List<PermissionSetAssignment> specialistList = new List<PermissionSetAssignment>();
        List<PermissionSetAssignment> officeList = new List<PermissionSetAssignment>();
        List<PermissionSetAssignment> staffList = new List<PermissionSetAssignment>();
        
        Map<String, ID> userMap = new Map<String, ID>();
        List<string> emailList = new List<string>();
        if(specialistEmails!=null)  emailList.addAll(specialistEmails);
        if(officeEmails!=null)  emailList.addAll(officeEmails);
        if(staffEmails!=null)   emailList.addAll(staffEmails);
        List<User> users = new List<User>();
        users = [select User_ID_Formula__c,User_Email__c from User where User_Email__c IN :emailList];
        system.debug('user size:----'+users.size());
        if(users.size()>0)
        {
            for(User u : users){
            userMap.put(u.User_Email__c, u.User_ID_Formula__c);
            }
            
            //specialist permission
            for(string e : specialistEmails){
                PermissionSetAssignment psa = new PermissionSetAssignment();
                psa.AssigneeId = userMap.get(e);
                psa.PermissionSetId = specialistUserSet.Id;
                specialistList.add(psa);
            }
            
            //office permission
            for(string e : officeEmails){
                PermissionSetAssignment psa = new PermissionSetAssignment();
                psa.AssigneeId = userMap.get(e);
                psa.PermissionSetId = officeUserSet.Id;
                officeList.add(psa);
            }
            
            //office staff permission
            for(string e : staffEmails){
                PermissionSetAssignment psa = new PermissionSetAssignment();
                psa.AssigneeId = userMap.get(e);
                psa.PermissionSetId = staffUserSet.Id;
                staffList.add(psa);
            }
            
            insert specialistList;
            insert officeList;
            insert staffList;
        }
        
     }
     
     @future
     //remove permissions, based on Office Staff permission flags update
     public static void removePermissions(List<String>removeList, String permissionId){
        List<User> users = [select User_ID_Formula__c from User where User_Email__c IN :removeList];
        List<PermissionSetAssignment> psas = [select Id from PermissionSetAssignment where AssigneeId IN :users AND PermissionSetId = :permissionId];
        delete psas;
     }

     @future
     //assign new permissions, based on Office Staff permission flags update
     public static void assignPermissions(List<String>assignPermissionList){
        List<String> emailList = new List<String>();
        Map<String,String> assignMap = new Map<String,String>();
        for(String permissionStr : assignPermissionList){
            List<String> assignSet = permissionStr.split('::');
            String email = assignSet[0];
            String permissionId = assignSet[1];
            emailList.add(email);
            assignMap.put(email,permissionId);
        }
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        for(User u : [select User_ID_Formula__c, User_Email__c from User where User_Email__c IN :emailList]){
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.PermissionSetId = assignMap.get(u.User_Email__c);
            psa.AssigneeId = u.User_ID_Formula__c;
            psas.add(psa);
        }
        if(psas.size()>0)   insert psas;
     }
     
     @future
     //Inavtive user, when Office staff is deleted(Inactive)
     public static void deleteUser(List<string> emailList){
        List<User> users = new List<User>();
        users = [select User_ID_Formula__c, User_Email__c, IsActive from User where User_Email__c IN :emailList];
        if(users.size()>0){
            for(User u : users){
                u.IsActive = false;
            }
            update users;
        }
     }
     
     @future
     //avtivate user, when Office staff is activate
     public static void activateUser(List<string> emailList){
        List<User> users = new List<User>();
        users = [select User_ID_Formula__c, User_Email__c, IsActive from User where User_Email__c IN :emailList];
        if(users.size()>0){
            for(User u : users){
                u.IsActive = true;
            }
            update users;
        }
     }
     
     @future
     //Update user in future method, based on Office Staff permission flags update
     public static void updateUsers(List<String> userChanges){
        List<String> emailList = new List<String>();
        Map<String,String> emailMap = new Map<String,String>();
        Map<String,String> firstNameMap = new Map<String,String>();
        Map<String,String> lastNameMap = new Map<String,String>();
        for(String userChange : userChanges){
            system.debug(userChange);
            List<String> changeSet = userChange.split('::');
            String oldEmail = changeSet[0];
            String newEmail = changeSet[1];
            String newFirstName = changeSet[2];
            String newLastName = changeSet[3];
            emailList.add(oldEmail);
            emailMap.put(oldEmail,newEmail);
            firstNameMap.put(oldEmail,newFirstName);
            lastNameMap.put(oldEmail,newLastName);
        }
        List<User> users = [select User_ID_Formula__c, Email, FirstName, LastName, Alias, UserName, User_Email__c from User where User_Email__c IN :emailList];
        String nameappend = Label.LWC_User_Name_Append;
        if(nameappend=='production')	nameappend='';
        for(User u : users){
            u.Email = emailMap.get(u.User_Email__c);
            u.FirstName = firstNameMap.get(u.User_Email__c);
            u.LastName = lastNameMap.get(u.User_Email__c);
            string aname = firstNameMap.get(u.User_Email__c)+'.'+string.valueOf(lastNameMap.get(u.User_Email__c)).substring(0, 1);
            if(aname.length() > 8)  aname = aname.substring(0, 8);
            u.Alias = aname;
            u.userName = emailMap.get(u.User_Email__c) + nameappend;
            u.User_Email__c = emailMap.get(u.User_Email__c);
        }
        update users;
     }
     
     public static void handlerOnDelete( List<Office_Staff__c> deleteOSs ){
        List<string> emailList = new List<string>();
        for(Office_Staff__c os : deleteOSs){
            emailList.add(os.E_Mail__c);
        }
        deleteUser(emailList);
     }
     
     public static void handlerOnActivate( List<Office_Staff__c> activeOSs ){
        List<string> emailList = new List<string>();
        for(Office_Staff__c os : activeOSs){
            emailList.add(os.E_Mail__c);
        }
        activateUser(emailList);
     }
     
     //handler for office staff update, update contact, user and reassign permission sets
     public static void handlerOnUpdate(List<Office_Staff__c> oldOSs, List<Office_Staff__c> newOSs){
        
        List<Contact> contactList = new List<Contact>();
        List<String> userChanges = new List<String>();
        PermissionSet specialistUserSet = [select Id from PermissionSet where name = 'Specialist_User_Permission_Set' limit 1];
        PermissionSet officeUserSet = [select Id from PermissionSet where name = 'Office_User_Permission_Set' limit 1];
        PermissionSet staffUserSet = [select Id from PermissionSet where name = 'Office_Staff_User_Permission_Set' limit 1];
        List<String> addSpecialistUseList = new List<String>();
		List<String> addOfficeUserList = new List<String>();
        List<String> addStaffUserList = new List<String>();
        List<String> removeSpecialistUseList = new List<String>();
        List<String> removeOfficeUserList = new List<String>();
        List<String> removeStaffUserList = new List<String>();
        Map<String,Contact> contactMap = new Map<String,Contact>();
        List<Contact> contactoldList = [select FirstName, LastName, Email from Contact where OfficeStaff__c IN :oldOSs];
        for(Contact c : contactoldList){
            contactMap.put(c.Email,c);
        }
        for(Integer i=0; i<oldOSs.size(); i++){
            Office_Staff__c oldOS = oldOSs[i];
            Office_Staff__c newOS = newOSs[i];
            Contact c = contactMap.get(oldOS.E_mail__c);
            if(c!=null){
                c.FirstName = newOS.First_Name__c;
                c.LastName = newOS.Last_Name__c;
                c.Email = newOS.E_mail__c;
                contactList.add(c);
            }
            String userChange = oldOS.E_mail__c+'::'+newOS.E_mail__c+'::'+newOS.First_Name__c+'::'+newOS.Last_Name__c;
            system.debug('userChange:----'+userChange);
            userChanges.add(userChange);    
            
            if(oldOS.Specialist_Profile__c!=newOS.Specialist_Profile__c){
                String permissionStr = newOS.E_mail__c+'::'+specialistUserSet.Id;
                if(oldOS.Specialist_Profile__c==false)  addSpecialistUseList.add(permissionStr);
                else    removeSpecialistUseList.add(newOS.E_mail__c);
            }
            
            if(oldOS.Office_Profiles__c!=newOS.Office_Profiles__c){
                String permissionStr = newOS.E_mail__c+'::'+officeUserSet.Id;
                if(oldOS.Office_Profiles__c==false) addOfficeUserList.add(permissionStr);
                else    removeOfficeUserList.add(newOS.E_mail__c);
            }
            
            
            if(oldOS.Office_Staff_Profiles__c!=newOS.Office_Staff_Profiles__c){
                String permissionStr = newOS.E_mail__c+'::'+staffUserSet.Id;
                if(oldOS.Office_Staff_Profiles__c==false)   addStaffUserList.add(permissionStr);
                else    removeStaffUserList.add(newOS.E_mail__c);
            }
            
         }
         update contactList;
         if(userChanges.size()>0)   updateUsers(userChanges);
		 if(addSpecialistUseList.size()>0)  assignPermissions(addSpecialistUseList);
		 if(addOfficeUserList.size()>0)  assignPermissions(addOfficeUserList);
         if(addStaffUserList.size()>0)  assignPermissions(addStaffUserList);
         if(removeSpecialistUseList.size()>0)   removePermissions(removeSpecialistUseList, specialistUserSet.Id);
         if(removeOfficeUserList.size()>0)  removePermissions(removeOfficeUserList, officeUserSet.Id);
         if(removeStaffUserList.size()>0)   removePermissions(removeStaffUserList, staffUserSet.Id);
     }
}
/*****************************************************
Trigger: LWOfficeTrigger
Trigger handler for Office
Author – Vinay L
Date – 01/29/2014
******************************************************/
trigger LWOfficeTrigger on Office__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    if(Trigger.isInsert){
        if(Trigger.isAfter){
            LWOfficeTriggerHandler.handlerOnAfterInsert(trigger.new);
        }else{
            
        }
    }
    
    if(Trigger.isUpdate){
        if(Trigger.isBefore){
            LWOfficeTriggerHandler.handlerOnBeforeUpdate(trigger.oldMap, trigger.newMap);
        }else{
            LWOfficeTriggerHandler.handlerOnAfterUpdate(trigger.oldMap, trigger.newMap);
        }
    }
}
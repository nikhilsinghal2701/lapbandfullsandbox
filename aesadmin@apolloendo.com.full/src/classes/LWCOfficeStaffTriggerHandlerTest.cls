/*****************************************************
Class: LWCOfficeStaffTriggerHandlerTest
test for LWCOfficeStaffTriggerHandler and Office Staff trigger
Author – Wade Liu
Date – 02/20/2014
******************************************************/
@isTest
private class LWCOfficeStaffTriggerHandlerTest {

    static testMethod void officStaffTriggerTest() {
        //test Office Staff After Insert Trigger
        String str = LWCConstantDeclaration.SURGEONACCOUNTRECORD;
        Account account = TestUtil.createAccounts( 1, true, new Map<String, Object>{ 'RecordTypeId' => LWCWithoutSharing.getRecordTypeId(new List<String>{str})[0].Id })[0];
        List<Office_Staff__c> osList = TestUtil.createOfficeStaffs(100, true, new Map<String, Object>{ 'Specialist__c' => account.Id, 
                                                                                                    'Specialist_Profile__c' => true,
                                                                                                    'Office_Profiles__c' => false, 
                                                                                                    'Office_Staff_Profiles__c' => false});
        List<String> emailList = new List<String>();
        for(Office_Staff__c os : osList){
            emailList.add(os.E_mail__c);
        }
        Profile p = [SELECT Id FROM Profile WHERE Name='LAP-BAND® Central Registered User'];
        Integer contactCount = [select count() from Contact where OfficeStaff__c IN :osList];
        Integer userCount = [select count() from User where User_Email__c IN :emailList AND ProfileId = :p.Id];
        List<User> users = [select User_ID_Formula__c from User where User_Email__c IN :emailList];
        List<String> userIdList = new List<String>();
        for(User u : users){
            userIdList.add(u.User_ID_Formula__c);
        }
        Integer permissionCount = [select count() from PermissionSetAssignment where AssigneeId IN :userIdList];
        system.assertEquals(100, contactCount);
        system.assertEquals(100, userCount);
        system.assertEquals(100, permissionCount);
        
        //test update Office Staff
        for(Office_Staff__c os : osList){
            os.First_Name__c = 'updateTest';
            os.Specialist_Profile__c = false;
            os.Office_Profiles__c = true;
            os.Office_Staff_Profiles__c = true;
        }
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.runAs(usr){
            Test.startTest();
            update osList;
            Test.stopTest();
        }
  
        List<Contact> cList = [select FirstName from Contact where OfficeStaff__c IN :osList];
        system.assertEquals('updateTest', cList[50].FirstName);
        
        
    }   
    
    static testMethod void officStaffTriggerTest2() {
        //test Office Staff After Insert Trigger
        String str = LWCConstantDeclaration.SURGEONACCOUNTRECORD;
        Account account = TestUtil.createAccounts( 1, true, new Map<String, Object>{ 'RecordTypeId' => LWCWithoutSharing.getRecordTypeId(new List<String>{str})[0].Id })[0];
        List<Office_Staff__c> osList = TestUtil.createOfficeStaffs(100, true, new Map<String, Object>{ 'Specialist__c' => account.Id, 
                                                                                                    'Specialist_Profile__c' => true,
                                                                                                    'Office_Profiles__c' => false, 
                                                                                                    'Office_Staff_Profiles__c' => false});
        List<String> emailList = new List<String>();
        for(Office_Staff__c os : osList){
            emailList.add(os.E_mail__c);
        }
        Profile p = [SELECT Id FROM Profile WHERE Name='LAP-BAND® Central Registered User'];
        Integer contactCount = [select count() from Contact where OfficeStaff__c IN :osList];
        Integer userCount = [select count() from User where User_Email__c IN :emailList AND ProfileId = :p.Id];
        List<User> users = [select User_ID_Formula__c from User where User_Email__c IN :emailList];
        List<String> userIdList = new List<String>();
        for(User u : users){
            userIdList.add(u.User_ID_Formula__c);
        }
        system.assertEquals(100, contactCount);
        system.assertEquals(100, userCount);
        
        //test delete Office Staff
        for(Office_Staff__c os : osList){
            os.Is_Active__c = false;
        }
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.runAs(usr){
            Test.startTest();
            update osList;
            Test.stopTest();
        }
        
    } 
    
    static testMethod void officStaffTriggerTest3() {
        //test Office Staff After Insert Trigger
        String str = LWCConstantDeclaration.SURGEONACCOUNTRECORD;
        Account account = TestUtil.createAccounts( 1, true, new Map<String, Object>{ 'RecordTypeId' => LWCWithoutSharing.getRecordTypeId(new List<String>{str})[0].Id })[0];
        List<Office_Staff__c> osList = TestUtil.createOfficeStaffs(100, true, new Map<String, Object>{ 'Specialist__c' => account.Id, 
                                                                                                    'Specialist_Profile__c' => true,
                                                                                                    'Office_Profiles__c' => false, 
                                                                                                    'Office_Staff_Profiles__c' => false});
        List<String> emailList = new List<String>();
        for(Office_Staff__c os : osList){
            emailList.add(os.E_mail__c);
        }
        Profile p = [SELECT Id FROM Profile WHERE Name='LAP-BAND® Central Registered User'];
        Integer contactCount = [select count() from Contact where OfficeStaff__c IN :osList];
        Integer userCount = [select count() from User where User_Email__c IN :emailList AND ProfileId = :p.Id];
        List<User> users = [select User_ID_Formula__c from User where User_Email__c IN :emailList];
        List<String> userIdList = new List<String>();
        for(User u : users){
            userIdList.add(u.User_ID_Formula__c);
        }
        system.assertEquals(100, contactCount);
        system.assertEquals(100, userCount);
        
        //test activate Office Staff
        for(Office_Staff__c os : osList){
            os.Is_Active__c = true;
        }
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.runAs(usr){
            Test.startTest();
            update osList;
            Test.stopTest();
        }
    } 

}
public with sharing class LWOfficeWrapper {
		public Office__c office {get;set;}
		public String operation	 {get;set;} 
		
		public LWOfficeWrapper(String accId){
			Id recTypId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Pending_Office' AND SobjectType = 'Office__c'][0].Id;
			office = new Office__c(Specialist__c=accId,RecordTypeId= recTypId,Status__c = 'Submitted');  
			operation = 'edit';
		}
}
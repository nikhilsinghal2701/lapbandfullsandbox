<apex:page applyBodyTag="false" showheader="false" standardStylesheets="false" sidebar="false" >
<!--  <apex:composition template="LWTemplate"/> -->
    <head>
        <title>Affording LAP-BAND&#174; SYSTEM</title>
    </head>
    
    <c:LWTemplate />
 
    <body>
    <div id="container">
        <c:LWHeader />
            <div id="content" class="inner-page">
                <div id="inner-page-content">
                   <ul id="breadcrumbs">
                       <li>
                       <apex:outputLink value="{!URLFOR($Page.LWLearnAboutLapband,null,null)}" >How LAP-BAND<sup>&#174;</sup> SYSTEM Works</apex:outputLink>
                       <!--<a href="lapband_LearnAboutLapband" class="active">How LAP-BAND<sup>&#174;</sup> Works</a>-->
                       </li>
                       <li>|</li>
                       <li>
                           <apex:outputLink value="{!URLFOR($Page.LWLosingWeight,null,null)}">Losing Weight With LAP-BAND<sup>&#174;</sup> SYSTEM</apex:outputLink>
                           <!--<a href="lapband_LosingWeight">Losing Weight With LAP-BAND<sup>&#174;</sup></a>-->
                       </li>
                       <li>|</li>
                       <li>
                           <apex:outputLink value="{!URLFOR($Page.LWIsForYou,null,null)}">Is LAP-BAND<sup>&#174;</sup> SYSTEM Right for You?</apex:outputLink>
                           <!--<a href="lapband_IsForYou">Is LAP-BAND<sup>&#174;</sup> Right for You?</a>-->
                       </li>
                       <li>|</li>
                       <li>
                           <apex:outputLink value="{!URLFOR($Page.LWAffordingLapband,null,null)}" styleClass="active">Affording LAP-BAND<sup>&#174;</sup> SYSTEM</apex:outputLink>
                           <!--<a href="lapband_AffordingLapband">Affording LAP-BAND<sup>&#174;</sup></a>-->
                       </li>
                   </ul>
                   <h1 class="affording-lapband">Affording LAP-BAND&#174; SYSTEM</h1>
                   <div id="content-left">
                       <h3>The personal costs of obesity</h3>
                       <p>With today's rising health costs, it's normal to wonder if now is the time to consider the LAP-BAND<sup>&#174;</sup> Adjustable Gastric Banding System. But know this: extra weight often means extra costs. Studies show, on average, a severely obese person of average height (between 90-120 lbs overweight for women, 107-142 lbs overweight for men) spends $1,566 more a year on healthcare costs compared with people in a normal weight range. A morbidly obese person of average height (above 120 lbs overweight for women, and above 142 lbs overweight for men) spends $2,845 more when compared to people in a normal weight range.<sup>1</sup></p>
                       <h4>So, acting now &#8212; using the LAP-BAND<sup>&#174;</sup> System to significantly reduce your weight &#8212; may also reduce your weight-related costs.</h4>
                       <h3>Determining costs &#38; payment options</h3>
                       <p>The actual costs of your LAP-BAND<sup>&#174;</sup> System will depend on your individual situation. It will be based on several factors, such as your health plan, the surgeon/hospital you choose, etc.&nbsp; <apex:outputLink value="{!URLFOR($Page.LWFindASpecialist,null,null)}">Use the LAP-BAND<sup>&#174;</sup> System Specialist locator</apex:outputLink><!--<a href="../../find-a-specialist/index.html">Use the LAP-BAND<sup>&#174;</sup> System Specialist locator</a>--> to find a LAP-BAND<sup>&#174;</sup> System specialist who can help you understand your insurance coverage and financing options.</p>
                       <h3>Insurance and health plans</h3>
                       <p>Over 90% of the LAP-BAND<sup>&#174;</sup> System cases performed in 2012 were reimbursed in full or in part by insurance, according to market research.<sup>2</sup> Some policies only cover the procedure when medically necessary. In this case, your surgery should be covered if you meet national guidelines for the care of morbid obesity. For help understanding your insurance coverage and financing options, contact your local LAP-BAND<sup>&#174;</sup> System specialist.</p>
                       <h3>Considering having LAP-BAND<sup>&#174;</sup> surgery outside the U.S.?</h3>
                       <p>The LAP-BAND<sup>&#174;</sup> AP System is the most technologically advanced gastric band in the world today; however it is not available in Mexico.</p>
                       <p>For long-term success all bariatric procedures, including the LAP-BAND<sup>&#174;</sup> System, require follow-up and support from a bariatric practice&#8212;something that a doctor in another country may not be able to provide. Regular care and follow-up with a qualified surgeon close to home is important for losing the most weight with your LAP-BAND<sup>&#174;</sup> System.</p>
                       <p><strong>Contact a local&nbsp; <apex:outputLink value="{!URLFOR($Page.LWFindASpecialist,null,null)}">LAP-BAND<sup>&#174;</sup> System specialist</apex:outputLink><!--<a href="../../find-a-specialist/index.html">LAP-BAND<sup>&#174;</sup> System specialist</a>-->, to learn more about your insurance coverage and financing options.</strong></p>
                   </div>
                   <c:LWLearnLapbandRighTrail />
                </div>
            </div>
        <c:LWMapOnFoot />
        <c:LWFooter />
         <div id="footer" style="background:none !important;">
        <div class="footer-references">
                <h5>References:</h5>
                <ol>
                    <li>Dor A, Ferguson C, Langwith C, Tan E. A Heavy Burden: The Individual Costs of Being Overweight and Obese in the United States. September 2010.</li>
                    <li>Allergan Data on File. Bariatric Surgeon Market Share Tracker. November 2012, Slide 14.</li>
                </ol>
            </div>
        </div>
         <c:LWFooter2 />
    </div>
    </body>

</apex:page>
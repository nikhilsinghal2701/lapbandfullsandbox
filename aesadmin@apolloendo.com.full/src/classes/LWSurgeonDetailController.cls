/*****************************************************
Class: LWSurgeonDetailController
This class is used to get surgeon detail include location.
Author – Chris C
Date – 01/27/2014
Revision History
******************************************************/
public class LWSurgeonDetailController{
    public LWSurgeonDetailController(){
        
    }
    Id accountId = '001R000000oZSRa';
    
    public Surgeon__c getSurgeon(){
        Surgeon__c accountSurgeon = new Surgeon__c();
        accountSurgeon = [SELECT Surgeon_Account__c, Phone__c, Name, Gender__c, Email__c, 
                                 Amr_Society_of_Metabolic_Bariatric__c, American_College_of_Surgeons_ACS__c, 
                                 American_Board_of_Medical_Specialties__c,Outpatient_with_LAP_BAND_System_Surgery__c,
                                 Accepts_out_of_town_patients_OOTN__c,Provides_Financing_Options__c,Years_of_LAP_BAND_System_Experience__c,
                                 Frequency_of_LAP_BAND_System_Surgeries__c,Bariatrics__c
                          FROM Surgeon__c 
                          WHERE Surgeon_Account__c=:accountId];
        return accountSurgeon;
    }
    
    public List<Membership_Accreditation__c> getMembershipsOrAccreditations(){
        List<Membership_Accreditation__c> accountMembershipsOrAccreditions = new List<Membership_Accreditation__c>();
        accountMembershipsOrAccreditions = [SELECT Name,Specialist__c 
                                            FROM Membership_Accreditation__c
                                            WHERE Specialist__c =:accountId];
        return accountMembershipsOrAccreditions;
    }
    
    public List<Office__c> getOffices(){
        List<Office__c> accountOffices = new List<Office__c>();
        String aOffice = LWCConstantDeclaration.APPROVEDOFFICERECORD;
    	ID aId = LWCWithoutSharing.getRecordTypeId(new List<String>{aOffice})[0].Id;
        accountOffices = [Select Address_2__c, Address_1__c,Website__c, Email__c,Fax__c,Wheelchair_Access__c, 
                                 Vietnamese__c, Spanish__c, Russian__c, Phone__c, Korean__c, Specialist__c,
                                 Japanese__c, Italian__c, German__c, French__c, English__c,Office_Hours__c
                          From Office__c 
                          WHERE Status__c NOT IN ('Rejected','Deleted') AND Specialist__c =:accountId AND RecordTypeId =:aId AND Is_Edit_Flag__c= false AND Approved_Office__c = NULL];
        return accountOffices;
    }
    
    
}
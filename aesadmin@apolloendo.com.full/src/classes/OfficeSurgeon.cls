global with sharing class OfficeSurgeon {

	public Office__c office {get; private set;}
	public Surgeon__c surgeon {get; private set;}
	public List<Membership_Accreditation__c> memberships {get; private set;} 

	public OfficeSurgeon(Office__c office, Surgeon__c surgeon, List<Membership_Accreditation__c> memberships) {
		this.office = office;
		this.surgeon = surgeon;	
		this.memberships = memberships;
	}
}
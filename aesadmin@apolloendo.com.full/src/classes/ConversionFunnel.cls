global with sharing class ConversionFunnel extends SkyEditor2.SkyEditorPageBaseWithSharing {
    
    public Conversion_Funnel__c record {get{return (Conversion_Funnel__c)mainRecord;}}
    public class CanvasException extends Exception {}

    
    
    public ConversionFunnel(ApexPages.StandardController controller) {
        super(controller);


        SObjectField f;

        f = Conversion_Funnel__c.fields.Actual_Leads__c;
        f = Conversion_Funnel__c.fields.Forecast_Leads__c;
        f = Conversion_Funnel__c.fields.Actual_Seminar_Registration__c;
        f = Conversion_Funnel__c.fields.Actual_Conversion_Scheduled_Consults__c;
        f = Conversion_Funnel__c.fields.Forecast_Scheduled_Consults__c;
        f = Conversion_Funnel__c.fields.Forecast_Num_Scheduled_Consults__c;
        f = Conversion_Funnel__c.fields.Actual_Seminar_Attendance__c;
        f = Conversion_Funnel__c.fields.Actual_Conversion_Completed_Consults__c;
        f = Conversion_Funnel__c.fields.Forecast_Completed_Consults__c;
        f = Conversion_Funnel__c.fields.Forecast_Num_Completed_Consults__c;
        f = Conversion_Funnel__c.fields.Actual_Scheduled_Consults__c;
        f = Conversion_Funnel__c.fields.Actual_Conversion_Seminar_Attendance__c;
        f = Conversion_Funnel__c.fields.Forecast_Seminar_Attendance__c;
        f = Conversion_Funnel__c.fields.Forecast_Num_Seminar_Attendance__c;
        f = Conversion_Funnel__c.fields.Actual_Completed_Consults__c;
        f = Conversion_Funnel__c.fields.Actual_Conversion_Seminar_Registration__c;
        f = Conversion_Funnel__c.fields.Forecast_Seminar_Registration__c;
        f = Conversion_Funnel__c.fields.Forecast_Num_Seminar_Registration__c;
        f = Conversion_Funnel__c.fields.Actual_Number_LAP_BAND_Procedures__c;
        f = Conversion_Funnel__c.fields.Actual_Conversion_LAP_BAND_Procedures__c;
        f = Conversion_Funnel__c.fields.Forecast_Number_LAP_BAND_Procedures__c;
        f = Conversion_Funnel__c.fields.Forecast_Num_LAP_BAND_Procedures__c;
        f = Conversion_Funnel__c.fields.Forecast_Total_Pull_Through__c;
        f = Account.fields.Name;
        f = Conversion_Funnel__c.fields.Actual_Total_Pull_Through__c;

        List<RecordTypeInfo> recordTypes;
        try {
            mainSObjectType = Conversion_Funnel__c.SObjectType;
            setPageReferenceFactory(new PageReferenceFactory());
            
            mainQuery = new SkyEditor2.Query('Conversion_Funnel__c');
            mainQuery.addField('Actual_Leads__c');
            mainQuery.addField('Forecast_Leads__c');
            mainQuery.addField('Actual_Seminar_Registration__c');
            mainQuery.addField('Forecast_Scheduled_Consults__c');
            mainQuery.addField('Actual_Seminar_Attendance__c');
            mainQuery.addField('Forecast_Completed_Consults__c');
            mainQuery.addField('Actual_Scheduled_Consults__c');
            mainQuery.addField('Forecast_Seminar_Attendance__c');
            mainQuery.addField('Actual_Completed_Consults__c');
            mainQuery.addField('Forecast_Seminar_Registration__c');
            mainQuery.addField('Actual_Number_LAP_BAND_Procedures__c');
            mainQuery.addField('Forecast_Number_LAP_BAND_Procedures__c');
            mainQuery.addFieldAsOutput('Name');
            mainQuery.addFieldAsOutput('Actual_Conversion_Scheduled_Consults__c');
            mainQuery.addFieldAsOutput('Forecast_Num_Scheduled_Consults__c');
            mainQuery.addFieldAsOutput('Actual_Conversion_Completed_Consults__c');
            mainQuery.addFieldAsOutput('Forecast_Num_Completed_Consults__c');
            mainQuery.addFieldAsOutput('Actual_Conversion_Seminar_Attendance__c');
            mainQuery.addFieldAsOutput('Forecast_Num_Seminar_Attendance__c');
            mainQuery.addFieldAsOutput('Actual_Conversion_Seminar_Registration__c');
            mainQuery.addFieldAsOutput('Forecast_Num_Seminar_Registration__c');
            mainQuery.addFieldAsOutput('Actual_Conversion_LAP_BAND_Procedures__c');
            mainQuery.addFieldAsOutput('Forecast_Num_LAP_BAND_Procedures__c');
            mainQuery.addFieldAsOutput('Forecast_Total_Pull_Through__c');
            mainQuery.addFieldAsOutput('Practice_Name__r.Name');
            mainQuery.addFieldAsOutput('Actual_Total_Pull_Through__c');
            mainQuery.addFieldAsOutput('Actual_Conversion_Seminar_Registration__c');
            mainQuery.addWhere('Id', mainRecord.Id, SkyEditor2.WhereOperator.Eq)
                .limitRecords(1);
            
            
            
            mode = SkyEditor2.LayoutMode.LayoutFree; 
            
            queryMap = new Map<String, SkyEditor2.Query>();
            SkyEditor2.Query query;
            
            
            p_showHeader = true;
            p_sidebar = true;
            init();
            
            if (record.Id == null) {
                
                saveOldValues();
                
            }

            
            
        }  catch (SkyEditor2.Errors.FieldNotFoundException e) {
            fieldNotFound(e);
        } catch (SkyEditor2.Errors.RecordNotFoundException e) {
            recordNotFound(e);
        } catch (SkyEditor2.ExtenderException e) {
            e.setMessagesToPage();
        }
    }
    

    private void sObjectNotFound(SkyEditor2.Errors.SObjectNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void fieldNotFound(SkyEditor2.Errors.FieldNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    private void recordNotFound(SkyEditor2.Errors.RecordNotFoundException e) {
        SkyEditor2.Messages.addErrorMessage(e.getMessage());
        hidePageBody = true;
    }
    
    private static testMethod void testPageMethods() {        ConversionFunnel extension = new ConversionFunnel(new ApexPages.StandardController(new Conversion_Funnel__c()));
        SkyEditor2.Messages.clear();
        extension.sObjectNotFound(new SkyEditor2.Errors.SObjectNotFoundException(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.sObjectNotFound('SkyEditor2__SkyEditorDummy__c')));
        SkyEditor2.Messages.clear();
        extension.fieldNotFound(new SkyEditor2.Errors.FieldNotFoundException(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.fieldNotFound(SkyEditor2__SkyEditorDummy__c.SObjectType, 'Name')));
        SkyEditor2.Messages.clear();
        extension.recordNotFound(new SkyEditor2.Errors.RecordNotFoundException(SkyEditor2.Messages.DATA_NOT_FOUND));
        System.assert(extension.getErrorMessages().endsWith(SkyEditor2.Messages.DATA_NOT_FOUND));

        Integer defaultSize;
    }
    class PageReferenceFactory implements SkyEditor2.PageReferenceFactory.Implementation {
        public PageReference newPageReference(String url) {
            return new PageReference(url);
        }
    }
}
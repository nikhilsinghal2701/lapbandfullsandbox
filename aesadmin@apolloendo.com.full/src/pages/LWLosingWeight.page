<apex:page applyBodyTag="false" showheader="false" standardStylesheets="false" sidebar="false" >
<!--  <apex:composition template="LWTemplate"/> -->
    <head>
        <title>Losing Weight with LAP-BAND&#174; SYSTEM</title>
    </head>
    
    <c:LWTemplate />
 
    <body>
        <div id="container">
        <c:LWHeader />
        <div id="content" class="inner-page">
            <div id="inner-page-content">
                 <ul id="breadcrumbs">
                    <li>
                        <apex:outputLink value="{!URLFOR($Page.LWLearnAboutLapband,null,null)}" >How the LAP-BAND<sup>&#174;</sup> System Works</apex:outputLink>
                        <!--<a href="lapband_LearnAboutLapband" class="active">How LAP-BAND<sup>&#174;</sup> Works</a>-->
                    </li>
                    <li>
                        <apex:outputLink value="{!URLFOR($Page.LWLosingWeight,null,null)}" styleClass="active">Losing weight with the LAP-BAND<sup>&#174;</sup> System</apex:outputLink>
                        <!--<a href="lapband_LosingWeight">Losing Weight With LAP-BAND<sup>&#174;</sup></a>-->
                    </li>
                    <li>
                        <apex:outputLink value="{!URLFOR($Page.LWIsForYou,null,null)}">Is the LAP-BAND<sup>&#174;</sup> System Right for You?</apex:outputLink>
                        <!--<a href="lapband_IsForYou">Is LAP-BAND<sup>&#174;</sup> Right for You?</a>-->
                    </li>
                    <li>
                        <apex:outputLink value="{!URLFOR($Page.LWAffordingLapband,null,null)}">Affording the LAP-BAND<sup>&#174;</sup> System</apex:outputLink>
                        <!--<a href="lapband_AffordingLapband">Affording LAP-BAND<sup>&#174;</sup></a>-->
                    </li>
                </ul>
                 <h1 class="losing-weight">Losing Weight with LAP-BAND<sup>&#174;</sup> System</h1>
                <div id="content-left">
                    <p class="blueText">You&rsquo;re not a statistic or a BMI number. You’re someone with dreams and plans. You’ve tried medications, fitness routines, and rounds of dieting, only to get short-lived results. Still, your weight is affecting your health and your enjoyment of life.</p>
                    <p class="blueText">It&rsquo;s time to consider minimally invasive weight loss surgery with the LAP-BAND<span style="vertical-align: 35%; font-size: 50%">®</span> Adjustable Gastric Banding System. The LAP-BAND<span style="vertical-align: 35%; font-size: 50%">®</span> System has helped hundreds of thousands of people achieve lasting, healthy weight loss.<sup>1</sup>  Maybe it&rsquo;s the solution you’ve been looking for. Maybe it fits. We’re here to help you find out.</p>
                    <h3>Reasons to Consider the LAP-BAND<sup>&#174;</sup> System</h3>
                    <p>The LAP-BAND<sup>&#174;</sup> System has been proven to provide significant short-term weight loss in the first 1-2 years as well as durable weight loss that lasts.</p>
                        <ul>
                            <li>The LAP-BAND<sup>&#174;</sup> System has been proven to provide significant short-term weight loss within the first 1-2 years:</li>
                            <ul>
                                <li>The LAP-BAND<sup>&#174;</sup> System was shown to provide 52% excess weight loss at 2 years, in an ongoing clinical study of people with a BMI of 35 or above.<sup>2</sup></li>
                                <li>In a separate study, the LAP-BAND<sup>&#174;</sup> System was shown to provide 65% excess weight loss at 1 year and 70% excess weight loss at 2 years for people with a BMI between 30 and 40. That translates into shrinking an average of 6 inches off the waistline and 6 inches off the hips.*<sup>3, 4</sup>
                                <p class="reference">*Year-1 data from Apollo Endosurgery&rsquo;s LBMI pivotal study is based on a complete database of 143 obese adults with a Month-12 visit. The currently available Year-2 results are based on a database snapshot of an incomplete data set that has not been source verified. These data were reviewed by the FDA in support of the recently approved the LAP-BAND<sup>&#174;</sup> System label extension in the United States.</p>
                                </li>
                            </ul>
                            <li>The LAP-BAND<sup>&#174;</sup> System can help provide weight loss that lasts:</li>
                            <ul>
                                <li>In a long-term study, severely obese patients lost an average of 60% of their excess weight 5 years after the LAP-BAND<sup>&#174;</sup> System procedure.<sup>5</sup></li>
                            </ul>
                        </ul>
                        <p><span class="hover-text" id="lapband-footage-video">Gastric banding</span> was shown to be safer than other invasive weight loss surgeries, such as <span class="hover-text" id="gastrectomy-footage-video">sleeve gastrectomy</span> or <span class="hover-text" id="bypass-footage-video">gastric bypass</span>.<sup>6,7,8</sup></p>
                        <ul>
                            <li>Data from the Bariatric Outcomes Longitudinal Database&#8482; showed that sleeve gastrectomy had a 3x greater rate of total complications in the first year<sup>7&#134;</sup> when compared to gastric banding. Additionally, data from the American College of Surgeons database within the first 30 days showed<sup>6&#135;</sup></li>
                            <ul>
                                <li>With sleeve gastrectomy, 3 times greater chance of readmission to the hospital</li>
                                <li>With sleeve gastrectomy, 3 times greater chance of reoperation</li>
                                <li>With sleeve gastrectomy, 3 times the length of hospital stay following the procedure</li>
                            </ul>
                            <li>The minimally invasive LAP-BAND<sup>&#174;</sup> System is adjustable, reversible, and unlike sleeve gastrectomy or gastric bypass, involves no stomach amputation, cutting, or stapling. This means, while the LAP-BAND<sup>&#174;</sup> System is designed to be a long-term procedure, if necessary, it can be removed and the procedure reversed.<sup>9</sup> With sleeve gastrectomy, 85% of the stomach is amputated and there is no way to reverse the procedure or restore your body&rsquo;s anatomy to the way it was.<sup>10</sup></li>   
                        </ul>
                   <p>In four separate studies, weight loss following gastric banding was shown to improve or resolve several serious obesity-related comorbid conditions, including asthma, hypertension, diabetes, sleep apnea, and GERD (gastroesophageal reflux disease).<sup>11-14</sup></p>
                    
                    <p>Not all weight loss centers are the same. It&rsquo;s important that you go to a LAP-BAND<sup>&#174;</sup> System specialist for your surgery.&nbsp; <apex:outputLink value="{!URLFOR($Page.LWFindASpecialist,null,null)}">Use the LAP-BAND<sup>&#174;</sup> System specialist locator</apex:outputLink><!--<a href="losing-weight.html#">Use the LAP-BAND<sup>&#174;</sup> System specialist locator</a>--> to find a qualified specialist near you.</p>
                    <!--Todo: video
                    <p class="reference"><sup>&#134;</sup>The Bariatric Outcomes Longitudinal Database&#8482; (BOLD&#8482;) was developed to help ensure ongoing compliance with the American Society for Metabolic and Bariatric Surgery (ASMBS) Bariatric Surgery Center of Excellence (BSCOE) program and develop general knowledge about optimal bariatric surgery practices. This analysis of the BOLD&#8482; database includes data from 57,918 patients.</p>
                    <p class="reference"><sup>&#135;</sup>The American College of Surgeons Bariatric Surgery Center Network (ACS-BSCN) database was developed as part of the ACS-BSCN accreditation program. This analysis of the ACS database includes data from 28,616 patients.</p>
                    -->
        
                     <video width="576" height="324" class="mejs-lapband" type="video/mp4" id="comorbidities-video-player" 
                         poster="http://www.lapband.com/images/video/video-poster-comorbidities.jpg" preload="none">
                        <source type="video/mp4" src="{!$Resource.mp4}" />
                    </video> 
            
                    <p class="reference"><sup>&#134;</sup>The Bariatric Outcomes Longitudinal Database&#8482; (BOLD&#8482;) was developed to help ensure ongoing compliance with the American Society for Metabolic and Bariatric Surgery (ASMBS) Bariatric Surgery Center of Excellence (BSCOE) program and develop general knowledge about optimal bariatric surgery practices. This analysis of the BOLD&#8482; database includes data from 57,918 patients.</p>
                    <p class="reference"><sup>&#135;</sup>The American College of Surgeons Bariatric Surgery Center Network (ACS-BSCN) database was developed as part of the ACS-BSCN accreditation program. This analysis of the ACS database includes data from 28,616 patients.</p>

                </div>
                <div id="lapband-footage-video-balloon" class="video-balloon" style="left: 40px; top: 481px; display: none;">
                    <span id="lapband-footage-video-thumb" class="video-balloon-thumb">Play video</span>
                    <div class="video-balloon-label">Watch video of a gastric banding procedure</div>
                </div>
                <div id="gastrectomy-footage-video-balloon" class="video-balloon" style="left: 606px; top: 481px; display: none;">
                    <span id="gastrectomy-footage-video-thumb" class="video-balloon-thumb">Play video</span>
                    <div class="video-balloon-label">Watch video of a sleeve gastrectomy procedure</div>
                </div>
                <div id="bypass-footage-video-balloon" class="video-balloon" style="left: 57px; top: 500px; display: none;">
                    <span id="bypass-footage-video-thumb" class="video-balloon-thumb">Play video</span>
                    <div class="video-balloon-label">Watch video of a gastric bypass procedure</div>
                </div>
                <div id="lapband-footage-video-lightbox" class="video-lightbox"></div>
                <div id="gastrectomy-footage-video-lightbox" class="video-lightbox"></div>
                <div id="bypass-footage-video-lightbox" class="video-lightbox"></div>
        
                <c:LWVideoIncludeFile />
                <c:LWLearnLapbandRighTrail />
            </div>
        </div>
        <!-- <c:LWMapOnFoot /> -->
        <c:LWFooter />
         <div id="footer" style="background:none !important;">
        <div class="footer-references">
                <h5>References:</h5>
                <ol>
                    <li>Data on File. Allergan, Inc. Irvine, CA. December, 2012.</li>
                    <li>Data on file, Allergan, Inc. The LAP-BAND AP<sup>&#174;</sup> Experience (APEX) Study&#8212;2010 Interim Analysis. October 14.</li> 
                    <li>Data on file, FDA Core Deck. LAP-BAND<sup>&#174;</sup> Adjustable Gastric Banding System P000008/S017. Dec 2012, Slide 87.</li>
                    <li>Directions For Use (DFU). LAP-BAND AP<sup>&#174;</sup> Adjustable Gastric Banding System with OMNIFORM<sup>&#174;</sup> Design. Allergan, Inc. Irvine, CA. 11/12.</li>
                    <li>Ray James, Ray Shahla. Safety, efficacy, and durability of laparoscopic adjustable gastric banding in a single surgeon U.S. community practice. <em>Surgery for Obesity and Related Diseases.</em> 2011.</li>
                    <li>Hutter Matthew, et al. First Report from the American College of Surgeons Bariatric Surgery Center Network: <em>Laparoscopic Sleeve Gastrectomy has Morbidity and Effectiveness Positioned Between the Band and the Bypass.</em> September 2001.</li>
                    <li>DeMaria Eric, Pate Virginia, Warthen Michael, Winegar Deborah. Baseline data from American Society for Metabolic and Bariatric Surgery-designated Bariatric Surgery Centers of Excellence using the Bariatric Outcomes Longitudinal Database. <em>Surgery for Obesity and Related Diseases 6.</em> 2010.</li>
                    <li>The Longitudinal Assessment of Bariatric Surgery (LABS) Consortium. Perioperative Safety in the Longitudinal Assessment of Bariatric Surgery. <em>The New England Journal of Medicine Vol. 361 No. 5.</em> July 30, 2009.</li>
                    <li>The LAP-BAND<sup>&#174;</sup> System Surgical Aid in the Treatment of Obesity (PSI). Allergan, Inc. Irvine, CA. 02/11.</li>
                    <li>Weiner Rudolf, et al. Laparoscopic Sleeve Gastrectomy- Influence of Sleeve Size and Resected Gastric Volume. <em>Obesity Surgery.</em> 2007.</li>
                    <li>Dixon John, Chapman Leon, O&#8217;Brien Paul. Marked Improvement in Asthma after LAP-BAND<sup>&#174;</sup> Surgery for Morbid Obesity. <em>Obesity Surgery.</em> 1999.</li>
                    <li>Dixon John, O&#8217;Brien Paul. Health Outcomes of Severely Obese Type 2 Diabetic Subjects 1 Year After Laparoscopic Adjustable Gastric Banding. <em>Diabetes Care, Volume 25, Number 2.</em> February 2002.</li>
                    <li>Dixon John, Schachter Linda, O&#8217;Brien Paul. Sleep Disturbance and Obesity: Changes Following Surgically Induced Weight Loss. <em>Arch Intern Med/Vol 161.</em> Jan 8, 2001.</li>
                    <li>Dixon John, O&#8217;Brien Paul. Gastroesophageal Reflux in Obesity: The Effect of LAP-BAND<sup>&#174;</sup> Placement. <em>Obesity Surgery.</em> 1999.</li>
                </ol>
            </div>

        </div>
         <c:LWFooter2 />
    </div>
 </body>

</apex:page>
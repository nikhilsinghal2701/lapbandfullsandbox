/*****************************************************
Class: CurrencyConversionTriggerHandler 
This triggerHandler helps to Update On Sales Order Line Item
Conversion_Rate
Integration-LineAmount
Author – Wade Liu
Date – 03/17/2014
Revision History
******************************************************/
public with sharing class CurrencyConversionTriggerHandler {
	public static void handlerOnUpdate( List<Currency_Conversion__c> newList, Map<Id, Currency_Conversion__c> oldMap){
		//New Conversion_Rate from Currency Conversion
		Map<Id, Currency_Conversion__c> CurrencyRateChangedMap = new Map<Id, Currency_Conversion__c>();
		List<Currency_Conversion__c> CurrencyRateChangedList = new List<Currency_Conversion__c>();
		for(Currency_Conversion__c cc : newList){
			//Conversion_Rate changed
			if(cc.Conversion_Rate__c!=oldMap.get(cc.Id).Conversion_Rate__c){
				CurrencyRateChangedMap.put(cc.Id,cc);
				CurrencyRateChangedList.add(cc);
			}
		}
		if(CurrencyRateChangedList.size()>0){
			List<Sales_Order_Line_Item__c> salesOrderLineItems = [select Id, Unconverted_Amount__c, Conversion_Rate__c, Integration_LineAmount__c, Currency_Conversion__c
																from Sales_Order_Line_Item__c where Currency_Conversion__c IN :CurrencyRateChangedList];
			for(Sales_Order_Line_Item__c soli : salesOrderLineItems){
				//Formula: SalesOrderLineItem.Conversion_Rate = New Conversion_Rate from Currency Conversion
				soli.Conversion_Rate__c = CurrencyRateChangedMap.get(soli.Currency_Conversion__c).Conversion_Rate__c;
				/*
				Formula:  SalesOrderLineItem. Integration-LineAmount =  (SalesOrderLineItem.Unconverted_Amount/ New Conversion_Rate from Currency Conversion)
				Note: There is an existing work flow rule on SalesOrderLineItem. Integration-LineAmount to update the ‘Amount’ field on SalesOrderLineItem. 
				Integration-LineAmount is a TEXT field, round it to two decimals while updating this field
				*/
				Decimal toRoundLineAmount = soli.Unconverted_Amount__c/soli.Conversion_Rate__c;
				Decimal roundedLineAmount = toRoundLineAmount.setScale(2, RoundingMode.HALF_UP);
				soli.Integration_LineAmount__c = String.valueOf(roundedLineAmount);
			}
			update salesOrderLineItems;
		}
	}
}
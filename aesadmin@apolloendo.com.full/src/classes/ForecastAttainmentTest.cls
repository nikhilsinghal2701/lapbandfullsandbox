/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ForecastAttainmentTest {

    static testMethod void attainmentCalculationTest() {
    	//get record type ID for Account Ship To
    	Schema.DescribeSObjectResult sObjDescrAcc = Schema.SObjectType.Account;
    	Map<String, Schema.RecordTypeInfo> accRecTypeInfo = sObjDescrAcc.getRecordTypeInfosByName();

    	//get record type ID for Account Ship To
    	Schema.DescribeSObjectResult sObjDescrSO = Schema.SObjectType.Sales_Order__c;
    	Map<String, Schema.RecordTypeInfo> soRecTypeInfo = sObjDescrSO.getRecordTypeInfosByName();
    	 
    	//create account
    	Account a1 = TestUtil.createAccounts(1, false, null)[0];
    	a1.RecordTypeId = accRecTypeInfo.get('Ship To').getRecordTypeId();
    	a1.Targeted_Account__c = true;
    	a1.Region_ID__c = '11111';
    	a1.Territory_ID__c = '22222';
    	//a1.RP_Region__c = '33333';
    	a1.TM_ID__c = '44444';
   	
    	insert a1;

		Sales_Order__c so1 = new Sales_Order__c();
		so1.Name = 'TEST01';
		so1.PO__c = 'TEST_PO1';
		so1.Type__c ='US';
		so1.RecordTypeId = soRecTypeInfo.get('LAP-BAND SYSTEMS').getRecordTypeId();
		so1.Ship_To__c = a1.id;

		insert so1;
		
		Forecast__c fc1 = new Forecast__c();
		fc1.Account__c = a1.Id;
		fc1.Attainment_Amount__c = 900.0;
		fc1.Start_Date__c = Date.newInstance(2014, 05, 01);
		fc1.End_Date__c = Date.newInstance(2014, 05, 31);
		fc1.Health_Forecast__c = 500.0;
		fc1.RP_Forecast__c = 750.0;

		Forecast__c fc2 = new Forecast__c();
		fc2.Account__c = a1.Id;
		fc2.Attainment_Amount__c = 1900.0;
		fc2.Start_Date__c = Date.newInstance(2014, 07, 01);
		fc2.End_Date__c = Date.newInstance(2014, 07, 31);
		fc2.Health_Forecast__c = 1500.0;
		fc2.RP_Forecast__c = 1750.0;

		insert fc1;
		insert fc2;
		
		for (Forecast__c fcRec : [select Id, Attainment_Amount__c from Forecast__c where Account__c = :a1.Id]) {
			System.assertEquals(0, fcRec.Attainment_Amount__c);
		}
				
		Test.startTest();
		
		List<Sales_Order_Line_Item__c> items = new List<Sales_Order_Line_Item__c>();
		
		items.add(new Sales_Order_Line_Item__c(
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '1,500.00',
			Material__c = 'B-23423',
			Description__c = 'LAP-BAND',
			Quantity__c = 2,
			Bill_Date__c = Date.newInstance(2014, 05, 25),
			Due_Date__c = Date.newInstance(2014, 05, 15)
			)
		);

		items.add(new Sales_Order_Line_Item__c(
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '600.00',
			Material__c = 'B-23423',
			Description__c = 'LAP-BAND',
			Quantity__c = 1,
			Bill_Date__c = Date.newInstance(2014, 06, 03),
			Due_Date__c = Date.newInstance(2014, 06, 23)
			)
		);

		items.add(new Sales_Order_Line_Item__c(
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '400.00',
			Material__c = 'B-23423',
			Description__c = 'LAP-BAND',
			Quantity__c = 1,
			Bill_Date__c = Date.newInstance(2014, 06, 03),
			Due_Date__c = Date.newInstance(2014, 06, 23)
			)
		);

		items.add(new Sales_Order_Line_Item__c(
			Sales_Order__c = so1.Id, 
			Integration_LineAmount__c = '800.00',
			Material__c = 'B-23423',
			Description__c = 'LAP-BAND',
			Quantity__c = 1,
			Bill_Date__c = Date.newInstance(2014, 07, 20),
			Due_Date__c = Date.newInstance(2014, 07, 10)
			)
		);
		
		insert items;

		Test.stopTest();

		Integer fcCount = [select count() from Forecast__c where Account__c = :a1.Id];
		System.assertEquals(3, fcCount);

		for (Forecast__c fcRec : [select Id, Quarter__c, Attainment_Amount__c, Health_Forecast__c, RP_Forecast__c from Forecast__c where Account__c = :a1.Id]) {
			if (fcRec.Id == fc1.Id) {
				System.assertEquals('Q2-2014', fcRec.Quarter__c);
				System.assertEquals(1500.0, fcRec.Attainment_Amount__c);
				System.assertEquals(500.0, fcRec.Health_Forecast__c);
				System.assertEquals(750.0, fcRec.RP_Forecast__c);
			} else if (fcRec.Id == fc2.Id) {
				System.assertEquals('Q3-2014', fcRec.Quarter__c);
				System.assertEquals(800.0, fcRec.Attainment_Amount__c);
				System.assertEquals(1500.0, fcRec.Health_Forecast__c);
				System.assertEquals(1750.0, fcRec.RP_Forecast__c);
			} else {
				System.assertEquals('Q2-2014', fcRec.Quarter__c);
				System.assertEquals(1000.0, fcRec.Attainment_Amount__c);
				System.assertEquals(0, fcRec.Health_Forecast__c);
				System.assertEquals(0, fcRec.RP_Forecast__c);
				
			}
		}
		
		for (Sales_Order_Line_Item__c lineRec : [select Id, IsForecastable__c from Sales_Order_Line_Item__c where Sales_Order__c = :so1.Id]) {
			System.assert(lineRec.IsForecastable__c);
		}
		
    }
}
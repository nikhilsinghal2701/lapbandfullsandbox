global with sharing class Batch_ExectionController { 

 //This method is to get the view mode   

 webservice static ID executeBatchForPricingContract(Id PCID) {             

  Id batchProcessId;                 

  //Calling Constructor for Batch Execution       

  PricingContractAccountBatch dc = new PricingContractAccountBatch();   
	dc.query='SELECT id ,(Select Id,Pricing_Contract__c,Pricing_Contract__r.Contract_Start_Date__c,Pricing_Contract__r.Contract_End_Date__c,Pricing_Contract__r.Group_Units_Logic__c from Accounts__r) FROM Pricing_Contract__c WHERE IsDeleted = False and id = \'' + PCID + '\' ';
	
  batchProcessId = Database.executeBatch(dc);         
  return batchProcessId;   

 }

}